Kompilieren unter Linux funktioniert, aber nur unter Arch Linux getestet. Windows ungetestet und bestimmt schlecht erraten.

# Java Runtime und JDK installieren

_Für Nutzer von Windows_: Oracle Java 8 Runtime und JDK wird benötigt.
* Wenn Java 8 noch nicht installiert ist, kann es [hier (oracle.com)](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) herunter geladen werden. Anschließend installieren.
* Wenn JDK 8 noch nicht installiert ist, kann es [hier (oracle.com)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) herunter geladen werden. Anschließend installieren.

_Für Nutzer von Linux_: OpenJDK 8 Runtime und JDK wird benötigt.
* Arch Linux: `sudo pacman -S jdk8-openjdk`
* Debian und Ubuntu: `sudo apt-get install openjdk-8-jdk`

# Kompilieren unter Eclipse

_Für Nutzer von Windows_: Eclipse IDE for Java Developers [hier (eclipse.org)](https://www.eclipse.org/downloads/eclipse-packages/) herunterladen. Anschließend installieren.

_Für Nutzer von Linux_: Eclipse IDE for Java Developers installieren.
* Arch Linux: `sudo pacman -S eclipse-java`
* Debian und Ubuntu: Bitte Anweisungen für Windows folgen (aber die Linux-Version herunterladen).

## Gradle-Unterstützung installieren

Eclipse starten, "Help" → "Eclipse Marketplace". Dort nach "Buildship Gradle Integration" suchen und installieren. Dies kann dauern.

## Quellcode herunterladen und importieren

"File" → "Import" → "Git" → "Project from Git". Auf "Clone URI" gehen. Unter "URI" die Adresse

    https://gitlab.com/group5productions/dynamiteboy.git

eingeben. Unter "Branch Selection" nur den Branch "master" herunterladen. Downloadpfad bestätigen.

Als Assistent für den Import "Import using New Project Wizard" auswählen und "Finish" anklicken. Im neuen Fenster "Gradle" → "Gradle Project auswählen" und die Willkommensseite überspringen. Gebe einen aussagekräftigen Projektnamen ein und auf "Finish" klicken.

## Quellcode kompilieren

Im unteren Teil des Eclipse-Fensters ist der Tab "Gradle Tasks" zu sehen. Nach der Initialisierung, die einen Moment dauern kann, sind verschiedene Aufgaben aufgelistet. Für uns ist nur "build" wichtig, welches das Spiel kompiliert und die enthaltenen Testfälle ausführt. Doppelklicken und warten, bis der Vorgang abgeschlossen ist.

Enthält der Tab "Gradle Executions", zu dem automatisch gewechselt wird, nur grüne und/oder graue Punkte, und gibt der Tab "Console" am Ende "BUILD SUCCESSFUL" an, so wurde das Spiel erfolgreich kompiliert.

## Kompiliertes Spiel in Eclipse ausführen

"Run" → "Run Configurations". In der Kategorie "Java Application" werden folgende zwei Einträge hinzugefügt:

    * Name: Client
    * Main class: org.g5p.dynamiteboy.client.ClientWindow
    * Auf "Apply" klicken nicht vergessen.

    * Name: Verwaltungsserver
    * Main class: org.g5p.dynamiteboy.managementserver.ManagementServerWindow
    * Auf "Apply" klicken nicht vergessen.

Wähle in der linken Übersicht unter "Java Application" entweder Client oder Verwaltungsserver aus und klicke auf "Run".

## Kompiliertes Spiel verteilen

Im Verzeichnis des Projekts (üblicherweise im Workspace-Ordner von Eclipse) findet sich der Ordner "build" und dort drin der Ordner "distributions". Dort sind ein Tarball (.tar) und eine Zip-Datei (.zip) zu finden für die jeweiligen Betriebssysteme (Unix und Windows). Diese Kompilate können dann an andere weiter verteilt werden.

# Kompilieren auf der Kommandozeile

_Für Nutzer von Linux_: Gradle 3.4 oder höher wird benötigt.
* Arch Linux: `sudo pacman -S gradle`
* Debian und Ubuntu: Bitte den Anweisungen [hier (gradle.org)](https://docs.gradle.org/current/userguide/installation.html) folgen.

Funktioniert Gradle? `gradle -v` in der Kommandozeile ausgeben. Es sollte folgende Ausgabe (oder ähnlich) kommen:

    ------------------------------------------------------------
    Gradle 3.4.1
    ------------------------------------------------------------

    Build time:   2017-03-03 19:45:41 UTC
    Revision:     9eb76efdd3d034dc506c719dac2955efb5ff9a93

    Groovy:       2.4.7
    Ant:          Apache Ant(TM) version 1.9.6 compiled on June 29 2015
    JVM:          1.8.0_121 (Oracle Corporation 25.121-b13)
    OS:           Linux 4.10.6-1-ARCH amd64

Quellcode herunterladen und kompilieren:

```sh
mkdir ~/dynamiteboy/
cd ~/dynamiteboy/
git clone https://gitlab.com/group5productions/dynamiteboy.git .
gradle build
```
