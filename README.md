# Dynamite Boy

![build](https://gitlab.com/group5productions/dynamiteboy/badges/master/build.svg)

![Dynamite Boy](/src/main/resources/MainMenuBackground.png?raw=true "Dynamite Boy")

Dynamite Boy ist ein in Java geschriebener mittelmäßiger Klon des bekannten Geschicklichkeitsspiels "Bomberman" (Original aus dem Hause Hudson Soft). Das Spiel bietet folgende Features:

* Offline im Einzelspieler- und Zweispielermodus an einer Tastatur spielen
* Online auf Servern mit anderen Spielern spielen
* Liste von Servern abrufen
* Bis zu vier Spieler pro Runde
* Vielfältige Möglichkeiten, Runden einzustellen
* Bildschirmlupe
* Mittelmäßige Grafik
* Keine Soundeffekte
* Keine Gamepad-Unterstützung
* Läuft auf allen Betriebssystemen

# Systemvoraussetzungen

* Einen PC mit einem Betriebssystem
* Java 8 ([hier (oracle.com)](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) herunterladen)
* Tastatur und Monitor, sowie Maus zum Schaltflächen drücken
* Zum Online spielen: Internetverbindung mit 1 Megabit pro Sekunde im Up- und Download und niedriger Latenz (DSL 16000 oder schneller empfohlen)
* Viel Geschick

# Spiel starten

* Dieses Archiv in ein beliebiges Verzeichnis entpacken.
* Das beliebige Verzeichnis öffnen.
* Um den Verwaltungsserver zu starten, im Ordner `bin` auf `dynamiteboy-management-server.bat` klicken (Windows) bzw. `dynamiteboy-management-server` (Unix/Linux/macOS/BSD) starten.
* Um den Client (und Spieleserver in einem) zu starten, im Ordner `bin` auf `dynamiteboy.bat` klicken (Windows) bzw. `dynamiteboy` (Unix/Linux/macOS/BSD) starten.

# Offline spielen

* Client starten.
* Auf "Offline-Spiel" klicken und dort Spieler und Arena einstellen.
* Auf "Spiel starten" klicken.

# Online spielen, mit allen Servern auf gleichem PC

* Verwaltungsserver starten, auf "Server starten" klicken.
* Client starten, auf "Online-Spiel" klicken. Die Serverliste ist leer.
* Auf "Eigenes Spiel" klicken, besseren Namen als "Eigener Spieleserver" wählen, auf "Server starten" klicken.
* Zwei weitere Clients starten, auf "Online-Spiel" klicken.
* In der Serverliste steht der eigene Server drin. Doppelklick, einen Namen für den Spieler eingeben, Lobby wird angezeigt.
* Einer der beiden Clients ist der Spielleiter, der darf die Arena einstellen.
* Auf "Runde starten" klicken, sobald mindestens zwei Spieler dabei sind.

# Einen Verwaltungsserver im Internet bereitstellen

* Verwaltungsserver starten, auf "Server starten" klicken.
* Im Router den Port 51963 (TCP) freigeben.

# Einen Spieleserver im Internet bereitstellen

* Client starten, auf "Optionen" klicken. Adresse des Verwaltungsservers angeben, auf "Übernehmen" klicken.
* Im Hauptmenü auf "Online-Spiel" klicken.
* Auf "Eigenes Spiel" klicken, besseren Namen als "Eigener Spieleserver" wählen. Optional die Portnummer verändern.
* Als Adresse die Adresse angeben, unter der der Computer [aus dem Internet erreichbar (ifconfig.co)](https://ifconfig.co/) ist (nein, 192.168.x.x ist nicht aus dem Internet erreichbar).
* Auf "Server starten" klicken. Portnummer des Servers (TCP) im Router freigeben.

# Auf fremden Servern spielen

* Client starten, auf "Optionen" klicken. Adresse des Verwaltungsservers angeben, auf "Übernehmen" klicken.
* Im Hauptmenü auf "Online-Spiel" klicken.
* Einen gelisteten Server auswählen, einen Namen für den Spieler eingeben, Lobby wird angezeigt.
  * "Du bist Spielleiter!": Stelle die Arena an und klicke auf "Runde starten", wenn mindestens zwei Spieler dabei sind.
  * "Du bist in der nächsten Runde dabei!": Einstellen, ob die KI spielen soll und warten, bis der Spielleiter soweit ist.
  * "Du stehst auf der Warteliste!": Mehr Spieler auf dem Server als gegeneinander spielen können/dürfen. Abwarten und Tee trinken oder den Server wechseln.

# Autoren

Entwickelt und programmiert von [Group5Productions](https://gitlab.com/group5productions), einer Gruppe von <s>7</s> 6 Studenten der [TU Chemnitz](https://www.tu-chemnitz.de/) im Rahmen des Softwarepraktikums der [Professur Softwaretechnik](https://www.tu-chemnitz.de/informatik/ST/de/) im Wintersemester 2016/2017:

* [Thomas Weber](https://gitlab.com/pokehax-co), der gefühlt 95 Prozent des Codes geschrieben hat und alles andere auch und auf seine Semesterferien dafür verzichtet hat
* [Christian Ulrich](https://gitlab.com/tortawrer), der die Arena programmiert hat
* [Steven Jung](https://gitlab.com/dafukiw), der die Menüs programmiert und das Tutorial geschrieben hat
* [Michael Richter](https://gitlab.com/Hulax), der die Klassendiagramme entwickelt hat, aber uns leider verlassen musste
* [Zaur Rustamkhanov](https://gitlab.com/unizaur90), der das hier nicht veröffentlichte Handbuch geschrieben hat
* [Paul Gerber](https://gitlab.com/paulgerber), der die Qualität kontrolliert hat
* [Maik Hunger](https://gitlab.com/Probleme), Leiter von Group5Productions
