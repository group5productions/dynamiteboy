#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 app [args...]"
    exit 0
fi

_cp="$HOME/.gradle/caches/modules-2/files-2.1/com.google.code.gson/gson/2.8.0/c4ba5371a29ac9b2ad6129b1d39ea38750043eff/gson-2.8.0.jar"
_cp="$_cp:$(pwd)/build/libs/dynamiteboy.jar"

case $1 in
    managementserver|management-server|management|mgmt|m)
        _cn=org.g5p.dynamiteboy.managementserver.ManagementServerWindow
        ;;
    client|c)
        _cn=org.g5p.dynamiteboy.client.ClientWindow
        ;;
    aitest|a)
        _cp="$_cp:$HOME/.gradle/caches/modules-2/files-2.1/org.hamcrest/hamcrest-core/1.3/42a25dc3219429f0e5d060061f71acb49bf010a0/hamcrest-core-1.3.jar"
        _cp="$_cp:$HOME/.gradle/caches/modules-2/files-2.1/junit/junit/4.12/2973d150c0dc1fefe998f834810d68f278ea58ec/junit-4.12.jar"
        _cp="$_cp:$(pwd)/build/classes/test/"
        _cn=org.junit.runner.JUnitCore
        _arg=org.g5p.dynamiteboy.client.PlayerAITest
        ;;
    *)
        echo "error: unknown application \`$1\`"
        exit 1
esac

shift
if [ ! -z "$_arg" ]; then
    echo java -cp "$_cp" "$_cn" "$_arg" "$@"
    java -cp "$_cp" "$_cn" "$_arg" "$@"
else
    java -cp "$_cp" "$_cn" "$@"
fi
