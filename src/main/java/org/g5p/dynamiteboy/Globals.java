/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy;

/**
 * Diese Klasse enthält öffentlich verfügbare Konstanten aus allen
 * Programmbereichen.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class Globals {
    /**
     * Name dieses Projektes (der überall im Quellcode steht)
     */
    public static final String PROJECT_TITLE = "Dynamite Boy";

    /**
     * IP-Adresse oder Hostname, den Dynamiteboy als Standardadresse festlegt,
     * unter der ein Verwaltungsserver erreicht werden kann, an dem sich der
     * Spieleserver anmeldet.
     */
    public static final String MANAGEMENT_SERVER_DEFAULT_HOST = "127.0.0.1";

    /**
     * TCP-Port, unter welchem der Verwaltungsserver erreichbar ist. Dieser ist
     * in der Spezifikation festgelegt.
     */
    public static final int MANAGEMENT_SERVER_PORT = 51963;

    /**
     * IP-Adresse oder Hostname, den Dynamiteboy als Standardadresse festlegt,
     * unter der ein Spieleserver erreicht werden kann.
     */
    public static final String GAME_SERVER_DEFAULT_HOST = "127.0.0.1";

    /**
     * TCP-Port, auf dem Spieleserver standarmäßig Verbindungen annehmen. Dieser
     * kann vom Administrator des Servers dennoch verändert werden.
     */
    public static final int GAME_SERVER_DEFAULT_PORT = 51964;

    /**
     * Name, der standardmäßig an neu erstellte Spieleserver vergeben wird.
     */
    public static final String GAME_SERVER_DEFAULT_NAME = "Neuer Spieleserver";

    /**
     * Anzahl der Millisekunden, die zwischen dem Senden zweier Heartbeats
     * vergangen sein muss, bevor ein neuer Sendevorgang eingeleitet wird.
     */
    public static final int HEARTBEAT_INTERVAL_MS = 3000;

    /**
     * Anzahl der Millisekunden, die eine Verbindung zwischen Client und Server
     * warten soll, bis ein Zeitüberschreitungsfehler geworfen werden soll.
     */
    public static final int CONNECTION_TIMEOUT_MS = 30000;

    /**
     * Anzahl der Millisekunden, bis das Spiel beginnt.
     */
    public static final int GAME_ROUND_COUNTDOWN_MS = 3000;

    /**
     * Minimale Anzahl an Spielern, die auf einem Spieleserver gleichzeitig
     * spielen dürfen.
     */
    public static final int ROUND_MIN_PLAYERS = 2;

    /**
     * Maximale Anzahl an Spielern, die auf einem Spieleserver gleichzeitig
     * spielen dürfen.
     */
    public static final int ROUND_MAX_PLAYERS = 4;

    /**
     * Standardmäßig eingestellte Vergrößerung des Spielfeldes um das Vielfache
     * der Objektgröße.
     */
    public static final int CLIENT_DEFAULT_OBJECT_SCALE = 4;

    /*
     * Konstanten bezüglich des Spiels
     */

    /**
     * Gameplay: Anzahl der Minuten, die eine Runde standardmäßig dauert.
     */
    public static final int GAME_ROUND_DURATION_DEFAULT_MINUTES = 3;

    /**
     * Gameplay: Anzahl der Sekunden, die den Schutz des Powerups "Rüstung"
     * aufrecht erhält.
     */
    public static final int GAME_ARMOR_EFFECT_DURATION_SECONDS = 6;

    /**
     * Gameplay: Anzahl der Sekunden, die bis zur Detonation einer Bombe
     * verbleiben.
     */
    public static final int GAME_BOMB_COUNTDOWN_SECONDS = 4;

    /**
     * Gameplay: Standardmäßig eingestellte Breite der Arena. Diese muss
     * zwischen 7 und 21 liegen sowie ungerade sein.
     */
    public static final int GAME_ARENA_WIDTH_DEFAULT = 11;

    /**
     * Gameplay: Standardmäßig eingestellte Höhe der Arena. Diese muss zwischen
     * 7 und 21 liegen sowie ungerade sein.
     */
    public static final int GAME_ARENA_HEIGHT_DEFAULT = 11;

    /**
     * Gameplay: Wahrscheinlichkeit in Prozent, dass ein Powerup an der Stelle
     * einer weg gesprengten Mauer erscheint.
     */
    public static final int GAME_POWERUP_APPEARANCE_CHANCE_PERCENT = 55;

    /*
     * Die folgenden drei Wahrscheinlichkeiten müssen zusammen 100 Prozent
     * ergeben.
     */

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Radius Erweiterung" handelt (Standard A).
     */
    public static final int GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A = 333;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Extra Bombe" handelt (Standard A).
     */
    public static final int GAME_POWERUP_MORE_BOMBS_CHANCE_10TH_PERCENT_A = 334;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Rüstung" handelt (Standard A).
     */
    public static final int GAME_POWERUP_ARMOR_CHANCE_10TH_PERCENT_A = 333;

    /*
     * Die folgenden acht Wahrscheinlichkeiten müssen zusammen 100 Prozent
     * ergeben.
     */

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Radius Erweiterung" handelt (Standard A+B.
     */
    public static final int GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A_B = 146;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Extra Bombe" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_MORE_BOMBS_CHANCE_10TH_PERCENT_A_B = 146;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Rüstung" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_ARMOR_CHANCE_10TH_PERCENT_A_B = 146;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Beschleinigung" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_SPEED_10TH_PERCENT_A_B = 146;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Kick" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_BOMB_KICK_10TH_PERCENT_A_B = 146;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Super Bombe" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_SUPER_BOMB_10TH_PERCENT_A_B = 63;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Maximum Radius" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_MAX_RADIUS_10TH_PERCENT_A_B = 62;

    /**
     * Gameplay: Wahrscheinlichkeit in Zehntel Prozent, dass, wenn ein Powerup
     * erscheint, es sich um "Bombenläufer" handelt (Standard A+B).
     */
    public static final int GAME_POWERUP_BOMB_RUNNER_10TH_PERCENT_A_B = 145;

    /**
     * Gameplay: Anzahl der Ticks, die pro Sekunde etwa ausgeführt werden.
     */
    public static final int GAME_TICKS_PER_SECOND = 15;
}