/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import com.google.gson.annotations.Expose;

/**
 * Diese Hilfsklasse dient dazu, das Unterobjekt "aktuelle und maximale
 * Spielerzahl" in JSON zu codieren bzw. aus JSON wieder in ein Objekt zu
 * decodieren.
 *
 * Instanzen dieser Klasse dürfen ausschließlich vom Netzwerk-Stack erzeugt
 * werden. Für alle anderen Komponenten sind ausschließlich die Getter von
 * Belang.
 *
 * @author Thomas Weber
 */
public final class PlayerCount {
    @Expose
    private int current;
    @Expose
    private int max;

    /**
     * Initialisiere ein Objekt, welches aktuelle und maximale Spielerzahl eines
     * Spielers speichert.
     *
     * @param current
     * @param max
     */
    public PlayerCount(int current, int max) {
        this.current = current;
        this.max = max;
    }

    /**
     * Gibt die Zahl der Spieler, die momentan auf dem Spieleserver angemeldet
     * ist, zurück.
     *
     * @return aktuelle Spielerzahl auf dem Spieleserver
     */
    public int getCurrentPlayerCount() {
        return current;
    }

    /**
     * Gibt die Anzahl der maximal zulässigen Spieler auf dem Spieleserver
     * zurück.
     *
     * @return maximale Spielerzahl auf dem Spieleserver
     */
    public int getMaxPlayerCount() {
        return max;
    }
}
