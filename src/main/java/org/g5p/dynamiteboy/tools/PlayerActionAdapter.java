/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Diese Hilfsklasse übersetzt den Enumerations-Typ PlayerAction in Integer und
 * in die andere Richtung genauso.
 *
 * @author Thomas Weber
 */
public final class PlayerActionAdapter
    implements JsonSerializer<PlayerAction>, JsonDeserializer<PlayerAction> {
    @Override
    public PlayerAction deserialize(JsonElement json, Type typeOfT,
        JsonDeserializationContext context) throws JsonParseException {
        int value = json.getAsInt();
        return PlayerAction.fromValue(value);
    }

    @Override
    public JsonElement serialize(PlayerAction src, Type typeOfSrc,
        JsonSerializationContext context) {
        return new JsonPrimitive(src.getValue());
    }
}
