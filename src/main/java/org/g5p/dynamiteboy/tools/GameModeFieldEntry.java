/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import com.google.gson.annotations.Expose;

/**
 * Diese Hilfsklasse serialisiert ein aktualisiertes Feld in der Spielearena in
 * ein JSON-Objekt mit seinen Koordinaten und einer ID, die das Objekt
 * identifiziert, und wird in Updates der Arena verwendet, während der
 * Spieleserver das Spiel operiert.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class GameModeFieldEntry {
    @Expose
    private int x;
    @Expose
    private int y;
    @Expose
    private int id;

    /**
     * Erzeugt einen neuen Feldeintrag.
     *
     * @param x
     *            X-Koordinate (0-basiert) des Feldes
     * @param y
     *            Y-Koordinate (0-basiert) des Feldes
     * @param id
     *            Eindeutige ID des Feldes, aus der Enumeration
     *            GameObjectIdentifier entstammend
     * @see org.g5p.dynamiteboy.engine.objects.GameObjectIdentifier
     */
    public GameModeFieldEntry(int x, int y, int id) {
        if (x < 0) {
            throw new IllegalArgumentException(
                "X-Koordinate darf nicht negativ sein");
        }
        if (y < 0) {
            throw new IllegalArgumentException(
                "Y-Koordinate darf nicht negativ sein");
        }
        if (id < 0 || id > 255) {
            throw new IllegalArgumentException("ID muss in 8 Bit passen");
        }

        this.x = x;
        this.y = y;
        this.id = id;
    }

    /**
     * Gibt die X-Koordinate des Feldes zurück.
     *
     * @return X-Koordinate des Feldes
     */
    public int getX() {
        return this.x;
    }

    /**
     * Gibt die Y-Koordinate des Feldes zurück.
     *
     * @return Y-Koordinate des Feldes
     */
    public int getY() {
        return this.y;
    }

    /**
     * Gibt die eindeutige ID des Feldes zurück.
     *
     * @return eindeutige ID des Feldes
     */
    public int getId() {
        return this.id;
    }
}
