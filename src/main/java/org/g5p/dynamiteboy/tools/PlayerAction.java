/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

/**
 * Diese Enumeration legt fest, welche Aktionen ein Spieler an seiner Spielfigur
 * im Onlinemodus durchführen kann.
 *
 * @author Thomas Weber
 */
public enum PlayerAction {
    /**
     * Bombe platzieren
     */
    PLANT_BOMB(0),

    /**
     * Nach rechts laufen
     */
    MOVE_RIGHT(1),

    /**
     * Nach links laufen
     */
    MOVE_LEFT(2),

    /**
     * Nach oben laufen
     */
    MOVE_UP(3),

    /**
     * Nach unten laufen
     */
    MOVE_DOWN(4);

    private final int _value;

    /**
     * (Dieser Konstruktor dient der internen Nutzung.)
     *
     * @param value
     *            Wert, der in der Enumeration gespeichert werden soll
     */
    PlayerAction(int value) {
        this._value = value;
    }

    /**
     * Übersetzt eine Spieleraktion zu einem Integer, der über das Netzwerk
     * transportiert werden kann.
     *
     * @param m
     *            Spieleraktion
     */
    public int getValue() {
        return _value;
    }

    /**
     * Übersetzt eine über das Netzwerk übertragene Spieleraktion zurück.
     *
     * @param v
     *            Spieleraktion, wie er über das Netzwerk übertragen wurde
     */
    public static PlayerAction fromValue(int v) {
        for (PlayerAction x : PlayerAction.values()) {
            if (x._value == v)
                return x;
        }
        return null;
    }
}