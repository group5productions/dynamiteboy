/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

/**
 * Diese Enumeration definiert die Spielmodi aus dem Lastenheft, eines der
 * Dokumente, die dem Spiel zugrunde liegt.
 *
 * @author Thomas Weber
 */
public enum GameplayMode {
    /**
     * Spielmodus "Standard A"
     */
    STANDARD_A(1),

    /**
     * Spielmodus "Standard A+B"
     */
    STANDARD_A_B(2);

    private final int _value;

    /**
     * (Dieser Konstruktor dient der internen Nutzung.)
     *
     * @param value
     *            Wert, der in der Enumeration gespeichert werden soll
     */
    GameplayMode(int value) {
        this._value = value;
    }

    /**
     * Übersetzt einen Spielmodus zu einem Integer, der über das Netzwerk
     * transportiert werden kann.
     *
     * @param m
     *            Spielmodus
     */
    public int getValue() {
        return _value;
    }

    /**
     * Übersetzt einen über das Netzwerk übertragenen Spielmodus zurück.
     *
     * @param v
     *            Spielmodus, wie er über das Netzwerk übertragen wurde
     */
    public static GameplayMode fromValue(int v) {
        switch (v) {
        case 1:
            return GameplayMode.STANDARD_A;
        case 2:
            return GameplayMode.STANDARD_A_B;
        default:
            return null;
        }
    }
}