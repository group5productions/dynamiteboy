/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Diese Hilfsklasse übersetzt den Enumerations-Typ GameplayMode in Integer und
 * in die andere Richtung genauso.
 *
 * @author Thomas Weber
 */
public final class GameplayModeAdapter
    implements JsonSerializer<GameplayMode>, JsonDeserializer<GameplayMode> {
    @Override
    public GameplayMode deserialize(JsonElement json, Type typeOfT,
        JsonDeserializationContext context) throws JsonParseException {
        int value = json.getAsInt();
        return GameplayMode.fromValue(value);
    }

    @Override
    public JsonElement serialize(GameplayMode src, Type typeOfSrc,
        JsonSerializationContext context) {
        return new JsonPrimitive(src.getValue());
    }
}
