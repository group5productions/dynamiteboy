/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.util.function.Consumer;

/**
 * Diese Klasse kann überall als Consumer eingesetzt werden, erlaubt jedoch über
 * den Konstruktor das Übergeben eines beliebigen Typs, dessen Typ in der
 * Typvariable T steht. Das Objekt ist in der run()-Methode unter dem Namen
 * "originalThis" abrufbar.
 *
 * @param <T>
 *            Typ des Objektes, welches dem Runnable übergeben werden soll
 * @author Thomas Weber
 */
public abstract class ConsumerWithThis<T, V> implements Consumer<V> {
    /**
     * Objekt, welches dem Consumer übergeben wurde
     */
    protected T originalThis;

    /**
     * Erzeugt einen neuen Consumer mit einem beliebigen Objekt.
     *
     * @param originalThis
     *            Objekt, welches dem Consumer übergeben werden soll
     */
    public ConsumerWithThis(T originalThis) {
        this.originalThis = originalThis;
    }

    /**
     * Führt den Consumer mit einem neuen Objekt aus.
     *
     * @param arg
     *            neues Objekt für den Consumer
     */
    @Override
    public abstract void accept(V arg);
}