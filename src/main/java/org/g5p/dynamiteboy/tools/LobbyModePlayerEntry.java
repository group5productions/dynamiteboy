/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import com.google.gson.annotations.Expose;

/**
 * Diese Hilfsklasse serialisiert einen Spieler in ein JSON-Objekt mit seinem
 * Namen und einem booleschen Wert, ob es sich bei dem Spieler um den
 * Spielleiter handelt, und wird in Updates der Spielerliste verwendet, während
 * die Lobby des Spieleservers offen ist.
 *
 * @author Thomas Weber
 */
public final class LobbyModePlayerEntry {
    @Expose
    private String name;
    @Expose
    private boolean isLeader;

    /**
     * Erzeugt einen neuen Spielereintrag.
     *
     * @param name
     *            Name des Spielers
     * @param isLeader
     *            true, wenn der Spieler Spielleiter ist, ansonsten false
     */
    public LobbyModePlayerEntry(String name, boolean isLeader) {
        this.name = name;
        this.isLeader = isLeader;
    }

    /**
     * Gibt den Namen des Spielers zurück.
     *
     * @return Name des Spielers
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gibt zurück, ob der Spieler Spielleiter ist.
     *
     * @return true, wenn der Spieler Spielleiter ist, ansonsten false
     */
    public boolean isLeader() {
        return this.isLeader;
    }
}
