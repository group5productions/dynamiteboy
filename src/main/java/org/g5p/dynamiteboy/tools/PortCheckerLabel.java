/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.JLabel;

/**
 * Diese Klasse implementiert ein Label, welches den Anwender ermöglicht, zu
 * überprüfen, ob ein Port, der einen aktiven Serverdienst hostet, aus dem
 * Internet erreichbar ist. Hierfür werden zwei HTTPS-Anfragen gesendet: eine
 * Anfrage an die Webseite ifconfig.co, um die öffentliche IP-Adresse des
 * Anwenders zu ermitteln, und eine weitere Anfrage an die Webseite admin.md, um
 * die eigentliche Überprüfung durchzuführen (welche die öffentliche IP-Adresse
 * benötigt). Danach wird dem Anwender angezeigt, ob der Port des Serverdienstes
 * offen oder geschlossen ist.
 *
 * @author Thomas Weber
 */
public final class PortCheckerLabel extends JLabel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * dieses Feld Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Zustände, die das Label einnehmen kann.
     */
    private enum _PortCheckerState {
        /**
         * Zustand "Mit gewisser Sicherheit ist der Port geschlossen"
         */
        DEFINITELY_CLOSED,

        /**
         * Zustand "Mit gewisser Sicherheit ist der Port offen"
         */
        DEFINITELY_OPEN,

        /**
         * Zustand "Es ist unbekannt, ob der Port offen oder geschlossen ist".
         * Bietet dem Anwender an, dies zu überprüfen.
         */
        POTENTIALLY_OPEN,

        /**
         * Zustand "Überprüfung des Ports ist in Arbeit"
         */
        IN_PROGRESS,

        /**
         * Zustand "Es ist ein Fehler in der Überprüfung des Ports aufgetreten".
         * Bietet dem Anwender an, es erneut zu probieren.
         */
        ERROR
    }

    private int _port;
    private final Cursor _originalCursor;
    private final Cursor _handCursor;
    private final _PerformPortCheckMouseListener _clickListener;
    private _PortCheckerState _state;
    private boolean _useBlueForOK;

    /**
     * Dieser Runnable, welcher individuell oder von einem Thread aus überprüft
     * werden kann, führt die eigentliche Überprüfung des Ports durch.
     */
    private final class _PortCheckerRunner implements Runnable {
        /**
         * Dauer in Millisekunden, wie lange die Bearbeitung einer Anfrage durch
         * den Server dauern darf, bis die Anfrage abgebrochen wird.
         */
        private static final int _HTTP_REQUEST_TIMEOUT_MS = 5000;

        /**
         * Kennung, unter der sich die Überprüfung bei den Internetdiensten
         * meldet (user-agent). Er identifiziert sich sowohl als das Spiel als
         * auch als einen populären Internetbrowser, um mögliche Blockaden der
         * verwendeten Dienste gegen Nicht-Internetbrowser abzuwehren.
         */
        private static final String _HTTP_USER_AGENT = "Mozilla/5.0 "
            + "DynamiteBoy/20170328 like Gecko/20100101 Firefox/52.0";

        /**
         * Liest einen InputStream, der nur in kleinen Einzelteilen durch
         * mehrere Aufrufe komplett gelesen werden kann, in einen ByteBuffer,
         * der nicht größer ist als das angegebene Limit.
         *
         * @param stream
         *            InputStream, der gelesen werden soll und üblicherweise aus
         *            einer HttpURLConnection bzw. HttpsURLConnecion stammt.
         * @param limit
         *            Maximale Größe der zu lesenden Daten in Bytes. Wird die
         *            Grenze überschritten, wird eine IOException geworfen.
         * @return Byte-Array, welches die Daten aus dem InputStream enthält und
         *         kleiner als oder gleich dem Limit groß ist
         * @throws IOException
         *             wenn beim Lesen des Streams ein Fehler auftritt oder das
         *             gesetzte Limit überschritten wird
         */
        private byte[] readFromStreamAtMost(final InputStream stream,
            final int limit) throws IOException {
            /*
             * Lege zuerst ein Byte-Array mit Maximalgröße an und überwache
             * bisher gelesene Bytes.
             */
            final byte[] temp = new byte[limit];
            int currentOffset = 0;
            int totalReadBytes = 0;

            /*
             * Solange wir die Grenze noch nicht überschritten haben, können wir
             * weiter Daten lesen.
             */
            while (totalReadBytes <= limit) {
                /*
                 * Lese den InputStream. Wir nehmen an, dass mit 0 oder -1
                 * gelesenen Bytes die Eingabe zu Ende ist, und brechen ab.
                 */
                final int readBytes = stream.read(temp, currentOffset,
                    limit - totalReadBytes);
                if (readBytes <= 0)
                    break;

                /* Passe Offsets für nächste Schleife an */
                currentOffset += readBytes;
                totalReadBytes += readBytes;
            }

            /* Breche bei Überschreitung des Limits ab */
            if (totalReadBytes > limit)
                throw new IOException("Gelesener Inhalt überschreitet Limit");

            /*
             * Kopiere in ein neues Byte-Array, aber nur so viel, wie gelesen
             * wurde.
             */
            final byte[] out = new byte[totalReadBytes];
            System.arraycopy(temp, 0, out, 0, totalReadBytes);
            return out;
        }

        /**
         * Sendet eine einfache HTTPS-Anfrage an einen Server und gibt die
         * Antwort als ASCII-codierten String zurück. Antworten des Servers
         * dürfen nicht größer als 1 Kilobyte sein. Die Anfrage wird mit der
         * Operation GET durchgeführt und benutzt einen festgelegten User-Agent.
         *
         * @param url
         *            URL, die durch die Anfrage abgerufen werden soll. Es
         *            werden nur https:// URLs unterstützt.
         * @return ASCII-codierter String, der die Antwort enthält
         * @throws Exception
         *             wenn beim Abrufen oder beim Lesen ein Fehler auftritt
         */
        private String _sendSimpleHttpsRequest(final String url)
            throws Exception {
            HttpsURLConnection connection = null;
            String result = null;
            try {
                /* Verarbeite die URL und mache mit ihr eine neue Anfrage auf */
                URL urlObject = new URL(url);
                connection = (HttpsURLConnection) urlObject.openConnection();

                /* Timeout für Anfragen, GET-Request, User-Agent, kein Cachen */
                connection.setConnectTimeout(_HTTP_REQUEST_TIMEOUT_MS);
                connection.setReadTimeout(_HTTP_REQUEST_TIMEOUT_MS);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("User-Agent", _HTTP_USER_AGENT);
                connection.setUseCaches(false);

                /*
                 * Überprüfe den Statuscode der Antwort, welcher 200 lauten
                 * muss. Interessanterweise führt getResponseCode() bereits zur
                 * Übermittlung der Anfrage.
                 */
                int responseCode = connection.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    throw new Exception(
                        String.format("HTTP-Fehler %d", responseCode));
                }

                /*
                 * Überprüfe auf das Antwortattribut "Content-Length". Steht
                 * dies drin, setze die Limits nach dieser. Ansonsten lege ein
                 * Limit fest. In beiden Fällen darf dieser nicht größer als 1
                 * Kilobyte sein.
                 */
                long contentLength = connection.getContentLengthLong();
                if (contentLength > 1024)
                    throw new Exception("Inhalt zu groß zum Verarbeiten");
                else if (contentLength < 0)
                    contentLength = 1024;

                /*
                 * Erzeuge einen InputStream für die vom Server übermittelten
                 * Daten und lese diese.
                 */
                InputStream responseStream = connection.getInputStream();
                byte[] responseData = readFromStreamAtMost(responseStream,
                    (int) contentLength);

                /*
                 * Erzeugt einen String aus den Daten. Daten dürfen
                 * ausschließlich ASCII-Zeichen enthalten.
                 */
                result = new String(responseData, StandardCharsets.US_ASCII);
            } finally {
                /* Trenne die Verbindung beim Säubern */
                if (connection != null)
                    connection.disconnect();
            }
            return result;
        }

        /**
         * Führt die Portüberprüfung durch. Dabei werden Anfragen an die
         * Webseiten ifconfig.co und admin.md gesendet (beide verschlüsselt über
         * HTTPS). Die Anfragen werden ausgewertet und dem Anwender präsentiert.
         */
        @Override
        public void run() {
            /*
             * Frage ifconfig.co nach unserer öffentlichen IP-Adresse, liefert
             * Plaintext mit der IP-Adresse allein (und damit ASCII) zurück.
             */
            String ip = null;
            try {
                /* Entferne Newline-Zeichen */
                ip = _sendSimpleHttpsRequest("https://ifconfig.co/ip").trim();
            } catch (Exception e) {
                _displayError();
                return;
            }

            /*
             * Gebe die IP-Adresse an admin.md weiter, welche den eigentlichen
             * Portcheck durchführt. Liefert JSON zurück, welches sich aber auch
             * auf ASCII beschränkt.
             */
            final String portCheckerURL = String.format(
                "https://www.admin.md/portchecker/check?ip=%s&port=%d", ip,
                _port);
            String result = null;
            try {
                result = _sendSimpleHttpsRequest(portCheckerURL);
            } catch (Exception e) {
                _displayError();
                return;
            }

            /*
             * Anstatt aufwendig eine Instanz von GSON zu erstellen, um ein paar
             * Byte JSON zu parsen, suchen wir händisch nach '"error": 0' (Port
             * ist offen) bzw. '"error": 1' (Port ist geschlossen), um zwischen
             * Erfolg und Misserfolg zu unterscheiden.
             */
            if (result.contains("\"error\": 0"))
                _displayDefinitelyOpen();
            else if (result.contains("\"error\": 1"))
                displayDefinitelyClosed();

            /* Alles andere ist ein Fehler, zeige als solches */
            else
                _displayError();
        }
    }

    /**
     * Eine Implementierung von MouseAdapter, die auf Mausklicks auf das Label
     * reagiert und die Portüberprüfung startet.
     */
    private final class _PerformPortCheckMouseListener extends MouseAdapter {
        /**
         * Diese Methode wird ausgeführt, wenn das Label mit der Maus angeklickt
         * wird.
         *
         * @param event
         *            Daten zu diesem Ereignis
         */
        @Override
        public void mouseClicked(MouseEvent event) {
            /* Zeige an, dass die Überprüfung läuft */
            _displayWaiting();
            repaint();

            /* Führe die Überprüfung in einem neuen Thread aus */
            new Thread(new _PortCheckerRunner()).start();
        }
    }

    /**
     * Zeigt einen vom aktuellen Zustand des Labels abhängigen Text (stets HTML)
     * an. Wenn angezeigt wird, dass der Port eventuell offen ist oder bei der
     * Überprüfung ein Fehler aufgetreten ist, dann ermögliche das Anklicken des
     * Labels.
     */
    private void _updateByState() {
        /*
         * Entscheide je nach Zustand, ob das Klicken eingeschaltet werden soll
         * oder nicht. Deute das Klicken im Fall, dass es eingeschaltet werden
         * soll, durch den Wechsel auf einen entsprechend andeutenden Mauszeiger
         * an.
         */
        if (_state == _PortCheckerState.POTENTIALLY_OPEN
            || _state == _PortCheckerState.ERROR) {
            addMouseListener(_clickListener);
            setCursor(_handCursor);
        } else {
            removeMouseListener(_clickListener);
            setCursor(_originalCursor);
        }

        /*
         * Wähle je nach Zustand einen Text aus. Es sollte im Normalfall kein
         * unerwarteter Zustand auftreten (auch nicht null oder so).
         */
        String text = null;
        switch (_state) {
        case DEFINITELY_CLOSED:
            text = String.format("<html>Port %d ist <font color=\"#ff0000\"><b>"
                + "geschlossen</b></font>.</html>", _port);
            break;
        case DEFINITELY_OPEN:
            text = String.format(
                "<html>Port %d ist <font color=\"#%s\">"
                    + "<b>offen</b></font>.</html>",
                _port, _useBlueForOK ? "0000cc" : "00cc00");
            break;
        case ERROR:
            text = String.format("<html>Fehler beim Überprüfen von Port %d. "
                + "<a href=\"\">Erneut versuchen</a></html>", _port);
            break;
        case IN_PROGRESS:
            text = String.format("<html>Überprüfe Port %d...</html>", _port);
            break;
        case POTENTIALLY_OPEN:
            text = String.format("<html>Port %d ist <b>möglicherweise "
                + "offen</b>. <a href=\"\">Überprüfen</a></html>", _port);
            break;
        default:
            break;
        }

        /* Zeige den Text an */
        assert text != null;
        setText(text);
    }

    /**
     * Zeigt im Label an, dass während der Portüberprüfung ein Fehler auftrat,
     * und bietet dem Anwender an, es erneut zu versuchen.
     */
    private void _displayError() {
        _state = _PortCheckerState.ERROR;
        _updateByState();
    }

    /**
     * Zeigt im Label an, dass die Portüberprüfung momentan läuft.
     */
    private void _displayWaiting() {
        _state = _PortCheckerState.IN_PROGRESS;
        _updateByState();
    }

    /**
     * Zeigt im Label an, dass die Portüberprüfung ergeben hat, dass der Port
     * aus dem Internet erreichbar (offen) ist. Das Wort "offen" wird je nach
     * Einstellung grün oder blau dargestellt (siehe
     * {@link PortCheckerLabel#setBlueForOK(boolean)}).
     */
    private void _displayDefinitelyOpen() {
        _state = _PortCheckerState.DEFINITELY_OPEN;
        _updateByState();
    }

    /**
     * Zeigt im Label an, dass der Port möglicherweise offen ist, und bietet dem
     * Anwender an, eine Portüberprüfung durchzuführen.
     */
    public void displayPotentiallyOpen() {
        _state = _PortCheckerState.POTENTIALLY_OPEN;
        _updateByState();
    }

    /**
     * Zeigt im Label an, dass die Portüberprüfung ergeben hat, dass der Port
     * nicht aus dem Internet erreichbar (geschlossen) ist.
     */
    public void displayDefinitelyClosed() {
        _state = _PortCheckerState.DEFINITELY_CLOSED;
        _updateByState();
    }

    /**
     * Legt den Port fest, der durch die Portüberprüfung überprüft bzw. im Label
     * angezeigt werden soll.
     *
     * @param port
     *            Port-Nummer, die überprüft werden soll (zwischen 1 und 65535)
     */
    public void setPort(final int port) {
        /* Überprüfe Portbereich */
        if (port < 1 || port > 65535) {
            throw new IllegalArgumentException(
                "Ungültige Port-Nummer, muss zwischen 1 und 65535 liegen");
        }

        _port = port;
        _updateByState();
    }

    /**
     * Legt fest, ob der Hinweis, dass der Port offen ist, blau oder grün
     * dargestellt werden soll. Dies ist hilfreich, wenn aufgrund der Farbwahl
     * der Oberfläche Grüntöne schlecht lesbar sind.
     *
     * @param useBlueForOK
     *            Ist dieser Wert true, wird die Meldung blau dargestellt,
     *            ansonsten grün
     */
    public void setBlueForOK(final boolean useBlueForOK) {
        _useBlueForOK = useBlueForOK;
        if (_state == _PortCheckerState.DEFINITELY_OPEN)
            _updateByState();
    }

    /**
     * Erzeugt ein neues Label, welches eine Portüberprüfung ermöglicht.
     *
     * @param port
     *            Port-Nummer, die überprüft werden soll (zwischen 1 und 65535)
     */
    public PortCheckerLabel(final int port) {
        super();

        /* Schreibe Text normal und nicht fett, da wir die <b>-Tags benutzen */
        Font font = getFont();
        setFont(font.deriveFont(Font.PLAIN));

        /* Überprüfe Portbereich */
        if (port < 1 || port > 65535) {
            throw new IllegalArgumentException(
                "Ungültige Port-Nummer, muss zwischen 1 und 65535 liegen");
        }

        this._port = port;
        this._originalCursor = getCursor();
        this._handCursor = new Cursor(Cursor.HAND_CURSOR);
        this._clickListener = new _PerformPortCheckMouseListener();
        this._state = null;
        this._useBlueForOK = false;
        displayDefinitelyClosed();
    }
}
