/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Diese Klasse enthält ausschließlich die Methode "formatException". Mit dieser
 * kann der Backtrace eine Ausnahme in einen String geschrieben werden, welche
 * wiederum in Hinweismeldungen ausgegeben werden kann.
 *
 * @author Thomas Weber
 */
public final class ExceptionFormatter {
    /**
     * Anzahl der Zeilen des Backtraces (inklusive Begründung), die maximal
     * ausgegeben werden
     */
    private static final int _EXCEPTION_MAX_LINES = 20;

    /**
     * Mache aus einer Ausnahme einen leserlichen Text mit einer schicken
     * Überschrift, welcher in einen String gepackt wird.
     *
     * @param e
     *            Ausnahme, die formatiert in einen String geschrieben werden
     *            soll
     */
    public static String formatException(Exception e) {
        /* Schreibe den Backtrace in irgendeine Form von String */
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));

        /*
         * Teile den Backtrace in einzelne Zeilen auf und kopiere so viele, wie
         * zulässig sind. Wird dabei die Maximalzahl erlaubter Zeilen
         * überschritten, beende den Backtrace mit "<gekürzt>".
         */
        String[] lines = writer.toString().split("\n");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < Math.min(lines.length, _EXCEPTION_MAX_LINES); ++i) {
            if (i < _EXCEPTION_MAX_LINES - 1)
                builder.append(lines[i] + "\n");
            else {
                if (lines.length == _EXCEPTION_MAX_LINES)
                    builder.append(lines[i] + "\n");
                else
                    builder.append("<gekürzt>");
            }
        }

        /* Hänge vor die Begründung noch einen Hinweis an */
        return "Details für Entwickler: " + builder.toString();
    }
}