/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import com.google.gson.annotations.Expose;

/**
 * Diese Hilfsklasse serialisiert einen Spieler in ein JSON-Objekt mit seinem
 * Namen und verschiedenen Statistiken aus einer Spielerunde, die zusammen einen
 * Eintrag des Highscores bilden.
 *
 * @author Thomas Weber
 */
public final class HighscorePlayerEntry {
    @Expose
    private String name;
    @Expose
    private int walkedSteps;
    @Expose
    private int plantedBombs;
    @Expose
    private int destroyedWalls;
    @Expose
    private int killedPlayers;
    @Expose
    private int collectedPowerups;
    @Expose
    private boolean isSuicided;
    @Expose
    private boolean isLastPlayer;
    @Expose
    private int score;

    /**
     * Erzeugt einen neuen Highscoreeintrag für einen Spieler.
     *
     * @param name
     *            Name des Spielers
     * @param walkedSteps
     *            Anzahl der gelaufenen Schritte
     * @param plantedBombs
     *            Anzahl der gelegten Bomben
     * @param destroyedWalls
     *            Anzahl der zerstörten Wände
     * @param killedPlayers
     *            Anzahl der getöteten Spieler
     * @param collectedPowerups
     *            Anzahl der eingesammelten Powerups
     * @param isSuicided
     *            true wenn der Spieler selbst umgekommen ist, ansonsten false
     * @param isLastPlayer
     *            true wenn der letzte in der Arena ist, ansonsten false
     * @param score
     *            Highscore des Spielers
     */
    public HighscorePlayerEntry(String name, int walkedSteps, int plantedBombs,
        int destroyedWalls, int killedPlayers, int collectedPowerups,
        boolean isSuicided, boolean isLastPlayer, int score) {
        this.name = name;
        this.walkedSteps = walkedSteps;
        this.plantedBombs = plantedBombs;
        this.destroyedWalls = destroyedWalls;
        this.killedPlayers = killedPlayers;
        this.collectedPowerups = collectedPowerups;
        this.isSuicided = isSuicided;
        this.isLastPlayer = isLastPlayer;
        this.score = score;
    }

    /**
     * Gibt den Namen des Spielers zurück.
     *
     * @return Name des Spielers
     */
    public String getName() {
        return name;
    }

    /**
     * Gibt die Anzahl der gelaufenen Schritte zurück.
     *
     * @return Anzahl der gelaufenen Schritte
     */
    public int getWalkedSteps() {
        return walkedSteps;
    }

    /**
     * Gibt die Anzahl der gelegten Bomben zurück.
     *
     * @return Anzahl der gelegten Bomben
     */
    public int getPlantedBombs() {
        return plantedBombs;
    }

    /**
     * Gibt die Anzahl der zerstörten Wände zurück.
     *
     * @return Anzahl der zerstörten Wände
     */
    public int getDestroyedWalls() {
        return destroyedWalls;
    }

    /**
     * Gibt die Anzahl der getöteten Spieler zurück.
     *
     * @return Anzahl der getöteten Spieler
     */
    public int getKilledPlayers() {
        return killedPlayers;
    }

    /**
     * Gibt die Anzahl der eingesammelten Powerups zurück.
     *
     * @return Anzahl der eingesammelten Powerups
     */
    public int getCollectedPowerups() {
        return collectedPowerups;
    }

    /**
     * Gibt zurück, ob der Spieler selbst umgekommen ist.
     *
     * @return true wenn der Spieler selbst umgekommen ist, ansonsten false
     */
    public boolean isSuicided() {
        return isSuicided;
    }

    /**
     * Gibt zurück, ob der Spieler der letzte in der Arena ist.
     *
     * @return true wenn der letzte in der Arena ist, ansonsten false
     */
    public boolean isLastPlayer() {
        return isLastPlayer;
    }

    /**
     * Gibt den Highscore des Spielers zurück.
     *
     * @return Highscore des Spielers
     */
    public int getScore() {
        return score;
    }
}
