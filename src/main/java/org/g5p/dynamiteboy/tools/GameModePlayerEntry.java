/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import com.google.gson.annotations.Expose;

/**
 * Diese Hilfsklasse serialisiert einen Spieler in ein JSON-Objekt mit seinem
 * Namen, seiner Nummer, seinen Koordinaten und einem booleschen Wert, ob der
 * Spieler eine Rüstung trägt, und wird in Updates der Arena verwendet, während
 * der Spieleserver das Spiel operiert.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class GameModePlayerEntry {
    @Expose
    private int number;
    @Expose
    private String name;
    @Expose
    private int x;
    @Expose
    private int y;
    @Expose
    private boolean isArmored;

    /**
     * Erzeugt einen neuen Spielereintrag.
     *
     * @param number
     *            Nummer des Spielers (1, 2, 3 oder 4)
     * @param name
     *            Name des Spielers
     * @param x
     *            X-Koordinate (0-basiert) des Spielers
     * @param y
     *            Y-Koordinate (0-basiert) des Spielers
     * @param isArmored
     *            true, wenn der Spieler eine Rüstung trägt, ansonsten false
     */
    public GameModePlayerEntry(int number, String name, int x, int y,
        boolean isArmored) {
        /* So viel Zeit für Überprüfungen muss sein */
        if (number < 1 || number > 4) {
            throw new IllegalArgumentException(
                "Nummer des Spielers muss 1, 2, 3 oder 4 sein");
        }
        if (x < 0) {
            throw new IllegalArgumentException(
                "X-Koordinate darf nicht negativ sein");
        }
        if (y < 0) {
            throw new IllegalArgumentException(
                "Y-Koordinate darf nicht negativ sein");
        }

        this.number = number;
        this.name = name;
        this.x = x;
        this.y = y;
        this.isArmored = isArmored;
    }

    /**
     * Gibt die Nummer des Spielers zurück.
     *
     * @return Nummer des Spielers
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * Gibt den Namen des Spielers zurück.
     *
     * @return Name des Spielers
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gibt die X-Koordinate des Spielers zurück.
     *
     * @return X-Koordinate des Spielers
     */
    public int getX() {
        return this.x;
    }

    /**
     * Gibt die Y-Koordinate des Spielers zurück.
     *
     * @return Y-Koordinate des Spielers
     */
    public int getY() {
        return this.y;
    }

    /**
     * Gibt zurück, ob der Spieler eine Rüstung trägt.
     *
     * @return true, wenn der Spieler eine Rüstung trägt, ansonsten false
     */
    public boolean isArmored() {
        return this.isArmored;
    }
}
