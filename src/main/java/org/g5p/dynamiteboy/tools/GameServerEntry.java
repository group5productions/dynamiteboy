/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.validator.routines.InetAddressValidator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Diese Hilfsklasse dient dazu, das Unterobjekt "Eintrag eines Spieleservers"
 * in JSON zu codieren bzw. aus JSON wieder in ein Objekt zu decodieren.
 *
 * Instanzen dieser Klasse dürfen ausschließlich vom Netzwerk-Stack erzeugt
 * werden. Für alle anderen Komponenten sind ausschließlich die Getter von
 * Belang.
 *
 * @author Thomas Weber
 */
public final class GameServerEntry {
    @Expose
    @SerializedName("name")
    private String serverName;

    @Expose
    @SerializedName("address")
    private String serverAddress;

    @Expose
    @SerializedName("port")
    private int serverPort;

    @Expose
    @SerializedName("players")
    private PlayerCount playerCounts;

    @Expose
    private boolean isLobbyOpen;

    @Expose
    private GameplayMode gameplayMode;

    /**
     * Erzeugt einen neuen Spieleservereintrag.
     *
     * @param serverName
     *            Name, unter dem der Spieleserver in der Serverliste angezeigt
     *            wird
     * @param serverAddress
     *            IP-Adresse oder Hostname, unter dem der Spieleserver
     *            erreichbar ist
     * @param serverPort
     *            Port, unter dem der Spieleserver auf seiner Adresse erreichbar
     *            ist
     * @param currentPlayerCount
     *            Zahl der momentan angemeldeten Spieler
     * @param maxPlayerCount
     *            Zahl der maximal zugelassenen Spieler
     * @param isLobbyOpen
     *            true wenn die Lobby des Servers offen ist, ansonsten false
     * @param gameplayMode
     *            Aktueller Spielmodus auf dem Server. 1 für "Standard A" bzw. 2
     *            für "Standard A+B".
     */
    public GameServerEntry(String serverName, String serverAddress,
        int serverPort, int currentPlayerCount, int maxPlayerCount,
        boolean isLobbyOpen, GameplayMode gameplayMode) {

        /* Name darf nicht leer sein */
        String trimmedServerName = serverName.trim();
        if (trimmedServerName.length() == 0)
            throw new IllegalArgumentException(
                "Name des Servers darf nicht leer sein");

        /* Erlaubte Ports in TCP sind 1 bis 65535. 0 ist reserviert */
        if (serverPort <= 0 || serverPort > 65535)
            throw new IllegalArgumentException(
                "Port muss zwischen 1 und 65535 liegen");

        /* Erlaube gültige IPv4- und IPv6-Adressen */
        InetAddressValidator validator = new InetAddressValidator();
        if (!validator.isValid(serverAddress)) {
            /* Keine IP-Adresse, erlaube nur auflösbare Hostnamen */
            try {
                InetAddress.getByName(serverAddress);
            } catch (UnknownHostException e) {
                throw new IllegalArgumentException(
                    "Host " + serverAddress + " unbekannt");
            }
        }

        this.serverName = trimmedServerName;
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.setPlayerCounts(currentPlayerCount, maxPlayerCount);
        this.setLobbyOpen(isLobbyOpen);
        this.setGameplayMode(gameplayMode);
    }

    /**
     * Gibt den Namen zurück, unter dem der Spieleserver in der Server gelistet
     * werden soll.
     *
     * @return Name des Spieleservers
     */
    public String getServerName() {
        return this.serverName;
    }

    /**
     * Gibt die Adresse zurück, unter der der Spieleserver erreichbar ist.
     *
     * @return IPv4-Adresse, IPv6-Adresse oder Hostname, unter der der
     *         Spieleserver erreichbar ist
     */
    public String getServerAddress() {
        return this.serverAddress;
    }

    /**
     * Gibt den Port zurück, unter der der Spieleserver erreichbar ist.
     *
     * @return Port, unter der der Spieleserver erreichbar ist
     */
    public int getServerPort() {
        return this.serverPort;
    }

    /**
     * Gibt die aktuelle und maximale Spielerzahl des Servers zurück.
     *
     * @return Daten zu Spielerzahlen auf dem Spieleserver
     */
    public PlayerCount getPlayerCounts() {
        return this.playerCounts;
    }

    /**
     * Setzt die aktuelle und maximale Spielerzahl des Spieleservers.
     *
     * @param currentPlayerCount
     *            aktuelle Spielerzahl auf dem Spieleserver
     */
    public void setPlayerCounts(int currentPlayerCount, int maxPlayerCount) {
        /* Spielerzahl nicht negativ */
        if (currentPlayerCount < 0 || maxPlayerCount < 0)
            throw new IllegalArgumentException(
                "Spielerzahl darf nicht negativ sein");

        /* maximale Spielerzahl 2-4 Spieler */
        if (maxPlayerCount < 2 || maxPlayerCount > 4)
            throw new IllegalArgumentException(
                "maximale Spielerzahl muss 2, 3, 4 betragen");

        this.playerCounts = new PlayerCount(currentPlayerCount, maxPlayerCount);
    }

    /**
     * Gibt zurück, ob die Lobby des Spieleservers offen ist.
     *
     * @return true wenn die Lobby offen ist, ansonsten false
     */
    public boolean isLobbyOpen() {
        return this.isLobbyOpen;
    }

    /**
     * Legt fest, ob die Lobby des Spieleservers offen ist.
     *
     * @param isLobbyOpen
     *            true wenn die Lobby offen ist, ansonsten false
     */
    public void setLobbyOpen(boolean isLobbyOpen) {
        this.isLobbyOpen = isLobbyOpen;
    }

    /**
     * Gibt den aktuellen Spielmodus zurück, der auf dem Spieleserver
     * eingestellt ist bzw. gespielt wird.
     *
     * @return 1 für "Standard A" oder 2 für "Standard A+B"
     */
    public GameplayMode getGameplayMode() {
        return this.gameplayMode;
    }

    /**
     * Setzt den aktuellen Spielmodus, der auf dem Spieleserver eingestellt ist
     * bzw. gespielt wird.
     *
     * @param gameplayMode
     *            GameplayMode.STANDARD_A für "Standard A" oder
     *            GameplayMode.STANDARD_B für "Standard A+B"
     */
    public void setGameplayMode(GameplayMode gameplayMode) {
        this.gameplayMode = gameplayMode;
    }
}
