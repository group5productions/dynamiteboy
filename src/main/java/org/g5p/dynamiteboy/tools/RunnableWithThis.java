/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.tools;

/**
 * Diese Klasse kann überall als Runnable eingesetzt werden, erlaubt jedoch über
 * den Konstruktor das Übergeben eines beliebigen Typs, dessen Typ in der
 * Typvariable T steht. Das Objekt ist in der run()-Methode unter dem Namen
 * "originalThis" abrufbar.
 *
 * @param <T>
 *            Typ des Objektes, welches dem Runnable übergeben werden soll
 * @author Thomas Weber
 */
public abstract class RunnableWithThis<T> implements Runnable {
    /**
     * Objekt, welches dem Runnable übergeben wurde
     */
    protected T originalThis;

    /**
     * Erzeugt einen neuen Runnable mit einem beliebigen Objekt.
     *
     * @param originalThis
     *            Objekt, welches dem Runnable übergeben werden soll
     */
    public RunnableWithThis(T originalThis) {
        this.originalThis = originalThis;
    }

    /**
     * Führt das Runnable aus.
     */
    @Override
    public abstract void run();
}