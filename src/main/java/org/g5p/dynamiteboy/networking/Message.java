/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.nio.charset.StandardCharsets;

import org.g5p.dynamiteboy.tools.GameplayMode;
import org.g5p.dynamiteboy.tools.GameplayModeAdapter;
import org.g5p.dynamiteboy.tools.PlayerAction;
import org.g5p.dynamiteboy.tools.PlayerActionAdapter;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

/**
 * Diese Klasse repräsentiert den Inhalt von über das Netzwerk gesendeten
 * Nachrichten. Zudem ermöglicht sie das Lesen und Schreiben solcher über Ein-
 * und Ausgabeströme.
 *
 * Diese Klasse kann nicht instanziiert werden. Dafür sind im Paket
 * org.g5p.dynamiteboy.networking.messages abgeleitete Klassen verfügbar, mit
 * denen einzelne Nachrichten instanziiert werden können.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 * @see <a href=
 *      "https://gitlab.com/group5productions/bomberman-protocol/blob/master/protocol.md">
 *      Spezifikation des Datenformats
 *      (gitlab.com/group5productions/bomberman-protocol)</a>
 */
public abstract class Message {
    public static final int MESSAGE_SIZE_MAX = 65535;

    private static GsonBuilder _gson;

    private String _command;

    /**
     * Statischer Initialisierer, der die GSON-Bibliothek für spätere Verwendung
     * einrichtet.
     */
    static {
        /*
         * Erzeuge einen neuen Builder für Instanzen von GSON und registriere
         * die Adapter-Klasse zum Verarbeiten von Nachrichten
         */
        _gson = new GsonBuilder();
        _gson.excludeFieldsWithoutExposeAnnotation();

        /* Registere Adapter für Nachrichten und einige Bonus-Klassen */
        _gson.registerTypeAdapter(Message.class, new MessageAdapter());
        _gson.registerTypeAdapter(GameplayMode.class,
            new GameplayModeAdapter());
        _gson.registerTypeAdapter(PlayerAction.class,
            new PlayerActionAdapter());
    }

    /**
     * Erzeugt eine Nachricht durch Kopieren einer existierenden Nachricht.
     *
     * @param message
     *            Existierende Nachricht, die kopiert werden soll
     */
    protected Message(final Message message) {
        this._command = message._command;
    }

    /**
     * Erzeugt eine neue Nachricht mit dem Namen des Kommandos, so wie er im
     * JSON-Objekt transferiert werden soll.
     *
     * @param command
     *            Namen des Kommandos im JSON-Objekt
     */
    protected Message(final String command) {
        this._command = command;
    }

    /**
     * Gibt den Namen des Kommandos zurück, wie er im JSON-Objekt angegeben
     * wird.
     *
     * @return Namen des Kommandos im JSON-Objekt
     */
    public String getCommand() {
        return _command;
    }

    /**
     * Liest eine Nachricht von einem JSON-Objekt, das als String vorliegt. Die
     * Nachricht muss komplett über das Netzwerk transferiert sein und darf
     * ausschließlich das JSON-Objekt enthalten, aber nicht die Länge des
     * Objektes davor.
     *
     * @param input
     *            JSON-Objekt der Nachricht als String
     * @return Java-Instanz der zur Nachricht passenden Klasse, die aus input
     *         erzeugt wurde
     * @throws MessageMalformedException
     *             wenn die Nachricht nicht den Anforderungen des Datenformats
     *             genügt
     */
    public static Message unpackMessage(final String input)
        throws MessageMalformedException {
        try {
            /*
             * Deserialisiere das Objekt mittels des Adapters -- es wird uns die
             * richtige Unterklasse erzeugen
             */
            return _gson.create().fromJson(input, Message.class);
        } catch (JsonSyntaxException e) {
            /* Verpacke die Ausnahme in MessageMalformedException */
            throw new MessageMalformedException(
                "Nachricht ungültig: " + e.getLocalizedMessage());
        }
    }

    /**
     * Schreibt eine Nachricht in ein JSON-Objekt, welches als String codiert
     * ist. Dieser String kann über das Netzwerk transferiert werden.
     *
     * @returns Nachricht als JSON-String, der in Bytes codiert ist
     * @throws MessagePayloadTooLargeException
     *             wenn die erzeugte Nachricht zu groß ist
     */
    public byte[] packMessage() throws MessagePayloadTooLargeException {
        /* Serialisiere das Objekt in einen String mit Newline */
        String jsonString = _gson.create().toJson(this, Message.class) + "\n";

        /*
         * Mache aus dem String UTF-8 in Bytes und nehme dessen Länge, damit
         * Nicht-ASCII-Zeichen als mehrere Bytes gezählt werden
         */
        byte[] jsonBytes = jsonString.getBytes(StandardCharsets.UTF_8);

        /* String darf Maximalgröße nicht überschreiten */
        if (jsonBytes.length > MESSAGE_SIZE_MAX) {
            throw new MessagePayloadTooLargeException(String.format(
                "Zu übermittelnde Nachricht zu groß (%d > %d Bytes)",
                jsonBytes.length, MESSAGE_SIZE_MAX));
        }

        /* Codiere die Stringlänge in Zeichen */
        byte[] jsonBytesLength = Integer.toString(jsonBytes.length)
            .getBytes(StandardCharsets.US_ASCII);

        /* Setze das Byte-Array aus Länge + JSON zusammen */
        byte[] result = new byte[jsonBytesLength.length + jsonBytes.length];
        System.arraycopy(jsonBytesLength, 0, result, 0, jsonBytesLength.length);
        System.arraycopy(jsonBytes, 0, result, jsonBytesLength.length,
            jsonBytes.length);
        return result;
    }
}