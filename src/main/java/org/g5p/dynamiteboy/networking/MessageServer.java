/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Mit dieser Klasse können Server erzeugt werden, die verbundenen Clients
 * Dienste über ein eigenes Netzwerkprotokoll auf TCP/IP-Basis anbieten.
 *
 * @author Thomas Weber
 */
public final class MessageServer extends Thread {
    private static final Logger _logger = Logger
        .getLogger(MessageServer.class.getSimpleName());

    private ServerSocketChannel _serverChannel;
    private List<Thread> _connectionThreads;
    private Class<?> _connectionHandlerClass;
    private boolean _hasConnectionHandlerArgument;
    private Object _connectionHandlerObject;

    private final InetAddress _bindAddress;
    private final int _bindPort;
    private final long _timeout;

    /**
     * Überprüft, ob der Server bereits läuft.
     *
     * @return true wenn der Server läuft, sonst false
     */
    private boolean _isRunning() {
        return _connectionHandlerClass != null && _serverChannel != null;
    }

    /**
     * Fügt einen Verbindungsthread zur Überwachungsliste hinzu.
     *
     * @param thread
     *            Thread, der überwacht werden soll
     */
    private synchronized void _trackConnectionThread(Thread thread) {
        this._connectionThreads.add(thread);
    }

    /**
     * Entfernt einen Verbindungsthread aus der Überwachungsliste.
     *
     * @param thread
     *            Thread, der überwacht wurde
     */
    private synchronized void _untrackConnectionThread(Thread thread) {
        this._connectionThreads.remove(thread);
    }

    /**
     * Erzeugt einen neuen Server, der auf Nachrichten von Clients reagiert.
     *
     * @param bindAddress
     *            Adresse, auf die der Server Verbindungen entgegen nimmt. Um
     *            auf allen Netzwerkadaptern Verbindungen entgegen nehmen zu
     *            können, muss diese 0.0.0.0 lauten.
     * @param bindPort
     *            Port, auf dem der Server Verbindungen entgegen nimmt
     * @param timeout
     *            Zeitpunkt in Millisekunden, nachdem ein IMessageHandler
     *            darüber informiert wird, dass keine Kommunikation statt
     *            gefunden hat
     */
    public MessageServer(final InetAddress bindAddress, final int bindPort,
        final long timeout) {
        this._serverChannel = null;
        this._connectionThreads = new ArrayList<Thread>();
        this._connectionHandlerClass = null;
        this._hasConnectionHandlerArgument = false;
        this._connectionHandlerObject = null;

        this._bindAddress = bindAddress;
        this._bindPort = bindPort;
        this._timeout = timeout;
    }

    /**
     * Setzt eine Klasse, die IMessageHandler implementiert und Nachrichten, die
     * Server und Client austauschen, behandelt. Muss gesetzt werden, bevor der
     * Server-Thread gestartet wird.
     *
     * @param connectionHandler
     *            Klasse, die die Nachrichten behandelt
     */
    public <T extends IMessageHandler> void setConnectionHandler(
        Class<T> connectionHandler) {
        /* Nicht mehr setzen, wenn Server läuft */
        if (_isRunning())
            throw new IllegalStateException("Server läuft bereits");

        this._connectionHandlerClass = connectionHandler;
        this._hasConnectionHandlerArgument = false;
        this._connectionHandlerObject = null;
    }

    /**
     * Setzt eine Klasse, die IMessageHandler implementiert und Nachrichten, die
     * Server und Client austauschen, behandelt. Muss gesetzt werden, bevor der
     * Server-Thread gestartet wird.
     *
     * @param connectionHandler
     *            Klasse, die die Nachrichten behandelt
     * @param objectToPass
     *            Objekt, welches an den Konstruktor der Klasse weiter gereicht
     *            werden soll
     */
    public <T extends IMessageHandler> void setConnectionHandler(
        Class<T> connectionHandler, Object objectToPass) {
        /* Nicht mehr setzen, wenn Server läuft */
        if (_isRunning())
            throw new IllegalStateException("Server läuft bereits");

        this._connectionHandlerClass = connectionHandler;
        this._hasConnectionHandlerArgument = true;
        this._connectionHandlerObject = objectToPass;
    }

    /**
     * Überlagere die interrupt()-Methode, um den Server-Socket zu schließen und
     * ein eventuell wartendes accept() zu unterbrechen. Wird durch den in
     * main() installierten Shutdown-Hook ausgeführt.
     */
    @Override
    public void interrupt() {
        try {
            if (_serverChannel != null)
                _serverChannel.close();
        } catch (IOException __) {
        } finally {
            super.interrupt();
        }
    }

    /**
     * Diese Funktion wird in einem separaten Thread ausgeführt. Diese verwaltet
     * einen Server-Socket, der Verbindungen entgegen nimmt. Für jede Verbindung
     * zum Server wird ein neuer Thread eingerichtet, der die Kommunikation
     * übernimmt.
     */
    @Override
    public void run() {
        /*
         * Beende die Schleife, wenn der Thread unterbrochen wurde.
         */
        while (!this.isInterrupted()) {
            /*
             * Akzeptiere eine neue eingehende Verbindung. Behandle diese in
             * einem eigenen Thread, um weitere Verbindungen zu akzeptieren.
             */
            try {
                SocketChannel clientChannel = _serverChannel.accept();
                clientChannel.configureBlocking(false);

                /*
                 * Erstelle die Instanz einer Klasse, die die Verbindungen
                 * bearbeitet.
                 */
                IMessageHandler connectionHandler;
                if (_hasConnectionHandlerArgument) {
                    /* 1 Parameter */
                    connectionHandler = (IMessageHandler) _connectionHandlerClass
                        .getDeclaredConstructor(Object.class)
                        .newInstance(_connectionHandlerObject);
                } else {
                    /* Ohne Parameter */
                    connectionHandler = (IMessageHandler) _connectionHandlerClass
                        .newInstance();
                }

                /*
                 * Erstelle den Thread, der die Low-Level-Kommunikation mit dem
                 * Client vornimmt.
                 */
                Messenger connection = new Messenger(connectionHandler,
                    clientChannel, _timeout, false);

                /* Wir wollen ihn überwachen, aber nur während er läuft */
                _trackConnectionThread(connection);
                connection.setExitCallback(new Runnable() {
                    @Override
                    public void run() {
                        _untrackConnectionThread(connection);
                    }
                });

                /* Los geht's */
                connection.start();
            }

            /*
             * Wenn accept() eine Sorte von ClosedChannelException wirft, dann
             * wurde der aktuelle Thread unterbrochen. Dies ist unser Signal zum
             * Beenden.
             */
            catch (ClosedChannelException e) {
            }

            catch (Exception e) {
                _logger.severe(
                    String.format("Kann Client-Verbindung nicht annehmen: %s\n",
                        e.getMessage()));
                break;
            }
        }

        /* Schließe alle Verbindungen, indem ihre Threads unterbrochen werden */
        synchronized (this) {
            for (Thread t : this._connectionThreads)
                t.interrupt();
        }

        /*
         * Schließe den Server-Socket. Also wenn *das* eine Exception wirft, na
         * dann weiß ich auch nicht weiter.
         */
        try {
            _serverChannel.close();
        } catch (IOException e) {
            _logger.severe(String.format(
                "Kann Server-Socket nicht schließen: %s\n", e.getMessage()));
            System.exit(1);
        }
        _logger.info("Server wurde beendet");
    }

    /**
     * Startet einen Server. Der aktuelle Zustand des Servers wird überprüft,
     * ein Socket erstellt, und an die im Konstruktor angegebene Adresse sowie
     * Port gebunden. Schlägt eines dieser Schritte fehl, wird eine Ausnahme
     * geworfen.
     *
     * @throws IOException
     *             wenn der Socket des Servers nicht erstellt werden kann, der
     *             Socket nicht an Adresse und Port gebunden kann
     */
    public void startServer() throws IOException {
        /* Nicht mehr starten, wenn Server läuft */
        if (_isRunning())
            throw new IllegalStateException("Server läuft bereits");

        /* Wir brauchen eine Klasse, die Nachrichten verarbeitet */
        if (_connectionHandlerClass == null)
            throw new IllegalArgumentException(
                "Keine Klasse gesetzt, die Nachrichten verarbeitet");

        /* Erstelle den Server-Socket */
        _serverChannel = ServerSocketChannel.open();

        /*
         * Gebe den Port, auf den der Server hört, nach Beenden wieder frei
         * (address re-use).
         */
        _serverChannel.setOption(StandardSocketOptions.SO_REUSEADDR, true);

        /*
         * Binde den Server an ein oder mehrere Netzwerkadapter (auf Basis der
         * IP-Adresse) sowie einen festen Port.
         */
        InetSocketAddress serverAddress = new InetSocketAddress(_bindAddress,
            _bindPort);
        _serverChannel.bind(serverAddress, 10);

        _logger.info(
            String.format("Server akzeptiert Verbindungen unter Adresse %s:%d",
                serverAddress.getAddress().getHostAddress(),
                serverAddress.getPort()));

        /* Starte den Thread, der eingehende Verbindungen bearbeitet */
        this.start();
    }

    /**
     * Stoppt einen Server, indem sein Thread unterbrochen und anschließend
     * gewartet wird, bis er sich selbst beendet.
     */
    public void stopServer() {
        /* Nicht stoppen, wenn Server nicht läuft */
        if (!_isRunning())
            return;

        /* Logging */
        _logger.info(String.format("Schließe Server auf Adresse %s:%d",
            _bindAddress.toString(), _bindPort));

        /* Unterbreche den Thread des Messengers und warte auf sein Beenden */
        this.interrupt();
        try {
            this.join();
        } catch (InterruptedException e) {
            /*
             * Markiere den eigenen Thread als unterbrochen, um Threads, die auf
             * uns warten, zu unterbrechen
             */
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
