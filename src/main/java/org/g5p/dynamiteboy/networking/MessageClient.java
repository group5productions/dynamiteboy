/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

/**
 * Mit dieser Klasse können Clients erzeugt werden, die sich mit einem
 * existierenden Server verbinden und anschließend über ein eigenes
 * Netzwerkprotokoll auf TCP/IP-Basis miteinander kommunizieren.
 *
 * @author Thomas Weber
 */
public final class MessageClient {
    private static final Logger _logger = Logger
        .getLogger(MessageClient.class.getSimpleName());

    private IMessageHandler _connectionHandler;
    private Class<?> _connectionHandlerClass;
    private boolean _hasConnectionHandlerArgument;
    private Object _connectionHandlerObject;
    private SocketChannel _clientChannel;
    private Messenger _messenger;

    private final InetAddress _serverAddress;
    private final int _serverPort;
    private final long _timeout;

    /**
     * Überprüft, ob der Client bereits läuft.
     *
     * @return true wenn der Client läuft, sonst false
     */
    private boolean _isRunning() {
        return _connectionHandler != null && _clientChannel != null
            && _messenger != null;
    }

    /**
     * Erzeugt einen neuen Client, der mit einem Server kommuniziert.
     *
     * @param serverAddress
     *            Adresse, unter der ein existierender Server erreichbar ist
     * @param serverPort
     *            Port, auf dem ein existierender Server Verbindungen entgegen
     *            nimmt
     * @param timeout
     *            Zeitpunkt in Millisekunden, nachdem ein IMessageHandler
     *            darüber informiert wird, dass keine Kommunikation statt
     *            gefunden hat
     */
    public MessageClient(final InetAddress serverAddress, final int serverPort,
        final long timeout) {
        this._connectionHandler = null;
        this._connectionHandlerClass = null;
        this._hasConnectionHandlerArgument = false;
        this._connectionHandlerObject = null;
        this._clientChannel = null;
        this._messenger = null;

        this._serverAddress = serverAddress;
        this._serverPort = serverPort;
        this._timeout = timeout;
    }

    /**
     * Setzt eine Klasse, die IMessageHandler implementiert und Nachrichten, die
     * Server und Client austauschen, behandelt. Muss gesetzt werden, bevor der
     * Client gestartet wird.
     *
     * @param connectionHandler
     *            Klasse, die die Nachrichten behandelt
     */
    public <T extends IMessageHandler> void setConnectionHandler(
        Class<T> connectionHandler) {
        /* Nicht mehr setzen, wenn Client läuft */
        if (_isRunning())
            throw new IllegalStateException("Client läuft bereits");

        this._connectionHandlerClass = connectionHandler;
        this._hasConnectionHandlerArgument = false;
        this._connectionHandlerObject = null;
    }

    /**
     * Setzt eine Klasse, die IMessageHandler implementiert und Nachrichten, die
     * Server und Client austauschen, behandelt. Muss gesetzt werden, bevor der
     * Client gestartet wird.
     *
     * @param connectionHandler
     *            Klasse, die die Nachrichten behandelt
     * @param objectToPass
     *            Objekt, welches dem Konstruktor der Nachrichten behandelnden
     *            Klasse übergeben weden kann
     */
    public <T extends IMessageHandler> void setConnectionHandler(
        Class<T> connectionHandler, Object objectToPass) {
        /* Nicht mehr setzen, wenn Client läuft */
        if (_isRunning())
            throw new IllegalStateException("Client läuft bereits");

        this._connectionHandlerClass = connectionHandler;
        this._hasConnectionHandlerArgument = true;
        this._connectionHandlerObject = objectToPass;
    }

    /**
     * Gibt die Instanz der Klasse zurück, die erzeugt wurde, um Nachrichten zu
     * verarbeiten.
     *
     * @return Instanz der Klasse, die setConnectionHandler() übergeben wurde,
     *         oder null, wenn der Client noch nicht gestartet wurde
     */
    public IMessageHandler getConnectionHandlerInstance() {
        return this._connectionHandler;
    }

    /**
     * Startet einen neuen Client, der mit dem im Konstruktor angegebenen Server
     * kommuniziert.
     *
     * @throws IOException
     *             wenn ein Problem beim Verbindungsaufbau auftritt oder wenn
     *             die Klasse, die setConnectionHandler() übergeben wurde, nicht
     *             instanziiert werden kann
     */
    public void startClient() throws IOException {
        /* Nicht mehr starten, wenn Client läuft */
        if (_isRunning())
            throw new IllegalStateException("Client läuft bereits");

        /* Wir brauchen eine Klasse, die Nachrichten verarbeitet */
        if (_connectionHandlerClass == null)
            throw new IllegalArgumentException(
                "Keine Klasse gesetzt, die Nachrichten verarbeitet");

        /*
         * Erstelle die Instanz einer Klasse, die die Verbindungen bearbeitet.
         */
        try {
            if (_hasConnectionHandlerArgument) {
                /* 1 Parameter */
                _connectionHandler = (IMessageHandler) _connectionHandlerClass
                    .getDeclaredConstructor(Object.class)
                    .newInstance(_connectionHandlerObject);
            } else {
                /* Ohne Parameter */
                _connectionHandler = (IMessageHandler) _connectionHandlerClass
                    .newInstance();
            }
        } catch (Exception e) {
            /* Konvertiere zu IOException zur Einfachheit halber */
            throw new IOException(String.format(
                "Instanz der Verbindungen behandelnden Klasse nicht erstellbar: %s",
                e.getLocalizedMessage()));
        }

        /* Erzeuge den Socket-Kanal und schalte auf nicht blockierend um */
        _clientChannel = SocketChannel.open();
        _clientChannel.configureBlocking(false);

        /* Logging */
        _logger.info(String.format("Stelle Verbindung zu %s:%d her",
            _serverAddress.toString(), _serverPort));

        /* Baue eine Verbindung zum Server auf */
        InetSocketAddress packedAddress = new InetSocketAddress(_serverAddress,
            _serverPort);
        boolean connected = _clientChannel.connect(packedAddress);

        /*
         * Erzeuge neuen Client-Messenger mit erstelltem Handler und starte
         * diesen. Kam die Verbindung sofort zustande, muss der Messenger nicht
         * mehr selbst auf die Verbindung warten, ansonsten lass ihn auf sie
         * warten.
         */
        _messenger = new Messenger(_connectionHandler, _clientChannel, _timeout,
            !connected);
        _messenger.start();
    }

    /**
     * Stoppt einen Client, indem sein Thread unterbrochen und anschließend
     * gewartet wird, bis er sich selbst beendet.
     */
    public void stopClient() {
        /* Nicht stoppen, wenn Client nicht läuft */
        if (!_isRunning())
            return;

        /* Logging */
        _logger.info(String.format("Trenne Verbindung zu %s:%d",
            _serverAddress.toString(), _serverPort));

        /*
         * Versuche nicht, erneut den Messenger manuell zu unterbrechen,
         * insbesondere wenn dies von Seiten der Java VM geschah
         */
        if (_messenger.isInterrupted())
            return;

        /* Unterbreche den Thread des Messengers und warte auf sein Beenden */
        _messenger.interrupt();
        try {
            _messenger.join();
        } catch (InterruptedException e) {
            /*
             * Markiere den eigenen Thread als unterbrochen, um Threads, die auf
             * uns warten, zu unterbrechen
             */
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
