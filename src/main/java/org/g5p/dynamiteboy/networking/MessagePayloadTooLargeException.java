/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

/**
 * Diese Ausnahme wird geworfen, wenn ein zu sendender Payload als Teil einer
 * Nachricht seine Maximalgröße überschreitet. getMessage() gibt einen
 * entsprechenden Hinweis zurück.
 *
 * @author Thomas Weber
 */
public final class MessagePayloadTooLargeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Erzeugt eine neue MessagePayloadTooLargeException mit der angegebenen
     * Zeichenkette.
     *
     * @param message
     *            Nachricht, die in der Ausnahme gespeichert wird
     */
    public MessagePayloadTooLargeException(String message) {
        super(message);
    }
}
