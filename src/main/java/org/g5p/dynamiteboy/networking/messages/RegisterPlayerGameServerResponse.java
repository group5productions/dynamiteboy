/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Antwort "Benachrichtigung über
 * Spielerregistrierung", die vom Spieleserver an den Client gesendet wird. Der
 * Antwort geht ein Kommando voraus (vgl. Klasse QueryGameServersResponse).
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class RegisterPlayerGameServerResponse extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "gsRRegisterPlayer";

    @Expose
    private boolean rejected;
    @Expose
    private String adaptedName;

    /**
     * Erzeugt eine neue Nachricht "Benachrichtigung über Spielerregistrierung"
     * durch Kopie einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public RegisterPlayerGameServerResponse(
        RegisterPlayerGameServerResponse message) {
        super(message);
        this.rejected = message.rejected;
        this.adaptedName = message.adaptedName;
    }

    /**
     * Erzeugt eine neue Nachricht "Benachrichtigung über Spielerregistrierung".
     *
     * @param rejected
     *            true wenn Registrierung abgelehnt, ansonsten false
     * @param adaptedName
     *            Name, unter der Spieler auf dem Server identifiziert wird
     */
    public RegisterPlayerGameServerResponse(boolean rejected,
        String adaptedName) {
        super(JSON_COMMAND_NAME);
        this.rejected = rejected;
        this.adaptedName = adaptedName;
    }

    /**
     * Gibt zurück, ob die Registrierung des Spielers erfolgreich gewesen war.
     *
     * @return true bei Erfolg, false bei Misserfolg
     */
    public boolean isRejected() {
        return this.rejected;
    }

    /**
     * Gibt den Namen zurück, unter dem der Spieler auf dem Server zugeordnet
     * ist.
     *
     * @return Name des Spielers
     */
    public String getAdaptedName() {
        return this.adaptedName;
    }
}