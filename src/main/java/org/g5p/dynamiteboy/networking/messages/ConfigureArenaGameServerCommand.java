/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.GameplayMode;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Eine Spielarena für die nächste
 * Runde konfigurieren", die an den Spieleserver gesendet werden kann.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class ConfigureArenaGameServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "gsConfigureArena";

    @Expose
    private int arenaWidth;
    @Expose
    private int arenaHeight;
    @Expose
    private int playTimeMinutes;
    @Expose
    private int armorEffectSeconds;
    @Expose
    private int bombExplosionSeconds;
    @Expose
    private int maxPlayers;
    @Expose
    private GameplayMode gameplayMode;

    /**
     * Erzeugt eine neue Nachricht "Eine Spielarena für die nächste Runde
     * konfigurieren" durch Kopie einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public ConfigureArenaGameServerCommand(
        ConfigureArenaGameServerCommand message) {
        super(message);
        this.arenaWidth = message.arenaWidth;
        this.arenaHeight = message.arenaHeight;
        this.playTimeMinutes = message.playTimeMinutes;
        this.armorEffectSeconds = message.armorEffectSeconds;
        this.bombExplosionSeconds = message.bombExplosionSeconds;
        this.maxPlayers = message.maxPlayers;
        this.gameplayMode = message.gameplayMode;
    }

    /**
     * Erzeugt eine neue Nachricht "Eine Spielarena für die nächste Runde
     * konfigurieren".
     *
     * @param arenaWidth
     *            Breite des Spielfelds
     * @param arenaHeight
     *            Höhe des Spielfelds
     * @param playTimeMinutes
     *            Dauer der Runde in Minuten
     * @param armorEffectSeconds
     *            Dauer des Rüstungseffekts in Sekunden
     * @param bombExplosionSeconds
     *            Countdown zur Bombenexplosion in Sekunden
     * @param maxPlayers
     *            maximale Spielerzahl
     * @param gameplayMode
     *            GameplayMode.STANDARD_A für "Standard A",
     *            GameplayMode.STANDARD_A_B für "Standard A+B"
     */
    public ConfigureArenaGameServerCommand(int arenaWidth, int arenaHeight,
        int playTimeMinutes, int armorEffectSeconds, int bombExplosionSeconds,
        int maxPlayers, GameplayMode gameplayMode) {
        super(JSON_COMMAND_NAME);

        /* Alle Werte speichern und dann die Parameter überprüfen */
        this.arenaWidth = arenaWidth;
        this.arenaHeight = arenaHeight;
        this.playTimeMinutes = playTimeMinutes;
        this.armorEffectSeconds = armorEffectSeconds;
        this.bombExplosionSeconds = bombExplosionSeconds;
        this.maxPlayers = maxPlayers;
        this.gameplayMode = gameplayMode;
        performArgumentCheck();
    }

    /**
     * Diese Methode prüft, ob sämtliche Werte, die diese Nachricht umfasst, in
     * ihren erlaubten Wertebereichen liegen.
     *
     * @throws IllegalArgumentException
     *             wenn mindestens ein Wert von seinem Wertebereich abweicht
     */
    public void performArgumentCheck() throws IllegalArgumentException {
        /* Breite und Höhe zwischen 7 und 21 sowie ungerade */
        if (arenaWidth < 7 || arenaWidth > 21 || (arenaWidth & 1) == 0)
            throw new IllegalArgumentException(
                "Breite muss zwischen 7 und 21 liegen und ungerade sein");

        if (arenaHeight < 7 || arenaHeight > 21 || (arenaHeight & 1) == 0)
            throw new IllegalArgumentException(
                "Höhe muss zwischen 7 und 21 liegen und ungerade sein");

        /* Spielzeit 2 bis 10 Minuten */
        if (playTimeMinutes < 2 || playTimeMinutes > 10)
            throw new IllegalArgumentException(
                "Spielzeit muss zwischen 2 und 10 Minuten betragen");

        /* Rüstungseffekt 5 bis 10 Sekunden */
        if (armorEffectSeconds < 5 || armorEffectSeconds > 10)
            throw new IllegalArgumentException(
                "Rüstungseffekt muss zwischen 5 und 10 Sekunden andauern");

        /* Bombencountdown 2 bis 5 Sekunden */
        if (bombExplosionSeconds < 2 || bombExplosionSeconds > 5)
            throw new IllegalArgumentException(
                "Bombencountdown muss zwischen 2 und 5 Sekunden andauern");

        /* Spielerzahl nicht negativ */
        if (maxPlayers < 0 || maxPlayers < 0)
            throw new IllegalArgumentException(
                "Spielerzahl darf nicht negativ sein");

        /* maximale Spielerzahl 2-4 Spieler */
        if (maxPlayers < 2 || maxPlayers > 4)
            throw new IllegalArgumentException(
                "maximale Spielerzahl muss 2, 3, 4 betragen");

        /* nur zwei Spielmodi zulässig -- wirft eine IllegalArgumentException */
        if (gameplayMode == null)
            throw new IllegalArgumentException(
                "Spielmodus muss \"Standard A\" oder \"Standard A+B\" sein");
    }

    /**
     * Gibt die Breite des Spielfelds zurück.
     *
     * @return Breite des Spielfelds
     */
    public int getArenaWidth() {
        return this.arenaWidth;
    }

    /**
     * Gibt die Höhe des Spielfelds zurück.
     *
     * @return Höhe des Spielfelds
     */
    public int getArenaHeight() {
        return this.arenaHeight;
    }

    /**
     * Gibt die Dauer der Runde in Minuten zurück.
     *
     * @return Dauer der Runde in Minuten
     */
    public int getPlayTimeMinutes() {
        return this.playTimeMinutes;
    }

    /**
     * Gibt die Dauer des Rüstungseffekts in Sekunden zurück.
     *
     * @return Dauer des Rüstungseffekts in Sekunden
     */
    public int getArmorEffectSeconds() {
        return this.armorEffectSeconds;
    }

    /**
     * Gibt den Countdown zur Bombenexplosion in Sekunden zurück.
     *
     * @return Countdown zur Bombenexplosion in Sekunden
     */
    public int getBombExplosionSeconds() {
        return this.bombExplosionSeconds;
    }

    /**
     * Gibt die maximale Spielerzahl zurück.
     *
     * @return maximale Spielerzahl
     */
    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    /**
     * Gibt den eingestellten Spielmodus zurück.
     *
     * @return GameplayMode.STANDARD_A für "Standard A",
     *         GameplayMode.STANDARD_A_B für "Standard A+B"
     */
    public GameplayMode getGameplayMode() {
        return this.gameplayMode;
    }
}