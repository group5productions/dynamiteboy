/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import java.util.LinkedList;
import java.util.List;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.LobbyModePlayerEntry;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Änderung der Spielerliste", die vom
 * Spieleserver an die Clients gesendet wird.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class PlayersUpdatedClientCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "cPlayersUpdated";

    @Expose
    private List<LobbyModePlayerEntry> players;

    /**
     * Erzeugt eine neue Nachricht "Änderung der Spielerliste" durch Kopie einer
     * Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public PlayersUpdatedClientCommand(PlayersUpdatedClientCommand message) {
        super(message);
        this.players = new LinkedList<LobbyModePlayerEntry>(message.players);
    }

    /**
     * Erzeugt eine neue Nachricht "Änderung der Spielerliste" mit einer leeren
     * Spielerliste.
     */
    public PlayersUpdatedClientCommand() {
        super(JSON_COMMAND_NAME);
        this.players = new LinkedList<LobbyModePlayerEntry>();
    }

    /**
     * Gibt die Liste der in der Nachricht stehenden Spieler zurück.
     *
     * @return Liste der Spieler
     */
    public List<LobbyModePlayerEntry> getPlayers() {
        return this.players;
    }

    /**
     * Fügt einen neuen Spieler zur Nachricht hinzu.
     *
     * @param entry
     *            Spieler, der eingetragen werden soll
     */
    public void addPlayerEntry(LobbyModePlayerEntry entry) {
        players.add(entry);
    }
}