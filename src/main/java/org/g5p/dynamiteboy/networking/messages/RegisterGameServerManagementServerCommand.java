/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.g5p.dynamiteboy.networking.Message;
import org.apache.commons.validator.routines.InetAddressValidator;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Spieleserver registrieren", die an
 * den Verwaltungsserver gesendet werden kann.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class RegisterGameServerManagementServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "msRegisterGameServer";

    @Expose
    private String serverName;
    @Expose
    private String serverAddress;
    @Expose
    private int serverPort;

    /**
     * Erzeugt eine neue Nachricht "Spieleserver registrieren" durch Kopie einer
     * Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public RegisterGameServerManagementServerCommand(
        RegisterGameServerManagementServerCommand message) {
        super(message);
        this.serverName = message.serverName;
        this.serverAddress = message.serverAddress;
        this.serverPort = message.serverPort;
    }

    /**
     * Erzeugt eine neue Nachricht "Liste der Spieleserver abfragen".
     *
     * @param name
     *            Name, unter dem der Spieleserver in der Serverliste angezeigt
     *            werden soll
     * @param listenAddress
     *            Adresse (IPv4-Adresse, IPv6-Adresse oder Hostname), unter der
     *            der Spieleserver erreichbar ist
     * @param listenPort
     *            Port, unter der der Spieleserver erreichbar ist
     */
    public RegisterGameServerManagementServerCommand(String name,
        String listenAddress, int listenPort) {
        super(JSON_COMMAND_NAME);

        /* Alle Werte speichern und dann die Parameter überprüfen */
        this.serverName = name.trim();
        this.serverAddress = listenAddress;
        this.serverPort = listenPort;
        performArgumentCheck();
    }

    /**
     * Diese Methode prüft, ob sämtliche Werte, die diese Nachricht umfasst, in
     * ihren erlaubten Wertebereichen liegen.
     *
     * @throws IllegalArgumentException
     *             wenn mindestens ein Wert von seinem Wertebereich abweicht
     */
    public void performArgumentCheck() throws IllegalArgumentException {
        /* Name darf nicht leer sein */
        if (serverName.length() == 0)
            throw new IllegalArgumentException(
                "Name des Servers darf nicht leer sein");

        /* Erlaubte Ports in TCP sind 1 bis 65535. 0 ist reserviert */
        if (serverPort <= 0 || serverPort > 65535)
            throw new IllegalArgumentException(
                "Port muss zwischen 1 und 65535 liegen");

        /* Erlaube gültige IPv4- und IPv6-Adressen */
        InetAddressValidator validator = new InetAddressValidator();
        if (!validator.isValid(serverAddress)) {

            /* Keine IP-Adresse, erlaube nur auflösbare Hostnamen */
            try {
                InetAddress.getByName(serverAddress);
            } catch (UnknownHostException e) {
                throw new IllegalArgumentException(
                    "Host " + serverAddress + " unbekannt");
            }
        }

    }

    /**
     * Gibt den Namen zurück, unter dem der Spieleserver in der Server gelistet
     * werden soll.
     *
     * @return Name des Spieleservers
     */
    public String getServerName() {
        return this.serverName;
    }

    /**
     * Gibt die Adresse zurück, unter der der Spieleserver erreichbar ist.
     *
     * @return IPv4-Adresse, IPv6-Adresse oder Hostname, unter der der
     *         Spieleserver erreichbar ist
     */
    public String getListenAddress() {
        return this.serverAddress;
    }

    /**
     * Gibt den Port zurück, unter der der Spieleserver erreichbar ist.
     *
     * @return Port, unter der der Spieleserver erreichbar ist
     */
    public int getListenPort() {
        return this.serverPort;
    }
}