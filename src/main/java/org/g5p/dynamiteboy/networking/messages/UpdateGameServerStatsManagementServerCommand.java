/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.GameplayMode;
import org.g5p.dynamiteboy.tools.PlayerCount;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Zustand eines Spieleservers
 * aktualisieren", die an den Verwaltungsserver gesendet werden kann.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class UpdateGameServerStatsManagementServerCommand
    extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "msUpdateGameServerStats";

    @Expose
    private PlayerCount players;
    @Expose
    private boolean isLobbyOpen;
    @Expose
    private GameplayMode gameplayMode;

    /**
     * Erzeugt eine neue Nachricht "Zustand eines Spieleservers aktualisieren"
     * durch Kopie einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public UpdateGameServerStatsManagementServerCommand(
        UpdateGameServerStatsManagementServerCommand message) {
        super(message);
        this.players = message.players;
        this.isLobbyOpen = message.isLobbyOpen;
        this.gameplayMode = message.gameplayMode;
    }

    /**
     * Erzeugt eine neue Nachricht "Zustand eines Spieleservers aktualisieren".
     *
     * @param currentPlayerCount
     *            Anzahl der momentan auf dem Server angemeldeten Spieler
     * @param maxPlayerCount
     *            Anzahl der maximal gleichzeig auf dem Server angemeldeten
     *            Spieler
     * @param isLobbyOpen
     *            true, wenn die Lobby offen ist (und Spieler akzeptiert
     *            werden), ansonsten false
     * @param gameplayMode
     *            Aktuell gespielter Spielmodus auf dem Server. Dieser Wert ist
     *            entweder STANDARD_A für "Standard A" oder STANDARD_A_B für
     *            "Standard A+B".
     */
    public UpdateGameServerStatsManagementServerCommand(int currentPlayerCount,
        int maxPlayerCount, boolean isLobbyOpen, GameplayMode gameplayMode) {
        super(JSON_COMMAND_NAME);

        /* Alle Werte speichern und dann die Parameter überprüfen */
        this.players = new PlayerCount(currentPlayerCount, maxPlayerCount);
        this.isLobbyOpen = isLobbyOpen;
        this.gameplayMode = gameplayMode;
        performArgumentCheck();
    }

    /**
     * Diese Methode prüft, ob sämtliche Werte, die diese Nachricht umfasst, in
     * ihren erlaubten Wertebereichen liegen.
     *
     * @throws IllegalArgumentException
     *             wenn mindestens ein Wert von seinem Wertebereich abweicht
     */
    public void performArgumentCheck() throws IllegalArgumentException {
        int currentPlayerCount = players.getCurrentPlayerCount();
        int maxPlayerCount = players.getMaxPlayerCount();

        /* Spielerzahl nicht negativ */
        if (currentPlayerCount < 0 || maxPlayerCount < 0)
            throw new IllegalArgumentException(
                "Spielerzahl darf nicht negativ sein");

        /* maximale Spielerzahl 2-4 Spieler */
        if (maxPlayerCount < 2 || maxPlayerCount > 4)
            throw new IllegalArgumentException(
                "maximale Spielerzahl muss 2, 3, 4 betragen");

        /* nur zwei Spielmodi zulässig */
        if (gameplayMode == null)
            throw new IllegalArgumentException("unbekannter Spielmodus");
    }

    /**
     * Gibt die aktuelle und maximale Spielerzahl des Servers zurück.
     *
     * @return Daten zu Spielerzahlen auf dem Spieleserver
     */
    public PlayerCount getPlayerCounts() {
        return this.players;
    }

    /**
     * Gibt zurück, ob die Lobby des Spieleservers offen ist.
     *
     * @return true wenn die Lobby offen ist, ansonsten false
     */
    public boolean isLobbyOpen() {
        return this.isLobbyOpen;
    }

    /**
     * Gibt den aktuellen Spielmodus zurück, der auf dem Spieleserver
     * eingestellt ist bzw. gespielt wird.
     *
     * @return STANDARD_A für "Standard A" oder STANDARD_A_B für "Standard A+B"
     */
    public GameplayMode getGameplayMode() {
        return this.gameplayMode;
    }
}