/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;

/**
 * Diese Klasse repräsentiert die Nachricht "Herzschlag".
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class HeartbeatCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "xxHeartbeat";

    /**
     * Erzeugt eine neue Nachricht "Herzschlag" durch Kopie einer Nachricht der
     * gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public HeartbeatCommand(HeartbeatCommand message) {
        super(message);
    }

    /**
     * Erzeugt eine neue Nachricht "Herzschlag".
     */
    public HeartbeatCommand() {
        super(JSON_COMMAND_NAME);
    }
}