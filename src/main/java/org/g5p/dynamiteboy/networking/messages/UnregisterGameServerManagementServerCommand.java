/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;

/**
 * Diese Klasse repräsentiert die Nachricht "Spieleserver deregistrieren", die
 * an den Verwaltungsserver gesendet werden kann.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class UnregisterGameServerManagementServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "msUnregisterGameServer";

    /**
     * Erzeugt eine neue Nachricht "Spieleserver deregistrieren" durch Kopie
     * einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public UnregisterGameServerManagementServerCommand(
        UnregisterGameServerManagementServerCommand message) {
        super(message);
    }

    /**
     * Erzeugt eine neue Nachricht "Spieleserver deregistrieren".
     */
    public UnregisterGameServerManagementServerCommand() {
        super(JSON_COMMAND_NAME);
    }
}