/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import java.util.LinkedList;
import java.util.List;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.GameServerEntry;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Antwort "Liste registierter Spieleserver", die
 * vom Verwaltungsserver an den Client gesendet wird. Der Antwort geht ein
 * Kommando voraus (vgl. Klasse QueryGameServersManagementServerCommand).
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class QueryGameServersManagementServerResponse extends Message {
    public static final String JSON_COMMAND_NAME = "msRQueryGameServers";

    @Expose
    private List<GameServerEntry> servers;

    /**
     * Erzeugt eine neue Nachricht "Liste registierter Spieleserver" durch Kopie
     * einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public QueryGameServersManagementServerResponse(
        QueryGameServersManagementServerResponse message) {
        super(message);
        this.servers = new LinkedList<GameServerEntry>(message.servers);
    }

    /**
     * Erzeugt eine neue Nachricht "Liste registierter Spieleserver" ohne
     * eingetragene Spieleserver.
     */
    public QueryGameServersManagementServerResponse() {
        super(JSON_COMMAND_NAME);
        this.servers = new LinkedList<GameServerEntry>();
    }

    /**
     * Gibt die Liste der in der Nachricht gespeicherten Server zurück.
     *
     * @return Liste der gespeicherten Spieleserver
     */
    public List<GameServerEntry> getServers() {
        return servers;
    }

    /**
     * Fügt einen neuen Spieleserver zur Nachricht hinzu.
     *
     * @param entry
     *            Spieleserver, der hinzugefügt werden soll
     */
    public void addServerEntry(GameServerEntry entry) {
        servers.add(entry);
    }
}