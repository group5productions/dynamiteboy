/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;

/**
 * Diese Klasse repräsentiert die Nachricht "Liste der Spieleserver abfragen",
 * die an den Verwaltungsserver gesendet werden kann. Der Server sendet eine
 * Antwort (vgl. Klasse QueryGameServersResponse).
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class QueryGameServersManagementServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "msQueryGameServers";

    /**
     * Erzeugt eine neue Nachricht "Liste der Spieleserver abfragen" durch Kopie
     * einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public QueryGameServersManagementServerCommand(
        QueryGameServersManagementServerCommand message) {
        super(message);
    }

    /**
     * Erzeugt eine neue Nachricht "Liste der Spieleserver abfragen".
     */
    public QueryGameServersManagementServerCommand() {
        super(JSON_COMMAND_NAME);
    }
}