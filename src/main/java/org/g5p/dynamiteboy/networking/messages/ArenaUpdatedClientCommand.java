/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import java.util.LinkedList;
import java.util.List;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.GameModeFieldEntry;
import org.g5p.dynamiteboy.tools.GameModePlayerEntry;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Die Spielarena und/oder die
 * Spielerliste ändert sich in der Runde", die vom Spieleserver an die Clients
 * gesendet wird.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class ArenaUpdatedClientCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "cArenaUpdated";

    @Expose
    private List<GameModePlayerEntry> players;
    @Expose
    private List<GameModeFieldEntry> updatedFields;

    /**
     * Erzeugt eine neue Nachricht "Die Spielarena und/oder die Spielerliste
     * ändert sich in der Runde" durch Kopie einer Nachricht der gleichen
     * Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public ArenaUpdatedClientCommand(ArenaUpdatedClientCommand message) {
        super(message);
        this.players = new LinkedList<GameModePlayerEntry>(message.players);
        this.updatedFields = new LinkedList<GameModeFieldEntry>(
            message.updatedFields);
    }

    /**
     * Erzeugt eine neue Nachricht "Die Spielarena und/oder die Spielerliste
     * ändert sich in der Runde" mit einer leeren Spielerliste.
     */
    public ArenaUpdatedClientCommand() {
        super(JSON_COMMAND_NAME);
        this.players = new LinkedList<GameModePlayerEntry>();
        this.updatedFields = new LinkedList<GameModeFieldEntry>();
    }

    /**
     * Gibt die Liste der in der Nachricht stehenden Spieler zurück.
     *
     * @return Liste der Spieler
     */
    public List<GameModePlayerEntry> getPlayers() {
        return this.players;
    }

    /**
     * Fügt einen neuen Spieler zur Nachricht hinzu.
     *
     * @param entry
     *            Spieler, der eingetragen werden soll
     */
    public void addPlayerEntry(GameModePlayerEntry entry) {
        players.add(entry);
    }

    /**
     * Gibt die Liste der in der Nachricht stehenden Felder zurück.
     *
     * @return Liste der Spieler
     */
    public List<GameModeFieldEntry> getUpdatedFields() {
        return this.updatedFields;
    }

    /**
     * Fügt einen neuen Spieler zur Nachricht hinzu.
     *
     * @param entry
     *            Spieler, der eingetragen werden soll
     */
    public void addFieldEntry(GameModeFieldEntry entry) {
        updatedFields.add(entry);
    }
}