/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.PlayerAction;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Mit der Spielfigur in der Runde
 * interagieren", die an den Spieleserver gesendet werden kann.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class PlayerActionGameServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "gsPlayerAction";

    @Expose
    private PlayerAction action;

    /**
     * Erzeugt eine neue Nachricht "Mit der Spielfigur in der Runde
     * interagieren" durch Kopie einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public PlayerActionGameServerCommand(
        PlayerActionGameServerCommand message) {
        super(message);
        this.action = message.action;
    }

    /**
     * Erzeugt eine neue Nachricht "Mit der Spielfigur in der Runde
     * interagieren".
     *
     * @param action
     *            Aktion, die der Spieler durchführen möchte
     */
    public PlayerActionGameServerCommand(PlayerAction action) {
        super(JSON_COMMAND_NAME);
        this.action = action;
    }

    /**
     * Gibt die Aktion zurück, Aktion, die der Spieler durchführen möchte.
     *
     * @return Aktion, die der Spieler durchführen möchte
     */
    public PlayerAction getAction() {
        return this.action;
    }
}