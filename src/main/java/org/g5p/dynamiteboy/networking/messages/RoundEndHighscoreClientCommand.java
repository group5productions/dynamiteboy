/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import java.util.LinkedList;
import java.util.List;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.tools.HighscorePlayerEntry;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Einen Highscore übermitteln", die
 * vom Spieleserver an die Clients gesendet wird.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class RoundEndHighscoreClientCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "cRoundEndHighscore";

    @Expose
    private List<HighscorePlayerEntry> players;

    /**
     * Erzeugt eine neue Nachricht "Änderung der Spielerliste" durch Kopie einer
     * Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public RoundEndHighscoreClientCommand(
        RoundEndHighscoreClientCommand message) {
        super(message);
        this.players = new LinkedList<HighscorePlayerEntry>(message.players);
    }

    /**
     * Erzeugt eine neue Nachricht "Änderung der Spielerliste" mit einer leeren
     * Spielerliste.
     */
    public RoundEndHighscoreClientCommand() {
        super(JSON_COMMAND_NAME);
        this.players = new LinkedList<HighscorePlayerEntry>();
    }

    /**
     * Gibt die Liste der in der Nachricht stehenden Spieler zurück.
     *
     * @return Liste der Spieler
     */
    public List<HighscorePlayerEntry> getPlayers() {
        return this.players;
    }

    public void addPlayerEntry(HighscorePlayerEntry entry) {
        players.add(entry);
    }
}