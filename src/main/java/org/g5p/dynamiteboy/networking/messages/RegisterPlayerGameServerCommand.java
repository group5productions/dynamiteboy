/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking.messages;

import org.g5p.dynamiteboy.networking.Message;

import com.google.gson.annotations.Expose;

/**
 * Diese Klasse repräsentiert die Nachricht "Einen Spieler registrieren", die an
 * den Spieleserver gesendet werden kann. Der Server sendet eine Antwort (vgl.
 * Klasse RegisterPlayerResponse).
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public final class RegisterPlayerGameServerCommand extends Message {
    /* Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
    public static final String JSON_COMMAND_NAME = "gsRegisterPlayer";

    @Expose
    private String name;

    /**
     * Erzeugt eine neue Nachricht "Einen Spieler registrieren" durch Kopie
     * einer Nachricht der gleichen Klasse.
     *
     * @param message
     *            Nachricht, die kopiert werden soll
     */
    public RegisterPlayerGameServerCommand(
        RegisterPlayerGameServerCommand message) {
        super(message);
        this.name = message.name;
    }

    /**
     * Erzeugt eine neue Nachricht "Einen Spieler registrieren".
     *
     * @param name
     *            Name, unter dem sich der Spieler identifizieren möchte
     */
    public RegisterPlayerGameServerCommand(String name) {
        super(JSON_COMMAND_NAME);

        /* Alle Werte speichern und dann die Parameter überprüfen */
        this.name = name;
        performArgumentCheck();
    }

    /**
     * Diese Methode prüft, ob sämtliche Werte, die diese Nachricht umfasst, in
     * ihren erlaubten Wertebereichen liegen.
     *
     * @throws IllegalArgumentException
     *             wenn mindestens ein Wert von seinem Wertebereich abweicht
     */
    public void performArgumentCheck() throws IllegalArgumentException {
        /* Erlaube Groß- und Kleinbuchstaben sowie Ziffern */
        if (!name.matches("[A-Za-z0-9]+"))
            throw new IllegalArgumentException(
                "Name enthält ungültige Zeichen");
    }

    /**
     * Gibt den Namen zurück, unter dem sich der Spieler identifizieren möchte.
     *
     * @return Wunschname des Spielers
     */
    public String getName() {
        return this.name;
    }
}