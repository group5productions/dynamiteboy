/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.net.SocketException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import org.g5p.dynamiteboy.networking.messages.QueryGameServersManagementServerCommand;
import org.g5p.dynamiteboy.networking.messages.QueryGameServersManagementServerResponse;
import org.g5p.dynamiteboy.networking.messages.RegisterGameServerManagementServerCommand;
import org.g5p.dynamiteboy.networking.messages.UnregisterGameServerManagementServerCommand;
import org.g5p.dynamiteboy.networking.messages.UpdateGameServerStatsManagementServerCommand;
import org.g5p.dynamiteboy.tools.GameplayMode;

/**
 * Diese Klasse ermöglich den Empfang und das Senden von Nachrichten vom bzw. an
 * den Verwaltungsserver als dessen Client.
 *
 * @author Thomas Weber
 */
public class ManagementClientHandler implements IMessageHandler {
    private Messenger _connection;

    private volatile boolean _communicationError = false;
    private Exception _communicationException = null;
    private volatile boolean _readyForCommunication = false;

    private Runnable _connectionTerminationCallback = null;

    private volatile boolean _gameServerListReceived = false;
    private Consumer<QueryGameServersManagementServerResponse> _gameServerListCallback = null;

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zum
     * Verwaltungsserver hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zum Verwaltungsserver
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Speichere die Verbindung */
        this._connection = connection;

        /* Signalisiere eventuell wartende Threads, dass die Verbindung steht */
        synchronized (this) {
            _readyForCommunication = true;
            notify();
        }

        /* Verbindung annehmen */
        return true;
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Verwaltungsserver eine Nachricht
     * an den Messenger schickt.
     *
     * @param clientCommand
     *            Nachricht, die der Verwaltungsserver gesendet hat
     */
    @Override
    public void onCommandReceived(Message clientCommand) {
        /*
         * Der Verwaltungsserver sendet nur Listen von Spieleservern an Clients.
         */
        if (clientCommand instanceof QueryGameServersManagementServerResponse) {
            QueryGameServersManagementServerResponse response = (QueryGameServersManagementServerResponse) clientCommand;

            synchronized (this) {
                _gameServerListReceived = true;
                notify();
            }
            if (_gameServerListCallback != null)
                _gameServerListCallback.accept(response);
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit dem Verwaltungsserver erfolgt ist.
     *
     * @return Stets false
     */
    @Override
    public boolean onConnectionTimeout() {
        /* Timeout tritt nicht ein */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    @Override
    public void onConnectionError(Exception e) {
        /*
         * Markiere Verbindungsfehler und signalisiere eventuell wartende
         * Threads
         */
        _communicationException = e;
        synchronized (this) {
            _communicationError = true;
            notify();
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation mit dem
     * Verwaltungsserver beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param closedByUs
     *            true, wenn die Trennung von unserer Seite ausgeht, sonst
     *            false, wenn die Trennung von der Gegenstelle ausgeht
     */
    @Override
    public void onConnectionTerminated(boolean closedByUs) {
        /* Benachrichtige Trennung der Verbindung */
        if (_connectionTerminationCallback != null)
            _connectionTerminationCallback.run();
    }

    /**
     * Gibt eine eventuell existierende Ausnahme zurück, die während der
     * Verbindung geworfen wurde.
     *
     * @return Ausnahme während der Verbindung oder null
     */
    public Exception getCommunicationException() {
        return _communicationException;
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn die Verbindung
     * zum Verwaltungsserver terminiert wird.
     *
     * @param callback
     *            Funktion, die bei Verbindungsterminierung ausgeführt werden
     *            soll
     */
    public void setConnectionTerminationCallback(Runnable callback) {
        _connectionTerminationCallback = callback;
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn der
     * Verwaltungsserver eine Liste von Spieleservern an den Client sendet.
     *
     * @param callback
     *            Funktion, die ausgeführt werden soll, wenn eine Liste von
     *            Spieleservern eintrifft
     */
    public void setGameServerListCallback(
        Consumer<QueryGameServersManagementServerResponse> callback) {
        _gameServerListCallback = callback;
    }

    /**
     * Diese Methode wartet, bis sowohl der Verwaltungsserver als auch der mit
     * ihm verbundene Client miteinander kommunizieren können, aber nicht länger
     * als die angegebene Wartezeit.
     *
     * @param timeout
     *            Dauer in Millisekunden, die gewartet werden soll
     * @throws TimeoutException
     *             wenn die Wartezeit abgelaufen ist und noch immer keine
     *             Kommunikation erfolgen kann
     * @throws Exception
     *             wenn es während des Verbindungsversuchs ein kritisches
     *             Problem gab
     */
    public synchronized void waitUntilReadyOrTimeout(long timeout)
        throws TimeoutException, Exception {
        /*
         * Es ist nicht möglich, mit eingebauten Java-Hilfsmitteln festzustellen
         * ob wait() durch ein "spurious wakeup" oder durch Timeout zurück
         * gekehrt ist. Tracke daher die bisland abgelaufene Zeit. Wurde wait()
         * in dieser Zeit unterbrochen, ohne dass die Variablen der Schleife
         * gesetzt wurden, wird beim nächsten Aufruf von wait() die Wartezeit um
         * die bisher abgelaufene Zeit gekürzt, damit die Wartezeit nahezu stats
         * bei {@literal timeout} bleibt.
         */
        long waitStart = System.nanoTime();
        while (!_communicationError && !_readyForCommunication) {
            wait(timeout);

            long waitEnd = System.nanoTime();
            if ((waitEnd - waitStart) >= timeout * 1000000)
                throw new TimeoutException(
                    "Zeitüberschreitung, während Verbindung zum "
                        + "Verwaltungsserver hergestellt wird");
            else
                timeout -= (waitEnd - waitStart) / 1000000;
        }

        /* Werfe die Ausnahme, die im Client entstanden ist, weiter */
        if (_communicationError)
            throw _communicationException;
    }

    /**
     * Diese Methode wartet, bis der Verwaltungsserver eine Serverliste an den
     * Client gesendet hat, aber nicht länger als die angegebene Wartezeit.
     *
     * @param timeout
     *            Dauer in Millisekunden, die gewartet werden soll
     * @throws TimeoutException
     *             wenn die Wartezeit abgelaufen ist und noch immer keine
     *             Kommunikation erfolgen kann
     * @throws Exception
     *             wenn es während des Verbindungsversuchs ein kritisches
     *             Problem gab
     */
    public synchronized void waitUntilGameServerListReceived(long timeout)
        throws TimeoutException, Exception {
        /*
         * Es ist nicht möglich, mit eingebauten Java-Hilfsmitteln festzustellen
         * ob wait() durch ein "spurious wakeup" oder durch Timeout zurück
         * gekehrt ist. Tracke daher die bisland abgelaufene Zeit. Wurde wait()
         * in dieser Zeit unterbrochen, ohne dass die Variablen der Schleife
         * gesetzt wurden, wird beim nächsten Aufruf von wait() die Wartezeit um
         * die bisher abgelaufene Zeit gekürzt, damit die Wartezeit nahezu stats
         * bei {@literal timeout} bleibt.
         */
        long waitStart = System.nanoTime();
        while (!_communicationError && !_gameServerListReceived) {
            wait(timeout);

            long waitEnd = System.nanoTime();
            if ((waitEnd - waitStart) >= timeout * 1000000)
                throw new TimeoutException(
                    "Zeitüberschreitung, während auf Spieleserver gewartet "
                        + "wurde");
            else
                timeout -= (waitEnd - waitStart) / 1000000;
        }

        /* Werfe die Ausnahme, die im Client entstanden ist, weiter */
        _gameServerListReceived = false;
        if (_communicationError)
            throw _communicationException;
    }

    /**
     * Diese Methode fragt den Verwaltungsserver nach verfügbaren Spieleservern.
     *
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void queryGameServers()
        throws MessageMalformedException, SocketException {
        _connection.sendMessage(new QueryGameServersManagementServerCommand());
    }

    /**
     * Diese Methode registriert einen neuen Spieleserver beim
     * Verwaltungsserver.
     *
     * @param name
     *            Name des Spieleservers
     * @param listenAddress
     *            Adresse, unter der der Spieleserver erreichbar ist
     * @param listenPort
     *            Port auf der Adresse, unter der der Spieleserver erreichbar
     *            ist
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void registerGameServer(String name, String listenAddress,
        int listenPort) throws MessageMalformedException, SocketException {
        _connection.sendMessage(new RegisterGameServerManagementServerCommand(
            name, listenAddress, listenPort));
    }

    /**
     * Diese Methode meldet einen Spieleserver beim Verwaltungsserver ab.
     *
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void unregisterGameServer()
        throws MessageMalformedException, SocketException {
        _connection
            .sendMessage(new UnregisterGameServerManagementServerCommand());
    }

    /**
     * Diese Methode aktualisiert die Statistiken des eigenen Spieleservers auf
     * dem Verwaltungsserver.
     *
     * @param currentPlayerCount
     *            Anzahl verbundener Spieler
     * @param maxPlayerCount
     *            Anzahl Spieler, die in der nächsten Runde mitspielen dürfen
     * @param isLobbyOpen
     *            true, wenn die Lobby offen ist, sonst false
     * @param gameplayMode
     *            Aktueller Spielmodus (nur während des Spiels relevant).
     *            STANDARD_A für "Standard A", STANDARD_A_B für "Standard A+B"
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void updateGameServerStats(int currentPlayerCount,
        int maxPlayerCount, boolean isLobbyOpen, GameplayMode gameplayMode)
        throws MessageMalformedException, SocketException {
        _connection.sendMessage(
            new UpdateGameServerStatsManagementServerCommand(currentPlayerCount,
                maxPlayerCount, isLobbyOpen, gameplayMode));
    }
}
