/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.util.ArrayList;
import java.util.List;

import org.g5p.dynamiteboy.networking.messages.*;

/**
 * Diese Klasse sammelt alle Klassen, die in Form von Nachrichten versendet bzw.
 * empfangen werden können, somit technisch von Message abgeleitet sind. Sie
 * wird genutzt, um diese Klassen iterieren zu können, damit beim Empfang von
 * Nachrichten eine Instanz der zugehörigen Klasse erzeugt werden kann.
 *
 * @author Thomas Weber
 */
public final class AllMessages {
    private static List<Class<? extends Message>> _list;

    /**
     * Statischer Initialisierer, der die Liste mit allen Klassen füllt, die von
     * Message abgeleitet wurden.
     */
    static {
        _list = new ArrayList<Class<? extends Message>>();
        _list.add(HeartbeatCommand.class);

        /* Verwaltungsserver */
        _list.add(QueryGameServersManagementServerCommand.class);
        _list.add(QueryGameServersManagementServerResponse.class);
        _list.add(RegisterGameServerManagementServerCommand.class);
        _list.add(UnregisterGameServerManagementServerCommand.class);
        _list.add(UpdateGameServerStatsManagementServerCommand.class);

        /* Spieleserver */
        _list.add(RegisterPlayerGameServerCommand.class);
        _list.add(RegisterPlayerGameServerResponse.class);
        _list.add(ConfigureArenaGameServerCommand.class);
        _list.add(PlayersUpdatedClientCommand.class);
        _list.add(RoundEndHighscoreClientCommand.class);
        _list.add(ArenaUpdatedClientCommand.class);
        _list.add(PlayerActionGameServerCommand.class);
    }

    /**
     * Gibt die Liste der Klassen zurück, die von Message abgeleitet wurden.
     *
     * @return Liste der Klassen, die von Message abgeleitet wurden
     */
    public static List<Class<? extends Message>> getList() {
        return _list;
    }
}
