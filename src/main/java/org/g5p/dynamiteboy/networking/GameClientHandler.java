/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.HeartbeatCommand;
import org.g5p.dynamiteboy.networking.messages.PlayerActionGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.PlayersUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.RegisterPlayerGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.RegisterPlayerGameServerResponse;
import org.g5p.dynamiteboy.networking.messages.RoundEndHighscoreClientCommand;

/**
 * Diese Klasse ermöglich den Empfang und das Senden von Nachrichten vom bzw. an
 * den Spieleserver als dessen Client.
 *
 * @author Thomas Weber
 */
public class GameClientHandler implements IMessageHandler {
    private Messenger _connection;

    /**
     * Interne Ereignisse, auf die gewartet werden kann.
     */
    private enum InternalEvent {
        /**
         * Auf einen Fehler in der Kommunikation warten.
         */
        COMMUNICATION_ERROR,

        /**
         * Auf erfolgreicher Herstellung der Verbindung warten.
         */
        CONNECTION_ESTABLISHED,

        /**
         * Auf Empfang der Antwort einer Spielerregistrierung warten.
         */
        PLAYER_REGISTRATION_RECEIVED
    }

    private volatile boolean _communicationError = false;
    private Exception _communicationException = null;
    private volatile boolean _readyForCommunication = false;
    private volatile boolean _playerRegistrationResponse = false;

    private String _capturedPlayerName = null;

    private Timer _heartbeatTimer = null;

    private Consumer<Boolean> _connectionTerminationCallback = null;
    private Consumer<RegisterPlayerGameServerResponse> _playerRegistrationReceivedCallback = null;
    private Consumer<PlayersUpdatedClientCommand> _playersUpdatedInLobbyCallback = null;
    private Consumer<ArenaUpdatedClientCommand> _arenaUpdatedCallback = null;
    private Consumer<RoundEndHighscoreClientCommand> _highscoreReceivedCallback = null;

    /**
     * Diese Methode wartet, bis mindestens 1 internes Ereignis eingetreten
     * sind, aber nicht länger als die angegebene Wartezeit.
     *
     * @param timeout
     *            Dauer in Millisekunden, die gewartet werden soll
     * @param events
     *            Ereignisse, auf die gewartet werden soll
     * @throws TimeoutException
     *             wenn die Wartezeit abgelaufen ist und noch immer keine
     *             Kommunikation erfolgen kann
     * @throws Exception
     *             wenn es während des Verbindungsversuchs ein kritisches
     *             Problem gab
     */
    private synchronized void _waitForEvents(long timeout,
        InternalEvent... events) throws TimeoutException, Exception {
        /*
         * Es ist nicht möglich, mit eingebauten Java-Hilfsmitteln festzustellen
         * ob wait() durch ein "spurious wakeup" oder durch Timeout zurück
         * gekehrt ist. Tracke daher die bisland abgelaufene Zeit. Wurde wait()
         * in dieser Zeit unterbrochen, ohne dass die Variablen der Schleife
         * gesetzt wurden, wird beim nächsten Aufruf von wait() die Wartezeit um
         * die bisher abgelaufene Zeit gekürzt, damit die Wartezeit nahezu stats
         * bei {@literal timeout} bleibt.
         */
        boolean waitsForCommError = false;
        long waitStart = System.nanoTime();
        while (true) {
            boolean eventReceived = false;

            /* Überprüfe Ereignisse */
            for (int i = 0; i < events.length; ++i) {
                switch (events[i]) {
                case COMMUNICATION_ERROR:
                    waitsForCommError = true;
                    eventReceived |= _communicationError;
                    break;
                case CONNECTION_ESTABLISHED:
                    eventReceived |= _readyForCommunication;
                    break;
                case PLAYER_REGISTRATION_RECEIVED:
                    eventReceived |= _playerRegistrationResponse;
                    break;
                default:
                    break;
                }
            }
            if (eventReceived)
                break;

            /* Warte auf irgendein Signal aus der Klasse heraus */
            wait(timeout);

            /*
             * Wie lange haben wir gewartet? Bei Zeitüberschreitung wird
             * abgebrochen. Ansonsten passe den Timeout an, überprüfe erneut die
             * Ereignisse, und warte erneut.
             */
            long waitEnd = System.nanoTime();
            if ((waitEnd - waitStart) >= timeout * 1000000)
                throw new TimeoutException(
                    "Zeitüberschreitung, während auf Operation des Servers gewartet wurde");
            else
                timeout -= (waitEnd - waitStart) / 1000000;
        }

        /*
         * Werfe stets Ausnahmen, die aus einem Verbindungsfehler resultieren,
         * weiter, wenn danach gefragt wurde
         */
        if (waitsForCommError && _communicationError)
            throw _communicationException;
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zum Spieleserver
     * hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zum Spieleserver
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Speichere die Verbindung */
        this._connection = connection;

        /* Starte den Timer für regelmäßig gesendete Heartbeats */
        _heartbeatTimer = new Timer();
        _heartbeatTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                /* Sende bei jedem Aufruf einen Heartbeat an den Server */
                try {
                    _connection.sendMessage(new HeartbeatCommand());
                } catch (SocketException e) {
                }
            }
        }, Globals.HEARTBEAT_INTERVAL_MS, Globals.HEARTBEAT_INTERVAL_MS);

        /* Signalisiere eventuell wartende Threads, dass die Verbindung steht */
        synchronized (this) {
            _readyForCommunication = true;
            notify();
        }

        /* Verbindung annehmen */
        return true;
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Spieleserver eine Nachricht an
     * den Messenger schickt.
     *
     * @param clientCommand
     *            Nachricht, die der Verwaltungsserver gesendet hat
     */
    @Override
    public void onCommandReceived(Message clientCommand) {
        /* Ignoriere Heartbeats */
        if (clientCommand instanceof HeartbeatCommand) {
        }

        /* Antwort auf Spielerregistrierung */
        else if (clientCommand instanceof RegisterPlayerGameServerResponse) {
            RegisterPlayerGameServerResponse response = (RegisterPlayerGameServerResponse) clientCommand;

            /* Informiere erst das Callback */
            if (_playerRegistrationReceivedCallback != null)
                _playerRegistrationReceivedCallback.accept(response);

            /* Dann ist _waitForEvents() dran */
            synchronized (this) {
                _playerRegistrationResponse = true;
                notify();
            }
        }

        /* Änderung der Liste zugelassener Spieler */
        else if (clientCommand instanceof PlayersUpdatedClientCommand) {
            PlayersUpdatedClientCommand command = (PlayersUpdatedClientCommand) clientCommand;

            /* Informiere bloß das Callback */
            if (_playersUpdatedInLobbyCallback != null)
                _playersUpdatedInLobbyCallback.accept(command);
        }

        /* Änderung der Spielarena */
        else if (clientCommand instanceof ArenaUpdatedClientCommand) {
            ArenaUpdatedClientCommand command = (ArenaUpdatedClientCommand) clientCommand;

            /* Informiere bloß das Callback */
            if (_arenaUpdatedCallback != null)
                _arenaUpdatedCallback.accept(command);
        }

        /* Übermittlung des Highscores (Spielende) */
        else if (clientCommand instanceof RoundEndHighscoreClientCommand) {
            RoundEndHighscoreClientCommand command = (RoundEndHighscoreClientCommand) clientCommand;

            /* Informiere bloß das Callback */
            if (_highscoreReceivedCallback != null)
                _highscoreReceivedCallback.accept(command);
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit dem Spieleserver erfolgt ist.
     *
     * @return Stets false
     */
    @Override
    public boolean onConnectionTimeout() {
        /* Stoppe den Timer für Heartbeats so früh wie möglich */
        _heartbeatTimer.cancel();

        /*
         * Markiere Timeout als Verbindungsfehler und signalisiere eventuell
         * wartende Threads
         */
        onConnectionError(new TimeoutException(
            "Zeitüberschreitung während der Kommunikation mit dem Spieleserver"));

        /* Trenne die Verbindung */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    @Override
    public void onConnectionError(Exception e) {
        /* Stoppe den Timer für Heartbeats so früh wie möglich */
        _heartbeatTimer.cancel();

        /*
         * Markiere Verbindungsfehler und signalisiere eventuell wartende
         * Threads
         */
        _communicationException = e;
        synchronized (this) {
            _communicationError = true;
            notify();
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation mit dem
     * Spieleserver beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param closedByUs
     *            true, wenn die Trennung von unserer Seite ausgeht, sonst
     *            false, wenn die Trennung von der Gegenstelle ausgeht
     */
    @Override
    public void onConnectionTerminated(boolean closedByUs) {
        /*
         * Stoppe den Timer für Heartbeats -- wenn er bereits gestoppt wurde,
         * gibt es keine Konsequenzen.
         */
        _heartbeatTimer.cancel();

        /* Benachrichtige Trennung der Verbindung */
        if (_connectionTerminationCallback != null)
            _connectionTerminationCallback.accept(closedByUs);
    }

    /**
     * Gibt eine eventuell existierende Ausnahme zurück, die während der
     * Verbindung geworfen wurde.
     *
     * @return Ausnahme während der Verbindung oder null
     */
    public Exception getCommunicationException() {
        return _communicationException;
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn die Verbindung
     * zum Spieleserver terminiert wird.
     *
     * @param callback
     *            Funktion, die bei Verbindungsterminierung ausgeführt werden
     *            soll
     */
    public void setConnectionTerminationCallback(Consumer<Boolean> callback) {
        _connectionTerminationCallback = callback;
    }

    /**
     * Diese Methode wartet, bis sowohl der Spieleserver als auch der mit ihm
     * verbundene Client miteinander kommunizieren können, aber nicht länger als
     * die angegebene Wartezeit.
     *
     * @param timeout
     *            Dauer in Millisekunden, die gewartet werden soll
     * @throws TimeoutException
     *             wenn die Wartezeit abgelaufen ist und noch immer keine
     *             Kommunikation erfolgen kann
     * @throws Exception
     *             wenn es während des Verbindungsversuchs ein kritisches
     *             Problem gab
     */
    public synchronized void waitUntilReadyOrTimeout(long timeout)
        throws TimeoutException, Exception {
        /* Wrapper um _waitForEvents() */
        _waitForEvents(timeout, InternalEvent.COMMUNICATION_ERROR,
            InternalEvent.CONNECTION_ESTABLISHED);
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn der
     * Spieleserver eine Antwort auf eine Spielerregistrierung sendet.
     *
     * @param callback
     *            Funktion, die ausgeführt werden soll, wenn eine Antwort auf
     *            eine Spielerregistrierung eintrifft
     */
    public void setPlayerRegistrationReceivedCallback(
        Consumer<RegisterPlayerGameServerResponse> callback) {
        _playerRegistrationReceivedCallback = callback;
    }

    /**
     * Diese Methode registriert einen neuen Spieler beim Spieleserver.
     *
     * @param name
     *            Name des Spielers
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void registerPlayer(String name)
        throws MessageMalformedException, SocketException {

        /* Fange Spielernamen ab */
        if (name != null)
            _capturedPlayerName = name;

        /* Sende zugehörige Nachricht */
        _playerRegistrationResponse = false;
        _connection.sendMessage(new RegisterPlayerGameServerCommand(name));
    }

    /**
     * Gibt den Namen des Spielers zurück, den sich der Handler aus der
     * Registrierung des Spielers gemerkt hat.
     *
     * @return Name des Spielers aus der Registrierung
     */
    public String getCapturedPlayerName() {
        return _capturedPlayerName;
    }

    /**
     * Diese Methode wartet, bis sowohl der Spieleserver einen Status über die
     * Spielerregistrierung versendet hat, aber nicht länger als die angegebene
     * Wartezeit.
     *
     * @param timeout
     *            Dauer in Millisekunden, die gewartet werden soll
     * @throws TimeoutException
     *             wenn die Wartezeit abgelaufen ist und noch immer keine
     *             Kommunikation erfolgen kann
     * @throws Exception
     *             wenn es während des Verbindungsversuchs ein kritisches
     *             Problem gab
     */
    public void waitForPlayerRegistration(long timeout)
        throws TimeoutException, Exception {

        /* Benutze interne Wartefunktion */
        _waitForEvents(timeout, InternalEvent.COMMUNICATION_ERROR,
            InternalEvent.PLAYER_REGISTRATION_RECEIVED);
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn der
     * Spieleserver eine Aktualisierung der Spielerliste sendet, während sich
     * dieser im Lobbymodus befindet (im Spielemodus sind die mit Updates der
     * Arena vereinigt).
     *
     * @param callback
     *            Funktion, die ausgeführt werden soll, wenn eine Aktualisierung
     *            der Spielerliste eintrifft
     */
    public void setPlayersUpdatedInLobbyCallback(
        Consumer<PlayersUpdatedClientCommand> callback) {
        _playersUpdatedInLobbyCallback = callback;
    }

    /**
     * Diese Methode konfiguriert die Arena des Spielserver für die nächste
     * Runde. Enthält die Konfiguration keine weiteren Fehler, so startet der
     * Spieleserver das Spiel automatisch.
     *
     * @param command
     *            Kommando, welches die Konfiguration der Runde enthält.
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void configureArena(ConfigureArenaGameServerCommand command)
        throws MessageMalformedException, SocketException {
        _connection.sendMessage(command);
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn der
     * Spieleserver ein Update der Arena sendet, während sich dieser im
     * Spielmodus befindet.
     *
     * @param callback
     *            Funktion, die ausgeführt werden soll, wenn ein Update der
     *            Arena eintrifft
     * @return Callback, der vorher gesetzt war
     */
    public Consumer<ArenaUpdatedClientCommand> setArenaUpdatedCallback(
        Consumer<ArenaUpdatedClientCommand> callback) {
        Consumer<ArenaUpdatedClientCommand> oldCallback = _arenaUpdatedCallback;
        _arenaUpdatedCallback = callback;
        return oldCallback;
    }

    /**
     * Setzt eine Funktion, welches ausgeführt werden soll, wenn der
     * Spieleserver zum Ende der Runde bzw. bei gleichzeitigem Verlassen des
     * Spielemodus den Highscore an alle Spieler übermittelt.
     *
     * @param callback
     *            Funktion, die ausgeführt werden soll, wenn der Highscore
     *            empfangen wird
     * @return Callback, der vorher gesetzt war
     */
    public Consumer<RoundEndHighscoreClientCommand> setHighscoreReceivedCallback(
        Consumer<RoundEndHighscoreClientCommand> callback) {
        Consumer<RoundEndHighscoreClientCommand> oldCallback = _highscoreReceivedCallback;
        _highscoreReceivedCallback = callback;
        return oldCallback;
    }

    /**
     * Diese Methode sendet eine Aktion, die die Spielfigur durchführen soll,
     * während der Spieleserver sich im Spielmodus befindet.
     *
     * @param command
     *            Kommando, welches die Aktion des Spielers enthält
     * @throws MessageMalformedException
     *             sollte nicht auftreten
     * @throws SocketException
     *             wenn die Kommunikation schief geht
     */
    public void doPlayerAction(PlayerActionGameServerCommand command)
        throws MessageMalformedException, SocketException {
        _connection.sendMessage(command);
    }
}
