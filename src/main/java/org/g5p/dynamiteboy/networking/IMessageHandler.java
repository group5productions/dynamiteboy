/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

/**
 * Dieses Interface wird von Klassen implementiert, die Verbindungen zu einer
 * Gegenstelle bearbeiten möchten.
 *
 * @author Thomas Weber
 */
public interface IMessageHandler {
    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    public boolean onConnectionEstablished(Messenger connection);

    /**
     * Diese Methode wird aufgerufen, wenn die Gegenstelle eine Nachricht an den
     * Messenger schickt.
     *
     * @param clientCommand
     *            Nachricht, die die Gegenstelle gesendet hat
     */
    public void onCommandReceived(Message clientCommand);

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit der Gegenstelle erfolgt ist.
     *
     * @return true wenn die Verbindung trotzdem beibehalten werden soll, oder
     *         false, wenn die Verbindung getrennt werden soll
     */
    public boolean onConnectionTimeout();

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    public void onConnectionError(Exception e);

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param closedByUs
     *            true, wenn die Trennung von unserer Seite ausgeht, sonst
     *            false, wenn die Trennung von der Gegenstelle ausgeht
     */
    public void onConnectionTerminated(boolean closedByUs);
}
