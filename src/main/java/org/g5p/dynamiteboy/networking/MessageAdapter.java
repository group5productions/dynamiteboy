/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Diese Klasse hilft der GSON-Bibliothek, Nachrichten aus JSON zu lesen bzw.
 * diese in JSON zu schreiben.
 *
 * @author Thomas Weber
 * @see <a href="https://stackoverflow.com/a/16873051">Stack Overflow</a>
 */
public final class MessageAdapter
    implements JsonSerializer<Message>, JsonDeserializer<Message> {

    /**
     * Diese Methode wird aufgerufen, während GSON ein als String codiertes
     * JSON-Objekt verarbeitet und eine Instanz von Message erzeugt werden soll.
     *
     * @param json
     *            Ein aus einem String verarbeitetes JSON-Objekt
     * @param typeOfT
     *            Java-Typ, in die das Objekt überführt werden soll
     * @param context
     *            Kontext, der den aktuellen Zustand der Deserialisierung
     *            beschreibt
     */
    @Override
    public Message deserialize(JsonElement json, Type typeOfT,
        JsonDeserializationContext context) throws JsonParseException {

        /* Lese Namen des Kommandos */
        JsonObject jsonObject = json.getAsJsonObject();
        String command = jsonObject.get("command").getAsString();
        JsonElement content = jsonObject.get("content");

        /*
         * Iteriere die Klassen von org.g5p.dynamiteboy.networking.messages, um
         * die Klasse zu finden, die dieses Kommando unterstützt
         */
        Class<? extends Message> classForCommand = null;
        for (Class<? extends Message> klass : AllMessages.getList()) {
            try {
                /* Extrahiere die Konstante JSON_COMMAND_NAME aus der Klasse */
                String classCommand = (String) klass
                    .getDeclaredField("JSON_COMMAND_NAME").get(null);
                if (classCommand.equals(command)) {
                    classForCommand = klass;
                    break;
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new JsonParseException(
                    "cannot get JSON command name for class: "
                        + e.getLocalizedMessage());
            }
        }
        if (classForCommand == null)
            throw new JsonParseException(
                "cannot find class which supports command \"" + command + "\"");

        /*
         * Erzeuge Instanz ohne Inhalt. Funktioniert nur, wenn die zugehörige
         * Klasse einen Konstruktor ohne Parameter erlaubt.
         */
        if (content == null) {
            try {
                return classForCommand.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new JsonParseException(
                    "cannot instantiate class: " + e.getLocalizedMessage());
            }
        } else {
            /* Deserialisiere die Nachricht mit der gefundenen Klasse */
            return context.deserialize(content, classForCommand);
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn GSON ein Java-Objekt in ein als
     * String codiertes JSON-Objekt verarbeiten soll, welches den Typ Message in
     * seiner Klassenhierarchie besitzt.
     *
     * @param src
     *            Nachricht, die in JSON übersetzt werden soll
     * @param typeOfSrc
     *            Genauer Typ, den src besitzt
     * @param context
     *            Kontext, der den aktuellen Zustand der Serialisierung
     *            beschreibt
     */
    @Override
    public JsonElement serialize(Message src, Type typeOfSrc,
        JsonSerializationContext context) {
        /*
         * Erzeuge ein neues JSON-Objekt mit den Elementen "command" und
         * "content" (optional)
         */
        JsonObject object = new JsonObject();
        object.add("command", new JsonPrimitive(src.getCommand()));
        JsonElement content = context.serialize(src, src.getClass());

        /*
         * Besitzt ein Derivat von Message keine offen gelegten Variablen, so
         * kann die Serialisierung ein leeres Objekt erzeugen. Wir möchten es
         * nicht übertragen.
         */
        if (content != null && content.getAsJsonObject().size() > 0)
            object.add("content", content);
        return object;
    }
}
