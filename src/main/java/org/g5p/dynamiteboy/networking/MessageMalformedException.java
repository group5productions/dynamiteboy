/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

/**
 * Diese Ausnahme wird bei Nachrichten geworfen, die nicht dem implementierten
 * Datenformat entsprechen. getMessage() gibt einen Hinweis zurück, an welcher
 * Stelle das Datenformat ungültig ist.
 *
 * @author Thomas Weber
 */
public final class MessageMalformedException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Erzeugt eine neue MessageMalformedException mit der angegebenen
     * Zeichenkette.
     *
     * @param message
     *            Nachricht, die in der Ausnahme gespeichert wird
     */
    public MessageMalformedException(String message) {
        super(message);
    }
}
