/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Diese Klasse behandelt eine einzelne Verbindung zu einer Gegenstelle. Sie
 * empfängt, decodiert, codiert und sendet Nachrichten und ruft Callbacks auf,
 * wenn auf eine Nachricht reagiert werden kann.
 *
 * Diese Klasse kann nicht instanziiert werden. Auf der Serverseite
 * MessengerServer, auf der Clientseite MessengerClient nutzen.
 *
 * @author Thomas Weber
 */
public final class Messenger extends Thread {
    private static final Logger _logger = Logger
        .getLogger(Messenger.class.getSimpleName());

    /**
     * Größe des Lesepuffers in Bytes
     */
    private static final int _READ_BUFFER_SIZE_BYTES = 16384;

    private Runnable _exitCallback;

    private final long _timeout;
    private final SocketChannel _remoteSocketChannel;
    private final Selector _remoteSocketSelector;
    private SelectionKey _remoteSocketSelectionKey;
    private String _remoteLoggableSocketAddress;

    private Lock _queueLock;
    private List<Message> _pendingWrites;

    private MessageReader _messageReader;
    private ByteBuffer _pendingWriteBuffer;
    private IMessageHandler _connectionHandler;

    private volatile boolean _stopping;
    private volatile boolean _connecting;

    /**
     * Erzeugt einen neuen Thread, der eine einzelne Verbindung zu einer
     * Gegenstelle verwaltet.
     *
     * @param connectionHandler
     *            Instanz einer Klasse, die Nachrichten bearbeitet
     * @param remote
     *            Kanal der Gegenstelle
     * @param timeout
     *            Zeitpunkt in Millisekunden, nachdem connectionHandler darüber
     *            informiert wird, dass keine Kommunikation statt gefunden hat
     * @param connecting
     *            Ist der Wert auf true gesetzt, übernimmt der Thread das Warten
     *            auf das Zustandekommen der Verbindung. Ansonsten (false) wird
     *            davon ausgegangen, dass die Verbindung zustande gekommen ist.
     * @throws IOException
     *             wenn während der Erstellung ein Netzwerkfehler eintritt
     */
    public Messenger(IMessageHandler connectionHandler, SocketChannel remote,
        long timeout, boolean connecting) throws IOException {
        this._exitCallback = null;

        this._timeout = timeout;
        this._remoteSocketChannel = remote;
        this._remoteSocketSelector = Selector.open();
        this._remoteSocketSelectionKey = null;
        this._remoteLoggableSocketAddress = null;

        this._queueLock = new ReentrantLock();
        this._pendingWrites = new LinkedList<Message>();

        this._messageReader = new MessageReader();
        this._connectionHandler = connectionHandler;

        this._stopping = false;
        this._connecting = connecting;
    }

    /**
     * Setzt ein Callback (in Form eines Runnable), welches bei Beendigung des
     * Threads wird.
     *
     * @param exitCallback
     *            Callback, welches am Threadende ausgeführt wird
     */
    public void setExitCallback(Runnable exitCallback) {
        this._exitCallback = exitCallback;
    }

    /**
     * Gibt die Netzwerkadresse sowie den Port der Gegenstelle in der Form
     * "[Adresse]:[Port]" zurück.
     *
     * @return Netzwerkadresse der Form "[Adresse]:[Port]"
     */
    public String getRemoteAddressString() {
        return _remoteLoggableSocketAddress;
    }

    /**
     * Diese Methode sendet eine Nachricht an die Gegenstelle.
     *
     * @param message
     *            Nachricht, die an die Gegenstelle übermittelt werden soll
     * @throws SocketException
     *             wenn die Verbindung geschlossen wurde oder noch nicht
     *             zustande kam, egal ob server- oder clientseitig
     */
    public void sendMessage(Message message) throws SocketException {
        if (_connecting)
            throw new SocketException("Verbindung wird gerade hergestellt");
        if (_stopping)
            throw new SocketException("Verbindung wurde geschlossen");

        _logger.fine("Enqueuing new message");

        /*
         * Benutze den Kopierkonstruktor der Klasse, um eine Kopie der Nachricht
         * zu erzeugen, die anschließend in die Warteschlange eingefügt wird.
         */
        Message copiedMessage = null;
        try {
            Constructor<? extends Message> ctor = message.getClass()
                .getConstructor(message.getClass());

            copiedMessage = ctor.newInstance(message);
        } catch (Exception e) {
            /* Programmierfehler */
            throw new RuntimeException(
                message.getClass().getName() + " kann nicht kopiert werden");
        }

        /* Reihe zu sendende Nachricht in die Warteschlange ein */
        _queueLock.lock();
        _pendingWrites.add(copiedMessage);
        _queueLock.unlock();

        /*
         * Wir wollen eine Nachricht versenden, daher möchten wir erfahren, wenn
         * die Gegenstelle von uns Nachrichten empfangen möchte.
         */
        _enableSocketWrite();
    }

    /**
     * Durch Aufruf dieser Hilfsroutine hört der Thread der Verbindung nicht
     * mehr darauf, ob von der Gegenstelle gelesen werden kann.
     */
    private void _disableSocketRead() {
        _remoteSocketSelectionKey.interestOps(
            _remoteSocketSelectionKey.interestOps() & (~SelectionKey.OP_READ));
    }

    /**
     * Durch Aufruf dieser Hilfsroutine hört der Thread der Verbindung nicht
     * mehr darauf, ob zur Gegenstelle geschrieben werden kann.
     */
    private void _disableSocketWrite() {
        _remoteSocketSelectionKey.interestOps(
            _remoteSocketSelectionKey.interestOps() & (~SelectionKey.OP_WRITE));
    }

    /**
     * Durch Aufruf dieser Hilfsroutine hört der Thread der Verbindung wieder
     * darauf, ob zur Gegenstelle geschrieben werden kann.
     */
    private void _enableSocketWrite() {
        _remoteSocketSelectionKey.interestOps(
            _remoteSocketSelectionKey.interestOps() | SelectionKey.OP_WRITE);
        _remoteSocketSelector.wakeup();
    }

    /**
     * Schließt die Verbindung zur Gegenstelle. Scheitert dies, wird eine
     * RuntimeException geworfen.
     */
    private void _closeConnectionMustSucceed() {
        /*
         * Kommunikation mit der Gegenstelle ist beendet (z.B. durch ein Beende-
         * Kommando, schließe die Verbindung manuell.
         */
        _logger.info(String.format("Verbindung zu %s wird geschlossen",
            _remoteLoggableSocketAddress));

        try {
            _remoteSocketChannel.close();
        } catch (IOException e) {
            _logger.severe(
                String.format("Kann Client-Verbindung nicht schließen: %s\n",
                    e.getMessage()));
            throw new RuntimeException(e);
        }
    }

    /**
     * Ist die Verbindung zur Gegenstelle TCP-seitig entstanden, wird diese
     * Methode aufgerufen. Diese richtet Variablen ein, die zur Logerstellung
     * verwendet werden, sowie den Selector, damit der Thread der Verbindung mit
     * der Gegenstelle kommuniziert werden kann.
     *
     * @return true, wenn die Verbindung durch das Callback akzeptiert wird,
     *         ansonsten false
     */
    private boolean _processSocketConnect() {
        /* Verbindung loggen */
        _logger.info(String.format("Verbindung zu %s hergestellt",
            _remoteLoggableSocketAddress));

        /*
         * Der Verbindungsprozess wurde abgeschlossen. Höre auf, Connect-Events
         * zu beobachten und beobachte stattdessen Lese-Events.
         */
        _connecting = false;
        int interestOps = _remoteSocketSelectionKey.interestOps();
        interestOps &= ~SelectionKey.OP_CONNECT;
        interestOps |= SelectionKey.OP_READ;
        _remoteSocketSelectionKey.interestOps(interestOps);

        /*
         * Rufe den Callback des Handlers auf
         */
        return _connectionHandler.onConnectionEstablished(this);
    }

    /**
     * Können wir von der Gegenstelle Daten empfangen, wird diese Methode
     * aufgerufen. Die Daten werden zu Nachrichten ver- und anschließend
     * abgearbeitet.
     *
     * @return false wenn die Gegenstelle ihre Verbindung geschlossen hat,
     *         ansonsten false
     * @throws MessageMalformedException
     *             wenn die Gegenstelle Nachrichten sendet, die nicht dem
     *             erwarteten Datenformat entsprechen
     * @throws IOException
     *             wenn während der Leseoperation ein Netzwerkfehler auftritt.
     */
    private boolean _processSocketRead()
        throws MessageMalformedException, IOException {
        /*
         * Erzeuge ein neues ByteBuffer und lese neue Daten ein.
         */
        ByteBuffer inputData = ByteBuffer.allocate(_READ_BUFFER_SIZE_BYTES);
        int readLength = _remoteSocketChannel.read(inputData);
        if (readLength == 0)
            return true;

        /*
         * Gibt read() einen negativen Wert zurück, hat die Gegenstelle ihren
         * Socket geschlossen
         */
        if (readLength < 0)
            return false;
        inputData.limit(readLength);

        /*
         * Daten durch Automat verarbeiten, der eventuell Nachrichten aus ihnen
         * extrahiert
         */
        _messageReader.sendBuffer(inputData);

        /*
         * Verarbeite neu extrahierte Nachrichten.
         */
        while (_messageReader.hasMessages()) {
            Message message = _messageReader.popMessage();

            /*
             * Wir haben ein Kommando erhalten. Rufe das Callback auf, es
             * versendet selbstständig Antworten an und (eventuell) Kommandos
             * für die Gegenstelle.
             */
            _connectionHandler.onCommandReceived(message);
        }
        return true;
    }

    /**
     * Können wir Daten zur Gegenstelle schreiben, wird diese Methode
     * aufgerufen. Zu schreibende Nachrichten werden für den Transfer
     * vorbereitet und einzeln an die Gegenstelle gesendet.
     *
     * @throws IOException
     *             wenn während der Schreiboperation ein Netzwerkfehler
     *             auftritt.
     */
    private void _processSocketWrite() throws IOException {
        /*
         * Haben wir ausstehende Schreiboperationen? Diese zuerst durchführen.
         * Wenn der entsprechende Puffer leer ist, bereiten wir weitere Schreib-
         * operationen vor.
         */
        if (_pendingWriteBuffer != null) {
            if (_pendingWriteBuffer.hasRemaining()) {
                _remoteSocketChannel.write(_pendingWriteBuffer);
                return;
            } else {
                _pendingWriteBuffer = null;
            }
        }

        /*
         * Bearbeite Nachrichten, die an die Gegenstelle gesendet werden. Zum
         * eigentlichen Senden wird ein ByteBuffer angelegt, der beim nächsten
         * Aufruf dieser Methode gesendet wird.
         */
        _queueLock.lock();
        if (_pendingWrites.size() > 0) {
            Message currentMessage = _pendingWrites.remove(0);
            _pendingWriteBuffer = ByteBuffer.wrap(currentMessage.packMessage());
        }

        /*
         * Nichts mehr zu schreiben, höre nicht mehr auf die Gegenstelle, selbst
         * wenn sie uns von uns etwas haben möchte.
         */
        else {
            _disableSocketWrite();
        }
        _queueLock.unlock();
    }

    /**
     * Diese Methode weist den Thread der Verbindung an, keine Nachrichten mehr
     * zu empfangen sowie sich noch in der Warteliste befindliche Nachrichten so
     * bald wie möglich zu versenden. Zudem wird auf den Abschluss dieser
     * Operationen gewartet.
     */
    private synchronized void _stopAndFlush() {
        if (!_stopping) {
            _stopping = true;

            /* Erwache aus dem select()-Aufruf, wenn notwendig */
            this._remoteSocketSelector.wakeup();

            /* Warte, bis der Warteliste komplett versendet wurde */
            try {
                wait();
            } catch (InterruptedException e) {
                /*
                 * Unwahrscheinliches Eintreten. Threads können nur durch
                 * interrupt() unterbrochen werden, wir überlagern aber die
                 * Methode, sodass wir zuerst aufgerufen werden, bevor ein
                 * Interrupt überhaupt eintreten kann.
                 */
            }
        }
    }

    /**
     * Diese Funktion behandelt eine Kommunikation mit einer einzelnen
     * Gegenstelle.
     *
     * @return 1, wenn wir die Verbindung schließen, 0, wenn die Gegenstelle die
     *         Verbindung schließt, oder -1, wenn die Verbindung abgelehnt wird
     */
    private int _handleConnection() throws IOException {
        /*
         * Hole Adresse der Gegenstelle, inklusive einer Repräsentation als
         * String. Bei Fehler wird die Verbindung getrennt. Werfe eine
         * entstehende IOException weiter, damit onConnectionError() diese
         * benutzen kann.
         */
        InetSocketAddress remoteAddress;
        try {
            remoteAddress = (InetSocketAddress) _remoteSocketChannel
                .getRemoteAddress();
            _remoteLoggableSocketAddress = remoteAddress.toString();
        } catch (IOException e) {
            _logger.info(String.format(
                "Konfiguration der Verbindung zur Gegenstelle fehlgeschlagen: %s",
                e.getMessage()));
            throw e;
        }

        /*
         * Muss die Verbindung erst zustande kommen, höre zuerst auf
         * Connect-Events. Ansonsten rufe den Callback, der sonst für das
         * Zustandekommen einer Verbindung aufgerufen werden würde, sofort auf.
         * Registriere dabei vorher den Selector mit der Verbindung. Werfe eine
         * entstehende IOException weiter, damit onConnectionError() diese
         * benutzen kann.
         */
        try {
            _remoteSocketSelectionKey = _remoteSocketChannel.register(
                _remoteSocketSelector,
                _connecting ? SelectionKey.OP_CONNECT : SelectionKey.OP_READ);
        } catch (IOException e) {
            _logger.info(
                String.format("Kommunikationsfehler in Verbindung zu %s: %s",
                    _remoteLoggableSocketAddress, e.getMessage()));
            throw e;
        }
        if (!_connecting) {
            /*
             * Wird die Verbindung durch das Callback abgelehnt, trenne diese
             * wieder
             */
            if (!_processSocketConnect())
                return -1;
        }

        /*
         * Halte die Kommunikation mit der Gegenstelle solange aufrecht, wie der
         * Thread nicht von einem anderen unterbrochen wird.
         */
        long timeOfLastRead = System.currentTimeMillis();
        while (true) {
            /*
             * Nur wenn keine Schreiboperationen mehr ausgeführt werden, ist es
             * zulässig, dass der Thread beendet werden kann.
             */
            _queueLock.lock();
            int writesCount = _pendingWrites.size();
            _queueLock.unlock();

            if (_pendingWriteBuffer == null && writesCount == 0) {
                /*
                 * Schreibpuffer ist leer. Wecke wartende Threads auf, wenn sie
                 * auf dieses Ereignis gewartet haben
                 */
                synchronized (this) {
                    notifyAll();
                }

                /* Gerade am Stoppen? */
                if (_stopping)
                    break;
            }

            try {
                /* Gestoppt? Nehme keine weiteren Nachrichten mehr an */
                if (_stopping) {
                    _disableSocketRead();
                    _enableSocketWrite();
                    _remoteSocketChannel.shutdownInput();
                }

                /*
                 * Warte auf I/O-Operationen und delegiere diese weiter.
                 * Prioritisiere Schreiben (welches Warteliste abbaut) über
                 * Lesen (welches Warteliste aufbaut).
                 */
                if (_remoteSocketSelector.select(_timeout) > 0) {
                    Iterator<SelectionKey> it = _remoteSocketSelector
                        .selectedKeys().iterator();

                    while (it.hasNext()) {
                        SelectionKey key = it.next();
                        it.remove();

                        try {
                            /* Verbunden? Richte den Rest der Verbindung ein */
                            if (key.isValid() && key.isConnectable()) {
                                /*
                                 * Schließe Verbindungsvorgang ab. Methode
                                 * sollte nie false zurückgeben, da bei
                                 * gescheiterter Verbindung eine Ausnahme
                                 * geworfen wird.
                                 */
                                _remoteSocketChannel.finishConnect();

                                /*
                                 * Richte Rest der Verbindung ein. Wird die
                                 * Verbindung durch das Callback abgelehnt,
                                 * trenne diese wieder.
                                 */
                                if (!_processSocketConnect())
                                    return -1;
                            } else {
                                /*
                                 * Schreibbar? Wir senden der Gegenstelle
                                 * Nachrichten
                                 */
                                if (key.isValid() && key.isWritable())
                                    _processSocketWrite();

                                /* Lesbar? Gegenstelle sendet uns Nachricht */
                                if (key.isValid() && key.isReadable()) {
                                    /*
                                     * Gebe false zurück, wenn die Gegenstelle
                                     * ihre Verbindung schließt. Inmitten eines
                                     * Spiels gilt das als ein schwerer Fehler.
                                     */
                                    if (!_processSocketRead())
                                        return 0;
                                    timeOfLastRead = System.currentTimeMillis();
                                }
                            }
                        } catch (MessageMalformedException e) {
                            _logger.severe(String.format(
                                "Nachricht von %s ungültig (%s), trenne Verbindung",
                                _remoteLoggableSocketAddress, e.getMessage()));
                            break;
                        } catch (IOException e) {
                            /* Übergeordnetes try-catch behandelt I/O-Fehler */
                            throw e;
                        }
                    }
                } else {
                    /*
                     * Wirklich ein Timeout? Überprüfe, wie viel Zeit seit
                     * select() vergangen ist. Nur wenn Intervall über der
                     * Schwelle für Timeout liegt, wird Callback aufgerufen.
                     * Wenn die Gegenstelle nicht mehr akzeptiert wird, so wird
                     * die Verbindung ohne Netzwerkfehler getrennt.
                     *
                     * (Java ist bekannt dafür, einfachste Dinge wie diese zu
                     * verhauen.)
                     */
                    if (_timeout > 0
                        && System.currentTimeMillis()
                            - timeOfLastRead >= _timeout
                        && !_connectionHandler.onConnectionTimeout()) {

                        _logger.info(String.format(
                            "Zeitüberschreitung in Verbindung zu %s, Kommunikation soll beendet werden",
                            _remoteLoggableSocketAddress));
                        return 1;
                    }
                }
            } catch (IOException e) {
                /* Logge die Ausnahme und reiche sie weiter */
                _logger.info(String.format(
                    "Kommunikationsfehler in Verbindung zu %s: %s",
                    _remoteLoggableSocketAddress, e.getMessage()));
                throw e;
            }
        }
        return 1;
    }

    /**
     * Diese Funktion wird in einem Thread ausgeführt. Sie behandelt eine
     * einzelne Verbindung zu einer Gegenstelle und schließt die Verbindung nach
     * erfolgreichem Beenden oder einem Kommunikationsabbruch.
     */
    @Override
    public void run() {
        /*
         * Behandle die Verbindung -- trat ein Netzwerkfehler auf, wird ein
         * weiteres Callback ausgeführt
         */
        int closureResult = 1;
        try {
            closureResult = _handleConnection();
        } catch (Exception e) {
            _connectionHandler.onConnectionError(e);
        }

        /*
         * Rufe ein Callback auf, bevor die Verbindung geschlossen wird (nur
         * wenn die Verbindung akzeptiert wurde)
         */
        if (closureResult > -1)
            _connectionHandler.onConnectionTerminated(closureResult == 1);

        /* Schließe die Verbindung */
        _closeConnectionMustSucceed();

        /*
         * Führe ein optionales Callback aus, welches bei Beenden des Threads
         * ausgeführt werden soll
         */
        if (_exitCallback != null)
            _exitCallback.run();

        /*
         * Markiere als gestoppt, sodass ein interrupt() danach nicht versucht,
         * auf Aufräumoperationen zu warten
         */
        _stopping = true;
    }

    /**
     * Überlagere die interrupt()-Methode, um den Remote-Socket zu schließen und
     * ein eventuell wartendes accept() zu unterbrechen. Wird durch den in
     * main() installierten Shutdown-Hook ausgeführt.
     */
    @Override
    public void interrupt() {
        try {
            /* Erst Schreiboperationen beenden, dann Verbindung schließen */
            _stopAndFlush();
            _remoteSocketChannel.close();
        } catch (IOException e) {
        } finally {
            super.interrupt();
        }
    }
}
