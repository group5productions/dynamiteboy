/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

/**
 * Diese Klasse extrahiert aus Sequenzen von Bytes vollständige Nachrichten
 * mithilfe eines Automatenkonstrukts.
 *
 * @author Thomas Weber
 */
public final class MessageReader {
    private List<Message> _messages;
    private int _state;
    private int _messageSize;
    private StringBuilder _messageBuilder;

    /**
     * Erzeugt eine neue Instanz von MessageReader.
     */
    public MessageReader() {
        _messages = new LinkedList<Message>();
        reset();
    }

    /**
     * Setzt den Automaten zurück an den Anfangszustand. Nachrichten, die sich
     * noch nicht verarbeitet wurden, werden nicht entfernt.
     */
    public void reset() {
        _state = 0;
        _messageSize = 0;
        _messageBuilder = null;
    }

    /**
     * Überprüft, ob Nachrichten gelesen werden können.
     *
     * @return true wenn Nachrichten vorhanden sind, sonst false
     */
    public boolean hasMessages() {
        return !_messages.isEmpty();
    }

    /**
     * Entfernt die älteste Nachricht, die gelesen werden kann.
     *
     * @return Nachricht oder null, wenn keine da sind
     */
    public Message popMessage() {
        if (_messages.isEmpty())
            return null;
        else {
            Message message = _messages.get(0);
            _messages.remove(0);
            return message;
        }
    }

    /**
     * Übermittelt eine Bytesequenz in Form eines ByteBuffer und versucht,
     * komplette Nachrichten zu extrahieren. Dies benötigt je nach Umfang der
     * Daten mehr als einen Aufruf dieser Methode.
     *
     * @param buffer
     *            partielle Bytesequenz
     * @throws MessageMalformedException
     *             wenn der konkrete Inhalt einer Nachricht zu groß ist
     */
    public void sendBuffer(ByteBuffer buffer) throws MessageMalformedException {
        /*
         * Lese den Puffer von Anfang an und so lange, bis er leer ist.
         */
        buffer.rewind();
        while (buffer.hasRemaining()) {
            switch (_state) {

            /*
             * Zustand 0: Lese die Größe einer Nachricht
             */
            case 0:
                byte input = buffer.get();

                /* Lese eine Zahl ein und füge sie zur Gesamtgröße hinzu */
                if (input >= '0' && input <= '9') {
                    _messageSize = _messageSize * 10 + (input - '0');
                }

                /*
                 * Ansonsten haben wir das erste Byte des JSON-Objekts.
                 * Überprüfe die Nachrichtengröße und lege den String an, um das
                 * JSON-Objekt einzulesen.
                 */
                else {
                    if (_messageSize >= Message.MESSAGE_SIZE_MAX)
                        throw new MessageMalformedException(
                            "Nachricht zu groß");

                    _messageBuilder = new StringBuilder(new String(
                        new byte[] { input }, StandardCharsets.UTF_8));
                    --_messageSize;

                    _state = 1;
                }
                break;

            /*
             * Zustand 1: Lese den Rest der Nachricht, da dessen Größe bekannt
             * ist
             */
            case 1:
                /*
                 * Kopiere alles, was der ByteBuffer uns hergibt, aber nie mehr
                 * als wir benötigen.
                 */
                int copyLength = buffer.remaining();
                if (_messageSize < copyLength)
                    copyLength = _messageSize;

                byte[] inputBuffer = new byte[copyLength];
                buffer.get(inputBuffer, 0, copyLength);
                _messageSize -= copyLength;

                /* Erweitere den String des JSON-Objektes */
                _messageBuilder
                    .append(new String(inputBuffer, StandardCharsets.UTF_8));

                /* ByteBuffer alle und wir haben noch nicht alles? */
                if (_messageSize > 0)
                    break;

                /*
                 * Erzeuge eine neue Nachricht aus dem gelesenen Byte-Array.
                 * (Nullpointer oder I/O-Exceptions werden in eine
                 * MalformedMessageException umgewandelt.)
                 */
                Message message = null;
                try {
                    message = Message.unpackMessage(_messageBuilder.toString());
                } catch (MessageMalformedException e) {
                    throw new MessageMalformedException("Nachricht ungültig");
                }

                /* Nachricht einreihen */
                _messages.add(message);

                /* Setze Automat in den Anfangszustand zurück */
                reset();
            }
        }
    }
}
