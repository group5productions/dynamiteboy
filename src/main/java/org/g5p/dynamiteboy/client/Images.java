/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Diese Klasse lädt bei erstmaligem Laden sämtliche Bilder, die im Spiel
 * enthalten sind, um schnell auf diese zugreifen zu können.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class Images {
    /**
     * Hintergrund des Hauptmenüs
     */
    public static final BufferedImage menuBackground;

    /**
     * Avatar jedes Spielers
     */
    public static final BufferedImage playerAvatar;

    /**
     * Maske über Spieler, die andeutet, dass er eine Rüstung trägt
     */
    public static final BufferedImage playerArmorOverlay;

    /**
     * Hintergrund von Spieler 1, nicht gedrückt
     */
    public static final BufferedImage player1Background;

    /**
     * Hintergrund von Spieler 1, gedrückt
     */
    public static final BufferedImage player1PushedBackground;

    /**
     * Hintergrund von Spieler 2, nicht gedrückt
     */
    public static final BufferedImage player2Background;

    /**
     * Hintergrund von Spieler 2, gedrückt
     */
    public static final BufferedImage player2PushedBackground;

    /**
     * Hintergrund von Spieler 3, nicht gedrückt
     */
    public static final BufferedImage player3Background;

    /**
     * Hintergrund von Spieler 3, gedrückt
     */
    public static final BufferedImage player3PushedBackground;

    /**
     * Hintergrund von Spieler 4, nicht gedrückt
     */
    public static final BufferedImage player4Background;

    /**
     * Hintergrund von Spieler 4, gedrückt
     */
    public static final BufferedImage player4PushedBackground;

    /**
     * Freies, begehbares Feld
     */
    public static final BufferedImage walkableTile;

    /**
     * Feste Wand
     */
    public static final BufferedImage wallTile;

    /**
     * Mit Bombe zerstörbare Wand
     */
    public static final BufferedImage breakableWallTile;

    /**
     * Bombe, Zustand 1 (frisch platziert)
     */
    public static final BufferedImage bombState1;

    /**
     * Bombe, Zustand 2 (Schnur wird kürzer)
     */
    public static final BufferedImage bombState2;

    /**
     * Bombe, Zustand 3 (kurz vor Explosion)
     */
    public static final BufferedImage bombState3;

    /**
     * Explosion, ausgelöst durch Bombe
     */
    public static final BufferedImage bombExplosion;

    /**
     * Hintergrund aller Powerups
     */
    public static final BufferedImage powerupBackground;

    /**
     * Powerup: mehr Bomben gleichzeitig platzieren
     */
    public static final BufferedImage powerupBombs;

    /**
     * Powerup: Durch Rüstung vor Explosionen kurzzeitig geschützt sein
     */
    public static final BufferedImage powerupArmor;

    /**
     * Powerup: Radius der Explosion erhöht sich um 1
     */
    public static final BufferedImage powerupExplosionRadius;

    /**
     * Powerup: Spieler läuft mit doppelter Geschwindigkeit (Standard A+B)
     */
    public static final BufferedImage powerupPlayerFaster;

    /**
     * Powerup: Spieler kann über Bomben laufen (Standard A+B)
     */
    public static final BufferedImage powerupRunOverBombs;

    /**
     * Powerup: Radius der Explosion erhöht sich auf ein Maximum
     */
    public static final BufferedImage powerupExplosionRadiusMax;

    /**
     * Powerup: Superbombe mit besonderen Eigenschaften
     */
    public static final BufferedImage powerupSuperBomb;

    /**
     * Powerup: Spieler kann Bombe bis zu einem Hindernis treten
     */
    public static final BufferedImage powerupPlayerKickBomb;

    private static final GraphicsConfiguration _screenGfxConfig;

    /**
     * Lädt eine Bilddatei aus den zur Laufzeit zur Verfügung stehenden
     * Klassenpfaden und konvertiert diese in ein Bild, welches mit dem
     * aktuellen Anzeigegerät (grafiktechnisch) kompatibel ist, also wo die
     * Codierung der Farben im Bild mit der Codierung der Farben des
     * Anzeigegerätes überein stimmt.
     *
     * @param resourceName
     *            Name der Ressource, die geladen werden soll
     * @return Bild mit zum Anzeigegerät kompatiblen Farbformat
     * @throws FileNotFoundException
     *             wenn die Ressource nicht gefunden werden kann
     * @throws IOException
     *             wenn beim Laden des Bildes ein Fehler auftritt
     */
    private static BufferedImage _loadImageFileAsCompatibleImage(
        final String resourceName) throws FileNotFoundException, IOException {
        /* Finde die Ressource */
        URL resource = Images.class.getResource(resourceName);
        if (resource == null) {
            throw new FileNotFoundException(
                String.format("Ressource '%s' nicht gefunden", resourceName));
        }

        /* Lade Bild unter Ressource und erzeuge kompatibles Bitmap */
        BufferedImage temporaryImage = ImageIO.read(resource);
        BufferedImage compatibleImage = createCompatibleImage(
            temporaryImage.getWidth(), temporaryImage.getHeight());

        /* Male Bilddatei in kompatibles Bild -- dafür ist Transparenz nötig */
        Graphics2D g = compatibleImage.createGraphics();
        g.drawImage(temporaryImage, 0, 0, null);
        g.dispose();

        return compatibleImage;
    }

    /**
     * Erzeugt ein Bild mit den angegebenen Dimensionen, dessen Farbformat
     * kompatibel zum Farbformat des Anzeigegerätes ist.
     *
     * @param width
     *            Breite des Bildes in Pixeln
     * @param height
     *            Höhe des Bildes in Pixeln
     * @return Neues Bild mit zum Anzeigegerät kompatiblen Farbformat
     */
    public static BufferedImage createCompatibleImage(final int width,
        final int height) {
        /* Erzeuge das Bild und initialisiere es mit Transparenz */
        return _screenGfxConfig.createCompatibleImage(width, height,
            Transparency.TRANSLUCENT);
    }

    /**
     * Diese Klasse lädt alle Bilder vor, um schnell auf sie zugreifen zu
     * können.
     */
    static {
        BufferedImage _menuBackground = null;
        BufferedImage _playerAvatar = null;
        BufferedImage _playerArmorOverlay = null;
        BufferedImage _player1Background = null;
        BufferedImage _player1PushedBackground = null;
        BufferedImage _player2Background = null;
        BufferedImage _player2PushedBackground = null;
        BufferedImage _player3Background = null;
        BufferedImage _player3PushedBackground = null;
        BufferedImage _player4Background = null;
        BufferedImage _player4PushedBackground = null;
        BufferedImage _walkableTile = null;
        BufferedImage _wallTile = null;
        BufferedImage _breakableWallTile = null;
        BufferedImage _bombState1 = null;
        BufferedImage _bombState2 = null;
        BufferedImage _bombState3 = null;
        BufferedImage _bombExplosion = null;
        BufferedImage _powerupBackground = null;
        BufferedImage _powerupBombs = null;
        BufferedImage _powerupArmor = null;
        BufferedImage _powerupExplosionRadius = null;
        BufferedImage _powerupPlayerFaster = null;
        BufferedImage _powerupRunOverBombs = null;
        BufferedImage _powerupExplosionRadiusMax = null;
        BufferedImage _powerupSuperBomb = null;
        BufferedImage _powerupPlayerKickBomb = null;

        /*
         * Lade die Grafikkonfiguration des aktuellen Anzeigegerätes, um alle
         * Bilder in einem zu ihm kompatiblen Bildformat laden zu können.
         */
        _screenGfxConfig = GraphicsEnvironment.getLocalGraphicsEnvironment()
            .getDefaultScreenDevice().getDefaultConfiguration();

        try {
            _menuBackground = _loadImageFileAsCompatibleImage(
                "/MainMenuBackground.png");
            _playerAvatar = _loadImageFileAsCompatibleImage("/Player.png");
            _playerArmorOverlay = _loadImageFileAsCompatibleImage(
                "/Schild.png");
            _player1Background = _loadImageFileAsCompatibleImage("/P1up.png");
            _player1PushedBackground = _loadImageFileAsCompatibleImage(
                "/P1down.png");
            _player2Background = _loadImageFileAsCompatibleImage("/P2up.png");
            _player2PushedBackground = _loadImageFileAsCompatibleImage(
                "/P2down.png");
            _player3Background = _loadImageFileAsCompatibleImage("/P3up.png");
            _player3PushedBackground = _loadImageFileAsCompatibleImage(
                "/P3down.png");
            _player4Background = _loadImageFileAsCompatibleImage("/P4up.png");
            _player4PushedBackground = _loadImageFileAsCompatibleImage(
                "/P4down.png");
            _walkableTile = _loadImageFileAsCompatibleImage("/Leer.png");
            _wallTile = _loadImageFileAsCompatibleImage("/fWand.png");
            _breakableWallTile = _loadImageFileAsCompatibleImage("/zWand.png");
            _bombState1 = _loadImageFileAsCompatibleImage("/Bomb1.png");
            _bombState2 = _loadImageFileAsCompatibleImage("/Bomb2.png");
            _bombState3 = _loadImageFileAsCompatibleImage("/Bomb3.png");
            _bombExplosion = _loadImageFileAsCompatibleImage("/Explosion.png");
            _powerupBackground = _loadImageFileAsCompatibleImage(
                "/Powerup.png");
            _powerupBombs = _loadImageFileAsCompatibleImage("/BombPlus.png");
            _powerupArmor = _loadImageFileAsCompatibleImage("/Ruestung.png");
            _powerupExplosionRadius = _loadImageFileAsCompatibleImage(
                "/RadiusPlus.png");
            _powerupPlayerFaster = _loadImageFileAsCompatibleImage(
                "/Speed.png");
            _powerupRunOverBombs = _loadImageFileAsCompatibleImage(
                "/BombRunner.png");
            _powerupExplosionRadiusMax = _loadImageFileAsCompatibleImage(
                "/RadiusMax.png");
            _powerupSuperBomb = _loadImageFileAsCompatibleImage(
                "/SuperBomb.png");
            _powerupPlayerKickBomb = _loadImageFileAsCompatibleImage(
                "/BombKick.png");
        } catch (Exception e) {
            ClientWindow.fatalExit("Es können eine oder mehrere Grafiken nicht "
                + "geladen werden", e);
        }

        /* Weise finale Variablen erst nach den Exceptions zu */
        menuBackground = _menuBackground;
        playerAvatar = _playerAvatar;
        playerArmorOverlay = _playerArmorOverlay;
        player1Background = _player1Background;
        player1PushedBackground = _player1PushedBackground;
        player2Background = _player2Background;
        player2PushedBackground = _player2PushedBackground;
        player3Background = _player3Background;
        player3PushedBackground = _player3PushedBackground;
        player4Background = _player4Background;
        player4PushedBackground = _player4PushedBackground;
        walkableTile = _walkableTile;
        wallTile = _wallTile;
        breakableWallTile = _breakableWallTile;
        bombState1 = _bombState1;
        bombState2 = _bombState2;
        bombState3 = _bombState3;
        bombExplosion = _bombExplosion;
        powerupBackground = _powerupBackground;
        powerupBombs = _powerupBombs;
        powerupArmor = _powerupArmor;
        powerupExplosionRadius = _powerupExplosionRadius;
        powerupPlayerFaster = _powerupPlayerFaster;
        powerupRunOverBombs = _powerupRunOverBombs;
        powerupExplosionRadiusMax = _powerupExplosionRadiusMax;
        powerupSuperBomb = _powerupSuperBomb;
        powerupPlayerKickBomb = _powerupPlayerKickBomb;
    }
}
