/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.client.ArenaPanelOfflineModeSettings.PlayerOperationMode;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;

/**
 * Diese Klasse implementiert das Panel, welches es ermöglicht, eine Spielerunde
 * im Offline-Modus zu konfigurieren.
 *
 * @author Thomas Weber
 */
public final class OfflinePlayPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final ButtonGroup _playerMatrixGroups[];
    private final JRadioButton _playerMatrixRadios[][];
    private final ArenaConfigurationSubPanel _arenaConfigurationPanel;

    /**
     * Ein ActionListener, dessen Ereignis ausgelöst wird, wenn auf den Button
     * "Spiel starten" geklickt wird.
     */
    private final class _StartGameButtonActionListener
        implements ActionListener {

        /**
         * Diese Methode wird ausgeführt, wenn auf den Button "Spiel starten"
         * geklickt wird.
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            /* Hole Einstellungen der Arena ab */
            ConfigureArenaGameServerCommand configCommand;
            try {
                configCommand = _arenaConfigurationPanel
                    .getArenaConfiguration();
            } catch (IllegalArgumentException e) {
                ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                    "Eine oder mehrere Eingaben sind leider nicht korrekt.\n\n"
                        + "Grund: %s.\n\nBitte korrigiere deine Eingaben und "
                        + "versuche es anschließend erneut.",
                    e.getLocalizedMessage());
                return;
            }

            /*
             * Lade die Konfiguration der Spieler aus der Matrix
             */
            final PlayerOperationMode operationModes[] = new PlayerOperationMode[Globals.ROUND_MAX_PLAYERS];
            for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i) {
                if (_playerMatrixRadios[i][1].isSelected())
                    operationModes[i] = PlayerOperationMode.PHYSICAL_PLAYER_1;
                else if (_playerMatrixRadios[i][2].isSelected())
                    operationModes[i] = PlayerOperationMode.PHYSICAL_PLAYER_2;
                else if (_playerMatrixRadios[i][3].isSelected())
                    operationModes[i] = PlayerOperationMode.AI_CONTROLLED;
                else
                    operationModes[i] = PlayerOperationMode.DISABLED;
            }

            /* Fülle die Einstellungen und validiere diese */
            ArenaPanelOfflineModeSettings offlineSettings = new ArenaPanelOfflineModeSettings(
                configCommand, operationModes);
            try {
                offlineSettings.validateSettings();
            } catch (IllegalArgumentException e) {
                ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                    "Eine oder mehrere Eingaben sind leider nicht korrekt.\n\n"
                        + "Grund: %s.\n\nBitte korrigiere deine Eingaben und "
                        + "versuche es anschließend erneut.",
                    e.getLocalizedMessage());
                return;
            }

            /* Speichere Einstellungen */
            _saveOptions(offlineSettings);

            /* Wechsle in das Spiel */
            ClientWindow.getInstance()
                .setWindowState(ClientWindowState.GAME_ARENA, offlineSettings);
        }
    }

    /**
     * Aktiviert Zeilen der Spielermatrix, die eine Zeilennummer kleiner oder
     * gleich row haben, und deaktiviert alle weiteren Zeilen. Werden Zeilen
     * deaktiviert, so lösche den eingestellten Wert, damit dies für die
     * Arenakonfiguration als "kein Spieler" gilt.
     *
     * @param row
     *            Zeilennummer, ab welcher Zeilen der Spielermatrix deaktiviert
     *            werden
     */
    private void _enableAllowedMatrixRows(final int row) {
        for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
            if (i <= row) {
                for (int j = 0; j < 4; ++j)
                    _playerMatrixRadios[i - 1][j].setEnabled(true);
            } else {
                _playerMatrixGroups[i - 1].clearSelection();
                for (int j = 0; j < 4; ++j)
                    _playerMatrixRadios[i - 1][j].setEnabled(false);
            }
        }
    }

    /**
     * Ein JRadioButton, der keinen Text enhält, dafür aber zentriert in einem
     * GridLayout ausgerichtet wird.
     */
    private final class _CenterAlignedRadioButton extends JRadioButton {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Erzeugt eine Instanz eines JRadioButton ohne Text und mit zentrierter
         * Ausrichtung.
         */
        public _CenterAlignedRadioButton() {
            super();
            setHorizontalAlignment(AbstractButton.CENTER);
        }
    }

    /**
     * Diese Methode speichert die geänderten Einstellungen.
     *
     * @param offlineSettings
     *            Konfiguration des Offline-Spiels zum Zeitpunkt der
     *            Speicherung. Es wird angenommen, dass die Konfiguration keine
     *            Fehler enthält.
     */
    private void _saveOptions(ArenaPanelOfflineModeSettings offlineSettings) {
        /* Im globalen Optionsobjekt ablegen */
        ClientWindow.getInstance().getOptions()
            .setOfflinePlaySettings(offlineSettings);
    }

    /**
     * Diese Methode lädt alle konfigurierbaren Steuerelemente mit den bislang
     * gesetzten Optionen.
     */
    public void loadOptions() {
        /*
         * Noch keine Einstellungen verfügbar? Lade Standardeinstellungen
         */
        ArenaPanelOfflineModeSettings settings = ClientWindow.getInstance()
            .getOptions().getOfflinePlaySettings();
        if (settings == null) {
            /* Setze auf Standardeinstellungen zurück */
            for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i)
                _playerMatrixGroups[i].clearSelection();
            _enableAllowedMatrixRows(Globals.ROUND_MAX_PLAYERS);

            _arenaConfigurationPanel.setArenaConfiguration(null);
        } else {
            /* Lade Einstellungen für die Arena */
            _arenaConfigurationPanel
                .setArenaConfiguration(settings.getConfigureArenaCommand());

            /* Lade Einstellungen der Spieler */
            final int maxPlayers = settings.getConfigureArenaCommand()
                .getMaxPlayers();
            for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i) {
                /* Zeile in erlaubter Spielerzahl? */
                if (i < maxPlayers) {
                    PlayerOperationMode operationMode = settings
                        .getOperationForPlayer(i + 1);
                    int selectedIndex = -1;

                    switch (operationMode) {
                    case DISABLED:
                        selectedIndex = 0;
                        break;
                    case PHYSICAL_PLAYER_1:
                        selectedIndex = 1;
                        break;
                    case PHYSICAL_PLAYER_2:
                        selectedIndex = 2;
                        break;
                    case AI_CONTROLLED:
                        selectedIndex = 3;
                        break;
                    default:
                        break;
                    }
                    if (selectedIndex > -1)
                        _playerMatrixRadios[i][selectedIndex].setSelected(true);
                }

                /* Deaktiviere die Zeile völlig */
                else {
                    for (int j = 0; j < 4; ++j) {
                        _playerMatrixRadios[i][j].setSelected(false);
                        _playerMatrixRadios[i][j].setEnabled(false);
                    }
                }
            }
        }
    }

    /**
     * Erzeugt das Panel, mit dem ein Offline-Spiel konfiguriert werden kann.
     */
    public OfflinePlayPanel() {
        super("Offline-Spiel konfigurieren");

        /*
         * Erzeuge Buttons für die Kopfzeile
         */
        final JButton startGameButton = createStyledJButton("Spiel starten");
        startGameButton.addActionListener(new _StartGameButtonActionListener());

        /* Button führt zurück ins Hauptmenü */
        final JButton mainMenuButton = createStyledJButton("Hauptmenü");
        mainMenuButton.addActionListener((ActionEvent event) -> ClientWindow
            .getInstance().setWindowState(ClientWindowState.MAIN_MENU));

        /* Füge diese zur Kopfzeile hinzu */
        createLeftAlignedButtonPanel(startGameButton, mainMenuButton);

        /*-
         * Erzeuge ein Grid mit Radio-Buttons nach folgendem Schema:
         *           | Kein | 1. Spieler | 2. Spieler |  KI
         * ----------+------+------------+------------+------
         * Spieler 1 |  ( ) |    (x)     |    ( )     | ( )
         * Spieler 2 |  (x) |    ( )     |    ( )     | ( )
         * Spieler 3 |  ( ) |    ( )     |    ( )     | (x)
         * Spieler 4 |  ( ) |    ( )     |    (x)     | ( )
         */
        final JPanel playerGrid = new JPanel();
        playerGrid.setOpaque(false);
        this.add(playerGrid);

        /* Definiere Spaltennamen */
        final String[] columnNames = new String[] { "Nicht gesetzt",
            "1. Spieler", "2. Spieler", "KI-Steuerung" };

        /* Lege das Layout an */
        final GridLayout playerGridLayout = new GridLayout(
            columnNames.length + 1, Globals.ROUND_MAX_PLAYERS + 1,
            BUTTON_SPACE_H, BUTTON_SPACE_V);
        playerGrid.setLayout(playerGridLayout);

        /* Füge alle Steuerelemente von links oben nach rechts unten hinzu */
        _playerMatrixGroups = new ButtonGroup[Globals.ROUND_MAX_PLAYERS];
        _playerMatrixRadios = new JRadioButton[Globals.ROUND_MAX_PLAYERS][];
        for (int i = 0; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
            /* 1. Zeile */
            if (i == 0) {
                for (int j = 0; j <= columnNames.length; ++j) {
                    /* Platzhalter */
                    if (j == 0)
                        playerGrid.add(new JLabel());

                    /* Name der Spalte */
                    else {
                        playerGrid.add(new JLabel(columnNames[j - 1],
                            SwingConstants.CENTER));
                    }
                }
            }

            /* Alle weiteren Zeilen */
            else {
                /* Lege Button-Gruppe und Array für diese Zeile an */
                _playerMatrixGroups[i - 1] = new ButtonGroup();
                _playerMatrixRadios[i
                    - 1] = new JRadioButton[columnNames.length];

                for (int j = 0; j <= columnNames.length; ++j) {
                    /* Name der Zeile */
                    if (j == 0) {
                        playerGrid
                            .add(new JLabel(String.format("Spieler %d", i),
                                SwingConstants.CENTER));
                    }

                    /* Radio-Button für diese Zeile */
                    else {
                        _CenterAlignedRadioButton radioButton = new _CenterAlignedRadioButton();
                        _playerMatrixGroups[i - 1].add(radioButton);
                        radioButton.setActionCommand(String.format("%d", j));
                        playerGrid.add(radioButton);
                        _playerMatrixRadios[i - 1][j - 1] = radioButton;
                    }
                }
            }
        }

        /* Halte Abstand ein */
        this.add(createVerticalRigid(2));

        /* Füge ein komplettes Subpanel für die Konfiguration der Arena hinzu */
        _arenaConfigurationPanel = new ArenaConfigurationSubPanel();
        this.add(_arenaConfigurationPanel);

        /*
         * Höre auf Änderungen des Spinners für die maximale Spielerzahl und
         * aktiviere/deaktiviere Zeilen anhand des eingestellten Wertes
         */
        JSpinner maxPlayersSpinner = _arenaConfigurationPanel
            .getMaxPlayersSpinner();
        maxPlayersSpinner
            .addChangeListener((ChangeEvent event) -> _enableAllowedMatrixRows(
                (int) maxPlayersSpinner.getValue()));
    }
}
