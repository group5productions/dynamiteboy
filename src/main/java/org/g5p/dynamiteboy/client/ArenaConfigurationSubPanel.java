/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatter;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.tools.GameplayMode;

import net.sf.jaxodraw.gui.swing.spinner.JaxoSpinnerIntModel;

/**
 * Diese Klasse implementiert ein Subpanel, welches die Konfiguration einer
 * Spielarena ermöglicht. Die Konfiguration umfasst Breite und Höhe der Arena,
 * die Länge einer Runde, die Dauer des Rüstungseffekts, der Countdown bis zur
 * Explosion von Bomben, die maximale Spielerzahl und den Spielmodus. Es können
 * Einstellungen in das Panel oder aus dem Panel geladen werden, zudem werden
 * die Eingaben validiert.
 *
 * @author Thomas Weber
 */
public final class ArenaConfigurationSubPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final JSpinner _arenaWidthSpinner;
    private final JSpinner _arenaHeightSpinner;
    private final JSpinner _roundDurationSpinner;
    private final JSpinner _armorEffectSpinner;
    private final JSpinner _bombCountdownSpinner;
    private final JSpinner _maxPlayersSpinner;
    private final ButtonGroup _gameplayModeGroup;
    private final JRadioButton _standardARadio;
    private final JRadioButton _standardABRadio;

    /**
     * Erstellt einen JSpinner, dessen Textfeld nicht bearbeitet werden kann,
     * eine feste Breite von 100 Pixeln besitzt und bei gültigen Bearbeitungen
     * einen Commit durchführt.
     *
     * @param model
     *            Datenmodell des Spinners
     * @return Neuer JSpinner
     */
    private JSpinner _createUneditableSpinner(SpinnerModel model) {
        /* Erzeuge den Spinner */
        JSpinner spinner = new JSpinner(model);

        /* Setze die Größe */
        Dimension size = spinner.getPreferredSize();
        size.width = 100;
        spinner.setMaximumSize(size);
        spinner.setPreferredSize(size);
        spinner.setMinimumSize(size);

        /* Deaktiviere das Textfeld */
        JSpinner.DefaultEditor textEditor = (JSpinner.DefaultEditor) spinner
            .getEditor();
        JFormattedTextField textField = textEditor.getTextField();
        textField.setEditable(false);

        /* Aktiviere Commits im Formatter des Textfelds */
        ((DefaultFormatter) textField.getFormatter())
            .setCommitsOnValidEdit(true);
        return spinner;
    }

    /**
     * Validiert die Eingabe und gibt für diese eine Konfiguration der
     * Spielarena im Netzwerkformat zurück.
     *
     * @return Konfiguration der Spielarena
     * @throws IllegalArgumentException
     *             wenn mindestens eine Eingabe nicht zulässig ist
     */
    public ConfigureArenaGameServerCommand getArenaConfiguration()
        throws IllegalArgumentException {

        /* Ist ein Spielmodus ausgewählt? */
        GameplayMode gameplayMode = null;
        if (_standardARadio.isSelected())
            gameplayMode = GameplayMode.STANDARD_A;
        else if (_standardABRadio.isSelected())
            gameplayMode = GameplayMode.STANDARD_A_B;
        else {
            throw new IllegalArgumentException(
                "Es wurde kein Spielmodus ausgewählt");
        }

        /*
         * Lasse alle Einstellungen durch den Konstruktor von
         * ConfigureArenaGameServerCommand validieren.
         */
        return new ConfigureArenaGameServerCommand(
            (int) _arenaWidthSpinner.getValue(),
            (int) _arenaHeightSpinner.getValue(),
            (int) _roundDurationSpinner.getValue(),
            (int) _armorEffectSpinner.getValue(),
            (int) _bombCountdownSpinner.getValue(),
            (int) _maxPlayersSpinner.getValue(), gameplayMode);
    }

    /**
     * Schreibt die Werte einer Arenakonfiguration in die Steuerelemente.
     *
     * @param command
     *            Arenakonfiguration, mit der die Steuerelemente befüllt werden
     *            sollen. Ist diese null, so werden die Steuerelemente mit
     *            Standardwerten befüllt.
     */
    public void setArenaConfiguration(ConfigureArenaGameServerCommand command) {

        /* Kein Kommando übergeben? Lade Standardeinstellungen */
        if (command == null) {
            _arenaWidthSpinner.setValue(Globals.GAME_ARENA_WIDTH_DEFAULT);
            _arenaHeightSpinner.setValue(Globals.GAME_ARENA_HEIGHT_DEFAULT);
            _roundDurationSpinner
                .setValue(Globals.GAME_ROUND_DURATION_DEFAULT_MINUTES);
            _armorEffectSpinner
                .setValue(Globals.GAME_ARMOR_EFFECT_DURATION_SECONDS);
            _bombCountdownSpinner.setValue(Globals.GAME_BOMB_COUNTDOWN_SECONDS);
            _maxPlayersSpinner.setValue(Globals.ROUND_MAX_PLAYERS);
            _gameplayModeGroup.clearSelection();
        }

        /* Lade Werte aus Konfiguration */
        else {
            _arenaWidthSpinner.setValue(command.getArenaWidth());
            _arenaHeightSpinner.setValue(command.getArenaHeight());
            _roundDurationSpinner.setValue(command.getPlayTimeMinutes());
            _armorEffectSpinner.setValue(command.getArmorEffectSeconds());
            _bombCountdownSpinner.setValue(command.getBombExplosionSeconds());
            _maxPlayersSpinner.setValue(command.getMaxPlayers());
            if (command.getGameplayMode() == GameplayMode.STANDARD_A_B)
                _standardABRadio.setSelected(true);
            else
                _standardARadio.setSelected(true);
        }
    }

    /**
     * Gibt das Steuerelement zurück, welches die maximale Anzahl der Spieler in
     * der Arena kontrolliert.
     *
     * @return Der Beschreibung entsprechendes Steuerelement
     */
    public JSpinner getMaxPlayersSpinner() {
        return _maxPlayersSpinner;
    }

    /**
     * Erzeugt das Subpanel, welches die Konfiguration der Spielarena
     * ermöglicht.
     */
    public ArenaConfigurationSubPanel() {
        /* Kein Rahmen im Panel */
        super(true);

        JPanel horizontalPanel;
        JLabel label;

        /* Erzeuge Modelle für sämtliche Spinner */
        JaxoSpinnerIntModel _arenaWidthModel = new JaxoSpinnerIntModel(7, 21, 2,
            7);
        _arenaWidthModel.setIntValue(Globals.GAME_ARENA_WIDTH_DEFAULT);
        JaxoSpinnerIntModel _arenaHeightModel = new JaxoSpinnerIntModel(7, 21,
            2, 7);
        _arenaHeightModel.setIntValue(Globals.GAME_ARENA_HEIGHT_DEFAULT);
        SpinnerNumberModel _roundDurationModel = new SpinnerNumberModel(
            Globals.GAME_ROUND_DURATION_DEFAULT_MINUTES, 2, 10, 1);
        SpinnerNumberModel _armorEffectModel = new SpinnerNumberModel(
            Globals.GAME_ARMOR_EFFECT_DURATION_SECONDS, 5, 10, 1);
        SpinnerNumberModel _bombCountdownModel = new SpinnerNumberModel(
            Globals.GAME_BOMB_COUNTDOWN_SECONDS, 2, 5, 1);
        SpinnerNumberModel _maxPlayersModel = new SpinnerNumberModel(
            Globals.ROUND_MAX_PLAYERS, Globals.ROUND_MIN_PLAYERS,
            Globals.ROUND_MAX_PLAYERS, 1);

        /*
         * Horizontales Panel: Breite der Arena
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Breite der Arena in Felder:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _arenaWidthSpinner = _createUneditableSpinner(_arenaWidthModel);
        horizontalPanel.add(_arenaWidthSpinner);
        label.setLabelFor(_arenaWidthSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Höhe der Arena
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Höhe der Arena in Felder:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _arenaHeightSpinner = _createUneditableSpinner(_arenaHeightModel);
        horizontalPanel.add(_arenaHeightSpinner);
        label.setLabelFor(_arenaHeightSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Dauer der Runde
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Dauer der Runde in Minuten:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _roundDurationSpinner = _createUneditableSpinner(_roundDurationModel);
        horizontalPanel.add(_roundDurationSpinner);
        label.setLabelFor(_roundDurationSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Dauer des Rüstungseffekts
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Dauer des Effekts der Rüstung in Sekunden:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _armorEffectSpinner = _createUneditableSpinner(_armorEffectModel);
        horizontalPanel.add(_armorEffectSpinner);
        label.setLabelFor(_armorEffectSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Dauer bis Explosion
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Dauer bis zur Bombenexplosion in Sekunden:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _bombCountdownSpinner = _createUneditableSpinner(_bombCountdownModel);
        horizontalPanel.add(_bombCountdownSpinner);
        label.setLabelFor(_bombCountdownSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Anzahl der Spieler
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Maximale Anzahl Spieler auf dem Feld:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _maxPlayersSpinner = _createUneditableSpinner(_maxPlayersModel);
        horizontalPanel.add(_maxPlayersSpinner);
        label.setLabelFor(_maxPlayersSpinner);
        this.add(horizontalPanel);

        /*
         * Horizontales Panel: Spielmodus
         */
        horizontalPanel = createHorizontalJPanel();
        label = new JLabel("Modus, der gespielt werden soll:");
        horizontalPanel.add(label);
        horizontalPanel.add(Box.createHorizontalGlue());
        _standardARadio = new JRadioButton("Standard A");
        horizontalPanel.add(_standardARadio);
        horizontalPanel.add(createHorizontalRigid(1));
        _standardABRadio = new JRadioButton("Standard A+B");
        horizontalPanel.add(_standardABRadio);
        this.add(horizontalPanel);

        /* Bilde Gruppe aus Radio-Buttons */
        _gameplayModeGroup = new ButtonGroup();
        _gameplayModeGroup.add(_standardARadio);
        _gameplayModeGroup.add(_standardABRadio);
    }
}