/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.InetAddress;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.ManagementClientHandler;
import org.g5p.dynamiteboy.networking.MessageClient;
import org.g5p.dynamiteboy.networking.messages.QueryGameServersManagementServerResponse;
import org.g5p.dynamiteboy.tools.GameServerEntry;
import org.g5p.dynamiteboy.tools.GameplayMode;
import org.g5p.dynamiteboy.tools.PlayerCount;

/**
 * Diese Klasse implementiert das Panel, welches es ermöglicht, einen fremden
 * Spieleserver zu kontaktieren, um online im Multiplayer zu spielen.
 *
 * @author Thomas Weber
 */
public final class ServerListPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final JTable _gameServerTable;
    private final GameServerTableModel _serverTableModel;
    private final JButton _connectServerButton;

    /**
     * Diese interne Klasse implementiert das Datenmodell für die Tabelle, die
     * die Liste der verwalteten Spieleserver darstellt.
     */
    class GameServerTableModel extends AbstractTableModel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Index der Spalte "Name"
         */
        private static final int _COLUMN_NAME = 0;

        /**
         * Index der Spalte "Adresse"
         */
        private static final int _COLUMN_ADDRESS = 1;

        /**
         * Index der Spalte "Spieler"
         */
        private static final int _COLUMN_PLAYERS = 2;

        /**
         * Index der Spalte "Lobby?"
         */
        private static final int _COLUMN_LOBBY = 3;

        /**
         * Index der Spalte "Modus"
         */
        private static final int _COLUMN_MODE = 4;

        private List<GameServerEntry> _servers = null;

        /**
         * Gibt den Namen der Spalte für einen Spaltenindex col zurück.
         *
         * @param col
         *            Index der Spalte, ab 0 beginnend
         */
        @Override
        public String getColumnName(int col) {
            switch (col) {
            case _COLUMN_NAME:
                return "Name";
            case _COLUMN_ADDRESS:
                return "Adresse";
            case _COLUMN_PLAYERS:
                return "Spieler";
            case _COLUMN_LOBBY:
                return "Lobby?";
            case _COLUMN_MODE:
                return "Modus";
            }
            return "";
        }

        /**
         * Gibt die Anzahl der Spalten in der Tabelle zurück.
         *
         * @return Stets letzter Spaltenindex plus 1
         */
        @Override
        public int getColumnCount() {
            return _COLUMN_MODE + 1;
        }

        /**
         * Gibt die Anzahl der Zeilen in der Tabelle zurück.
         *
         * @return Anzahl der Zeilen = Anzahl der Spieleserver
         */
        @Override
        public int getRowCount() {
            /* Geht nur dann, wenn es eine Serverliste gibt */
            if (_servers != null)
                return _servers.size();
            else
                return 0;
        }

        /**
         * Fragt den Inhalt einer Tabellenzelle in Zeile arg0 und Spalte arg1
         * ab.
         *
         * @param arg0
         *            Zeile der Tabelle, beginnend ab 0
         * @param arg1
         *            Spalte der Tabelle, beginnend ab 0
         * @return Inhalt der Tabellenzelle
         */
        @Override
        public Object getValueAt(int arg0, int arg1) {
            /* Geht nur dann, wenn es eine Serverliste gibt */
            if (_servers == null)
                return null;

            /* Nicht die Grenzen der Liste überschreiten */
            if (arg0 >= _servers.size())
                return null;

            /* Lade den Eintrag für die Zeile */
            GameServerEntry entry = _servers.get(arg0);
            if (entry == null)
                return null;

            /* Gebe das zurück, was die Tabelle für die jeweilige Spalte will */
            switch (arg1) {
            case _COLUMN_NAME:
                return entry.getServerName();
            case _COLUMN_ADDRESS:
                return String.format("%s:%d", entry.getServerAddress(),
                    entry.getServerPort());
            case _COLUMN_PLAYERS:
                PlayerCount counts = entry.getPlayerCounts();
                return String.format("%d/%d", counts.getCurrentPlayerCount(),
                    counts.getMaxPlayerCount());
            case _COLUMN_LOBBY:
                return entry.isLobbyOpen() ? "Ja" : "Nein";
            case _COLUMN_MODE:
                /* Zeige Spielmodus nur an, wenn das Spiel läuft */
                if (!entry.isLobbyOpen())
                    return entry.getGameplayMode() == GameplayMode.STANDARD_A_B
                        ? "A+B" : "A";
                return "";
            }
            return null;
        }

        /**
         * Gibt den Klassentyp zurück, welchen alle Werte in einer einzelnen
         * Spalte besitzen.
         *
         * @param index
         *            0-basierter Index der Spalte, dessen Wertetyp abgefragt
         *            werden soll
         * @return Stets die Klasse "String"
         */
        @Override
        public Class<?> getColumnClass(int index) {
            return String.class;
        }

        /**
         * Diese Methode befüllt dieses Modell mit Spieleservern, die der
         * Verwaltungsserver momentan kennt.
         *
         * @param response
         *            Antwort des Verwaltungsservers mit Spieleservern
         */
        public void updateGameServers(
            QueryGameServersManagementServerResponse response) {
            /*
             * Extrahiere die Serverliste und benachrichtige die Tabelle über
             * eine Datenänderung
             */
            _servers = response.getServers();
            fireTableDataChanged();
        }

        /**
         * Diese Methode ruft einen Spieleserver nach seinem Zeilenindex ab.
         *
         * @param row
         *            Zeile des Spieleservers
         * @return Spieleserver oder null
         */
        public GameServerEntry getGameServerEntry(int row) {
            /*
             * Extrahiere den entsprechenden Eintrag und scheitere außerhalb der
             * Listengrenzen.
             */
            try {
                return _servers.get(row);
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }

        /**
         * Diese Methode passt die Spalten der Tabelle, in der das Modell
         * dargestellt wird, von der Größe her optimal an.
         *
         * @param table
         *            Tabelle, deren Spalten angepasst werden sollen
         */
        public void adjustTableColumnWidths(JTable table) {
            /*
             * Lege feste Breiten für die Spalten fest, sodass die Informationen
             * optimal dargestellt werden.
             */
            TableColumnModel model = table.getColumnModel();

            model.getColumn(_COLUMN_NAME).setPreferredWidth(260);
            model.getColumn(_COLUMN_ADDRESS).setPreferredWidth(130);
            model.getColumn(_COLUMN_PLAYERS).setMaxWidth(60);
            model.getColumn(_COLUMN_LOBBY).setMaxWidth(60);
            model.getColumn(_COLUMN_MODE).setMaxWidth(60);
        }
    }

    /**
     * Diese interne Klasse implementiert eine Aktion, die einen
     * Verbindungsversuch zum ausgewählten Spieleserver startet.
     */
    class ConnectToGameServerActionListener extends AbstractAction {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent evt) {
            /*
             * Frage nach dem aktuell ausgewählten Eintrag in der Tabelle, und
             * stelle für diesen eine Verbindung zum Spieleserver her.
             */
            GameServerEntry entry = _serverTableModel
                .getGameServerEntry(_gameServerTable.getSelectedRow());
            if (entry != null)
                _connectToServerWithPreCheck(entry);
        }
    }

    /**
     * Überprüft, ob die Lobby des ausgewählten Spieleservers nach Stand des
     * letzten Updates der Serverliste offen ist, und wechselt erst zum Panel
     * der Lobby, wenn dies der Fall ist. Ansonsten wird der Verbindungsversuch
     * abgebrochen.
     *
     * @param serverEntry
     *            Eintrag des Spieleservers, zu dem die Verbindung aufgebaut
     *            werden soll
     */
    private void _connectToServerWithPreCheck(GameServerEntry serverEntry) {
        /* Ist der Server im Spielmodus? Verweigere die Verbindung */
        if (!serverEntry.isLobbyOpen()) {
            ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                "Entschuldige, aber auf dem Server läuft gerade ein Spiel. "
                    + "Daher kann ich keine Verbindung zum Server "
                    + "herstellen.\n\nWarte, bis das Spiel auf dem Server zu "
                    + "Ende ist oder wähle einen anderen Server aus.");
            return;
        }

        /* Wechsle ins Lobby-Panel mit dem ausgewählten Server */
        ClientWindow.getInstance().setWindowState(
            ClientWindowState.GAME_SERVER_LOBBY_MENU, serverEntry);
    }

    /**
     * Diese Methode lädt alle konfigurierbaren Steuerelemente mit den bislang
     * gesetzten Optionen.
     */
    public boolean loadOptions() {
        boolean success = false;
        MessageClient managementClient = null;

        /* Hole Adresse aus den Einstellungen */
        String managementServerAddress = ClientWindow.getInstance().getOptions()
            .getManagementServerAddress();

        try {
            /*
             * Erstelle Client zum Verwaltungsserver
             */
            managementClient = new MessageClient(
                InetAddress.getByName(managementServerAddress),
                Globals.MANAGEMENT_SERVER_PORT, Globals.CONNECTION_TIMEOUT_MS);
            managementClient
                .setConnectionHandler(ManagementClientHandler.class);

            /*
             * Stelle die Verbindung zum Verwaltungsserver her
             */
            managementClient.startClient();
            ManagementClientHandler handler = (ManagementClientHandler) managementClient
                .getConnectionHandlerInstance();

            /* Callback, der das Datenmodell der Tabelle aktualisiert */
            handler.setGameServerListCallback(
                new Consumer<QueryGameServersManagementServerResponse>() {
                    @Override
                    public void accept(
                        QueryGameServersManagementServerResponse t) {
                        _serverTableModel.updateGameServers(t);
                    }
                });

            /* Warte, bis die Verbindung hergestellt wurde */
            handler.waitUntilReadyOrTimeout(Globals.CONNECTION_TIMEOUT_MS);

            /* Frage den Server nach Spieleservern */
            handler.queryGameServers();

            /* Warte auf Antwort */
            handler
                .waitUntilGameServerListReceived(Globals.CONNECTION_TIMEOUT_MS);

            /* Erfolg */
            success = true;
        } catch (Exception e) {
            /* Zeige eine Fehlermeldung an */
            ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                "Das Abrufen von Spieleservern vom Verwaltungsserver '%s' "
                    + "ist leider fehlgeschlagen.\n\nGrund: %s.\n\nVersuche es "
                    + "ein weiteres Mal, vielleicht klappt es dann!",
                managementServerAddress, e.getLocalizedMessage());
        } finally {
            /* Räume alle Ressourcen auf */
            if (managementClient != null)
                managementClient.stopClient();
        }

        /*
         * Setze den Zustand des "Verbinden"-Buttons abhängig davon, ob in der
         * Tabelle eine Zeile markiert ist
         */
        _connectServerButton.setEnabled(_gameServerTable.getSelectedRow() >= 0);
        return success;
    }

    /**
     * Erzeugt das Panel, mit welchem ein Mehrspielerspiel betreten oder
     * erstellt werden kann.
     */
    public ServerListPanel() {
        super("Verfügbare Spieleserver");

        /*
         * Button, um selbst einen Spieleserver zu eröffnen. Implementiere den
         * Wechsel in das Panel mittels Lambda-Ausdruck.
         */
        JButton customGameButton = createStyledJButton("Eigenes Spiel");
        customGameButton
            .addActionListener((ActionEvent event) -> ClientWindow.getInstance()
                .setWindowState(ClientWindowState.CUSTOM_SERVER_MENU));

        /*
         * Button, um die Serverliste neu zu laden. Implementiere das Neuladen
         * mittels Lambda-Ausdruck.
         */
        JButton reloadListButton = createStyledJButton("Liste neu laden");
        reloadListButton.addActionListener((ActionEvent event) -> {
            /* Wenn das Laden scheitert, gehe ins Hauptmenü */
            if (!loadOptions()) {
                ClientWindow.getInstance()
                    .setWindowState(ClientWindowState.MAIN_MENU);
            }
        });

        /*
         * Button, um sich mit dem aktuell ausgewählten Spieleserver zu
         * verbinden -- standardmäßig deaktiviert.
         */
        _connectServerButton = createStyledJButton("Verbinden");
        _connectServerButton
            .addActionListener(new ConnectToGameServerActionListener());

        /*
         * Button, um zum Hauptmenü zurückzukehren. Implementiert mittels
         * Lambda-Ausdruck.
         */
        JButton mainMenuButton = createStyledJButton("Hauptmenü");
        mainMenuButton.addActionListener((ActionEvent event) -> ClientWindow
            .getInstance().setWindowState(ClientWindowState.MAIN_MENU));

        /* Füge die Buttons zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(customGameButton, reloadListButton,
            _connectServerButton, mainMenuButton);

        /*
         * Erzeuge ein ScrollPane, in welchem eine Tabelle gescrollt werden
         * kann.
         */
        JScrollPane scrollPane = new JScrollPane();
        this.add(scrollPane);

        /* Tabelle, in der die Spieleserver dargestellt werden */
        _serverTableModel = new GameServerTableModel();
        _gameServerTable = new JTable(_serverTableModel);
        /*
         * Das Nimbus-Design hat sämtliche Farben in den Tabellenköpfen hart
         * codiert. Das tut den Entwickern überhaupt nicht Leid.
         */
        _gameServerTable.setFillsViewportHeight(true);
        _gameServerTable.setAutoCreateRowSorter(true);
        scrollPane.setViewportView(_gameServerTable);
        _serverTableModel.adjustTableColumnWidths(_gameServerTable);

        /*
         * Gebe bei Selektionswechsel (nur eine Selektion möglich) den
         * Verbindungsbutton frei.
         */
        _gameServerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _gameServerTable.getSelectionModel()
            .addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent event) {
                    /*
                     * Gebe den Button nur dann frei, wenn tatsächlich eine
                     * Reihe markiert ist.
                     */
                    if (!event.getValueIsAdjusting()
                        && _gameServerTable.getSelectedRow() >= 0) {
                        _connectServerButton.setEnabled(true);
                    }
                }
            });

        /*
         * Das Doppelklicken auf einen Server startet auch einen
         * Verbindungsversuch.
         */
        _gameServerTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                /* Frage die Zeile unter dem Mauszeiger ab */
                Point p = event.getPoint();
                int row = _gameServerTable.rowAtPoint(p);

                /*
                 * Wurde zwei Mal geklickt und die Reihe ist gültig, dann stelle
                 * die Verbindung für den Eintrag in der Zeile her.
                 */
                if (event.getClickCount() == 2 && row > -1) {
                    GameServerEntry entry = _serverTableModel
                        .getGameServerEntry(row);

                    _connectToServerWithPreCheck(entry);
                }
            }
        });

        /*
         * Eingabe von Enter oder Leertaste auf der Tastatur, während der
         * ausgewählte Server Fokus hat, startet genauso einen
         * Verbindungsversuch.
         *
         * https://stackoverflow.com/a/9095442
         */
        KeyStroke returnKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
        KeyStroke spaceKey = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0);
        InputMap tableInputMap = _gameServerTable
            .getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        tableInputMap.put(returnKey, "connectToGS");
        tableInputMap.put(spaceKey, "connectToGS");
        _gameServerTable.getActionMap().put("connectToGS",
            new ConnectToGameServerActionListener());
    }
}
