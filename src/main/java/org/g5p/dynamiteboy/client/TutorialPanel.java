/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.StringWriter;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.g5p.dynamiteboy.tools.ExceptionFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**-
 * Diese Klasse implementiert das Panel, welches das Tutorial zur Bedienung des
 * Spieles enthält. Das Tutorial liegt in den Ressourcen als einfach zu
 * bearbeitende HTML-Datei vor und wird in die hier erstellten Steuerelemente
 * geladen. Auch Bilder in Ressourcen werden unterstützt.
 *
 * @author Thomas Weber
 * @see <a href="http://alvinalexander.com/blog/post/jfc-swing/how-create-simple-swing-html-viewer-browser-java">
 *      How to create a simple Swing HTML viewer with Java (alvinalexander.com)
 *      </a>
 */
public final class TutorialPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Liest eine HTML-Datei von einem Ressourcenpfad ein, parst sie in ein
     * Document Object Model (DOM), passt die URLs aller im HTML enthaltenen
     * Bilder an, und gibt den HTML-Neuexport als String wieder aus.
     *
     * @param resourcePath
     *            Pfad der HTML-Ressource, die geladen werden soll
     * @return String, der das HTML aus der Ressource enthält, mit URLs zu
     *         Bildern angepasst
     */
    private String _parseHTMLFromResource(final String resourcePath) {
        /* Erzeuge eine DocumentBuilderFactory */
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(false);
        dbf.setValidating(false);
        dbf.setIgnoringComments(true);

        /* Erzeuge einen DocumentBuilder */
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            return "<html>Entschuldigung, aber bei der Darstellung des "
                + "Tutorials ist ein Problem aufgetreten.<br><br>Grund: "
                + e.getLocalizedMessage() + "<br><br>"
                + ExceptionFormatter.formatException(e);
        }

        /* Lese die Datei ein */
        Document doc = null;
        try {
            doc = db.parse(getClass().getResourceAsStream(resourcePath));
        } catch (SAXException | IOException e) {
            return "<html>Entschuldigung, aber bei der Darstellung des "
                + "Tutorials ist ein Problem aufgetreten.<br><br>Grund: "
                + e.getLocalizedMessage() + "<br><br>"
                + ExceptionFormatter.formatException(e);
        }

        /*
         * Durchsuche das Dokument nach <img>-Tags und benutze ihre
         * src-Attribute, um eine passende URL zu erzeugen, welches auf die im
         * .jar enthaltene Ressource zeigt.
         */
        NodeList images = doc.getElementsByTagName("img");
        if (images != null) {
            for (int i = 0; i < images.getLength(); ++i) {
                final Element image = (Element) images.item(i);
                final String url = image.getAttribute("src");
                image.setAttribute("src",
                    getClass().getResource(url).toString());
            }
        }

        /* Erzeuge einen Writer für das Dokument */
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);

        /* Führe eine Transformation des DOM durch */
        TransformerFactory tf = TransformerFactory.newInstance();
        try {
            Transformer transformer = tf.newTransformer();
            /* Lasse XML-Deklarationen weg, die gehören nicht in HTML */
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
                "yes");
            transformer.transform(domSource, result);
        } catch (TransformerException e) {
            return "<html>Entschuldigung, aber bei der Darstellung des "
                + "Tutorials ist ein Problem aufgetreten.<br><br>Grund: "
                + e.getLocalizedMessage() + "<br><br>"
                + ExceptionFormatter.formatException(e);
        }

        /* Gebe den String zurück */
        return writer.toString();
    }

    /**
     * Erzeugt das Panel, in welchem das Tutorial des Spiels dargestellt wird.
     */
    public TutorialPanel() {
        super("Tutorial");

        /*
         * Button zur Rückkehr ins Hauptmenü. Implementiere es mittels
         * Lambda-Ausdruck.
         */
        final JButton mainMenuButton = createStyledJButton("Hauptmenü");
        mainMenuButton.addActionListener((ActionEvent event) -> ClientWindow
            .getInstance().setWindowState(ClientWindowState.MAIN_MENU));

        /* Füge die Buttons zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(mainMenuButton);

        /*
         * Erstelle ein Scroll-Pane, um das Label scrollen zu können. Mache den
         * Hintergrund weiß, so wie im Internetbrowser. Erhöhe die Scrollrate,
         * um ein angenehmeres Scrollgefühl zu bekommen.
         */
        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.getViewport().setBackground(Color.WHITE);
        scrollPane.getVerticalScrollBar().setUnitIncrement(25);

        /* Setze Größe des Panes fest */
        final Dimension paneSize = new Dimension(
            PANEL_DEFAULT_CONTENT_WIDTH_PIXELS, 550);
        scrollPane.setPreferredSize(paneSize);
        scrollPane.setMaximumSize(paneSize);
        this.add(scrollPane);

        /*
         * Füge ein JEditorPane mit einem HTMLEditorKit hinzu. Dieses kann HTML
         * besser darstellen als ein JLabel. Deaktiviere das Bearbeiten.
         */
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
        HTMLEditorKit kit = new HTMLEditorKit();
        editorPane.setEditorKit(kit);
        editorPane.setDocument(kit.createDefaultDocument());
        editorPane.setText(_parseHTMLFromResource("/tutorial.html"));
        scrollPane.setViewportView(editorPane);
    }
}
