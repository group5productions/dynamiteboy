/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;

/**
 * Diese Klasse packt Einstellungen für den Offline-Modus und ermöglicht einen
 * Wechsel in die Arena in diesen Modus.
 *
 * @author Thomas Weber
 */
public final class ArenaPanelOfflineModeSettings {
    /**
     * Legt den Betriebsmodus eines einzelnen Spielers fest.
     */
    public enum PlayerOperationMode {
        /**
         * Spieler ist nicht auf dem Spielfeld präsent
         */
        DISABLED,

        /**
         * Spieler wird durch Tastenbelegung für Spieler 1 kontrolliert
         */
        PHYSICAL_PLAYER_1,

        /**
         * Spieler wird durch Tastenbelegung für Spieler 2 kontrolliert
         */
        PHYSICAL_PLAYER_2,

        /**
         * Spieler wird durch KI kontrolliert
         */
        AI_CONTROLLED
    }

    private final PlayerOperationMode _playerOperationModes[];
    private final ConfigureArenaGameServerCommand _configureArenaCommand;

    /**
     * Erzeugt eine Objekt-Instanz, welches Einstellungen für den Offline-Modus
     * der Arena verpackt. Diese Klasse führt keine Parameterüberprüfungen
     * durch.
     *
     * @param configureArenaCommand
     *            Kommando im Netzwerkformat, welches die Spielarena
     *            konfiguriert
     * @param playerOperationModes
     *            Betriebsmodi für die maximal zulässige Zahl an Spielern auf
     *            dem Feld ({@link Globals#ROUND_MAX_PLAYERS}).
     */
    public ArenaPanelOfflineModeSettings(
        ConfigureArenaGameServerCommand configureArenaCommand,
        PlayerOperationMode... playerOperationModes) {

        /* Überprüfe Spielerzahl */
        if (playerOperationModes.length != Globals.ROUND_MAX_PLAYERS) {
            throw new IllegalArgumentException(
                String.format("Es müssen genau %d Spieler angegeben werden",
                    Globals.ROUND_MAX_PLAYERS));
        }

        /* Speichere Einträge */
        this._playerOperationModes = new PlayerOperationMode[Globals.ROUND_MAX_PLAYERS];
        for (int i = 0; i < this._playerOperationModes.length; ++i)
            this._playerOperationModes[i] = playerOperationModes[i];
        this._configureArenaCommand = configureArenaCommand;
    }

    /**
     * Gibt den Betriebsmodus für einen ausgewählten Spieler zurück.
     *
     * @param playerNumber
     *            Nummer des Spielers
     * @return Betriebsmodus des Spielers mit der angegebenen Nummer
     */
    public PlayerOperationMode getOperationForPlayer(int playerNumber) {
        /* Spielernummer gültig? */
        if (playerNumber < 1 || playerNumber > Globals.ROUND_MAX_PLAYERS) {
            throw new IllegalArgumentException(
                String.format("Spielernummer %d nicht zulässig", playerNumber));
        }
        return _playerOperationModes[playerNumber - 1];
    }

    /**
     * Gibt das Kommando zurück, welches die Nicht-Spieler-Aspekte der Arena
     * konfiguriert.
     *
     * @return Kommando der Konfiguration der Arena
     */
    public ConfigureArenaGameServerCommand getConfigureArenaCommand() {
        return _configureArenaCommand;
    }

    /**
     * Validiert die Einstellungen des Offlinemodus.
     *
     * @throws IllegalArgumentException
     *             wenn mindestens eine Einstellung nicht zulässig ist
     */
    public void validateSettings() throws IllegalArgumentException {
        /*-
         * - Wir brauchen mindestens zwei Spieler auf dem Feld.
         * - Es darf höchstens 1 Spieler drin sein, der von Spieler 1
         *   kontrolliert wird.
         * - Es darf höchstens 1 Spieler drin sein, der von Spieler 2
         *   kontrolliert wird.
         */
        int playersOnField = 0;
        int playersControlledBy1 = 0;
        int playersControlledBy2 = 0;
        for (int i = 0; i < _playerOperationModes.length; ++i) {
            if (_playerOperationModes[i] != PlayerOperationMode.DISABLED) {
                ++playersOnField;
                if (_playerOperationModes[i] == PlayerOperationMode.PHYSICAL_PLAYER_1)
                    ++playersControlledBy1;
                else if (_playerOperationModes[i] == PlayerOperationMode.PHYSICAL_PLAYER_2)
                    ++playersControlledBy2;
            }
        }
        if (playersOnField < Globals.ROUND_MIN_PLAYERS) {
            throw new IllegalArgumentException(String.format(
                "Es müssen sich mindestens %d Spieler auf dem Spielfeld befinden",
                Globals.ROUND_MIN_PLAYERS));
        }
        if (playersControlledBy1 >= 2) {
            throw new IllegalArgumentException(
                "Spieler 1 kann nur eine Spielfigur kontrollieren");
        }
        if (playersControlledBy2 >= 2) {
            throw new IllegalArgumentException(
                "Spieler 2 kann nur eine Spielfigur kontrollieren");
        }

        /*
         * Die Einstellungen der Spielarena wurden bereits durch den Konstruktor
         * der Klsase der Nachricht validiert.
         */
    }
}