/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.client.ArenaPanelOfflineModeSettings.PlayerOperationMode;
import org.g5p.dynamiteboy.engine.GameArenaClient;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.engine.objects.GameActor;
import org.g5p.dynamiteboy.engine.objects.GameObjectIdentifier;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.PlayerActionGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.RoundEndHighscoreClientCommand;
import org.g5p.dynamiteboy.tools.ConsumerWithThis;
import org.g5p.dynamiteboy.tools.GameModeFieldEntry;
import org.g5p.dynamiteboy.tools.GameModePlayerEntry;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * Diese Klasse implementiert das Panel, welches die Arena des Spiels rendert.
 * Es unterstützt den Betrieb sowohl offline (durch eigene Operation der
 * Spiele-Engine) als auch online (komplett netzwerkgestützte Operation mit der
 * Spiele-Engine auf einem anderen Server).
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class ArenaPanel extends JPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * dieses Feld Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Mathematisch korrekt gerundeter Abstand zwischen Ausführungen des
     * KI-Schritts. (Kann sich vom Abstand zwischen Ticks des Servers
     * unterscheiden.)
     */
    private static final long _AI_STEP_TIME_MARGIN_MS = Math
        .round(1000.0 / Globals.GAME_TICKS_PER_SECOND);

    private static final Logger _logger = Logger
        .getLogger(GameActor.class.getSimpleName());

    private GameArenaClient _gameArenaClient;
    private BufferedImage _gameSurface;
    private GameEngine _gameEngine;
    private ArenaKeyListener _keyListener;

    private PlayerAIExecutor _playerAIExecutor;

    private ArenaPanelOnlineModeSettings _onlineSettings;
    private Consumer<RoundEndHighscoreClientCommand> _onlineOldHighscoreCallback;
    private Consumer<ArenaUpdatedClientCommand> _onlineOldArenaUpdateCallback;

    private ArenaPanelOfflineModeSettings _offlineSettings;
    private int _offlinePlayer1Control;
    private int _offlinePlayer2Control;

    /**
     * Ein Runnable, der periodisch aufgerufen wird und die KI für jeden
     * ungefähren Schritt ausführt.
     */
    private final class PlayerAIExecutor implements Runnable {
        private final ScheduledExecutorService _aiExecutor;
        private ScheduledFuture<?> _aiPlayerFuture;
        private boolean _shutdown;
        private boolean _running;

        private final int _onlineModePlayerNumber;
        private final boolean _isOnline;

        private PlayerAI _aiPlayerOnline;
        private PlayerAI _aiPlayersOffline[];
        private int _activeAICount;

        /**
         * Initialisiert eine neue KI.
         *
         * @param onlineModePlayerNumber
         *            Nummer des eigenen Spielers (1-4) im Onlinemodus, welche
         *            für die Ausführung der KI benötigt wird, um den eigenen
         *            Spieler zu lokalisieren. Im Offlinemodus muss diese Nummer
         *            stets -1 sein; die KIs der Spieler werden auf Basis der
         *            Offline-Konfiguration initialisiert.
         */
        public PlayerAIExecutor(int onlineModePlayerNumber) {
            /*
             * Erzeuge einen UnconfigurableScheduledExecutor, um die Ausführung
             * der KI präzise zu timen. (Dieser ist von den Java-Entwicklern als
             * Ersatz für die alte Timer-Klasse vorgesehen.)
             *
             * Weitere Details unter https://stackoverflow.com/a/14423578
             */
            ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(
                1);
            exec.setRemoveOnCancelPolicy(true);
            _aiExecutor = Executors
                .unconfigurableScheduledExecutorService(exec);

            this._shutdown = false;
            this._running = false;

            this._onlineModePlayerNumber = onlineModePlayerNumber;
            this._isOnline = onlineModePlayerNumber > -1;

            this._aiPlayerOnline = null;
            this._aiPlayersOffline = new PlayerAI[Globals.ROUND_MAX_PLAYERS];
            this._activeAICount = 0;
        }

        /**
         * Diese Methode führt einen Schritt der KI jedes durch eine KI
         * kontrollierten Spielers durch.
         */
        @Override
        public void run() {
            /* Nur, wenn die KI läuft */
            if (!_running)
                return;

            /*
             * Offline: Führe einen Schritt für jede initialisierte KI aus
             */
            if (!_isOnline) {
                for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
                    PlayerAI currentAI = _aiPlayersOffline[i - 1];
                    if (currentAI != null) {
                        PlayerAction action = currentAI.performAIStep();
                        if (action != null)
                            _gameEngine.performActorAction(i, action);
                    }
                }
            }

            /*
             * Online: Führe einen Schritt für die spielerkontrollierende KI aus
             */
            else {
                PlayerAction action = _aiPlayerOnline.performAIStep();
                if (action != null) {
                    /* Sende den Schritt über das Netzwerk */
                    try {
                        _onlineSettings.getConnectionHandler().doPlayerAction(
                            new PlayerActionGameServerCommand(action));
                    } catch (Exception e) {
                    }
                }
            }
        }

        /**
         * Diese Methode wird ausgeführt, wenn die Arena aktualisiert wird.
         *
         * @param command
         *            Update, welches die Ausführung dieser Methode ausgelöst
         *            hat
         */
        public void onArenaUpdated(ArenaUpdatedClientCommand command) {
            /* Nur, wenn KIs in Betrieb sind */
            if (!_running)
                return;

            /*
             * Haben wir eine Spielerliste im Update? Durchsuche diese nach
             * getöteten KIs, um sie abzuschalten.
             */
            List<GameModePlayerEntry> playerList = command.getPlayers();
            if (playerList != null && !playerList.isEmpty()) {
                boolean playerAlive[] = new boolean[Globals.ROUND_MAX_PLAYERS];

                /* Finde heraus, welche Spieler noch leben */
                for (GameModePlayerEntry player : playerList) {
                    int number = player.getNumber();
                    if (number >= 1 && number <= Globals.ROUND_MAX_PLAYERS)
                        playerAlive[number - 1] = true;
                }

                /*
                 * Online: Ist die KI für den eigenen Spieler noch aktiv? Wenn
                 * nicht, schalte diese ab.
                 */
                if (_isOnline) {
                    if (!playerAlive[_onlineModePlayerNumber - 1]) {
                        _logger.info(
                            String.format("Schalte KI für Spieler %d ab\n",
                                _onlineModePlayerNumber));

                        _aiPlayerOnline = null;
                        --_activeAICount;
                    }
                }

                /*
                 * Offline: Suche nach Spielern, die nicht mehr leben und von
                 * einer KI gesteuert wurden; schalte diese ab
                 */
                else {
                    for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
                        if (playerAlive[i - 1])
                            continue;

                        if (_aiPlayersOffline[i - 1] != null) {
                            _logger.info(String
                                .format("Schalte KI für Spieler %d ab\n", i));
                            _aiPlayersOffline[i - 1] = null;
                            --_activeAICount;
                        }
                    }
                }

                /* Schalte den Executor ab, wenn keine KIs mehr aktiv sind */
                if (_activeAICount == 0) {
                    _logger.info("Alle KIs abgeschaltet");
                    stopExecution();
                }
            }

            /*
             * Es ist Updatezeit. Breche den aktuellen KI-Durchlauf ab und reihe
             * ihn neu ein. Gebe der Engine im Offlinemodus 15 Millisekunden
             * mehr Zeit für ein weiteres Update, um die Anzahl der wiederholten
             * Sendungen zu reduzieren.
             */
            if (_aiPlayerFuture != null) {
                _aiPlayerFuture.cancel(false);
                _aiPlayerFuture = _aiExecutor.scheduleAtFixedRate(this, 15,
                    _AI_STEP_TIME_MARGIN_MS, TimeUnit.MILLISECONDS);
            }
        }

        /**
         * Diese Methode startet die KI der Spieler.
         */
        public void startExecution() {
            /* Nur, wenn die KI nicht bereits heruntergefahren wurde */
            if (_shutdown)
                return;

            /*
             * Offline: Initialisiere die KI der Spieler, die als KI markiert
             * sind
             */
            if (!_isOnline) {
                for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
                    if (_offlineSettings.getOperationForPlayer(
                        i) == PlayerOperationMode.AI_CONTROLLED) {
                        _aiPlayersOffline[i - 1] = new PlayerAI(
                            _gameArenaClient, i);
                        ++_activeAICount;
                    }
                }
            }

            /* Online: Initialisiere KI des Online-Spielers */
            else {
                _aiPlayerOnline = new PlayerAI(_gameArenaClient,
                    _onlineModePlayerNumber);
                ++_activeAICount;
            }

            /*
             * Lege ihn als Oneshot an und schedule jedes Mal im Arenaupdate neu
             * (auch wenn dieser bereits periodisch ausgeführt wird, um das
             * Timing zwischen KI und Engine, die miteinander nicht
             * synchronisieren, zu verfeinern)
             */
            _aiPlayerFuture = _aiExecutor.scheduleAtFixedRate(this, 0,
                _AI_STEP_TIME_MARGIN_MS, TimeUnit.MILLISECONDS);
            _running = true;
        }

        /**
         * Stoppt die Ausführung der KI.
         */
        public void stopExecution() {
            if (_aiPlayerFuture != null) {
                _aiPlayerFuture.cancel(false);
                _aiPlayerFuture = null;
            }
            _aiExecutor.shutdown();
            _shutdown = true;
            _running = false;
        }
    }

    /**
     * Eine Unterklasse, die Tasteneingaben entgegen nimmt und zu Aktionen der
     * Spieler verarbeitet.
     */
    private final class ArenaKeyListener implements KeyListener {
        private final boolean _isOnline;

        /**
         * Erzeugt einen KeyListener, der Tasteneingaben bezüglich des Gameplays
         * verarbeitet.
         *
         * @param isOnline
         *            true, wenn die Steuerung für den Onlinemodus aktiviert
         *            wird, ansonsten false
         */
        public ArenaKeyListener(boolean isOnline) {
            _isOnline = isOnline;
        }

        /**
         * Wird aufgerufen, wenn eine Taste auf der Tastatur gedrückt (gehalten)
         * wird.
         *
         * @param event
         *            Zum Ereignis gehörende Daten
         */
        @Override
        public void keyPressed(KeyEvent event) {
            /*
             * Onlinemodus: Nur der eigene Spieler kann gesteuert werden. Aktion
             * wird über das Netzwerk gesendet.
             */
            if (_isOnline) {
                PlayerAction action = null;

                /* Wähle nach Tastencode aus */
                switch (event.getKeyCode()) {
                case KeyEvent.VK_W:
                case KeyEvent.VK_UP:
                    action = PlayerAction.MOVE_UP;
                    break;
                case KeyEvent.VK_A:
                case KeyEvent.VK_LEFT:
                    action = PlayerAction.MOVE_LEFT;
                    break;
                case KeyEvent.VK_S:
                case KeyEvent.VK_DOWN:
                    action = PlayerAction.MOVE_DOWN;
                    break;
                case KeyEvent.VK_D:
                case KeyEvent.VK_RIGHT:
                    action = PlayerAction.MOVE_RIGHT;
                    break;
                case KeyEvent.VK_SPACE:
                case KeyEvent.VK_CONTROL:
                    action = PlayerAction.PLANT_BOMB;
                    break;
                }

                /* Sende Nachricht */
                if (action != null) {
                    try {
                        _onlineSettings.getConnectionHandler().doPlayerAction(
                            new PlayerActionGameServerCommand(action));
                    } catch (Exception e) {
                    }
                }
            }

            /*
             * Offlinemodus: Bis zu zwei Spieler sind möglich. Aktion wird
             * direkt an die Spieleengine übergeben.
             */
            else {
                PlayerAction actionPlayer1 = null;
                PlayerAction actionPlayer2 = null;
                switch (event.getKeyCode()) {

                /*
                 * Steuerung für Spieler 1
                 */
                case KeyEvent.VK_W:
                    actionPlayer1 = PlayerAction.MOVE_UP;
                    break;
                case KeyEvent.VK_A:
                    actionPlayer1 = PlayerAction.MOVE_LEFT;
                    break;
                case KeyEvent.VK_S:
                    actionPlayer1 = PlayerAction.MOVE_DOWN;
                    break;
                case KeyEvent.VK_D:
                    actionPlayer1 = PlayerAction.MOVE_RIGHT;
                    break;
                case KeyEvent.VK_SPACE:
                    actionPlayer1 = PlayerAction.PLANT_BOMB;
                    break;

                /*
                 * Steuerung für Spieler 2
                 */
                case KeyEvent.VK_UP:
                    actionPlayer2 = PlayerAction.MOVE_UP;
                    break;
                case KeyEvent.VK_LEFT:
                    actionPlayer2 = PlayerAction.MOVE_LEFT;
                    break;
                case KeyEvent.VK_DOWN:
                    actionPlayer2 = PlayerAction.MOVE_DOWN;
                    break;
                case KeyEvent.VK_RIGHT:
                    actionPlayer2 = PlayerAction.MOVE_RIGHT;
                    break;
                case KeyEvent.VK_CONTROL:
                    actionPlayer2 = PlayerAction.PLANT_BOMB;
                    break;
                }

                /* Sende Aktion(en) an die Engine */
                if (_offlinePlayer1Control > -1) {
                    if (actionPlayer1 != null)
                        _gameEngine.performActorAction(_offlinePlayer1Control,
                            actionPlayer1);
                }
                if (_offlinePlayer2Control > -1) {
                    if (actionPlayer2 != null)
                        _gameEngine.performActorAction(_offlinePlayer2Control,
                            actionPlayer2);
                }
            }
        }

        /**
         * Wird aufgerufen, wenn eine Taste auf der Tastatur losgelassen wird.
         *
         * @param event
         *            Zum Ereignis gehörende Daten
         */
        @Override
        public void keyReleased(KeyEvent evenz) {
            /* Nichts tun */
        }

        /**
         * Wird aufgerufen, wenn ein Unicode-Zeichen auf der Tastatur eingeben
         * wird. (Klingt eigentlich doof, Tastaturen haben keinen Plan von
         * Unicode, das ist eine High-Level-Sonderbehandlung durch Anwenden der
         * Modifier (Shift, ...) auf eine normalen Tastendruck, der das
         * entsprechende Unicode-Zeichen generiert und für dieses gibt es dieses
         * Event. Brauchen wir nicht, muss aber dabei sein.)
         *
         * @param event
         *            Zum Ereignis gehörende Daten
         */
        @Override
        public void keyTyped(KeyEvent event) {
            /* Nichts tun */
        }
    }

    /**
     * Dieser Callback reagiert auf Arenaupdates. Er zeichnet die geänderten
     * Teile der Arena neu, speichert diese lokal und erzwingt ein Neumalen des
     * Panels.
     */
    private final class ArenaPanelArenaUpdateCallback
        extends ConsumerWithThis<ArenaPanel, ArenaUpdatedClientCommand> {

        /**
         * Konstruiert das Callback mit einem existierenden Objekt als "this".
         *
         * @param originalThis
         *            Instanz des Panels
         */
        public ArenaPanelArenaUpdateCallback(ArenaPanel originalThis) {
            super(originalThis);
        }

        /**
         * Wird aufgerufen, wenn ein neues Update eintrifft.
         *
         * @param command
         *            Neu eingetroffenes Arenaupdate
         */
        @Override
        public void accept(ArenaUpdatedClientCommand command) {
            List<GameModeFieldEntry> updatedFields = command.getUpdatedFields();
            if (updatedFields.isEmpty())
                return;

            /* Zeichne das Update in das Surface */
            _paintUpdate(updatedFields);

            /* Aktualisiere die lokale Arena */
            _gameArenaClient.applyDeltaUpdates(command);

            /* Aktualisiere die KI, sofern eine läuft */
            if (_playerAIExecutor != null)
                _playerAIExecutor.onArenaUpdated(command);

            /* Erzwinge Neuzeichnen des Panels */
            originalThis.repaint();
        }
    }

    /**
     * Dieser Callback reagiert auf einen eintreffenden Highscore. An diesem
     * Punkt wird die Arena verlassen und auf den Highscore geschaltet.
     */
    private final class ArenaPanelHighscoreCallback
        implements Consumer<RoundEndHighscoreClientCommand> {

        /**
         * Wird aufgerufen, wenn ein neuer Highscore eintritt.
         *
         * @param command
         *            Neu eingetroffener Highscore
         */
        @Override
        public void accept(RoundEndHighscoreClientCommand command) {
            /*
             * Gebe den Highscore an das Panel weiter. setWindowState() kümmert
             * sich darum, dass man vom Highscore zurück in die Lobby kommt.
             */
            ClientWindow.getInstance()
                .setWindowState(ClientWindowState.STATISTICS_MENU, command);
        }
    }

    /**
     * Diese Unterklasse implementiert das Callback, welches ausgeführt, wenn
     * die Spieleengine terminiert.
     */
    private final class _GameEngineEndCallback implements Runnable {
        /**
         * Wird ausgeführt, wenn die Engine des Spiels terminiert. Wird das
         * Fenster zum Zeitpunkt der Terminierung nicht geschlossen, wechsle in
         * das Statistikmenü.
         */
        @Override
        public void run() {
            /* Nur, wenn die Anwendung nicht beendet wird */
            ClientWindow window = ClientWindow.getInstance();
            if (window.isWindowClosing())
                return;

            /*
             * Wechsle ins Statistikmenü mit der Statistik aus der Engine,
             * terminiert die Engine
             */
            window.setWindowState(ClientWindowState.STATISTICS_MENU,
                _gameEngine.getPlayerScores());
        }
    }

    /**
     * Diese Methode zeichnet das neueste Update in das Surface, welches später
     * von der GUI gemalt wird.
     */
    private void _paintUpdate(List<GameModeFieldEntry> update) {
        /*
         * Bestimme die Zielgröße der Bilder unter der Anwendung des
         * Skalierfaktors.
         */
        int imageSizePixels = ClientWindow.getInstance().getOptions()
            .getArenaTileScaleFactor() * 16;

        /* Lege Grafikobjekt zum Zeichnen an */
        Graphics g = _gameSurface.createGraphics();

        for (GameModeFieldEntry entry : update) {
            /* Extrahiere Informationen aus dem Eintrag */
            int x = entry.getX();
            int y = entry.getY();
            GameObjectIdentifier ident = GameObjectIdentifier
                .fromValue(entry.getId());

            /* Für den Fall, dass ein unbekannter Bezeichner gemalt wird */
            if (ident == null)
                ident = GameObjectIdentifier.WALKABLE_TILE;

            /*
             * Wähle Vordergrund- und/oder Hintergrundbild zum Zeichnen aus
             */
            BufferedImage selectedBackground = null;
            BufferedImage selectedForeground = null;

            switch (ident) {
            case BREAKABLE_WALL:
                selectedBackground = Images.breakableWallTile;
                break;
            case EXPLOSION:
                selectedBackground = Images.bombExplosion;
                break;
            case FIXED_WALL:
                selectedBackground = Images.wallTile;
                break;
            case WALKABLE_TILE:
                selectedBackground = Images.walkableTile;
                break;

            /* Spezialfall Bomben */
            case BOMB_STATE_1_PLAYER_1:
                selectedBackground = Images.player1PushedBackground;
                selectedForeground = Images.bombState1;
                break;
            case BOMB_STATE_1_PLAYER_2:
                selectedBackground = Images.player2PushedBackground;
                selectedForeground = Images.bombState1;
                break;
            case BOMB_STATE_1_PLAYER_3:
                selectedBackground = Images.player3PushedBackground;
                selectedForeground = Images.bombState1;
                break;
            case BOMB_STATE_1_PLAYER_4:
                selectedBackground = Images.player4PushedBackground;
                selectedForeground = Images.bombState1;
                break;
            case BOMB_STATE_2_PLAYER_1:
                selectedBackground = Images.player1PushedBackground;
                selectedForeground = Images.bombState2;
                break;
            case BOMB_STATE_2_PLAYER_2:
                selectedBackground = Images.player2PushedBackground;
                selectedForeground = Images.bombState2;
                break;
            case BOMB_STATE_2_PLAYER_3:
                selectedBackground = Images.player3PushedBackground;
                selectedForeground = Images.bombState2;
                break;
            case BOMB_STATE_2_PLAYER_4:
                selectedBackground = Images.player4PushedBackground;
                selectedForeground = Images.bombState2;
                break;
            case BOMB_STATE_3_PLAYER_1:
                selectedBackground = Images.player1PushedBackground;
                selectedForeground = Images.bombState3;
                break;
            case BOMB_STATE_3_PLAYER_2:
                selectedBackground = Images.player2PushedBackground;
                selectedForeground = Images.bombState3;
                break;
            case BOMB_STATE_3_PLAYER_3:
                selectedBackground = Images.player3PushedBackground;
                selectedForeground = Images.bombState3;
                break;
            case BOMB_STATE_3_PLAYER_4:
                selectedBackground = Images.player4PushedBackground;
                selectedForeground = Images.bombState3;
                break;

            /* Spezialfall Spieler */
            case PLAYER_1_ARMORED:
                selectedForeground = Images.playerArmorOverlay;
                /* "fall through", aber Overlay für Rüstung wird gemalt */
            case PLAYER_1:
                /* Erst Hintergrund, dann der Spieler */
                g.drawImage(Images.player1Background, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
                selectedBackground = Images.playerAvatar;
                break;
            case PLAYER_2_ARMORED:
                selectedForeground = Images.playerArmorOverlay;
                /* "fall through", aber Overlay für Rüstung wird gemalt */
            case PLAYER_2:
                /* Erst Hintergrund, dann der Spieler */
                g.drawImage(Images.player2Background, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
                selectedBackground = Images.playerAvatar;
                break;
            case PLAYER_3_ARMORED:
                selectedForeground = Images.playerArmorOverlay;
                /* "fall through", aber Overlay für Rüstung wird gemalt */
            case PLAYER_3:
                /* Erst Hintergrund, dann der Spieler */
                g.drawImage(Images.player3Background, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
                selectedBackground = Images.playerAvatar;
                break;
            case PLAYER_4_ARMORED:
                selectedForeground = Images.playerArmorOverlay;
                /* "fall through", aber Overlay für Rüstung wird gemalt */
            case PLAYER_4:
                /* Erst Hintergrund, dann der Spieler */
                g.drawImage(Images.player4Background, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
                selectedBackground = Images.playerAvatar;
                break;

            /* Spezialfall Powerup */
            case POWERUP_ARMOR:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupArmor;
                break;
            case POWERUP_BOMB_RADIUS:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupExplosionRadius;
                break;
            case POWERUP_MORE_BOMBS:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupBombs;
                break;

            case POWERUP_FASTER_RUN:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupPlayerFaster;
                break;
            case POWERUP_KICK_BOMBS:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupPlayerKickBomb;
                break;
            case POWERUP_SUPER_BOMB:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupSuperBomb;
                break;
            case POWERUP_BOMB_RADIUS_MAX:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupExplosionRadiusMax;
                break;
            case POWERUP_BOMB_RUNNER:
                selectedBackground = Images.powerupBackground;
                selectedForeground = Images.powerupRunOverBombs;
                break;

            default:
                break;
            }

            /*
             * Zeichne Hintergrund und Vordergrund, wenn vorhanden
             */
            if (selectedBackground != null) {
                g.drawImage(selectedBackground, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
            }

            /* Zeichne einfaches Bild */
            if (selectedForeground != null) {
                g.drawImage(selectedForeground, x * imageSizePixels,
                    y * imageSizePixels, imageSizePixels, imageSizePixels,
                    null);
            }
        }

        /* Verwerfe das Grafikobjekt wieder */
        g.dispose();
    }

    /**
     * Diese überlagerte Methode zeichnet das intern gemalte Bild der Arena,
     * welches mit jedem eintreffenden Update aktualisiert wird.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        /* Zeichne das vorgerenderte Surface */
        g.drawImage(_gameSurface, 0, 0, null);
    }

    /**
     * Diese überlagerte Methode gibt die gewünschte Breite sowie Höhe des
     * Panels zurück.
     *
     * @return gewünschte Breite und Höhe des Panels
     */
    @Override
    public Dimension getPreferredSize() {
        /*
         * Benutze die Größe des Surfaces, welches wir für die Darstellung
         * erstellt haben
         */
        return new Dimension(_gameSurface.getWidth(), _gameSurface.getHeight());
    }

    /**
     * Startet ein Offline-Spiel mit den angegebenen Einstellungen. Da keine
     * Netzwerkkommunikation erfolgt, muss die Spiele-Engine durch dieses Panel
     * betrieben werden.
     *
     * @param offlineSettings
     *            Einstellungen für dieses Spiel, welche aus dem Panel
     *            "Offline-Spiel konfigurieren" stammen
     */
    private void _startOfflineGame(
        ArenaPanelOfflineModeSettings offlineSettings) {

        /* Ermittle, welcher Spieler welche Figur im Spielfeld kontrolliert */
        _offlinePlayer1Control = -1;
        _offlinePlayer2Control = -1;
        for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
            switch (offlineSettings.getOperationForPlayer(i)) {
            case PHYSICAL_PLAYER_1:
                _offlinePlayer1Control = i;
                break;
            case PHYSICAL_PLAYER_2:
                _offlinePlayer2Control = i;
                break;
            default:
                break;
            }
        }

        /* Erzeuge Namen für die Spieler */
        int currentAIPlayerNumber = 1;
        String players[] = new String[Globals.ROUND_MAX_PLAYERS];
        for (int i = 1; i <= Globals.ROUND_MAX_PLAYERS; ++i) {
            switch (offlineSettings.getOperationForPlayer(i)) {
            case AI_CONTROLLED:
                players[i - 1] = String.format("KI-Spieler %d",
                    currentAIPlayerNumber++);
                break;
            case PHYSICAL_PLAYER_1:
                players[i - 1] = "Spieler 1";
                break;
            case PHYSICAL_PLAYER_2:
                players[i - 1] = "Spieler 2";
                break;
            default:
                break;
            }
        }

        /* Initialisiere die KI für den Offlinemodus */
        _playerAIExecutor = new PlayerAIExecutor(-1);

        /* Erzeuge eine Offline-Kopie der Spielarena für die KI */
        ConfigureArenaGameServerCommand arenaConfig = offlineSettings
            .getConfigureArenaCommand();
        _gameArenaClient = new GameArenaClient(arenaConfig.getArenaWidth(),
            arenaConfig.getArenaHeight());

        /* Erzeuge die neue Engine. Male bei Arenaupdate das Feld neu. */
        GameConfiguration config = new GameConfiguration(arenaConfig);
        _gameEngine = new GameEngine(config,
            /*
             * Callback zum Start: Beginne die Ausführung der KI
             */
            () -> _playerAIExecutor.startExecution(),

            /*
             * Callback bei Fehler: Terminiere das Programm
             */
            (Exception e) -> ClientWindow.fatalExit(
                "Es ist ein schwerwiegendes Problem in der Spiele-Engine aufgetreten",
                e),

            /*
             * Callback zum Ende: Wechsle ins Statistikmenü, wenn das Fenster
             * nicht geschlossen wird
             */
            new _GameEngineEndCallback(),
            new ArenaPanelArenaUpdateCallback(this), players);
        _gameEngine.resetArenaLayout();

        /* Erzeuge das Surface, auf welches die Arena gemalt werden soll */
        int imageSizePixels = ClientWindow.getInstance().getOptions()
            .getArenaTileScaleFactor() * 16;
        _gameSurface = Images.createCompatibleImage(
            arenaConfig.getArenaWidth() * imageSizePixels,
            arenaConfig.getArenaHeight() * imageSizePixels);

        /* Akzeptiere Tasteneingaben */
        _keyListener = new ArenaKeyListener(false);
        this.setFocusable(true);
        this.addKeyListener(_keyListener);

        /*
         * Speichere die Einstellungen, damit wir wissen, dass wir im
         * Offlinemodus sind
         */
        _offlineSettings = offlineSettings;

        /* Starte die Engine */
        _gameEngine.start();
    }

    /**
     * Beendet ein Offline-Spiel.
     */
    private void _stopOfflineGame() {
        /* Unterbreche den Thread der Engine, um das Spiel zu beenden */
        _gameEngine.interrupt();
        try {
            _gameEngine.join();
        } catch (InterruptedException e) {
        }
        _gameEngine = null;

        /* Lösche Einstellungen */
        _offlineSettings = null;
    }

    /**
     * Startet ein Online-Spiel, welches auf einem fremden Spieleserver
     * stattfindet. Dieses betreibt die Engine, sodass keine lokale Instanz
     * notwendig ist.
     *
     * @param onlineSettings
     *            Alle Einstellungen für den Onlinemodus in ein Objekt gepackt
     *            (muss Instanz von OnlineModeSettings sein)
     */
    private void _startOnlineGame(ArenaPanelOnlineModeSettings onlineSettings) {
        /*
         * Lese das Update, um die Dimensionen der Arena zu bekommen. Speichere
         * die größte X-Koordinate bzw. Y-Koordinate, die wir finden, und
         * addiere je 1 drauf.
         */
        int arenaWidth = -1;
        int arenaHeight = -1;
        List<GameModeFieldEntry> updatedFields = onlineSettings.getFirstUpdate()
            .getUpdatedFields();
        for (GameModeFieldEntry entry : updatedFields) {
            int x = entry.getX();
            if (x > arenaWidth)
                arenaWidth = x;

            int y = entry.getY();
            if (y > arenaHeight)
                arenaHeight = y;
        }
        ++arenaWidth;
        ++arenaHeight;

        /* Initialisiere lokale Kopie der Arena damit */
        _gameArenaClient = new GameArenaClient(arenaWidth, arenaHeight);

        /* Erzeuge das Surface, welches die Arena als Bild rendert */
        int imageSizePixels = ClientWindow.getInstance().getOptions()
            .getArenaTileScaleFactor() * 16;
        _gameSurface = Images.createCompatibleImage(
            arenaWidth * imageSizePixels, arenaHeight * imageSizePixels);

        /* Soll die KI spielen? Initialisiere die KI */
        if (onlineSettings.getEnableAI()) {
            String ourName = onlineSettings.getConnectionHandler()
                .getCapturedPlayerName();

            /* Finde die Nummer des Spielers anhand seines Namens */
            int playerNumber = -1;
            for (GameModePlayerEntry player : onlineSettings.getFirstUpdate()
                .getPlayers()) {
                if (player.getName().equals(ourName)) {
                    playerNumber = player.getNumber();
                    break;
                }
            }
            assert playerNumber >= 0;

            /* Initialisiere und starte die KI */
            _playerAIExecutor = new PlayerAIExecutor(playerNumber);
            _playerAIExecutor.startExecution();
        }

        /* Gieße das erste Update in Arena und Bild */
        ArenaPanelArenaUpdateCallback arenaCallback = new ArenaPanelArenaUpdateCallback(
            this);
        arenaCallback.accept(onlineSettings.getFirstUpdate());

        /* Leite das Update-Callback vom Lobby-Panel auf unseren eigenen um */
        _onlineOldArenaUpdateCallback = onlineSettings.getConnectionHandler()
            .setArenaUpdatedCallback(arenaCallback);

        /* Wir möchten auch den Highscore haben */
        _onlineOldHighscoreCallback = onlineSettings.getConnectionHandler()
            .setHighscoreReceivedCallback(new ArenaPanelHighscoreCallback());

        /* Akzeptiere Tasteneingaben, wenn die KI nicht aktiviert ist */
        if (!onlineSettings.getEnableAI()) {
            _keyListener = new ArenaKeyListener(true);
            this.setFocusable(true);
            this.addKeyListener(_keyListener);
        }

        /*
         * Speichere die Einstellungen, damit wir wissen, dass wir im
         * Onlinemodus sind
         */
        _onlineSettings = onlineSettings;
    }

    /**
     * Beendet ein Online-Spiel, welches auf einem fremden Spieleserver
     * stattgefunden hat.
     */
    private void _stopOnlineGame() {
        /* Setze Callbacks der Verbindung zurück */
        _onlineSettings.getConnectionHandler()
            .setHighscoreReceivedCallback(_onlineOldHighscoreCallback);
        _onlineSettings.getConnectionHandler()
            .setArenaUpdatedCallback(_onlineOldArenaUpdateCallback);

        /* Lösche Einstellungen */
        this._onlineOldArenaUpdateCallback = null;
        this._onlineOldHighscoreCallback = null;
        _onlineSettings = null;
    }

    /**
     * Startet ein Online- oder Offlinespiel.
     *
     * @param object
     *            Einstellungen des Spiels. Handelt es sich um eine Instanz von
     *            ArenaPanelOnlineModeSettings, so wird ein Onlinespiel
     *            gestartet. Handelt es sich um eine Instanz von
     *            ArenaPanelOfflineModeSettings, so wird ein Offlinespiel
     *            gestartet.
     */
    public void startGame(Object object) {
        /*
         * Überprüfe anhand des Objekttyps, ob wir ein Online- oder ein
         * Offlinespiel starten
         */
        if (object instanceof ArenaPanelOnlineModeSettings) {
            ArenaPanelOnlineModeSettings onlineSettings = (ArenaPanelOnlineModeSettings) object;
            _startOnlineGame(onlineSettings);
        }

        /*
         * Ansonsten starte ein Offlinespiel
         */
        else if (object instanceof ArenaPanelOfflineModeSettings) {
            ArenaPanelOfflineModeSettings offlineSettings = (ArenaPanelOfflineModeSettings) object;
            _startOfflineGame(offlineSettings);
        }

        /* Alles andere ist ein Fehler */
        else {
            throw new IllegalStateException(
                "Wechsel in Arena mit unbekanntem Objekttyp");
        }
    }

    /**
     * Gibt zurück, ob gerade ein Onlinespiel stattfindet.
     *
     * @return true, wenn gerade ein Onlinespiel stattfindet, ansonsten false
     */
    public boolean isCurrentGameOnline() {
        return _onlineSettings != null;
    }

    /**
     * Beendet ein Online- oder Offlinespiel.
     */
    public void stopGame() {
        /*
         * Überprüfe anhand der Nicht-null Einstellungen, ob wir ein Online-
         * oder ein Offlinespiel stoppen
         */
        if (_onlineSettings != null)
            _stopOnlineGame();

        /*
         * Ansonsten stoppe ein Offlinespiel
         */
        else
            _stopOfflineGame();

        /*
         * Deinitialisiere die KIs, sofern diese geladen sind
         */
        if (_playerAIExecutor != null) {
            _playerAIExecutor.stopExecution();
            _playerAIExecutor = null;
        }

        /*
         * Deinitialisiere zwischen beiden Modi geteilte Ressourcen
         */
        _gameArenaClient = null;
        _gameSurface = null;

        /* Akzeptiere keine Tasteneingaben mehr */
        this.removeKeyListener(_keyListener);
        _keyListener = null;
    }

    /**
     * Erzeugt ein neues Panel, welches die Arena darstellt und für diese
     * Tasteneingaben annimmt.
     */
    public ArenaPanel() {
        this._gameEngine = null;
        this._gameArenaClient = null;

        this._playerAIExecutor = null;

        this._onlineSettings = null;
        this._onlineOldHighscoreCallback = null;
        this._onlineOldArenaUpdateCallback = null;

        /* Paneleinstellungen */
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setLayout(null);
    }
}
