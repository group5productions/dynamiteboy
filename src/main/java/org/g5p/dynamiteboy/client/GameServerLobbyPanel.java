/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.GameClientHandler;
import org.g5p.dynamiteboy.networking.MessageClient;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.PlayersUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.RegisterPlayerGameServerResponse;
import org.g5p.dynamiteboy.tools.ExceptionFormatter;
import org.g5p.dynamiteboy.tools.GameServerEntry;
import org.g5p.dynamiteboy.tools.LobbyModePlayerEntry;

/*
 * Dieses Panel besitzt drei unterschiedliche Operationsmodi:
 *  - Spieler ist auf Warteliste: Nachricht darstellen.
 *  - Spieler in der nächsten Runde: Kann einstellen, ob er selbst spielt oder
 *    es die KI machen lässt.
 *  - Spielleiter: Kann Einstellungen für die nächste Runde vornehmen und diese
 *    auch starten
 * Es wird stets angezeigt, welche Spieler in der nächsten Runde dabei sind, wer
 * der Spielleiter ist, und wenn der eigene Spieler in der Liste steht, wird
 * dieser gekennzeichnet.
 */

/**
 * Diese Klasse implementiert das Panel, welches, während der Spieleserver sich
 * im Lobbymodus befindet, angezeigt wird.
 *
 * @author Thomas Weber
 */
public final class GameServerLobbyPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final JLabel _qualifiedPlayersLabel;
    private final JPanel _subpanelHostPanel;
    private final _WaitingListSubPanel _waitingListSubPanel;
    private final _QualifiedPlayerSubPanel _qualifiedPlayerSubPanel;
    private final _GameMasterSubPanel _gameMasterSubPanel;

    private MessageClient _gameClient;
    private GameClientHandler _gameClientHandler;
    private String _gameServerName;
    private String _playerName;
    private int _lastPlayerCount;
    private boolean _leftPanelForArena;
    private String _activePanelIdentifier;

    /* Caching von Daten aus Callbacks */
    RegisterPlayerGameServerResponse _cachedRegistrationResponse;
    PlayersUpdatedClientCommand _cachedParticipatingPlayers;

    /**
     * Dieses Panel wird Spielern angezeigt, die nicht für die nächste Runde
     * qualifiziert sind.
     */
    private final class _WaitingListSubPanel extends JPanel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Bezeichner des Panel für CardLayout.
         */
        public static final String CARD_LAYOUT_IDENTIFIER = "W";

        /**
         * Erzeugt das Subpanel, welches anzeigt, dass der Spieler auf der
         * Warteliste steht.
         */
        public _WaitingListSubPanel() {
            this.setLayout(new BorderLayout());
            this.setOpaque(false);

            String labelText = "<html><div style='text-align:center'><h1>"
                + "Du stehst auf der Warteliste!</h1><p>Leider bist du für "
                + "diese Runde noch nicht qualifiziert. Bitte habe Geduld, "
                + "bis du in die nächste Runde aufgenommen wirst. Die "
                + "Wartezeit ist davon abhängig, wie lange eine Runde dauert "
                + "beziehungsweise qualifizierte Spieler angemeldet "
                + "bleiben.</p></div></html>";

            JLabel label = new JLabel(labelText);
            label.setHorizontalAlignment(JLabel.CENTER);
            label.setVerticalAlignment(JLabel.CENTER);
            this.add(label, BorderLayout.CENTER);
        }
    }

    /**
     * Dieses Panel wird Spielern angezeigt, die für die nächste Runde
     * qualifiziert sind, aber nicht Spielleiter sind. Diesen ist es gestattet,
     * einzustellen, ob sie selbst spielen wollen oder die KI spielen lassen
     * wollen.
     */
    private final class _QualifiedPlayerSubPanel extends AbstractPanel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Bezeichner des Panel für CardLayout.
         */
        public static final String CARD_LAYOUT_IDENTIFIER = "Q";

        private final JCheckBox _useAICheck;

        /**
         * Erzeugt das Panel, welches qualifizierten Spielern anzeigt, dass
         * diese an der nächsten Runde teilnehmen dürfen und einstellen können,
         * ob man selbst spielen will oder die KI spielen soll.
         */
        public _QualifiedPlayerSubPanel() {
            /* Subpanel haben keine Rahmen */
            super(false);

            /* Vertikaler Abstand */
            this.add(Box.createVerticalGlue());

            final String labelText = "<html><div style='text-align:center'><h1>"
                + "Du bist in der nächsten Runde dabei!</h1><p>Du kannst in "
                + "der nächsten Runde mitspielen. Bitte habe Geduld, bis der "
                + "Spielleiter die Runde beginnt. In der Zwischenzeit kannst "
                + "du folgende Einstellungen vornehmen:</p></div></html>";

            /* Label, um obigen Text anzuzeigen */
            final JLabel label = new JLabel(labelText);
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            this.add(label);

            /* Vertikaler Abstand */
            this.add(createVerticalRigid(2));

            /* Checkbox, um KI zu konfigurieren */
            _useAICheck = new JCheckBox(
                "KI spielen lassen, anstatt selbst zu spielen");
            _useAICheck.setAlignmentX(Component.CENTER_ALIGNMENT);
            this.add(_useAICheck);

            /* Vertikaler Abstand */
            this.add(Box.createVerticalGlue());
        }

        /**
         * Gibt zurück, ob die KI in der nächsten Runde spielen soll.
         *
         * @return true, wenn die KI in der nächsten Runde spielen soll,
         *         ansonsten false
         */
        public boolean getUseAI() {
            return _useAICheck.isSelected();
        }
    }

    /**
     * Dieses Panel wird dem Spielleiter angezeigt. Dieser darf das Spiel
     * konfigurieren und anschließend starten.
     */
    private final class _GameMasterSubPanel extends AbstractPanel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Bezeichner des Panel für CardLayout.
         */
        public static final String CARD_LAYOUT_IDENTIFIER = "M";

        private final ArenaConfigurationSubPanel _arenaConfigSubpanel;
        private final JCheckBox _useAICheck;

        /**
         * Überprüft die Eingaben auf Korrektheit und erzeugt eine Nachricht,
         * die an den Spieleserver gesendet wird, um die Runde zu starten.
         */
        private void _validateFieldsAndStart() {
            /* Hole Einstellungen der Arena ab */
            ConfigureArenaGameServerCommand configCommand;
            try {
                configCommand = _arenaConfigSubpanel.getArenaConfiguration();
            } catch (IllegalArgumentException e) {
                ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                    "Eine oder mehrere Eingaben sind leider nicht korrekt.\n\n"
                        + "Grund: %s.\n\nBitte korrigiere deine Eingaben und "
                        + "versuche es anschließend erneut.",
                    e.getLocalizedMessage());
                return;
            }

            /*
             * Sind mindestens zwei Spieler angemeldet?
             */
            if (_lastPlayerCount < Globals.ROUND_MIN_PLAYERS) {
                ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                    "Es sind nicht genügend Spieler auf dem Server angemeldet, "
                        + "um ein Spiel starten zu können.\nBitte warte, bis "
                        + "mindestens %d Spieler auf dem Server angemeldet sind.",
                    Globals.ROUND_MIN_PLAYERS);
                return;
            }

            /*
             * Sende die Konfiguration ab. Das erste Arenaupdate startet das
             * Spiel.
             */
            try {
                _gameClientHandler.configureArena(configCommand);
            } catch (Exception e) {
                /*
                 * Zeige eine Fehlermeldung an und breche ab
                 */
                ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                    "Die Konfiguration der Arena am Spieleserver '%s' "
                        + "ist leider fehlgeschlagen.\n\nGrund: %s.\n\nDie "
                        + "Verbindung zum Spieleserver wird daher getrennt.",
                    _gameServerName, e.getLocalizedMessage());

                /* Abbruch mit Menüwechsel */
                disconnectFromServer(false);

                /*
                 * Wechsle in das Servermenü. Wenn dies scheitert, wechsle ins
                 * Hauptmenü.
                 */
                ClientWindow window = ClientWindow.getInstance();
                if (!window
                    .setWindowState(ClientWindowState.SERVER_SELECTION_MENU)) {
                    window.setWindowState(ClientWindowState.MAIN_MENU);
                }
            }
        }

        /**
         * Erzeugt das Panel, welches dem Spielleiter die Konfiguration der
         * nächsten Spielrunde ermöglicht.
         */
        public _GameMasterSubPanel() {
            /* Subpanel haben keine Rahmen */
            super(false);

            /* Vertikaler Abstand */
            this.add(Box.createVerticalGlue());

            final String labelText = "<html><div style='text-align:center'><h1>"
                + "Du bist Spielleiter!</h1><p>Als Spielleiter darfst du die "
                + "nächste Spielerunde konfigurieren. Ändere die Einstellungen "
                + "unten nach Belieben und klicke auf \"Runde starten\", um "
                + "die Runde zu beginnen und auch mitzuspielen.</p></div></html>";

            /* Label, um obigen Text anzuzeigen */
            final JLabel label = new JLabel(labelText);
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            this.add(label);

            /* Vertikaler Abstand */
            this.add(createVerticalRigid(2));

            /*
             * Subpanel der Arenakonfiguration
             */
            _arenaConfigSubpanel = new ArenaConfigurationSubPanel();
            this.add(_arenaConfigSubpanel);

            /* Vertikaler Abstand */
            this.add(createVerticalRigid(2));

            /*
             * Horizontales Panel: KI-Option / Spielstart
             */
            _useAICheck = new JCheckBox(
                "KI spielen lassen, anstatt selbst zu spielen");
            final JButton playButton = createStyledJButton("Runde starten");
            playButton.addActionListener((ActionEvent event) -> {
                _validateFieldsAndStart();
            });

            /*
             * Füge obige Steuerelemente zum Panel hinzu
             */
            final JPanel horizontalPanel = createHorizontalJPanel();
            horizontalPanel.add(_useAICheck);
            horizontalPanel.add(Box.createHorizontalGlue());
            horizontalPanel.add(playButton);
            this.add(horizontalPanel);

            /* Vertikaler Abstand */
            this.add(Box.createVerticalGlue());
        }

        /**
         * Gibt zurück, ob die KI in der nächsten Runde spielen soll.
         *
         * @return true, wenn die KI in der nächsten Runde spielen soll,
         *         ansonsten false
         */
        public boolean getUseAI() {
            return _useAICheck.isSelected();
        }
    }

    /**
     * Ein Callback, welcher Änderungen der Spielerliste vom Spieleserver
     * akzeptiert und die Liste qualifizierter Spieler im Panel aktualisiert.
     */
    private final class _LobbyModePlayerListUpdateCallback
        implements Consumer<PlayersUpdatedClientCommand> {
        @Override
        public void accept(PlayersUpdatedClientCommand command) {
            /*
             * Speichere das Update dazwischen und erzwinge UI-Update, aber nur,
             * wenn wir unseren eigenen Spielernamen kennen.
             */
            _cachedParticipatingPlayers = command;
            if (_playerName != null)
                _updatePanelsAndPlayers();
        }
    }

    /**
     * Ein Callback, der das erste Update der Arena zum Spielstart empfängt und
     * auf das Arena-Panel wechselt.
     */
    private final class _FirstArenaUpdateCallback
        implements Consumer<ArenaUpdatedClientCommand> {
        @Override
        public void accept(ArenaUpdatedClientCommand command) {
            /* Frage KI-Einstellung ab */
            boolean useAI = false;
            if (_activePanelIdentifier != null) {
                /* Teilnehmender Spieler */
                if (_activePanelIdentifier
                    .equals(_QualifiedPlayerSubPanel.CARD_LAYOUT_IDENTIFIER)) {
                    useAI = _qualifiedPlayerSubPanel.getUseAI();
                }

                /* Spielleiter */
                else if (_activePanelIdentifier
                    .equals(_GameMasterSubPanel.CARD_LAYOUT_IDENTIFIER)) {
                    useAI = _gameMasterSubPanel.getUseAI();
                }
            }

            /* Stelle sämtliche Einstellungen zusammen */
            ArenaPanelOnlineModeSettings settings = new ArenaPanelOnlineModeSettings(
                command, _gameClientHandler, useAI);

            /*
             * Wechsle mit diesen in die Arena, schütze die Instanz der
             * Verbindung vor Verlust
             */
            _leftPanelForArena = true;
            ClientWindow.getInstance()
                .setWindowState(ClientWindowState.GAME_ARENA, settings);
        }
    }

    /**
     * Ein Callback, der ausgeführt wird, wenn die Verbindung zum Spieleserver
     * getrennt wird. Unterscheide dabei zwischen normalem Disconnect und einem
     * Disconnect auf Fehlerbasis.
     */
    private final class _GameServerConnectionTerminationCallback
        implements Consumer<Boolean> {
        @Override
        public void accept(Boolean closedByUs) {
            boolean notUserRequested = false;

            /* Gab es eine Exception? Zeige diese an */
            Exception e = _gameClientHandler.getCommunicationException();
            if (e != null) {
                ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                    "Entschuldige, aber es ist ein Problem mit der "
                        + "Verbindung zum Spieleserver aufgetreten. Die "
                        + "Verbindung zum Spieleserver wurde daher "
                        + "getrennt.\n\nGrund: %s.\n\n%s",
                    e.getMessage(), ExceptionFormatter.formatException(e));
                notUserRequested = true;
            }

            /* Hat uns der Server rausgeschmissen? */
            else if (!closedByUs) {
                ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                    "Entschuldige, aber der Spieleserver hat dich aus "
                        + "unerklärlichen Gründen aus dem Spiel geworfen. "
                        + "Die Verbindung zum Spieleserver wurde daher "
                        + "getrennt.\n\nVersuche, dich erneut mit dem "
                        + "selben Spieleserver zu verbunden, oder suche dir "
                        + "einen anderen Spieleserver aus.");
                notUserRequested = true;
            }

            /*
             * Wechsle in einem anderen Thread zum Servermenü zurück, da während
             * des Wechsels disconnectFromServer() ausgeführt wird, und dieser
             * auf den Messenger-Thread wartet, wir aber von diesem aufgerufen
             * werden.
             *
             * Wenn die Verbindung durch Klick auf "Abmelden" getrennt wurde,
             * leite keinen weiteren Wechsel ein, dieser wird durch den Button
             * eingeleitet.
             */
            if (!notUserRequested)
                return;

            _leftPanelForArena = false;
            EventQueue.invokeLater(() -> {
                /*
                 * Wechsle in das Servermenü, und wenn das nicht funktioniert,
                 * ins Hauptmenü
                 */
                ClientWindow window = ClientWindow.getInstance();
                if (!window
                    .setWindowState(ClientWindowState.SERVER_SELECTION_MENU)) {
                    window.setWindowState(ClientWindowState.MAIN_MENU);
                }
            });
        }
    }

    /**
     * Aktualisiert die Benutzeroberfläche aufgrund einer Änderung der
     * Spielerliste. Dabei wird die Liste qualifizierter Spieler neu berechnet,
     * und je nach aktuellem Zustand des eigenen Spielers wird ein Subpanel
     * präsentiert, welches diesem Zustand entspricht.
     */
    private void _updatePanelsAndPlayers() {
        /* Nur wenn Update eingetroffen ist */
        if (_cachedParticipatingPlayers == null)
            return;

        /* Zähle die Spieler */
        List<LobbyModePlayerEntry> players = _cachedParticipatingPlayers
            .getPlayers();
        _lastPlayerCount = players.size();

        /* Bestimme den Zustand des eigenen Spielers */
        boolean isLeader = false;
        boolean isInList = false;
        for (LobbyModePlayerEntry entry : players) {
            if (entry.getName().equals(_playerName)) {
                isInList = true;
                isLeader = entry.isLeader();
                break;
            }
        }

        /* Schreibe eine neue Spielerliste */
        _writePlayerList(players);
        final CardLayout cardLayout = (CardLayout) _subpanelHostPanel
            .getLayout();

        /* Nicht in Liste? Warteliste anzeigen */
        if (!isInList) {
            _activePanelIdentifier = _WaitingListSubPanel.CARD_LAYOUT_IDENTIFIER;
            cardLayout.show(_subpanelHostPanel, _activePanelIdentifier);
        }

        /* In Liste, aber kein Spielleiter? Zeige KI-Option an */
        else if (!isLeader) {
            _activePanelIdentifier = _QualifiedPlayerSubPanel.CARD_LAYOUT_IDENTIFIER;
            cardLayout.show(_subpanelHostPanel, _activePanelIdentifier);
        }

        /* Spielleiter. Zeige komplette Konfiguration an */
        else {
            _activePanelIdentifier = _GameMasterSubPanel.CARD_LAYOUT_IDENTIFIER;
            cardLayout.show(_subpanelHostPanel, _activePanelIdentifier);
        }

        /*
         * Da sich die Fenstergröße aufgrund der Spielerliste oder der Panels
         * ändern kann, müssen wir das gesamte Fenster, inklusive dieses Panels,
         * neu validieren lassen.
         */
        SwingUtilities.invokeLater(() -> {
            this.revalidate();
            this.repaint();
            ClientWindow.getInstance().pack();
        });
    }

    /**
     * Schreibt die Liste zugelassener Spieler in das zugehörige Label.
     *
     * @param players
     *            Liste von zugelassenen Spielern, so wie sie der Spieleserver
     *            übermittelt hat
     */
    private void _writePlayerList(List<LobbyModePlayerEntry> players) {
        StringBuilder builder = new StringBuilder();

        /* Fange einfach an */
        builder.append(
            "<html><body style='width:100%'>Qualifiziert für die nächste Runde: ");

        /* Spielleiter fett, eigener Spieler unterstrichen */
        int i = 0;
        for (LobbyModePlayerEntry player : players) {
            String playerName = player.getName();

            /* Spieler selbst */
            if (playerName.equals(_playerName)) {
                if (player.isLeader()) {
                    builder.append(String.format(
                        "<b><u>%s</u></b> (Spielleiter, Du)", playerName));
                } else {
                    builder.append(String.format("<u>%s</u> (Du)", playerName));
                }
            }

            /* Jemand anderes */
            else {
                if (player.isLeader()) {
                    builder.append(
                        String.format("<b>%s</b> (Spielleiter)", playerName));
                } else {
                    builder.append(playerName);
                }
            }

            /* Sauber durch Kommata trennen */
            if (i < players.size() - 1)
                builder.append(", ");
            ++i;
        }

        /* Ende ganz einfach */
        builder.append("</body></html>");

        /* Und in das Label platzieren */
        _qualifiedPlayersLabel.setText(builder.toString());
    }

    /**
     * Diese Methode trennt die Verbindung zum Spieleserver, aber nur, wenn die
     * Verbindung nicht aufrecht erhalten werden soll.
     *
     * @param forceDisconnect
     *            Ist dieser Wert true, wird die Verbindung auch getrennt, wenn
     *            diese momentan gehalten werden soll (weil z.B. gerade eine
     *            Spielerunde läuft). Ansonsten wird die Verbindung nicht
     *            getrennt, wenn diese gehalten werden soll.
     */
    public void disconnectFromServer(boolean forceDisconnect) {
        /*
         * Wird auch beim Panelwechsel aufgerufen. Halte die Verbindung
         * aufrecht, wenn wir das Panel verlassen, um im Online-Modus die Arena
         * anzuzeigen.
         */
        if (!forceDisconnect && _leftPanelForArena)
            return;

        /* Beende den Client */
        if (_gameClient != null) {
            _gameClient.stopClient();

            _cachedRegistrationResponse = null;
            _cachedParticipatingPlayers = null;

            _leftPanelForArena = false;
            _playerName = null;
            _gameServerName = null;
            _gameClientHandler = null;
            _gameClient = null;
        }
    }

    /**
     * Diese Methode stellt die Verbindung zum Spieleserver her und bricht den
     * Wechsel in dieses Panel ab, wenn etwas hier schief geht.
     */
    public boolean connectToServer(Object object) {
        /*
         * Wird auch beim Panelwechsel von Highscore zu uns zurück aufgerufen.
         * Halte die bestehende Verbindung aufrecht, damit wir nahtlos in die
         * Lobby zurückkehren können.
         */
        if (_leftPanelForArena) {
            /*
             * Überspringe das Anmeldeprozedere, ermögliche wieder Trennung vom
             * Server
             */
            _leftPanelForArena = false;
            return true;
        }

        /* Sollte, wenn keine Verbindung existiert, nicht null sein */
        assert object != null;
        GameServerEntry gameServer = (GameServerEntry) object;

        /*
         * Stelle die Verbindung zum Spieleserver her. Steht diese, haben wir
         * den ersten Schritt geschafft.
         */
        boolean success = false;
        try {
            /*
             * Erstelle Client zum Spieleserver
             */
            _gameClient = new MessageClient(
                InetAddress.getByName(gameServer.getServerAddress()),
                gameServer.getServerPort(), Globals.CONNECTION_TIMEOUT_MS);
            _gameClient.setConnectionHandler(GameClientHandler.class);

            /*
             * Baue die Verbindung zum Spieleserver auf
             */
            _gameClient.startClient();
            _gameClientHandler = (GameClientHandler) _gameClient
                .getConnectionHandlerInstance();

            /* Warte, bis die Verbindung hergestellt wurde */
            _gameClientHandler
                .waitUntilReadyOrTimeout(Globals.CONNECTION_TIMEOUT_MS);

            /*
             * Setze ein Callback, dass über Terminierung wegen
             * Verbindungsfehler informiert
             */
            _gameClientHandler.setConnectionTerminationCallback(
                new _GameServerConnectionTerminationCallback());

            /*
             * Setze ein Callback, welches die Antwort auf eine
             * Spielerregistrierung speichert
             */
            _gameClientHandler.setPlayerRegistrationReceivedCallback(
                new Consumer<RegisterPlayerGameServerResponse>() {
                    @Override
                    public void accept(RegisterPlayerGameServerResponse t) {
                        _cachedRegistrationResponse = t;
                    }
                });

            /*
             * Setze ein Callback, das bei Änderungen der Spielerliste aktiv
             * wird
             */
            _gameClientHandler.setPlayersUpdatedInLobbyCallback(
                new _LobbyModePlayerListUpdateCallback());

            /* Setze ein Callback, das beim ersten Arenaupdate aktiv wird */
            _gameClientHandler
                .setArenaUpdatedCallback(new _FirstArenaUpdateCallback());
        } catch (Exception e) {
            /*
             * Zeige eine Fehlermeldung an
             */
            ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                "Die Verbindung zum Spieleserver '%s' unter der Adresse %s:%d "
                    + "ist leider fehlgeschlagen.\n\nGrund: %s.\n\nVersuche es "
                    + "ein weiteres Mal, vielleicht klappt es dann!",
                gameServer.getServerName(), gameServer.getServerAddress(),
                gameServer.getServerPort(), e.getLocalizedMessage());
            return false;
        }

        /*
         * Frage den Spieler nach einem Namen und versuche, sich mit dem Namen
         * am Server anzumelden. Scheitert dies, wird der Nutzer gebeten, einen
         * anderen Namen einzugeben.
         */
        while (true) {
            /* Zeige Dialog an */
            String answer = JOptionPane.showInputDialog(
                ClientWindow.getInstance(),
                "Unter welchem Namen möchtest du spielen?\n\n(Es sind nur "
                    + "Großbuchstaben, Kleinbuchstaben und Zahlen zulässig.)",
                Globals.PROJECT_TITLE, JOptionPane.QUESTION_MESSAGE);

            /* Abbruch? */
            if (answer == null) {
                disconnectFromServer(false);
                return false;
            }

            /* Versuche Registrierung, warte auf Antwort */
            try {
                _cachedRegistrationResponse = null;
                _gameClientHandler.registerPlayer(answer);
                _gameClientHandler
                    .waitForPlayerRegistration(Globals.CONNECTION_TIMEOUT_MS);

                /* Überprüfe die Antwort */
                if (_cachedRegistrationResponse.isRejected())
                    throw new IllegalArgumentException("Der Name " + answer
                        + " wurde vom Spieleserver abgelehnt.");

                /*
                 * Zeige einen Hinweistext an, wenn der Spieler umbenannt werden
                 * musste. Unabhängig davon haben wir die Anmeldephase
                 * bestanden.
                 */
                else {
                    String newName = _cachedRegistrationResponse
                        .getAdaptedName();
                    if (!newName.equals(answer)) {
                        ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                            "Der Name '%s' war auf dem Spieleserver bereits "
                                + "vergeben.\nDu wirst daher unter dem Namen "
                                + "'%s' gelistet.",
                            answer, newName);
                    }

                    /* Erfolg, speichere einige Daten */
                    success = true;
                    _playerName = newName;
                    _gameServerName = gameServer.getServerName();
                    this.setPanelHeading(
                        String.format("Lobby von '%s'", _gameServerName));

                    /*
                     * Erzwinge erneut UI-Update, um Status des eigenen Spielers
                     * korrekt zu reflektieren
                     */
                    _updatePanelsAndPlayers();
                    break;
                }
            } catch (IllegalArgumentException e) {
                /*
                 * Zeige eine Fehlermeldung an, setze aber weiter fort, weil nur
                 * der Name falsch ist
                 */
                ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                    "Die Registrierung unter dem Namen '%s' am Spieleserver "
                        + "'%s' ist leider fehlgeschlagen.\n\nGrund: %s.\n\n"
                        + "Versuche es ein weiteres Mal, vielleicht klappt es "
                        + "dann!",
                    answer, gameServer.getServerName(),
                    e.getLocalizedMessage());
                continue;
            } catch (Exception e) {
                /*
                 * Zeige eine Fehlermeldung an und breche ab
                 */
                ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                    "Die Registierung unter dem Namen '%s' am Spieleserver '%s' "
                        + "ist leider fehlgeschlagen.\n\nGrund: %s.\n\nDie "
                        + "Verbindung zum Spieleserver wird daher getrennt.",
                    answer, gameServer.getServerName(),
                    e.getLocalizedMessage());

                disconnectFromServer(false);
                return false;
            }
        }
        return success;
    }

    /**
     * Erzeugt das Panel, welches angezeigt wird, wenn der Spieler mit einem
     * Server verbunden ist und die Lobby offen ist.
     */
    public GameServerLobbyPanel() {
        super("Lobby von 'xxx'");

        this._gameClient = null;
        this._gameClientHandler = null;
        this._gameServerName = null;
        this._playerName = null;
        this._leftPanelForArena = false;
        this._activePanelIdentifier = null;

        this._cachedRegistrationResponse = null;
        this._cachedParticipatingPlayers = null;

        /*
         * Button zur Rückkehr ins Servermenü, und wenn dies nicht funktioniert,
         * ins Hauptmenü. Implementiere es mittels Lambda-Ausdruck.
         */
        final JButton logoutButton = createStyledJButton("Abmelden");
        logoutButton.addActionListener((ActionEvent event) -> {
            final ClientWindow window = ClientWindow.getInstance();
            if (!window.setWindowState(ClientWindowState.SERVER_SELECTION_MENU))
                window.setWindowState(ClientWindowState.MAIN_MENU);
        });

        /* Füge den Button zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(logoutButton);

        /*
         * (HTML-)Label, welches anzeigt, welche Spieler in der nächsten Runde
         * dabei sind. Beschränke die Breite des Labels in HTML, um die korrekte
         * Größe zu bestimmen. Benutze einen leeren HTML-Tag, um die Erstellung
         * der HTML-Komponente zu erzwingen.
         */
        _qualifiedPlayersLabel = new JLabel();
        _qualifiedPlayersLabel.setText("<html></html>");
        this.add(packJLabelAlignedIntoJPanel(_qualifiedPlayersLabel,
            Component.LEFT_ALIGNMENT));

        /* Halte Abstand ein */
        this.add(createVerticalRigid(1));

        /*
         * Unterpanel für die drei Zustände, die ein Spieler in der Lobby
         * besitzen kann.
         */
        _waitingListSubPanel = new _WaitingListSubPanel();
        _qualifiedPlayerSubPanel = new _QualifiedPlayerSubPanel();
        _gameMasterSubPanel = new _GameMasterSubPanel();

        /*
         * Ein JPanel, welches ein CardLayout benutzt, um mehrere Komponenten in
         * ein Panel zu stecken, aber nur eines gleichzeitig anzuzeigen. Dies
         * benutzen wir für den Wechsel zwischen den Subpanels.
         */
        _subpanelHostPanel = new JPanel(new CardLayout());
        _subpanelHostPanel.setOpaque(false);
        this.add(_subpanelHostPanel);

        /*
         * Füge die Unterpanel zum Hostpanel hinzu. Diese werden mit eindeutigen
         * String identifiziert.
         */
        _subpanelHostPanel.add(_waitingListSubPanel,
            _WaitingListSubPanel.CARD_LAYOUT_IDENTIFIER);
        _subpanelHostPanel.add(_qualifiedPlayerSubPanel,
            _QualifiedPlayerSubPanel.CARD_LAYOUT_IDENTIFIER);
        _subpanelHostPanel.add(_gameMasterSubPanel,
            _GameMasterSubPanel.CARD_LAYOUT_IDENTIFIER);
    }
}
