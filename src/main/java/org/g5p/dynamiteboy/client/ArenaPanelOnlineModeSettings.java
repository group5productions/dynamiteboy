/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import org.g5p.dynamiteboy.networking.GameClientHandler;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;

/**
 * Diese Klasse packt Einstellungen für den Online-Modus und ermöglicht
 * einen Wechsel in die Arena in diesen Modus.
 *
 * @author Thomas Weber
 */
public final class ArenaPanelOnlineModeSettings {
    private final ArenaUpdatedClientCommand _firstUpdate;
    private final GameClientHandler _connectionHandler;
    private final boolean _enableAI;

    /**
     * Erzeugt eine Objekt-Instanz, welches Einstellungen für den
     * Online-Modus der Arena verpackt. Diese Klasse führt keine
     * Parameterüberprüfungen durch.
     *
     * @param firstUpdate
     *            Erstes Update des Spieleservers
     * @param connectionHandler
     *            Handler der Verbindung des Spielers zum Spieleserver
     * @param enableAI
     *            true, wenn die KI die Steuerung übernehmen soll, ansonsten
     *            false
     */
    public ArenaPanelOnlineModeSettings(
        ArenaUpdatedClientCommand firstUpdate,
        GameClientHandler connectionHandler, boolean enableAI) {
        this._firstUpdate = firstUpdate;
        this._connectionHandler = connectionHandler;
        this._enableAI = enableAI;
    }

    /**
     * Gibt das erste Update des Spieleservers zurück.
     *
     * @return Erstes Update des Spieleservers
     */
    public ArenaUpdatedClientCommand getFirstUpdate() {
        return _firstUpdate;
    }

    /**
     * Gibt den Handler der Verbindung des Spielers zum Spieleserver zurück.
     *
     * @return Handler der Verbindung des Spielers zum Spieleserver
     */
    public GameClientHandler getConnectionHandler() {
        return _connectionHandler;
    }

    /**
     * Gibt zurück, ob die KI die Steuerung des eigenen Spielers übernehmen
     * soll.
     *
     * @return true, wenn die KI die Steuerung übernehmen soll, ansonsten
     *         false
     */
    public boolean getEnableAI() {
        return _enableAI;
    }
}