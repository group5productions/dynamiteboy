/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.tools.ExceptionFormatter;

/**
 * Diese Klasse beherbergt das Fenster, in welchem der Client dargestellt wird.
 * Jedes Untermenü ist als einzelnes Panel implementiert, wobei von jedem Punkt
 * der Anwendung (auch innerhalb der Panels) zu jedem anderen Panel gewechselt
 * werden kann. Dies verstärkt die Koherenz zwischen den einzelnen
 * Programmteilen.
 *
 * @author Thomas Weber
 */
public final class ClientWindow extends JFrame {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * dieses Feld Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private static final ClientWindow _instance;

    private final ClientOptions _options;

    private final ArenaPanel _arenaPanel;
    private final MainMenuPanel _mainMenuPanel;
    private final OptionsPanel _optionsPanel;
    private final ServerListPanel _serverListPanel;
    private final StatisticsPanel _statisticsPanel;
    private final OfflinePlayPanel _offlinePlayPanel;
    private final CustomServerPanel _customServerPanel;
    private final GameServerLobbyPanel _gameServerLobbyPanel;
    private final TutorialPanel _tutorialPanel;

    private boolean _windowIsClosing;
    private ClientWindowState _windowState;

    /**
     * Eine Unterklasse, die auf Fensterereignisse reagiert.
     */
    private final class ClientWindowListener extends WindowAdapter {
        /**
         * Diese Methode wird aufgerufen, wenn das Fenster geschlossen wird.
         *
         * @param e
         *            Daten über das Ereignis
         */
        @Override
        public void windowClosing(WindowEvent e) {
            /* Markiere Fenster als gerade geschlossen werdend */
            _windowIsClosing = true;

            /* "Entlade" das aktuell dargestellte Panel */
            setWindowState(ClientWindowState.NOT_EXISTING);
        }
    }

    /**
     * Erzeugt das Fenster mitsamt aller seiner Panel und zeigt das Panel des
     * Hauptmenüs an.
     */
    public ClientWindow() {
        /*
         * Gib dem Fenster den Projektnamen als Titel. Beende die Anwendung,
         * wenn das Fenster geschlossen wird. Verbiete das Ändern der
         * Fenstergröße.
         */
        super(Globals.PROJECT_TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.addWindowListener(new ClientWindowListener());

        /* Initialisiere den Client mit Standardeinstellungen */
        this._options = new ClientOptions();

        this._arenaPanel = new ArenaPanel();
        this._mainMenuPanel = new MainMenuPanel();
        this._optionsPanel = new OptionsPanel();
        this._serverListPanel = new ServerListPanel();
        this._statisticsPanel = new StatisticsPanel();
        this._offlinePlayPanel = new OfflinePlayPanel();
        this._customServerPanel = new CustomServerPanel();
        this._gameServerLobbyPanel = new GameServerLobbyPanel();
        this._tutorialPanel = new TutorialPanel();

        /* Schalte auf das Hauptmenü um */
        this._windowIsClosing = false;
        this._windowState = ClientWindowState.NOT_EXISTING;
        setWindowState(ClientWindowState.MAIN_MENU);
    }

    /**
     * Schließt dieses Fenster.
     */
    public void closeWindow() {
        this.setVisible(false);
        this.dispose();
    }

    /**
     * Überprüft, ob das Fenster momentan geschlossen wird.
     *
     * @return true, wenn das Fenster momentan geschlossen wird oder es bereits
     *         geschlossen ist, ansonsten false
     */
    public boolean isWindowClosing() {
        return _windowIsClosing;
    }

    /**
     * Diese Methode wechselt das im Fenster angezeigte Panel und passt das
     * Fenster an seine Größe an.
     *
     * @param windowState
     *            Zustand, zu dem das neue Fenster gehört
     * @return true, wenn der Wechsel erfolgreich lief, ansonsten false
     */
    public boolean setWindowState(ClientWindowState windowState) {
        return setWindowState(windowState, null);
    }

    /**
     * Diese Methode wechselt das im Fenster angezeigte Panel und passt das
     * Fenster an seine Größe an.
     *
     * @param windowState
     *            Zustand, zu dem das neue Fenster gehört
     * @param object
     *            Objekt, welches dem Zustandswechsel mitgegeben wird
     * @return true, wenn der Wechsel erfolgreich lief, ansonsten false
     */
    public boolean setWindowState(ClientWindowState windowState,
        Object object) {
        /*
         * Stelle sicher, dass ein Wechsel zum Statistikmenü nur von der Arena
         * aus erfolgen kann. Das Statistikmenü benötigt Informationen aus der
         * Arena dafür.
         */
        if (windowState == ClientWindowState.STATISTICS_MENU
            && _windowState != ClientWindowState.GAME_ARENA) {
            throw new IllegalStateException(
                "Wechsel ins Statistikmenü kann nur von Arena aus erfolgen");
        }

        /*
         * "Entlade" ein existierendes Panel, indem eine Bereinigungs-Operation
         * durchgeführt wird.
         */
        boolean gameWasOnline = false;
        switch (_windowState) {
        case NOT_EXISTING:
        case MAIN_MENU:
        case SERVER_SELECTION_MENU:
        case CUSTOM_SERVER_MENU:
        case OFFLINE_PLAY_MENU:
        case OPTIONS_MENU:
        case TUTORIAL_MENU:
            /* Nichts tun */
            break;

        case GAME_ARENA:
            /* Merken, ob das Spiel in der Arena online stattfand */
            gameWasOnline = _arenaPanel.isCurrentGameOnline();

            /* Beende das Spiel */
            _arenaPanel.stopGame();

            /*
             * Trenne Verbindung zum Spieleserver, wenn das Fenster geschlossen
             * wird. Ansonsten halte diese aufrecht.
             */
            if (windowState == ClientWindowState.NOT_EXISTING)
                _gameServerLobbyPanel.disconnectFromServer(true);
            break;

        case STATISTICS_MENU:
            /*
             * Trenne Verbindung zum Spieleserver, wenn das Fenster geschlossen
             * wird. Ansonsten halte diese aufrecht.
             */
            if (windowState == ClientWindowState.NOT_EXISTING)
                _gameServerLobbyPanel.disconnectFromServer(true);
            break;

        case GAME_SERVER_LOBBY_MENU:
            /*
             * Trenne Verbindung zum Spieleserver. Wenn das Fenster geschlossen
             * wird, dann erzwinge diese, selbst wenn die Trennung ausgeschaltet
             * ist.
             */
            _gameServerLobbyPanel.disconnectFromServer(
                windowState == ClientWindowState.NOT_EXISTING);
            break;

        default:
            /* Darf nicht passieren */
            throw new IllegalStateException(
                "aktueller Fensterzustand ungültig");
        }

        /*
         * "Lade" das neue Panel. Wähle die zum Zustand passende Instanz des
         * Panels aus und führe je nach Panel weitere Initialisierungen durch.
         */
        JPanel newPanel;
        switch (windowState) {
        case NOT_EXISTING:
            /* Zu keinem weiteren Panel wechseln */
            return true;
        case MAIN_MENU:
            newPanel = _mainMenuPanel;
            break;
        case SERVER_SELECTION_MENU:
            newPanel = _serverListPanel;

            /* Schlägt der Datenabruf fehl, dann breche Wechsel ab */
            if (!_serverListPanel.loadOptions())
                return false;
            break;
        case CUSTOM_SERVER_MENU:
            newPanel = _customServerPanel;

            /* Lade Einstellungen in die Steuerelemente */
            _customServerPanel.loadOptions();
            break;
        case GAME_SERVER_LOBBY_MENU:
            newPanel = _gameServerLobbyPanel;

            /* Schlägt die Verbindung fehl, dann breche Wechsel ab */
            if (!_gameServerLobbyPanel.connectToServer(object))
                return false;
            break;
        case OFFLINE_PLAY_MENU:
            newPanel = _offlinePlayPanel;

            /* Lade Einstellungen in die Steuerelemente */
            _offlinePlayPanel.loadOptions();
            break;
        case GAME_ARENA:
            newPanel = _arenaPanel;

            /* Starte das Spiel */
            if (object == null) {
                throw new IllegalArgumentException(
                    "Keine Einstellungen für Arena übergeben");
            }
            _arenaPanel.startGame(object);
            break;
        case STATISTICS_MENU:
            newPanel = _statisticsPanel;

            /* Übergebe dem Menü die Statistiken aus dem Spiel */
            if (object == null) {
                throw new IllegalArgumentException(
                    "Keine Statistiken übergeben");
            }
            _statisticsPanel.loadOptions(object, gameWasOnline);
            break;
        case OPTIONS_MENU:
            newPanel = _optionsPanel;

            /* Lade Einstellungen in die Steuerelemente */
            _optionsPanel.loadOptions();
            break;
        case TUTORIAL_MENU:
            newPanel = _tutorialPanel;
            break;
        default:
            /* Darf nicht passieren */
            throw new IllegalStateException(
                "aktueller Fensterzustand ungültig");
        }

        /*
         * Setze das neue Panel in das Fenster ein und lasse Swing die
         * Fenstergröße automatisch anpassen. Fordere von Swing, den Fokus auf
         * das Panel zu legen, damit dieses Tasteneingaben annehmen kann.
         */
        this.setContentPane(newPanel);
        this.pack();
        newPanel.requestFocusInWindow();
        this._windowState = windowState;
        return true;
    }

    /**
     * Gibt die Einstellungen des Clients zurück.
     *
     * @return Einstellungen des Clients
     */
    public ClientOptions getOptions() {
        return _options;
    }

    /**
     * Gibt die globale Instanz des Fensters zurück.
     *
     * @return globale Instanz des Fensters
     */
    public static ClientWindow getInstance() {
        return _instance;
    }

    /**
     * Zeigt eine Meldung an, die printf()-artig formatiert werden kann.
     *
     * @param dialogFlags
     *            Einstellungen des Dialogs
     * @param format
     *            Nachricht, die angezeigt werden soll
     * @param args
     *            Parameter, um Platzhalter der Nachricht zu füllen
     */
    public static void showMessage(int dialogFlags, String format,
        Object... args) {
        /* Zeige Dialog mit der Meldung an */
        JOptionPane.showMessageDialog(
            _instance != null ? _instance : new JFrame(),
            String.format(format, args), Globals.PROJECT_TITLE, dialogFlags);
    }

    /**
     * Diese Funktion zeigt eine schwerwiegende Fehlermeldung mit Begründung und
     * Informationen zur Ausnahme, die zu diesem Aufruf geführt hat. Danach wird
     * das Programm beendet.
     *
     * @param reason
     *            Einfach verständliche Begründung, warum das Programm beendet
     *            wird
     * @param triggeringException
     *            Ausnahme, die zur Terminierung des Programms geführt hat
     */
    public static void fatalExit(String reason, Exception triggeringException) {
        /* Formatiere die Nachricht */
        String fullMessage = String.format(
            "Es ist ein schwerwiegendes Problem aufgetreten. %s muss daher "
                + "beendet werden.\n\nGrund: %s.\n\n%s",
            Globals.PROJECT_TITLE, reason,
            ExceptionFormatter.formatException(triggeringException));

        /* Zeige die Nachricht an */
        showMessage(JOptionPane.ERROR_MESSAGE, "%s", fullMessage);

        /* Beende das Programm */
        if (_instance != null)
            _instance.closeWindow();

        /* Erzwinge das Beenden aller Threads */
        Runtime.getRuntime().halt(1);
    }

    /**
     * Dieser statische Initialisierer legt eine globale Instanz des Fensters an
     * (es gibt nur ein Fenster dieser Sorte).
     */
    static {
        /*
         * Eine frühe Version des Clients benutzte im Statistikpanel das mit
         * Java ausgelieferte Theme "Nimbus". Versuche, dieses einzustellen,
         * aber schlage nicht fehl, wenn dies nicht gelingen sollte.
         */
        try {
            for (LookAndFeelInfo theme : UIManager.getInstalledLookAndFeels()) {
                if (theme.getName().equals("Nimbus")) {
                    UIManager.setLookAndFeel(theme.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
        }

        /*
         * Erzwinge das Laden aller Bilder, damit sofort auf die Bilder
         * zugegriffen werden kann. Wenn versucht wird, die Grafik des
         * Titelbildschirms im Hauptmenü zu malen, und es müssen erst die Bilder
         * geladen werden, kann dies mächtig schief gehen.
         */
        try {
            Class.forName("org.g5p.dynamiteboy.client.Images");
        } catch (ClassNotFoundException e) {
        }

        /* Initialisiere die globale Instanz des Fensters */
        _instance = new ClientWindow();
    }

    /**
     * main() ist die Startfunktion eines jeden Java-Programms. Bereits vor dem
     * Betreten von main() wurde unser Fenster angelegt; es muss nur noch
     * angezeigt werden. Danach beendet sich der main-Thread, aber das Programm
     * nicht, da die Ereignisse des angezeigten Fensters in einem anderen Thread
     * behandelt werden.
     *
     * @param args
     *            Argumente, die dem Programm zum Start übergeben wurden
     */
    public static void main(String[] args) {
        /* Zeigt bloß das Fenster an. Mehr brauchen wir nicht zu tun */
        _instance.setVisible(true);
    }
}