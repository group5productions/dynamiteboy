/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

import org.apache.commons.validator.routines.InetAddressValidator;

/**
 * Diese Klasse implementiert das Panel, mit welchem Einstellungen des Clients
 * verändert werden können, wie z.B. Zoom und Tasteneingaben.
 *
 * @author Steven Jung
 * @author Thomas Weber
 */
public final class OptionsPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final JSlider _zoomSlider;
    private final JTextField _managementServerField;

    /**
     * Diese Methode speichert die geänderten Einstellungen.
     */
    private void _saveOptions() {
        String serverAddress = _managementServerField.getText().trim();
        if (serverAddress.isEmpty()) {
            ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                "Bitte gebe die Adresse eines erreichbaren Verwaltungsservers an.");
            return;
        }

        /* Überprüfe die Adresse: Erlaube gültige IPv4- und IPv6-Adressen */
        InetAddressValidator validator = new InetAddressValidator();
        if (!validator.isValid(serverAddress)) {
            /* Keine IP-Adresse, erlaube nur auflösbare Hostnamen */
            try {
                InetAddress.getByName(serverAddress);
            } catch (UnknownHostException e) {
                ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                    "Die Eingabe '%s' ist keine gültige Adresse eines "
                        + "Verwaltungsservers.\n\nBitte gebe die Adresse "
                        + "eines erreichbaren Verwaltungsservers an.",
                    serverAddress);
                return;
            }
        }

        /* Speichere die Optionen */
        ClientOptions options = ClientWindow.getInstance().getOptions();
        options.setArenaTileScaleFactor(_zoomSlider.getValue());
        options.setManagementServerAddress(serverAddress);
    }

    /**
     * Diese Methode lädt alle konfigurierbaren Steuerelemente mit den bislang
     * gesetzten Optionen.
     */
    public void loadOptions() {
        ClientOptions options = ClientWindow.getInstance().getOptions();
        _zoomSlider.setValue(options.getArenaTileScaleFactor());

        _managementServerField.setText(options.getManagementServerAddress());
    }

    /**
     * Erzeugt das Panel, mit welchem Einstellungen des Clients geändert werden
     * können.
     */
    public OptionsPanel() {
        super("Optionen");

        /*
         * Button zum Übernehmen der Einstellungen. Implementiere das Speichern
         * mittels Lambda-Ausdruck.
         */
        JButton applyButton = createStyledJButton("Übernehmen");
        applyButton.addActionListener((ActionEvent event) -> _saveOptions());

        /*
         * Button zum Zurücksetzen der Einstellungen. Implementiere das
         * Zurücksetzen mittels Lambda-Ausdruck.
         */
        JButton resetButton = createStyledJButton("Zurücksetzen");
        resetButton.addActionListener((ActionEvent event) -> loadOptions());

        /*
         * Button zur Rückkehr ins Hauptmenü. Implementiere es mittels
         * Lambda-Ausdruck.
         */
        JButton mainMenuButton = createStyledJButton("Hauptmenü");
        mainMenuButton.addActionListener((ActionEvent event) -> ClientWindow
            .getInstance().setWindowState(ClientWindowState.MAIN_MENU));

        /* Füge die Buttons zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(applyButton, resetButton, mainMenuButton);

        JLabel label;
        JPanel horizontalPanel;

        /*
         * Fenstergröße
         */
        horizontalPanel = createHorizontalJPanel();
        this.add(horizontalPanel);

        label = new JLabel("Zoom:");
        horizontalPanel.add(label);
        horizontalPanel.add(createHorizontalRigid(2));

        _zoomSlider = new JSlider(1, 7);
        label.setLabelFor(_zoomSlider);
        _zoomSlider.setMajorTickSpacing(1);
        _zoomSlider.setPaintLabels(true);
        _zoomSlider.setSnapToTicks(true);
        _zoomSlider.setOpaque(false);
        horizontalPanel.add(_zoomSlider);

        /* Abstand */
        this.add(createVerticalRigid(2));

        /*
         * Adresse Verwaltungsserver
         */
        horizontalPanel = createHorizontalJPanel();
        this.add(horizontalPanel);

        label = new JLabel("Adresse des Verwaltungsservers:");
        horizontalPanel.add(label);
        horizontalPanel.add(createHorizontalRigid(2));

        _managementServerField = new JTextField(40);
        horizontalPanel.add(_managementServerField);
    }
}
