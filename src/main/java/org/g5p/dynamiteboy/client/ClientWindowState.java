/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

/**
 * Diese Enumeration enthält alle Zustände, die das Clientfenster haben darf.
 *
 * @author Thomas Weber
 */
public enum ClientWindowState {
    /**
     * Ein Spezialzustand "nicht existierend", welcher benutzt wird, um ein
     * Panel bei Programmende manuell zu "entladen".
     */
    NOT_EXISTING,

    /**
     * Das Hauptmenü des Spiels.
     */
    MAIN_MENU,

    /**
     * Menü zur Darstellung des Tutorials.
     */
    TUTORIAL_MENU,

    /**
     * Menü zur Serverauswahl im Online-Multiplayer.
     */
    SERVER_SELECTION_MENU,

    /**
     * Menü zum Betrieb eines eigenen Spieleservers.
     */
    CUSTOM_SERVER_MENU,

    /**
     * Menü während des Lobbymoduses eines fremden Spieleservers.
     */
    GAME_SERVER_LOBBY_MENU,

    /**
     * Konfiguration des Offline-Spiels.
     */
    OFFLINE_PLAY_MENU,

    /**
     * Das eigentliche Spielfeld.
     */
    GAME_ARENA,

    /**
     * Der Highscore am Ende eines jeden Spiels
     */
    STATISTICS_MENU,

    /**
     * Konfiguration des Clients (Zoom, Steuerung...)
     */
    OPTIONS_MENU
}
