/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.util.ArrayList;
import java.util.List;

import org.g5p.dynamiteboy.engine.GameArenaClient;
import org.g5p.dynamiteboy.engine.GamePosition;
import org.g5p.dynamiteboy.engine.objects.GameObjectIdentifier;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * Diese Klasse implementiert die auf dem Client laufende KI. Sie wird im
 * Offline-Modus benutzt, um gegen lokale Bots zu spielen bzw. im Online-Modus,
 * um sich durch den Bot vertreten zu lassen.
 *
 * Es ist möglich, diese KI zu besiegen, wenn es einem gelingt, sie in die Enge
 * zu treiben -- wenn sie zwischen zwei Bomben eingeschlossen ist.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class PlayerAI {
    /**
     * Annahme über den Radius der Explosion. Dieser sollte relativ groß sein,
     * da die Chance auf ein radiuserhöhendes Powerup relativ hoch ist.
     */
    private static final int _AI_DEFAULT_BOMB_RADIUS = 5;

    /**
     * Anzahl der Versuche, die der State-Tracker der KI unternimmt, ein
     * gesendetes Kommando, welches timingbedingt nicht angekommen ist, erneut
     * zu senden.
     */
    private static final int _TRACKER_MAX_RETRIES = 3;

    private final GameArenaClient _gameArenaClient;
    private final int _playerIndex;

    private PlayerAction _lastDirection;
    private List<GamePosition> _dangerousPositions;

    private GamePosition _trackerLastPosition;
    private PlayerAction _trackerLastAction;
    private int _trackerRetries;

    /**
     * Sammelt Koordinaten, die bei der Explosion einer Bombe von dieser
     * getroffen werden. Diese gehen in die selbe Richtung, welche durch xDelta
     * und yDelta dargestellt ist.
     *
     * @param bomb
     *            Mittelpunkt der Bombe
     * @param xDelta
     *            Indikator der Richtung. +1 geht nach rechts, -1 nach links, 0
     *            bleibt stehen. Bei != 0 muss yDelta 0 betragen.
     * @param yDelta
     *            Indikator der Richtung. +1 geht nach unten, -1 nach oben, 0
     *            bleibt stehen. Bei != 0 muss xDelta 0 betragen.
     */
    private void _addDangerousBombSpread(GamePosition bomb, int xDelta,
        int yDelta) {
        /* Füge alle Felder entlang einer Explosionsrichtung hinzu */
        for (int pos = 1; pos <= _AI_DEFAULT_BOMB_RADIUS; ++pos) {

            /* Noch in den Limits der Arena? */
            int currentX = bomb.x + xDelta * pos;
            int currentY = bomb.y + yDelta * pos;
            if (!_gameArenaClient.areCoordinatesInBounds(currentX, currentY))
                break;

            /*
             * Stoppe an nicht durchlaufbaren Elementen, außer an zerstörbarer
             * Wand, da nehme es noch rein
             */
            if (!_gameArenaClient.isArenaObjectWalkable(currentX, currentY)) {
                if (_gameArenaClient.getArenaObject(currentX,
                    currentY) != GameObjectIdentifier.BREAKABLE_WALL)
                    break;
            }

            /* Neues Feld */
            _dangerousPositions.add(new GamePosition(currentX, currentY));

            /* Jetzt aber wirklich */
            if (!_gameArenaClient.isArenaObjectWalkable(currentX, currentY))
                break;
        }
    }

    /**
     * Sammelt Koordinaten, die bei der Explosion aller Bombe von dieser
     * getroffen werden. Für jede Bombe wird sein Mittelpunkt sowie die
     * geschätzte Ausbreitung der Explosion in alle vier Richtungen
     * hinzugenommen.
     */
    private void _determineDangerousPositions() {
        /*
         * Nehme für jede Bombe in der Arena sein Epizentrum und die Ausbreitung
         * der Explosion
         */
        List<GamePosition> bombPositions = _gameArenaClient.getBombPositions();
        for (GamePosition bomb : bombPositions) {
            _dangerousPositions.add(bomb);
            _addDangerousBombSpread(bomb, 1, 0);
            _addDangerousBombSpread(bomb, 0, 1);
            _addDangerousBombSpread(bomb, -1, 0);
            _addDangerousBombSpread(bomb, 0, -1);
        }
    }

    /**
     * Legt fest, ob eine Position aufgrund zukünftiger Bombenexplosionen
     * gefährlich ist.
     *
     * @param x
     *            X-Koordinate, die überprüft werden soll
     * @param y
     *            Y-Koordinate, die überprüft werden soll
     * @return true, wenn die Stelle gefährlich ist, ansonsten false
     */
    private boolean _isPositionDangerous(int x, int y) {
        /* Position außerhalb der Arena? */
        if (!_gameArenaClient.areCoordinatesInBounds(x, y))
            return false;

        /* Suche nach der Position im Array. Wenn gefunden, dann gefährlich */
        for (GamePosition position : _dangerousPositions)
            if (position.x == x && position.y == y) {
                return true;
            }

        /* Wenn Hindernis, dann gefährlich */
        if (!_gameArenaClient.isArenaObjectWalkable(x, y))
            return true;

        /* Ungefährlich */
        return false;
    }

    /**
     * Legt eine neue Instanz der künstlichen Intelligenz an.
     *
     * @param gameArenaClient
     *            Instanz einer Spielarena, so wie sie der Client kennt und
     *            darstellt
     * @param playerIndex
     *            Nummer des Spielers, entweder 1, 2, 3 oder 4
     */
    public PlayerAI(GameArenaClient gameArenaClient, int playerIndex) {
        this._gameArenaClient = gameArenaClient;
        this._playerIndex = playerIndex;

        this._lastDirection = PlayerAction.MOVE_LEFT;
        this._dangerousPositions = new ArrayList<GamePosition>();

        _trackerLastPosition = new GamePosition();
        _trackerLastAction = null;
        _trackerRetries = -1;
    }

    /**
     * Führt einen Schritt der KI durch, unabhängig von notwendigen
     * Wiederholungen.
     *
     * @return Aktion, die die KI seinen kontrollierten Spieler anweist, oder
     *         null, wenn keine Aktion durchgeführt werden soll
     */
    private PlayerAction _performAIStepInternal() {
        /* Bestimme alle Koordinaten, die für uns gefährlich sind */
        _dangerousPositions.clear();
        _determineDangerousPositions();

        /*
         * Lade Koordinaten von uns. Tue nichts, wenn wir nicht auf dem Feld
         * sichtbar oder gar tot sind.
         */
        GamePosition playerPosition = _gameArenaClient
            .getPlayerPosition(_playerIndex);
        if (playerPosition.x < 0 || playerPosition.y < 0)
            return null;

        /*
         * Gibt es gefährliche Orte? Dann Sonderstellung.
         */
        if (!_dangerousPositions.isEmpty()) {
            /*
             * Ungefährlich? Dann bleibe stehen, bis die Gefahr gebannt ist.
             */
            if (!_isPositionDangerous(playerPosition.x, playerPosition.y))
                return null;

            /*
             * Versuche, eine Ausweichmöglichkeit zu finden. Schaue in alle vier
             * Richtungen, ob wir ein Feld erreichen können, welches als
             * ungefährlich eingeschätzt wird. Wenn ja, laufe in Richtung des
             * Feldes. Ansonsten laufe in eine Richtung, die es uns ermöglich,
             * irgendwie weg zu kommen, auch wenn wir weiter im gefährlichen
             * Bereich bleiben.
             */
            else {
                boolean rightNotBlocked = true;
                boolean leftNotBlocked = true;
                boolean upNotBlocked = true;
                boolean downNotBlocked = true;

                /*
                 * Teste nach rechts.
                 */
                for (int i = playerPosition.x + 1; i < _gameArenaClient
                    .getArenaWidth(); ++i) {
                    if (!_gameArenaClient.isArenaObjectWalkable(i,
                        playerPosition.y)) {
                        if (i == playerPosition.x + 1)
                            rightNotBlocked = false;
                        break;
                    } else if (!_isPositionDangerous(i, playerPosition.y)) {
                        _lastDirection = PlayerAction.MOVE_RIGHT;
                        return _lastDirection;
                    }
                }

                /*
                 * Teste nach links.
                 */
                for (int i = playerPosition.x - 1; i >= 0; --i) {
                    if (!_gameArenaClient.isArenaObjectWalkable(i,
                        playerPosition.y)) {
                        if (i == playerPosition.x - 1)
                            leftNotBlocked = false;
                        break;
                    } else if (!_isPositionDangerous(i, playerPosition.y)) {
                        _lastDirection = PlayerAction.MOVE_LEFT;
                        return _lastDirection;
                    }
                }

                /*
                 * Teste nach oben.
                 */
                for (int i = playerPosition.y - 1; i >= 0; --i) {
                    if (!_gameArenaClient
                        .isArenaObjectWalkable(playerPosition.x, i)) {
                        if (i == playerPosition.y - 1)
                            upNotBlocked = false;
                        break;
                    } else if (!_isPositionDangerous(playerPosition.x, i)) {
                        _lastDirection = PlayerAction.MOVE_UP;
                        return _lastDirection;
                    }
                }

                /*
                 * Teste nach unten.
                 */
                for (int i = playerPosition.y + 1; i < _gameArenaClient
                    .getArenaHeight(); ++i) {
                    if (!_gameArenaClient
                        .isArenaObjectWalkable(playerPosition.x, i)) {
                        if (i == playerPosition.y + -1)
                            downNotBlocked = false;
                        break;
                    } else if (!_isPositionDangerous(playerPosition.x, i)) {
                        _lastDirection = PlayerAction.MOVE_DOWN;
                        return _lastDirection;
                    }
                }

                /*
                 * Kann nirgends hin rennen. Laufe in eine Richtung, in der wir
                 * nicht sofort blockiert werden.
                 */
                if (rightNotBlocked) {
                    _lastDirection = PlayerAction.MOVE_RIGHT;
                    return _lastDirection;
                }
                if (leftNotBlocked) {
                    _lastDirection = PlayerAction.MOVE_LEFT;
                    return _lastDirection;
                }
                if (upNotBlocked) {
                    _lastDirection = PlayerAction.MOVE_UP;
                    return _lastDirection;
                }
                if (downNotBlocked) {
                    _lastDirection = PlayerAction.MOVE_DOWN;
                    return _lastDirection;
                }
            }
        }

        /*
         * Herrscht in allen vier Richtungen Gefahr und/oder Unpassierbarkeit,
         * dann bleibe stehen, dann bleibe stehen, denn die eigene Stelle ist
         * sicher.
         */
        if (_isPositionDangerous(playerPosition.x + 1, playerPosition.y)
            && _isPositionDangerous(playerPosition.x, playerPosition.y + 1)
            && _isPositionDangerous(playerPosition.x - 1, playerPosition.y)
            && _isPositionDangerous(playerPosition.x, playerPosition.y - 1)
            && !_isPositionDangerous(playerPosition.x, playerPosition.y)) {
            return null;
        }

        /*
         * Ansonsten versuche Bewegungen in drei Richtungen. Wenn wir in einer
         * Sackgasse landen, platziere eine Bombe.
         */
        for (int i = 0; i < 3; i++) {
            switch (_lastDirection) {
            case MOVE_DOWN:
                /*
                 * Versuche, nach links zu laufen, ansonten nach rechts, dann
                 * mit Wiederholung.
                 */
                if (!_isPositionDangerous(playerPosition.x - 1,
                    playerPosition.y)) {
                    _lastDirection = PlayerAction.MOVE_LEFT;
                    return _lastDirection;
                } else {
                    _lastDirection = PlayerAction.MOVE_RIGHT;
                }
                break;
            case MOVE_LEFT:
                /*
                 * Versuche, nach oben zu laufen, ansonten nach unten, dann mit
                 * Wiederholung.
                 */
                if (!_isPositionDangerous(playerPosition.x,
                    playerPosition.y - 1)) {
                    _lastDirection = PlayerAction.MOVE_UP;
                    return _lastDirection;
                } else {
                    _lastDirection = PlayerAction.MOVE_DOWN;
                }
                break;
            case MOVE_RIGHT:
                /*
                 * Versuche, nach unten zu laufen, ansonten nach oben, dann mit
                 * Wiederholung.
                 */
                if (!_isPositionDangerous(playerPosition.x,
                    playerPosition.y + 1)) {
                    _lastDirection = PlayerAction.MOVE_DOWN;
                    return _lastDirection;
                } else {
                    _lastDirection = PlayerAction.MOVE_UP;
                }
                break;
            case MOVE_UP:
                /*
                 * Versuche, nach rechts zu laufen, ansonten nach links, dann
                 * mit Wiederholung.
                 */
                if (!_isPositionDangerous(playerPosition.x + 1,
                    playerPosition.y)) {
                    _lastDirection = PlayerAction.MOVE_RIGHT;
                    return _lastDirection;
                } else {
                    _lastDirection = PlayerAction.MOVE_LEFT;
                }
                break;

            /* Wir speichern nur die Himmelsrichtungen */
            default:
                break;
            }
        }

        /* Von drei Seiten blockiert: Platziere eine Bombe */
        return PlayerAction.PLANT_BOMB;
    }

    /**
     * Führt einen Schritt der KI durch. Wenn diese feststellt, dass ihr Schritt
     * noch nicht durch ein Update reflektiert wurde, versucht sie, die
     * Ausführung mehrmals zu wiederholen. Scheitert dies, führt die KI
     * irgendeinen anderen Schritt durch.
     *
     * @return Aktion, die die KI seinen kontrollierten Spieler anweist
     */
    public PlayerAction performAIStep() {
        /*
         * Haben wir eine Aktion durchgeführt? Ist diese nicht "Nichts tun"?
         * Dann überprüfe, ob sich die Position des Spielers verändert hat (bei
         * einem Schritt) oder ob eine Bombe an seiner Stelle liegt (wenn Bombe
         * gelegt wurde) und sende den letzten Schritt erneut, wenn sich die
         * Position nicht verändert hat.
         */
        if (_trackerRetries >= 0 && _trackerRetries < _TRACKER_MAX_RETRIES
            && _trackerLastAction != null) {

            /*
             * Es wurde eine Bombe gelegt. Ist sie sichtbar? Wenn nicht,
             * wiederholen.
             */
            if (_trackerLastAction == PlayerAction.PLANT_BOMB) {
                GameObjectIdentifier ident = _gameArenaClient.getArenaObject(
                    _trackerLastPosition.x, _trackerLastPosition.y);

                if (!GameObjectIdentifier.isIdentifierABomb(ident)) {
                    ++_trackerRetries;
                    return _trackerLastAction;
                }
            }

            /*
             * Der Spieler hat sich bewegt. Hat sich seine Position geändert?
             * Wenn nicht, wiederholen.
             */
            else {
                GamePosition pos = _gameArenaClient
                    .getPlayerPosition(_playerIndex);

                if (_trackerLastPosition.x == pos.x
                    && _trackerLastPosition.y == pos.y) {
                    ++_trackerRetries;
                    return _trackerLastAction;
                }
            }
        }

        /*
         * Führe einen neuen Schritt aus. Dies geschieht dann, wenn es unser
         * aller erster Schritt ist, ODER der letzte Schritt wurde wiederholt
         * gesendet und die Wiederholungen sind abgelaufen, ODER der letzte
         * Schritt hat tatsächlich die Position verändert.
         */
        PlayerAction action = _performAIStepInternal();

        /*
         * Speichere den Zustand für den nächsten Aufruf. Auch wenn wir gerade
         * nichts tun.
         */
        _trackerRetries = 0;
        _trackerLastAction = action;
        _trackerLastPosition = _gameArenaClient.getPlayerPosition(_playerIndex);
        return action;
    }
}
