/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Diese abstrakte Klasse ermöglicht die Erzeugung von Panels und Subpanels
 * (Panel in Panel), die ein einheitliches Design bieten, welches auf den Stil
 * des Clients abgestimmt ist. Grundlage für die Anordnung von Steuerelementen
 * in diesen Panels ist ein vertikales BoxLayout; diese Klasse stellt zur
 * Anordnung von Steuerelementen Hilfsfunktionen zur Verfügung.
 *
 * @author Thomas Weber
 */
public abstract class AbstractPanel extends JPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    protected static final long serialVersionUID = 1L;

    /**
     * Horizontaler Abstand zwischen Steuerelementen in Pixeln
     */
    protected static final int BUTTON_SPACE_H = 13;

    /**
     * Vertikaler Abstand zwischen Steuerelementen in Pixeln
     */
    protected static final int BUTTON_SPACE_V = 13;

    /**
     * Hintergrundfarbe des Panels
     */
    protected static final Color PANEL_BACKGROUND_COLOR = new Color(0, 191,
        255);

    /**
     * Textfarbe von Schaltflächen
     */
    protected static final Color BUTTON_TEXT_COLOR = new Color(255, 255, 204);

    /**
     * Hintergrundfarbe von Schaltflächen
     */
    protected static final Color BUTTON_BACKGROUND_COLOR = new Color(51, 153,
        102);

    /**
     * Breite der im Client dargestellten Panels in Pixeln
     */
    public static final int PANEL_DEFAULT_WIDTH_PIXELS = 630;

    /**
     * Breite der im Client dargestellten Panels in Pixeln (ohne Rahmen)
     */
    public static final int PANEL_DEFAULT_CONTENT_WIDTH_PIXELS = PANEL_DEFAULT_WIDTH_PIXELS
        - 2 * BUTTON_SPACE_H;

    private final JPanel _horizontalButtonPanel;
    private final JLabel _headingLabel;

    /**
     * Erstellt ein JPanel, welches ein horizontales BoxLayout enthält.
     *
     * @return Neues JPanel mit horizontalem BoxLayout
     */
    protected JPanel createHorizontalJPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.setOpaque(false);
        return panel;
    }

    /**
     * Erstellt ein JPanel, welches ein vertikales BoxLayout enthält.
     *
     * @return Neues JPanel mit horizontalem BoxLayout
     */
    protected JPanel createVerticalJPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setOpaque(false);
        return panel;
    }

    /**
     * Erzeugt einen horizontalen Abstand eines Vielfachen der horizontalen
     * Button-Abstand-Größe.
     *
     * @param multiplier
     *            Vielfaches der Größe
     * @return Neuer Abstand
     */
    protected Component createHorizontalRigid(final int multiplier) {
        return Box
            .createRigidArea(new Dimension(multiplier * BUTTON_SPACE_H, 0));
    }

    /**
     * Erzeugt einen vertikalen Abstand eines Vielfachen der vertikalen
     * Button-Abstand-Größe.
     *
     * @param multiplier
     *            Vielfaches der Größe
     * @return Neuer Abstand
     */
    protected Component createVerticalRigid(final int multiplier) {
        return Box
            .createRigidArea(new Dimension(0, multiplier * BUTTON_SPACE_V));
    }

    /**
     * Erzeugt ein JPanel, welches ein JLabel mit der korrekten Ausrichtung
     * packt.
     *
     * @param label
     *            JLabel, welches in das JPanel gepackt werden soll
     * @return Neues JPanel mit existierendem Label in korrekter Ausrichtung
     */
    public JPanel packJLabelAlignedIntoJPanel(final JLabel label,
        final float orientation) {
        /* Erzeuge Panel */
        JPanel panel = createHorizontalJPanel();

        /* Rechts ausrichten: erst Glue, dann Label */
        if (orientation == Component.RIGHT_ALIGNMENT) {
            label.setAlignmentX(Component.RIGHT_ALIGNMENT);
            panel.add(Box.createHorizontalGlue());
            panel.add(label);
        }

        /* Zentriert ausrichten: Erst Glue, dann Label, dann Glue */
        else if (orientation == Component.CENTER_ALIGNMENT) {
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(Box.createHorizontalGlue());
            panel.add(label);
            panel.add(Box.createHorizontalGlue());
        }

        /* Links ausrichten: Erst Label, dann Glue */
        else {
            label.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(label);
            panel.add(Box.createHorizontalGlue());
        }
        return panel;
    }

    /**
     * Erzeugt eine Schaltfläche (JButton) mit dem Standardstil, der sich durch
     * den Client zieht.
     *
     * @param text
     *            Text, der in der Schaltfläche angezeigt werden soll
     * @return Neue Schaltfläche
     */
    protected JButton createStyledJButton(final String text) {
        /* Benutze styleComponent zum Einfärben */
        return styleComponent(new JButton(text));
    }

    /**
     * Legt die Farben einer Swing-Komponente in den Farben des Standardstils
     * fest, der sich durch den gesamten Client zieht.
     *
     * @param component
     *            Komponente, deren Farben festgelegt werden soll
     * @return Identisch mit component
     */
    protected <T extends JComponent> T styleComponent(final T component) {
        component.setBackground(BUTTON_BACKGROUND_COLOR);
        component.setForeground(BUTTON_TEXT_COLOR);
        return component;
    }

    /**
     * Legt ein horizontales JPanel an, welches eine Menge von Schaltflächen
     * umfasst, die alle links angeordnet sind. Das neue JPanel wird automatisch
     * zu diesem Panel hinzugefügt.
     *
     * @param buttons
     *            Menge von Schaltflächen, die zu diesem Panel gehören sollen
     */
    protected void createLeftAlignedButtonPanel(JButton... buttons) {
        /* Benötigt mindestens einen Button */
        if (buttons.length < 1)
            throw new IllegalArgumentException("Mindestens 1 Button benötigt");

        /* Benötigt das Panel */
        if (_horizontalButtonPanel == null) {
            throw new IllegalArgumentException(
                "Panel-Instanz erlaubt diese Operation nicht");
        }

        /* Füge die Buttons hinzu */
        for (int i = 0; i < buttons.length; ++i) {
            _horizontalButtonPanel.add(buttons[i]);

            /*
             * Zwischen Buttons kleiner Abstand, nach dem letzten Button kommt
             * ein Glue, welcher die Buttons nach links drückt
             */
            if (i < buttons.length - 1)
                _horizontalButtonPanel.add(createHorizontalRigid(1));
            else
                _horizontalButtonPanel.add(Box.createHorizontalGlue());
        }
    }

    /**
     * Ändert die angezeigte Überschrift.
     *
     * @param heading
     *            Überschrift, die angezeigt werden soll
     */
    public void setPanelHeading(final String heading) {
        /* Benötigt das Label */
        if (_horizontalButtonPanel == null) {
            throw new IllegalArgumentException(
                "Panel-Instanz erlaubt diese Operation nicht");
        }

        /* Ändere Text des Labels */
        _headingLabel.setText(heading);
    }

    /**
     * Diese überlagerte Methode gibt die gewünschte Breite sowie Höhe des
     * Panels zurück.
     *
     * @return gewünschte Breite und Höhe des Panels
     */
    @Override
    public Dimension getPreferredSize() {
        Dimension origSize = super.getPreferredSize();
        return new Dimension(PANEL_DEFAULT_WIDTH_PIXELS, origSize.height);
    }

    /**
     * Erzeugt ein neues (abstraktes) (Sub-)Panel mit dem Standardstil, der sich
     * durch den Client zieht. Es enthält keine Überschrift, und eine Reihe mit
     * links angeordneten Buttons ist auch nicht möglich.
     */
    public AbstractPanel(boolean disableBorder) {
        /* Paneleinstellungen */
        this.setBackground(PANEL_BACKGROUND_COLOR);
        if (!disableBorder) {
            this.setBorder(new EmptyBorder(BUTTON_SPACE_V, BUTTON_SPACE_H,
                BUTTON_SPACE_V, BUTTON_SPACE_H));
        }
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        /* Deaktiviere das Panel mit den Buttons sowie die Überschrift */
        _horizontalButtonPanel = null;
        _headingLabel = null;
    }

    /**
     * Erzeugt ein neues (abstraktes) Panel mit dem Standardstil, der sich durch
     * den Client zieht. Zur Anordnung der Steuerelemente im Panel wird die
     * Klasse BoxLayout verwendet.
     */
    public AbstractPanel(final String heading) {
        /* Paneleinstellungen */
        this.setBackground(PANEL_BACKGROUND_COLOR);
        this.setBorder(new EmptyBorder(BUTTON_SPACE_V, BUTTON_SPACE_H,
            BUTTON_SPACE_V, BUTTON_SPACE_H));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        /* Lege das horizontal Panel für die Buttons an */
        _horizontalButtonPanel = createHorizontalJPanel();
        this.add(_horizontalButtonPanel);

        /* Halte Abstand ein */
        this.add(createVerticalRigid(1));

        /* Lege die Überschrift an (kann geändert werden) */
        JPanel headingPanel = createHorizontalJPanel();
        _headingLabel = new JLabel(heading);
        _headingLabel.setFont(new Font("Sans", Font.BOLD, 16));
        _headingLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        headingPanel.add(_headingLabel);
        headingPanel.add(Box.createHorizontalGlue());
        this.add(headingPanel);

        /* Halte Abstand ein */
        this.add(createVerticalRigid(1));
    }
}
