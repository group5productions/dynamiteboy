/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import org.g5p.dynamiteboy.networking.messages.RoundEndHighscoreClientCommand;
import org.g5p.dynamiteboy.tools.HighscorePlayerEntry;

/**
 * Diese Klasse implementiert das Panel, welches es ermöglicht, einen fremden
 * Spieleserver zu kontaktieren, um online im Multiplayer zu spielen.
 *
 * @author Steven Jung
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class StatisticsPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Zustände, die dieses Panel annehmen kann.
     */
    private enum StatisticsPanelState {
        /**
         * Nur zwischen Konstruktoraufruf und erstem Wechsel hierher ungültig.
         */
        INVALID,

        /**
         * Statistiken für Onlinespiel: Ermögliche nur das Zurückkehren in die
         * Lobby.
         */
        ONLINE,

        /**
         * Statistiken für Offlinespiel: Ermögliche das Zurückkehren sowohl in
         * das Offline-Spiel-Menü als auch ins Hauptmenü.
         */
        OFFLINE
    }

    private static final StartNewGameActionListener _startNewGameActionListener = new StartNewGameActionListener();
    private static final ReturnToLobbyActionListener _returnToLobbyActionListener = new ReturnToLobbyActionListener();

    private final RoundStatisticsTableModel _highscoreTableModel;
    private final JButton _newRoundOrBackToLobbyButton;
    private final JButton _mainMenuButton;

    private StatisticsPanelState _lastState;

    /**
     * Diese interne Klasse implementiert das Datenmodell für die Tabelle, die
     * den Highscore der zuletzt gespielten Runde darstellt.
     */
    private final class RoundStatisticsTableModel extends AbstractTableModel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Index der Spalte "Spieler"
         */
        private static final int _COLUMN_PLAYER = 0;

        /**
         * Index der Spalte "Kills"
         */
        private static final int _COLUMN_KILLS = 1;

        /**
         * Index der Spalte "Wände"
         */
        private static final int _COLUMN_WALLS = 2;

        /**
         * Index der Spalte "Schritte"
         */
        private static final int _COLUMN_STEPS = 3;

        /**
         * Index der Spalte "Bomben"
         */
        private static final int _COLUMN_BOMBS = 4;

        /**
         * Index der Spalte "Powerups"
         */
        private static final int _COLUMN_POWERUPS = 5;

        /**
         * Index der Spalte "Selfkill?"
         */
        private static final int _COLUMN_SELFKILL = 6;

        /**
         * Index der Spalte "Letzter?"
         */
        private static final int _COLUMN_LAST_ONE = 7;

        /**
         * Index der Spalte "Punkte"
         */
        private static final int _COLUMN_SCORE = 8;

        private List<HighscorePlayerEntry> _players = null;

        /**
         * Gibt den Namen der Spalte für einen Spaltenindex col zurück.
         *
         * @param col
         *            Index der Spalte, ab 0 beginnend
         */
        @Override
        public String getColumnName(int col) {
            switch (col) {
            case _COLUMN_PLAYER:
                return "Spieler";
            case _COLUMN_KILLS:
                return "Kills";
            case _COLUMN_WALLS:
                return "Wände";
            case _COLUMN_STEPS:
                return "Schritte";
            case _COLUMN_BOMBS:
                return "Bomben";
            case _COLUMN_POWERUPS:
                return "Powerups";
            case _COLUMN_SELFKILL:
                return "Selfkill?";
            case _COLUMN_LAST_ONE:
                return "Letzter?";
            case _COLUMN_SCORE:
                return "Punkte";
            }
            return "";
        }

        /**
         * Gibt die Anzahl der Spalten in der Tabelle zurück.
         *
         * @return Stets letzter Spaltenindex plus 1
         */
        @Override
        public int getColumnCount() {
            return _COLUMN_SCORE + 1;
        }

        /**
         * Gibt die Anzahl der Zeilen in der Tabelle zurück.
         *
         * @return Anzahl der Zeilen = Anzahl der Highscore-Einträge
         */
        @Override
        public int getRowCount() {
            return _players.size();
        }

        /**
         * Fragt den Inhalt einer Tabellenzelle in Zeile arg0 und Spalte arg1
         * ab.
         *
         * @param arg0
         *            Zeile der Tabelle, beginnend ab 0
         * @param arg1
         *            Spalte der Tabelle, beginnend ab 0
         * @return Inhalt der Tabellenzelle
         */
        @Override
        public Object getValueAt(int arg0, int arg1) {
            /* Geht nur dann, wenn es einen Highscore gibt */
            if (_players == null)
                return null;

            /* Nicht die Grenzen der Liste überschreiten */
            if (arg0 >= _players.size())
                return null;

            /* Lade den Eintrag für die Zeile */
            HighscorePlayerEntry entry = _players.get(arg0);
            if (entry == null)
                return null;

            /* Gebe das zurück, was die Tabelle für die jeweilige Spalte will */
            switch (arg1) {
            case _COLUMN_PLAYER:
                return entry.getName();
            case _COLUMN_KILLS:
                return entry.getKilledPlayers();
            case _COLUMN_WALLS:
                return entry.getDestroyedWalls();
            case _COLUMN_STEPS:
                return entry.getWalkedSteps();
            case _COLUMN_BOMBS:
                return entry.getPlantedBombs();
            case _COLUMN_POWERUPS:
                return entry.getCollectedPowerups();
            case _COLUMN_SELFKILL:
                return entry.isSuicided() ? "Ja" : "Nein";
            case _COLUMN_LAST_ONE:
                return entry.isLastPlayer() ? "Ja" : "Nein";
            case _COLUMN_SCORE:
                return entry.getScore();
            }
            return null;
        }

        /**
         * Gibt den Klassentyp zurück, welchen alle Werte in einer einzelnen
         * Spalte besitzen.
         *
         * @param index
         *            0-basierter Index der Spalte, dessen Wertetyp abgefragt
         *            werden soll
         * @return Klasse "String" für Spalten Spalten "Name", "Selfkill?" und
         *         "Letzter?", sonst Klasse "Integer"
         */
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
            /* Spalten "Name", "Selfkill?" und "Letzter?" sind Strings */
            case _COLUMN_PLAYER:
            case _COLUMN_SELFKILL:
            case _COLUMN_LAST_ONE:
                return String.class;

            /* Alle anderen Spalten sind Integer */
            case _COLUMN_KILLS:
            case _COLUMN_WALLS:
            case _COLUMN_STEPS:
            case _COLUMN_BOMBS:
            case _COLUMN_POWERUPS:
            case _COLUMN_SCORE:
                return Integer.class;
            }
            return null;
        }

        /**
         * Befüllt dieses Tabellenmodell mit dem Highscore aus der letzten
         * Spielerunde.
         *
         * @param command
         *            Ein über das Netzwerk übertragener Highscore
         */
        public void updateHighscore(RoundEndHighscoreClientCommand command) {
            /* Frage die Liste ab und sortiere sie absteigend nach Score */
            _players = command.getPlayers();
            Collections.sort(_players, new Comparator<HighscorePlayerEntry>() {
                @Override
                public int compare(HighscorePlayerEntry arg0,
                    HighscorePlayerEntry arg1) {
                    /* Absteigend nach Score */
                    return arg1.getScore() - arg0.getScore();
                }
            });

            /* Weise Tabelle auf Änderung der Daten hin */
            fireTableDataChanged();
        }

        /**
         * Diese Methode passt die Spalten der Tabelle, in der das Modell
         * dargestellt wird, von der Größe her optimal an.
         *
         * @param table
         *            Tabelle, deren Spalten angepasst werden sollen
         */
        public void adjustTableColumnWidths(JTable table) {
            TableColumnModel model = table.getColumnModel();

            /*
             * Die Breiten der Tabelle wurden händisch angepasst, obwohl sie nur
             * unter einer bestimmten Konfiguration von Linux optimal sind.
             */
            model.getColumn(_COLUMN_KILLS).setMaxWidth(37);
            model.getColumn(_COLUMN_WALLS).setMaxWidth(55);
            model.getColumn(_COLUMN_STEPS).setMaxWidth(60);
            model.getColumn(_COLUMN_BOMBS).setMaxWidth(60);
            model.getColumn(_COLUMN_POWERUPS).setMaxWidth(70);
            model.getColumn(_COLUMN_SELFKILL).setMaxWidth(57);
            model.getColumn(_COLUMN_LAST_ONE).setMaxWidth(60);
            model.getColumn(_COLUMN_SCORE).setMaxWidth(55);
        }
    }

    /**
     * Dieser ActionListener wechselt zurück in die Arena für ein neues
     * Offline-Spiel.
     */
    private static final class StartNewGameActionListener
        implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            /*
             * Wechsle in das Menü des Offline-Spiels, damit Einstellungen
             * verändert werden können
             */
            ClientWindow.getInstance()
                .setWindowState(ClientWindowState.OFFLINE_PLAY_MENU);
        }
    }

    /**
     * Dieser ActionListener wechselt zurück in die Lobby für ein neues
     * Online-Spiel.
     */
    private static final class ReturnToLobbyActionListener
        implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            /*
             * Wechsle in das Menü der Lobby. Die existierende Verbindung wird
             * wieder genutzt.
             */
            ClientWindow.getInstance()
                .setWindowState(ClientWindowState.GAME_SERVER_LOBBY_MENU);
        }
    }

    /**
     * Diese Methode lädt alle konfigurierbaren Steuerelemente mit den bislang
     * gesetzten Optionen.
     *
     * @param object
     *            Eigentlich ein Highscore in Form eines
     *            RoundEndHighscoreClientCommand, aber typneutral durch das
     *            Clientfenster übertragen
     * @param isOnlineGame
     *            true, wenn das Spiel online stattfand, ansonsten false
     */
    public void loadOptions(Object object, boolean isOnlineGame) {
        RoundEndHighscoreClientCommand command = (RoundEndHighscoreClientCommand) object;

        /* Lade Daten in die Tabelle */
        _highscoreTableModel.updateHighscore(command);

        /* Verändere die Funktion der Buttons */
        if (isOnlineGame) {
            if (_lastState != StatisticsPanelState.ONLINE) {
                /* Ändere Bedeutung zu "Zurück zur Lobby" */
                _newRoundOrBackToLobbyButton.setText("Zurück zur Lobby");
                _newRoundOrBackToLobbyButton
                    .removeActionListener(_startNewGameActionListener);
                _newRoundOrBackToLobbyButton
                    .addActionListener(_returnToLobbyActionListener);

                /* Ermögliche keine Rückkehr ins Hauptmenü */
                _mainMenuButton.setVisible(false);
                _lastState = StatisticsPanelState.ONLINE;
            }
        } else {
            if (_lastState != StatisticsPanelState.OFFLINE) {
                /* Ändere Bedeutung zu "Neue Runde" */
                _newRoundOrBackToLobbyButton.setText("Neue Runde");
                _newRoundOrBackToLobbyButton
                    .removeActionListener(_returnToLobbyActionListener);
                _newRoundOrBackToLobbyButton
                    .addActionListener(_startNewGameActionListener);

                /* Ermögliche Rückkehr ins Hauptmenü */
                _mainMenuButton.setVisible(true);
                _lastState = StatisticsPanelState.OFFLINE;
            }
        }
    }

    /**
     * Erzeugt das Panel, mit welchem ein Mehrspielerspiel betreten oder
     * erstellt werden kann.
     */
    public StatisticsPanel() {
        super("Highscore");

        this._lastState = StatisticsPanelState.INVALID;

        /*
         * Button, um neue Runde zu starten / zur Lobby zurückzukehren.
         */
        _newRoundOrBackToLobbyButton = createStyledJButton("Button 1");

        /*
         * Button, um zum Hauptmenü zurückzukehren. Implementiert mittels
         * Lambda-Ausdruck.
         */
        _mainMenuButton = createStyledJButton("Hauptmenü");
        _mainMenuButton.addActionListener((ActionEvent event) -> ClientWindow
            .getInstance().setWindowState(ClientWindowState.MAIN_MENU));

        /* Füge die Buttons zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(_newRoundOrBackToLobbyButton,
            _mainMenuButton);

        /*
         * Erzeuge ein ScrollPane, in welchem eine Tabelle gescrollt werden
         * kann.
         */
        JScrollPane scrollPane = new JScrollPane();
        this.add(scrollPane);

        /* Tabelle, in der der Highscore dargestellt wird */
        _highscoreTableModel = new RoundStatisticsTableModel();
        JTable gsTable = new JTable(_highscoreTableModel);
        /*
         * Das Nimbus-Design hat sämtliche Farben in den Tabellenköpfen hart
         * codiert. Das tut den Entwickern überhaupt nicht Leid.
         */
        gsTable.setFillsViewportHeight(true);
        scrollPane.setViewportView(gsTable);
        _highscoreTableModel.adjustTableColumnWidths(gsTable);
    }
}
