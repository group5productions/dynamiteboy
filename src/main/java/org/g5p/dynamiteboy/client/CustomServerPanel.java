/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.gameserver.GameServer;
import org.g5p.dynamiteboy.gameserver.GameServerConfiguration;
import org.g5p.dynamiteboy.tools.ExceptionFormatter;
import org.g5p.dynamiteboy.tools.PortCheckerLabel;

/**
 * Diese Klasse implementiert das Panel, welches es ermöglicht, einen eigenen
 * Spieleserver zu betreiben.
 *
 * @author Thomas Weber
 */
public final class CustomServerPanel extends AbstractPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    private final JButton _startServerButton;
    private final JButton _stopServerButton;
    private final JButton _leaveMenuButton;
    private final PortCheckerLabel _portCheckerLabel;
    private final JTextField _serverNameField;
    private final JTextField _publicHostnameField;
    private final JFormattedTextField _publicPortField;

    private GameServerConfiguration _currentConfig;
    private GameServer _gameServer;

    /**
     * Aktiviert bzw. deaktiviert Steuerelemente, je nachdem, ob der
     * Spieleserver läuft oder nicht.
     *
     * @param serverActive
     *            true, wenn der Server läuft, ansonsten false
     */
    private void _setButtonState(boolean serverActive) {
        if (serverActive) {
            /* Aktiviere nur "Server stoppen", deaktiviere alles andere */
            _startServerButton.setEnabled(false);
            _stopServerButton.setEnabled(true);
            _leaveMenuButton.setEnabled(false);
            _serverNameField.setEnabled(false);
            _publicHostnameField.setEnabled(false);
            _publicPortField.setEnabled(false);

            /*
             * Aktualisiere den Port im Portchecker und ermögliche die
             * Überprüfung des Ports.
             */
            _portCheckerLabel.setPort((int) _publicPortField.getValue());
            _portCheckerLabel.displayPotentiallyOpen();
        } else {
            /* Aktiviere alle Steuerelemente außer "Server stoppen" */
            _startServerButton.setEnabled(true);
            _stopServerButton.setEnabled(false);
            _leaveMenuButton.setEnabled(true);
            _serverNameField.setEnabled(true);
            _publicHostnameField.setEnabled(true);
            _publicPortField.setEnabled(true);

            /* Da der Server nicht läuft, ist der Port auch nicht erreichbar */
            _portCheckerLabel.displayDefinitelyClosed();
        }
    }

    /**
     * Stellt die Konfiguration des Spieleservers aus den Eingaben der
     * Steuerelemente zusammen und startet mit dieser den Spieleserver. Tritt
     * währenddessen ein Problem auf, wird der Start mit einer Fehlermeldung
     * quittiert.
     */
    private void _startServer() {
        /* Lege eine neue Konfiguration an. Exception = Programmende */
        GameServerConfiguration config = new GameServerConfiguration();
        try {
            config.setManagementServerHost(ClientWindow.getInstance()
                .getOptions().getManagementServerAddress());

            config.setServerName(_serverNameField.getText());

            config.setServerListenHost(_publicHostnameField.getText());

            config.setServerListenPort((int) _publicPortField.getValue());
        } catch (Exception e) {
            /* Zeige Fehlermeldung an */
            ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                "Entschuldige, aber die Einstellungen kann ich nicht so "
                    + "übernehmen.\n\nGrund: %s.\n\nStelle sicher, dass die "
                    + "Einstellungen richtig eingegeben sind -- nicht alle "
                    + "werden auch akzeptiert! -- und versuche es nochmal.",
                e.getMessage());
            return;
        }

        /* Richte den Server ein und starte ihn */
        GameServer server;
        try {
            server = new GameServer();
            server.setServerConfiguration(config);

            /* Reflektiere den Stopp des Servers in den Steuerelementen */
            server.setServerStoppedCallback(() -> _stopServer(true));

            /* Los geht's */
            server.startMessageServer();
        } catch (Exception e) {
            /* Zeige Fehlermeldung an */
            ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                "Entschuldige, aber beim Starten des Spieleservers ist ein "
                    + "unerwartetes Problem aufgetreten.\n\nGrund: %s.\n\n%s",
                e.getMessage(), ExceptionFormatter.formatException(e));
            return;
        }
        _currentConfig = config;
        _gameServer = server;

        /* Wechsle den Zustand der Steuerelemente */
        _setButtonState(true);
    }

    /**
     * Stoppt einen eventuell laufenden Spieleserver.
     *
     * @param stoppedFromServerSide
     *            Ist dieser Wert true, wurde der Stopp des Servers selbst
     *            initiiert. In diesem Fall stoppe ihn nicht erneut, da dies zu
     *            einem Deadlock führt. Ansonsten wird der Server gestoppt und
     *            auf sein Beenden gewartet.
     */
    private void _stopServer(boolean stoppedFromServerSide) {
        if (_gameServer != null) {
            /*
             * Sofern der Spieleserver den Stopp nicht selbst ausgelöst hat,
             * stoppe ihn.
             */
            if (!stoppedFromServerSide) {
                _gameServer.stopMessageServer();
            } else {
                /*
                 * Gibt es eine Ausnahme zum Stopp, dann zeige eine
                 * Fehlermeldung an.
                 */
                Exception e = _gameServer.getStoppingException();
                if (e != null) {
                    ClientWindow.showMessage(JOptionPane.ERROR_MESSAGE,
                        "Entschuldige, aber der Spieleserver hat ein Problem "
                            + "festgestellt und muss beendet werden.\n\nGrund: "
                            + "%s.\n\n%s",
                        e.getMessage(), ExceptionFormatter.formatException(e));
                }
            }
        }

        /* Wechsle den Zustand der Steuerelemente */
        _setButtonState(false);
        _gameServer = null;
    }

    /**
     * Diese Methode speichert die geänderten Einstellungen.
     */
    private void _saveOptions() {
        /* Speichere Einstellungen */
        ClientWindow.getInstance().getOptions()
            .setCustomServerConfiguration(_currentConfig);
    }

    /**
     * Bildet bislang gespeicherte Optionen für dieses Panel in sämtlichen durch
     * den Nutzer konfigurierbare Steuerelemente ab.
     */
    public void loadOptions() {
        /* Lade Einstellungen */
        _currentConfig = ClientWindow.getInstance().getOptions()
            .getCustomServerConfiguration();
        _serverNameField.setText(_currentConfig.getServerName());
        _publicHostnameField.setText(_currentConfig.getServerListenHost());
        _publicPortField.setValue(_currentConfig.getServerListenPort());

        /* Wechsle den Zustand des Buttons */
        _setButtonState(false);
    }

    /**
     * Erzeugt das Panel, mit welchem ein eigener Spieleserver für den
     * Mehrspielermodus betrieben werden kann.
     */
    public CustomServerPanel() {
        super("Eigener Spieleserver");

        this._currentConfig = null;
        this._gameServer = null;

        /*
         * Button, um dem Server zu starten. Implementiere den Serverstart
         * mittels Lambda-Ausdruck.
         */
        _startServerButton = createStyledJButton("Server starten");
        _startServerButton
            .addActionListener((ActionEvent event) -> _startServer());

        /*
         * Button, um dem Server zu stoppen. Implementiere den Serverstopp
         * mittels Lambda-Ausdruck.
         */
        _stopServerButton = createStyledJButton("Server stoppen");
        _stopServerButton
            .addActionListener((ActionEvent event) -> _stopServer(false));

        /*
         * Button, um dem in die Serverliste zurückzukehren. Implementiere den
         * Wechsel mittels Lambda-Ausdruck.
         */
        _leaveMenuButton = createStyledJButton("Serverliste");
        _leaveMenuButton.addActionListener((ActionEvent event) -> {
            ClientWindow window = ClientWindow.getInstance();

            /* Speichere Einstellungen */
            _saveOptions();

            /*
             * Wechsle in die Serverauswahl. Wenn dies scheitert, wechsle ins
             * Hauptmenü.
             */
            if (!window.setWindowState(ClientWindowState.SERVER_SELECTION_MENU))
                window.setWindowState(ClientWindowState.MAIN_MENU);
        });

        /* Füge die Buttons zur ersten Reihe hinzu */
        createLeftAlignedButtonPanel(_startServerButton, _stopServerButton,
            _leaveMenuButton);

        JPanel horizontalPanel;
        JLabel label;

        /*
         * Füge einen Portscanner hinzu
         */
        _portCheckerLabel = new PortCheckerLabel(
            Globals.GAME_SERVER_DEFAULT_PORT);
        _portCheckerLabel.setBlueForOK(true);

        horizontalPanel = packJLabelAlignedIntoJPanel(_portCheckerLabel,
            Component.LEFT_ALIGNMENT);
        this.add(horizontalPanel);

        /* Abstand */
        this.add(createVerticalRigid(2));

        /*
         * Name des Spieleservers
         */
        horizontalPanel = createHorizontalJPanel();
        this.add(horizontalPanel);

        label = new JLabel("Name des Servers:");
        horizontalPanel.add(label);
        horizontalPanel.add(createHorizontalRigid(2));

        _serverNameField = new JTextField();
        horizontalPanel.add(_serverNameField);

        /* Abstand */
        this.add(createVerticalRigid(1));

        /*
         * Öffentliche Adresse und Port des Spieleservers in einer Zeile
         */
        horizontalPanel = createHorizontalJPanel();
        this.add(horizontalPanel);

        label = new JLabel("Öffentliche Adresse:");
        horizontalPanel.add(label);
        horizontalPanel.add(createHorizontalRigid(2));

        /* Adresse, unter der der Server öffentlich erreichbar ist */
        _publicHostnameField = new JTextField();
        horizontalPanel.add(_publicHostnameField);
        horizontalPanel.add(createHorizontalRigid(2));

        label = new JLabel("Port:");
        horizontalPanel.add(label);
        horizontalPanel.add(createHorizontalRigid(2));

        /* Nur TCP im Bereich der TCP-Ports (1-65535) zulässig */
        NumberFormatter formatter = new NumberFormatter(
            NumberFormat.getInstance());
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(1);
        formatter.setMaximum(65535);
        formatter.setAllowsInvalid(false);
        formatter.setCommitsOnValidEdit(true);

        _publicPortField = new JFormattedTextField(formatter);
        horizontalPanel.add(_publicPortField);

        /* Setze Breite des Textfelds für den Port herunter */
        Dimension portFieldSize = _publicPortField.getPreferredSize();
        portFieldSize.width = 90;
        _publicPortField.setMinimumSize(portFieldSize);
        _publicPortField.setPreferredSize(portFieldSize);
        _publicPortField.setMaximumSize(portFieldSize);

        /* Abstand */
        this.add(createVerticalRigid(1));

        /* Label mit Hinweis. Linebreaks leider nur mit HTML möglich. */
        label = new JLabel(
            String.format("<html><body><em>Hinweis:</em> Der Spieleserver "
                + "meldet sich an dem Verwaltungsserver an, der in den "
                + "Optionen eingestellt ist.<br><br><em>Hinweis:</em> Um "
                + "selbst auf deinem Spieleserver mitspielen zu können, musst "
                + "du %s ein zweites Mal starten und dich dort an diesem "
                + "anmelden.</body></html>", Globals.PROJECT_TITLE));

        /*
         * Standardmäßig gibt getPreferredSize() eine Breite zurück, die so groß
         * ist, dass der ausgegebene Text keinerlei Zeilenumbrüche bedarf. Ist
         * dies der Fall, so setze die Breite des Textes fest und verdopple die
         * Höhe (eine bessere Heuristik gibt es nicht).
         */
        Dimension labelSize = label.getPreferredSize();
        if (labelSize.width > PANEL_DEFAULT_CONTENT_WIDTH_PIXELS) {
            labelSize.width = PANEL_DEFAULT_CONTENT_WIDTH_PIXELS;
            labelSize.height *= 2;
        }
        label.setMinimumSize(labelSize);
        label.setPreferredSize(labelSize);
        label.setMaximumSize(labelSize);

        horizontalPanel = packJLabelAlignedIntoJPanel(label,
            Component.LEFT_ALIGNMENT);
        this.add(horizontalPanel);
    }
}
