/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.g5p.dynamiteboy.tools.ExceptionFormatter;

/**
 * Diese Klasse implementiert das Panel, welches das Hauptmenü enthält. Vom
 * Hauptmenü aus können alle Aspekte des Spieles (außer der Verwaltungsserver)
 * erreicht werden.
 *
 * @author Steven Jung
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class MainMenuPanel extends JPanel {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Diese Hilfsmethode erzeugt einen neuen Button mit einem beliebigen Text,
     * in diesem Panel platziert an einer beliebigen Position, mit einer
     * beliebigen Klick-Aktion. Alle Buttons haben die selbe Größe und Farbe.
     *
     * @param label
     *            Text, der im Button stehen soll
     * @param position
     *            X- und Y-Koordinate, an welcher der Button platziert werden
     *            soll
     * @param action
     *            Eine Aktion, die ausgeführt werden soll, wenn der Button vom
     *            Nutzer angeklickt wird
     * @return Neuer Button
     */
    private JButton _addStyledButton(String label, Point position,
        ActionListener action) {
        JButton button = new JButton(label);
        button.setForeground(Color.BLACK);
        button.setBackground(new Color(51, 153, 102));
        button.setBounds(position.x, position.y, 160, 48);
        button.addActionListener(action);
        this.add(button);
        return button;
    }

    /**
     * Öffnet eine Website im Standardbrowser des Benutzers.
     *
     * @param uri
     *            Adresse, die geöffnet werden soll
     * @see https://stackoverflow.com/a/10967469
     */
    private void _openWebsiteInBrowser(final String uri) {
        try {
            /* Verarbeite die URL */
            final URI uriObject = new URI(uri);

            /*
             * Frage nach einem Desktop ab. Unterstützt dieser das Aufrufen
             * einer Internetadresse, tue dies.
             */
            final Desktop desktop = Desktop.isDesktopSupported()
                ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(uriObject);
            }
        } catch (Exception e) {
            /* Zeige Hinweismeldung mit Verweis auf die übergebene Adresse an */
            ClientWindow.showMessage(JOptionPane.WARNING_MESSAGE,
                "Entschuldigung, aber der Internetbrowser konnte leider nicht "
                    + "geöffnet werden.\n\nBitte gebe die folgende Adresse "
                    + "manuell ein:\n\n    %s\n\n%s",
                uri, ExceptionFormatter.formatException(e));
        }
    }

    /**
     * Diese überlagerte Methode zeichnet das Hintergrundbild.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(Images.menuBackground, 0, 0, this);
    }

    /**
     * Diese überlagerte Methode gibt die gewünschte Breite sowie Höhe des
     * Panels zurück.
     *
     * @return gewünschte Breite und Höhe des Panels
     */
    @Override
    public Dimension getPreferredSize() {
        /* Panelgröße richtet sich nach dem Hintergrundbild */
        return new Dimension(Images.menuBackground.getWidth(),
            Images.menuBackground.getHeight());
    }

    /**
     * Erzeugt das Hauptmenü.
     */
    public MainMenuPanel() {
        /* Paneleinstellungen */
        this.setBackground(new Color(0, 191, 255));
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setLayout(null);

        /* Button für das Online-Spiel */
        _addStyledButton("Online-Spiel", new Point(240, 210),
            (ActionEvent event) -> {
                /*
                 * Wechsle in das Serverlistenmenü. Wenn dies scheitert, bleiben
                 * wir sowieso im Hauptmenü.
                 */
                ClientWindow.getInstance()
                    .setWindowState(ClientWindowState.SERVER_SELECTION_MENU);
            });

        /* Button für das Offline-Spiel */
        _addStyledButton("Offline-Spiel", new Point(240, 260),
            (ActionEvent event) -> {
                /* Wechsle in das Offline-Konfigurationsmenü */
                ClientWindow.getInstance()
                    .setWindowState(ClientWindowState.OFFLINE_PLAY_MENU);
            });

        /* Button für Werbung */
        _addStyledButton("Group5Productions", new Point(240, 310),
            (ActionEvent event) -> {
                _openWebsiteInBrowser("https://gitlab.com/group5productions");
            });

        /* Button für das Tutorial */
        _addStyledButton("Tutorial", new Point(240, 360),
            (ActionEvent event) -> {
                /* Wechsle in das Tutorialmenü */
                ClientWindow.getInstance()
                    .setWindowState(ClientWindowState.TUTORIAL_MENU);
            });

        /* Button für die Optionen */
        _addStyledButton("Optionen", new Point(240, 410), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                /* Wechsle in das Optionsmenü */
                ClientWindow.getInstance()
                    .setWindowState(ClientWindowState.OPTIONS_MENU);
            }
        });

        /* Button zum Beenden */
        _addStyledButton("Beenden", new Point(240, 460), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ClientWindow.getInstance().closeWindow();
            }
        });
    }
}
