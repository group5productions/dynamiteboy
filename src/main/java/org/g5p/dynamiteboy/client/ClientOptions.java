/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.gameserver.GameServerConfiguration;

/**
 * Diese Klasse speichert Einstellungen, die nur für den Client gelten.
 *
 * @author Thomas Weber
 */
public final class ClientOptions {
    private int _arenaTileScaleFactor;
    private String _managementServerAddress;
    private GameServerConfiguration _customServerConfiguration;

    private ArenaPanelOfflineModeSettings _offlinePlaySettings;

    /**
     * Initialisiert die Instanz mit Standardeinstellungen.
     */
    public ClientOptions() {
        this._arenaTileScaleFactor = Globals.CLIENT_DEFAULT_OBJECT_SCALE;
        this._managementServerAddress = Globals.MANAGEMENT_SERVER_DEFAULT_HOST;
        this._customServerConfiguration = new GameServerConfiguration();
        this._offlinePlaySettings = null;
    }

    /**
     * Gibt den Faktor zurück, um welchen Grafiken im Spiel vergrößert werden.
     *
     * @return Skalierfaktor der Grafiken
     */
    public int getArenaTileScaleFactor() {
        return _arenaTileScaleFactor;
    }

    /**
     * Setzt den Faktor, um welchen Grafiken im Spiel vergrößert werden.
     *
     * @param scaleFactor
     *            Skalierfaktor der Grafiken
     */
    public void setArenaTileScaleFactor(int scaleFactor) {
        if (scaleFactor < 1 || scaleFactor > 7)
            throw new IllegalArgumentException(
                "Skalierfaktor muss zwischen 1 und 7 liegen");
        this._arenaTileScaleFactor = scaleFactor;
    }

    /**
     * Gibt die Adresse des eingestellten Verwaltungsservers zurück.
     *
     * @return Adresse des eingestellten Verwaltungsservers
     */
    public String getManagementServerAddress() {
        return _managementServerAddress;
    }

    /**
     * Legt die Adresse des Verwaltungsservers fest.
     *
     * @param managementServerAddress
     *            Adresse des Verwaltungsservers
     */
    public void setManagementServerAddress(String managementServerAddress) {
        this._managementServerAddress = managementServerAddress;
    }

    /**
     * Gibt die aktuelle Konfiguration des eigenen Spieleservers zurück.
     *
     * @return Konfiguration des eigenen Spieleservers
     */
    public GameServerConfiguration getCustomServerConfiguration() {
        return _customServerConfiguration;
    }

    /**
     * Legt die aktuelle Konfiguration des eigenen Spieleservers fest.
     *
     * @param customServerConfiguration
     *            Konfiguration des eigenen Spieleservers
     */
    public void setCustomServerConfiguration(
        GameServerConfiguration customServerConfiguration) {
        this._customServerConfiguration = customServerConfiguration;
    }

    /**
     * Gibt die Einstellungen des Offline-Spiels zurück.
     *
     * @return Einstellungen des Offline-Spiels
     */
    public ArenaPanelOfflineModeSettings getOfflinePlaySettings() {
        return _offlinePlaySettings;
    }

    /**
     * Legt die Einstellungen des Offline-Spiels fest.
     *
     * @param _offlinePlaySettings
     *            Einstellungen des Offline-Spiels
     */
    public void setOfflinePlaySettings(
        ArenaPanelOfflineModeSettings _offlinePlaySettings) {
        this._offlinePlaySettings = _offlinePlaySettings;
    }
}
