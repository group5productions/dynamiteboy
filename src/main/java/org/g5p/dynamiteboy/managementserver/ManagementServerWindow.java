/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.managementserver;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.tools.ExceptionFormatter;
import org.g5p.dynamiteboy.tools.GameServerEntry;
import org.g5p.dynamiteboy.tools.GameplayMode;
import org.g5p.dynamiteboy.tools.PlayerCount;
import org.g5p.dynamiteboy.tools.PortCheckerLabel;

/**
 * Diese Klasse implementiert eine grafische Oberfläche den Verwaltungsservers.
 * Per Knopfdruck kann der Server gestartet bzw. beendet werden. Zudem wird die
 * Liste der registrierten Spieleserver angezeigt.
 *
 * @author Thomas Weber
 */
public final class ManagementServerWindow extends JFrame {
    /**
     * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher ist
     * diese Variable Pflicht.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Abstand zwischen Steuerelementen und Rändern in Pixel
     */
    private static final int BORDER_SIZE_IN_PIXELS = 8;

    private final _ManagementServerTableModel _serverTableModel;
    private final PortCheckerLabel _portCheckerLabel;
    private final JButton _startServerButton;
    private final JButton _stopServerButton;

    /**
     * Diese interne Klasse implementiert das Datenmodell für die Tabelle, die
     * die Liste der verwalteten Spieleserver darstellt.
     */
    private final class _ManagementServerTableModel extends AbstractTableModel {
        /**
         * Alle Steuerelemente in AWT/Swing implementieren Serializable, daher
         * ist diese Variable Pflicht.
         */
        private static final long serialVersionUID = 1L;

        /**
         * Index der Spalte "Name"
         */
        private static final int _COLUMN_NAME = 0;

        /**
         * Index der Spalte "Adresse"
         */
        private static final int _COLUMN_ADDRESS = 1;

        /**
         * Index der Spalte "Spieler"
         */
        private static final int _COLUMN_PLAYERS = 2;

        /**
         * Index der Spalte "Lobby?"
         */
        private static final int _COLUMN_LOBBY = 3;

        /**
         * Index der Spalte "Modus"
         */
        private static final int _COLUMN_MODE = 4;

        /**
         * Gibt den Namen der Spalte für einen Spaltenindex col zurück.
         *
         * @param col
         *            Index der Spalte, ab 0 beginnend
         */
        @Override
        public String getColumnName(int col) {
            switch (col) {
            case _COLUMN_NAME:
                return "Name";
            case _COLUMN_ADDRESS:
                return "Adresse";
            case _COLUMN_PLAYERS:
                return "Spieler";
            case _COLUMN_LOBBY:
                return "Lobby?";
            case _COLUMN_MODE:
                return "Modus";
            }
            return "";
        }

        /**
         * Gibt die Anzahl der Spalten in der Tabelle zurück.
         *
         * @return Stets letzter Spaltenindex plus 1
         */
        @Override
        public int getColumnCount() {
            return _COLUMN_MODE + 1;
        }

        /**
         * Gibt die Anzahl der Zeilen in der Tabelle zurück.
         *
         * @return Anzahl der Zeilen = Anzahl der Spieleserver
         */
        @Override
        public int getRowCount() {
            return ManagementServer.getInstance().getGameServerCount();
        }

        /**
         * Fragt den Inhalt einer Tabellenzelle in Zeile arg0 und Spalte arg1
         * ab.
         *
         * @param arg0
         *            Zeile der Tabelle, beginnend ab 0
         * @param arg1
         *            Spalte der Tabelle, beginnend ab 0
         * @return Inhalt der Tabellenzelle
         */
        @Override
        public Object getValueAt(int arg0, int arg1) {
            /* Lade den Eintrag für die Zeile */
            GameServerEntry entry = ManagementServer.getInstance()
                .getGameServer(arg0);
            if (entry == null)
                return null;

            /* Gebe das zurück, was die Tabelle für die jeweilige Spalte will */
            switch (arg1) {
            case _COLUMN_NAME:
                return entry.getServerName();
            case _COLUMN_ADDRESS:
                return String.format("%s:%d", entry.getServerAddress(),
                    entry.getServerPort());
            case _COLUMN_PLAYERS:
                PlayerCount counts = entry.getPlayerCounts();
                return String.format("%d/%d", counts.getCurrentPlayerCount(),
                    counts.getMaxPlayerCount());
            case _COLUMN_LOBBY:
                return entry.isLobbyOpen() ? "Ja" : "Nein";
            case _COLUMN_MODE:
                /* Zeige Spielmodus nur an, wenn das Spiel läuft */
                if (!entry.isLobbyOpen())
                    return entry.getGameplayMode() == GameplayMode.STANDARD_A_B
                        ? "A+B" : "A";
                return "";
            }
            return null;
        }

        /**
         * Diese Methode passt die Spalten der Tabelle, in der das Modell
         * dargestellt wird, von der Größe her optimal an.
         *
         * @param table
         *            Tabelle, deren Spalten angepasst werden sollen
         */
        public void adjustTableColumnWidths(JTable table) {
            /*
             * Lege feste Breiten für die Spalten fest, sodass die Informationen
             * optimal dargestellt werden.
             */
            TableColumnModel model = table.getColumnModel();

            model.getColumn(_COLUMN_NAME).setPreferredWidth(260);
            model.getColumn(_COLUMN_ADDRESS).setPreferredWidth(130);
            model.getColumn(_COLUMN_PLAYERS).setMaxWidth(60);
            model.getColumn(_COLUMN_LOBBY).setMaxWidth(60);
            model.getColumn(_COLUMN_MODE).setMaxWidth(60);
        }
    }

    /**
     * Diese Unterklasse implementiert die Funktionalität des Buttons "Server
     * starten".
     */
    private final class _StartServerButtonAction implements ActionListener {
        /**
         * Diese Methode wird ausgeführt, wenn auf den Button "Server starten"
         * geklickt wurde.
         *
         * @param event
         *            Daten über dieses Ereignis
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                /* Starte den Verwaltungsserver */
                ManagementServer.getInstance().startMessageServer();
            } catch (IOException e) {
                /* Zeige Fehlermeldung an */
                JOptionPane.showMessageDialog(ManagementServerWindow.this,
                    String.format(
                        "Der Verwaltungsserver konnte aufgrund eines "
                            + "Fehlers nicht gestartet werden.\n\nGrund: %s.\n\n%s",
                        e.getLocalizedMessage(),
                        ExceptionFormatter.formatException(e)),
                    Globals.PROJECT_TITLE, JOptionPane.ERROR_MESSAGE);
            } finally {
                /* Bei Erfolg: Deaktiviere uns und aktiviere Stopp-Button */
                _updateButtons();
            }
        }
    }

    /**
     * Diese Unterklasse implementiert die Funktionalität des Buttons "Server
     * stoppen".
     */
    private final class _StopServerButtonAction implements ActionListener {
        /**
         * Diese Methode wird ausgeführt, wenn auf den Button "Server stoppen"
         * geklickt wurde.
         *
         * @param event
         *            Daten über dieses Ereignis
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            /* Stoppe den Verwaltungsserver */
            ManagementServer.getInstance().stopMessageServer();

            /* Deaktiviere uns und aktiviere Start-Button */
            _updateButtons();
        }
    }

    /**
     * Fügt einen vertikalen Abstand von
     * {@link ManagementServerWindow#BORDER_SIZE_IN_PIXELS} Pixeln zu einem
     * Container hinzu.
     *
     * @param container
     *            Container, zu dem der Abstand hinzugefügt werden soll
     */
    private void _insertVerticalPadding(Container container) {
        Dimension vertical = new Dimension(0, BORDER_SIZE_IN_PIXELS);
        container.add(Box.createRigidArea(vertical));
    }

    /**
     * Erzeugt ein JPanel, dessen Steuerelemente horizontal angeordnet sind.
     *
     * @return Neu erzeugtes Panel
     */
    private JPanel _makeHorizontalPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        return panel;
    }

    /**
     * Erzeugt ein JPanel, welches ein JLabel mit der korrekten Ausrichtung
     * packt.
     *
     * @param label
     *            JLabel, welches in das JPanel gepackt werden soll
     * @return Neues JPanel mit existierendem Label in korrekter Ausrichtung
     */
    private JPanel _packJLabelAlignedIntoJPanel(final JLabel label,
        final float orientation) {
        /* Erzeuge Panel */
        JPanel panel = _makeHorizontalPanel();

        /* Rechts ausrichten: erst Glue, dann Label */
        if (orientation == Component.RIGHT_ALIGNMENT) {
            label.setAlignmentX(Component.RIGHT_ALIGNMENT);
            panel.add(Box.createHorizontalGlue());
            panel.add(label);
        }

        /* Zentriert ausrichten: Erst Glue, dann Label, dann Glue */
        else if (orientation == Component.CENTER_ALIGNMENT) {
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(Box.createHorizontalGlue());
            panel.add(label);
            panel.add(Box.createHorizontalGlue());
        }

        /* Links ausrichten: Erst Label, dann Glue */
        else {
            label.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.add(label);
            panel.add(Box.createHorizontalGlue());
        }
        return panel;
    }

    /**
     * Aktualisiert den Zustand der Schaltflächen "Server starten" und "Server
     * beenden", wenn der Server gestartet oder beendet wird, indem jeweils die
     * Schaltfläche, die dem Serverzustand entspricht, deaktiviert wird.
     */
    private void _updateButtons() {
        if (ManagementServer.getInstance().isMessageServerRunning()) {
            _startServerButton.setEnabled(false);
            _stopServerButton.setEnabled(true);
            _portCheckerLabel.displayPotentiallyOpen();
        } else {
            _startServerButton.setEnabled(true);
            _stopServerButton.setEnabled(false);
            _portCheckerLabel.displayDefinitelyClosed();
        }
    }

    /**
     * Konstruiert ein neues Fenster als Oberfläche des Verwaltungsservers,
     * welches ein Label, eine Liste in einem Scrollbereich sowie zwei
     * Schaltflächen enthält.
     */
    public ManagementServerWindow() {
        /* Stelle das Fenster ein */
        setTitle(String.format("%s-Verwaltungsserver", Globals.PROJECT_TITLE));
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /* Erzeugt äußeren Rahmen und ein BoxLayout */
        JRootPane root = getRootPane();
        Container content = getContentPane();
        root.setBorder(
            new EmptyBorder(BORDER_SIZE_IN_PIXELS, BORDER_SIZE_IN_PIXELS,
                BORDER_SIZE_IN_PIXELS, BORDER_SIZE_IN_PIXELS));
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        JPanel horizontalPanel;

        /*
         * Label "Registrierte Spieleserver"
         */
        JLabel label = new JLabel("Registrierte Spieleserver:");
        horizontalPanel = _packJLabelAlignedIntoJPanel(label,
            Component.LEFT_ALIGNMENT);
        content.add(horizontalPanel);

        _insertVerticalPadding(content);

        /*
         * Scrollpanel, welches Tabelle enthält
         */
        JScrollPane scrollPane = new JScrollPane();
        content.add(scrollPane);

        /* Liste der Spieleserver als Tabelle ins Scrollpanel eingebettet */
        _serverTableModel = new _ManagementServerTableModel();
        JTable gsTable = new JTable(_serverTableModel);
        gsTable.setFillsViewportHeight(true);
        scrollPane.setViewportView(gsTable);
        _serverTableModel.adjustTableColumnWidths(gsTable);

        _insertVerticalPadding(content);

        /* Label als Portchecker */
        _portCheckerLabel = new PortCheckerLabel(
            Globals.MANAGEMENT_SERVER_PORT);
        horizontalPanel = _packJLabelAlignedIntoJPanel(_portCheckerLabel,
            Component.LEFT_ALIGNMENT);
        content.add(horizontalPanel);

        _insertVerticalPadding(content);

        /*
         * Buttons horizontal
         */
        horizontalPanel = _makeHorizontalPanel();

        /* Button zum Starten des Servers */
        _startServerButton = new JButton("Server starten");
        _startServerButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        _startServerButton.addActionListener(new _StartServerButtonAction());
        horizontalPanel.add(_startServerButton);

        horizontalPanel.add(Box.createHorizontalGlue());

        /* Button zum Stoppen des Servers */
        _stopServerButton = new JButton("Server beenden");
        _stopServerButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
        _stopServerButton.addActionListener(new _StopServerButtonAction());
        horizontalPanel.add(_stopServerButton);
        content.add(horizontalPanel);

        /* Aktualisiere Zustand der Buttons */
        _updateButtons();
    }

    /**
     * Dieses Callback wird durch den Kern des Verwaltungsservers aufgerufen,
     * wenn ein Server aus der Liste der Spieleserver entfernt wird.
     *
     * @param hash
     *            Eine Zeichenkette, die den Spieleserver intern eindeutig
     *            identifiziert
     * @param entry
     *            Eintrag des Spieleservers mitsamt Statistiken
     */
    public void onServerRemoved(String hash, GameServerEntry entry) {
        /* Erzwinge Neurendern der Tabelle */
        _serverTableModel.fireTableDataChanged();
    }

    /**
     * Dieses Callback wird durch den Kern des Verwaltungsservers aufgerufen,
     * wenn ein Spieleserver seine Statistiken aktualisiert.
     *
     * @param hash
     *            Eine Zeichenkette, die den Spieleserver intern eindeutig
     *            identifiziert
     * @param entry
     *            Eintrag des Spieleservers mitsamt Statistiken
     */
    public void onServerStatsUpdated(String hash, GameServerEntry entry) {
        /* Erzwinge Neurendern der Tabelle */
        _serverTableModel.fireTableDataChanged();
    }

    /**
     * Dieses Callback wird durch den Kern des Verwaltungsservers aufgerufen,
     * wenn ein Server zur Liste der Spieleserver hinzugefügt wird.
     *
     * @param hash
     *            Eine Zeichenkette, die den Spieleserver intern eindeutig
     *            identifiziert
     * @param entry
     *            Eintrag des Spieleservers mitsamt Statistiken
     */
    public void onServerAdded(String hash, GameServerEntry entry) {
        /* Erzwinge Neurendern der Tabelle */
        _serverTableModel.fireTableDataChanged();
    }

    /**
     * main() ist die Startfunktion eines jeden Java-Programms. Um die Kraft
     * objektorientierter Programmierung auszunutzen als auch das Programm
     * sauber herunterfahren zu können (z.B. durch Strg-C), wird
     * ManagementServer als neuer Thread instanziert.
     *
     * @param args
     *            Argumente, die dem Programm zum Start übergeben wurden
     */
    public static void main(String[] args) {
        /* Schalte die Kantenglättung für Texte in Swing ein */
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");

        /* Erzeuge die Oberfläche und beende den main-Thread */
        ManagementServerWindow ui = new ManagementServerWindow();
        ManagementServer.getInstance().setUIForUpdate(ui);
        ui.setVisible(true);
    }
}
