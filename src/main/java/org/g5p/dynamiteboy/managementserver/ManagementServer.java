/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.managementserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.MessageServer;
import org.g5p.dynamiteboy.networking.messages.UpdateGameServerStatsManagementServerCommand;
import org.g5p.dynamiteboy.tools.GameServerEntry;

/**
 * Diese Klasse implementiert den Kern des Verwaltungsservers. Er verwaltet eine
 * Liste von Spieleservern, die momentan online sind. Spieleserver können am
 * Verwaltungsserver angemeldet bzw. abgemeldet werden, und Clients können die
 * Liste der Spieleserver abfragen.
 *
 * @author Thomas Weber
 */
public final class ManagementServer {
    private static final ManagementServer _instance;

    private ManagementServerWindow _uiForUpdate;
    private MessageServer _messageServer;
    private LinkedHashMap<String, GameServerEntry> _gameServerMap;

    /**
     * Erzeugt eine neue Instanz der Verwaltungsserver-Klasse, die die
     * verwaltungstechnischen Datenstrukturen beherbergt.
     */
    public ManagementServer() {
        this._messageServer = null;
        this._gameServerMap = new LinkedHashMap<>();
        this._uiForUpdate = null;
    }

    /**
     * Startet den Verwaltungsserver, sodass er über jedes Netzwerkinterface auf
     * dem Port {@link Globals#MANAGEMENT_SERVER_PORT} erreichbar ist.
     *
     * @return Instanz einer Ausnahme, wenn der Server nicht gestartet werden
     *         konnte
     * @throws IOException
     *             wenn während der Einrichtung des Sockets ein Fehler auftrat
     */
    public void startMessageServer() throws IOException {
        if (_messageServer == null) {
            /*
             * Erzeuge den Server, ohne auf Timeouts zu reagieren, so, dass auf
             * allen verfügbaren Interfaces Verbindungen entgegen genommen
             * werden können
             */
            InetAddress anyAddress = new InetSocketAddress(0).getAddress();
            _messageServer = new MessageServer(anyAddress,
                Globals.MANAGEMENT_SERVER_PORT, 0);

            /*
             * Eingehende Nachrichten werden von ManagementServerHandler
             * verwaltet
             */
            _messageServer.setConnectionHandler(ManagementServerHandler.class);

            /* Los geht's */
            _messageServer.startServer();
        }
    }

    /**
     * Überprüft, ob der Verwaltungsserver Verbindungen annimmt.
     *
     * @return true, wenn der Verwaltungsserver Verbindungen annimmt, ansonsten
     *         false
     */
    public boolean isMessageServerRunning() {
        return _messageServer != null;
    }

    /**
     * Stoppt den Verwaltungsserver, sodass er auf dem Port
     * {@link Globals#MANAGEMENT_SERVER_PORT} keine Verbindungen mehr entgegen
     * nimmt.
     */
    public void stopMessageServer() {
        if (_messageServer != null) {
            _messageServer.interrupt();
            try {
                _messageServer.join();
            } catch (InterruptedException e) {
            }
            _messageServer = null;
        }
    }

    /**
     * Entfernt einen Spieleserver von der Liste und benachrichtigt eine
     * eventuell registrierte grafische Oberfläche, die auf Änderungen der Liste
     * hört.
     *
     * @param hash
     *            Eindeutiger Bezeichner der Verbindung, dessen Spieleserver
     *            entfernt werden soll
     */
    public synchronized void removeGameServer(String hash) {
        if (_uiForUpdate != null) {
            GameServerEntry entry = _gameServerMap.get(hash);

            /* Nur wenn die Verbindung überhaupt einen Server eingetragen hat */
            if (entry != null) {
                _uiForUpdate.onServerRemoved(hash, entry);
            }
        }
        this._gameServerMap.remove(hash);
    }

    /**
     * Gibt die Liste der momentan registierten Spieleserver zurück.
     *
     * @return Liste der momentan registrierten Spieleserver
     */
    public synchronized List<GameServerEntry> getGameServers() {
        LinkedList<GameServerEntry> serverList = new LinkedList<>();

        for (Map.Entry<String, GameServerEntry> entry : this._gameServerMap
            .entrySet()) {
            serverList.add(entry.getValue());
        }
        return serverList;
    }

    /**
     * Gibt einen einzelnen Spieleserver anhand seines Indexwertes zurück.
     *
     * @param index
     *            0-basierter Indexwert des Spieleservers
     * @return Spieleserver am Indexwert, oder null wenn nicht gefunden
     */
    public synchronized GameServerEntry getGameServer(int index) {
        try {
            return new ArrayList<GameServerEntry>(_gameServerMap.values())
                .get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Gibt die Anzahl der momentan registierten Spieleserver zurück.
     *
     * @return Anzahl der momentan registrierten Spieleserver
     */
    public synchronized int getGameServerCount() {
        return _gameServerMap.size();
    }

    /**
     * Fügt einen Spieleserver zur Liste hinzu und benachrichtigt eine eventuell
     * registrierte grafische Oberfläche, die auf Änderungen der Liste hört.
     *
     * @param hash
     *            Eindeutiger Bezeichner der Verbindung, zu der ein Spieleserver
     *            hinzugeführt werden sol
     */
    public synchronized void addGameServer(String hash,
        GameServerEntry newEntry) {
        if (_uiForUpdate != null) {
            /* Lösche eventuell existierenden Eintrag der gleichen Verbindung */
            GameServerEntry previousEntry = _gameServerMap.get(hash);
            if (previousEntry != null)
                _uiForUpdate.onServerRemoved(hash, previousEntry);

            /* Füge neue Verbindung hinzu */
            _uiForUpdate.onServerAdded(hash, newEntry);
        }
        this._gameServerMap.put(hash, newEntry);
    }

    /**
     * Aktualisiert die Statistiken eines Spieleservers und benachrichtigt eine
     * eventuell registrierte grafische Oberfläche, die auf Änderungen der Liste
     * hört.
     *
     * @param hash
     *            Eindeutiger Bezeichner der Verbindung, zu der ein Spieleserver
     *            hinzugeführt werden sol
     * @param updateCommand
     *            Kommando, welches die Statistiken des zugehörigen
     *            Spieleservers enthält
     */
    public synchronized void updateGameServerStats(String hash,
        UpdateGameServerStatsManagementServerCommand updateCommand) {
        /* Aktualisiere Spieleservereintrag, sofern vorhanden */
        GameServerEntry entry = _gameServerMap.get(hash);
        if (entry != null) {
            entry.setPlayerCounts(
                updateCommand.getPlayerCounts().getCurrentPlayerCount(),
                updateCommand.getPlayerCounts().getMaxPlayerCount());
            entry.setLobbyOpen(updateCommand.isLobbyOpen());
            entry.setGameplayMode(updateCommand.getGameplayMode());

            if (_uiForUpdate != null)
                _uiForUpdate.onServerStatsUpdated(hash, entry);
        }
    }

    /**
     * Registriert eine grafische Oberfläche, die auf Änderungen der Liste der
     * Spieleserver hört.
     *
     * @param gui
     *            Instanz einer grafischen Oberfläche für den Verwaltungsserver
     */
    public void setUIForUpdate(ManagementServerWindow gui) {
        _uiForUpdate = gui;
    }

    /**
     * Gibt die globale Instanz des Verwaltungsservers zurück.
     *
     * @return globale Instanz des Verwaltungsservers
     */
    public static ManagementServer getInstance() {
        return _instance;
    }

    /**
     * Der statische Initialisierer legt eine globale Instanz des
     * Verwaltungsservers an und registriert einen Hook, der den Server sauber
     * herunterfährt, wenn das Programm beendet wird.
     */
    static {
        /*
         * Initialisiere eine globale Instanz des kompletten Verwaltungsservers
         */
        _instance = new ManagementServer();

        /*
         * Installiere Shutdown-Hook, der den Server beim Beenden des Programms
         * automatisch schließt
         */
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                _instance.stopMessageServer();
            }
        });
    }
}
