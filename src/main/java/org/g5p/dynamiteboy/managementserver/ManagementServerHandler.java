/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.managementserver;

import java.net.SocketException;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.IMessageHandler;
import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;
import org.g5p.dynamiteboy.networking.messages.*;
import org.g5p.dynamiteboy.tools.GameServerEntry;
import org.g5p.dynamiteboy.tools.GameplayMode;

/**
 * Diese Klasse empfängt Nachrichten, die für den Verwaltungsserver gedacht sind
 * und verarbeitet diese.
 *
 * @author Thomas Weber
 */
public final class ManagementServerHandler implements IMessageHandler {
    private Messenger _connection;
    private String _connectionIdentifier;

    /**
     * Diese Methode wird aufgerufen, wenn sich der Client mit dem Server
     * verbunden hat.
     *
     * @param connection
     *            Instanz der Verbindung zum Client
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Speichere die Verbindung zum Client */
        this._connection = connection;

        /* Identifiziere die Verbindung anhand der Adresse zur Gegenstelle */
        this._connectionIdentifier = connection.getRemoteAddressString();

        /* Nehme die Verbindung an */
        return true;
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Client eine Nachricht an den
     * Server schickt.
     *
     * @param clientCommand
     *            Nachricht, die der Client gesendet hat
     */
    @Override
    public void onCommandReceived(Message clientCommand) {
        /* Spieleserver registrieren */
        if (clientCommand instanceof RegisterGameServerManagementServerCommand) {
            RegisterGameServerManagementServerCommand command = (RegisterGameServerManagementServerCommand) clientCommand;

            /*
             * Erzeuge neuen Spieleservereintrag, dessen Statistiken momentan
             * unbekannt sind
             */
            GameServerEntry newEntry = new GameServerEntry(
                command.getServerName(), command.getListenAddress(),
                command.getListenPort(), 0, Globals.ROUND_MAX_PLAYERS, true,
                GameplayMode.STANDARD_A);

            ManagementServer.getInstance().addGameServer(_connectionIdentifier,
                newEntry);
        }

        /* Spieleserver deregistrieren */
        else if (clientCommand instanceof UnregisterGameServerManagementServerCommand) {
            ManagementServer.getInstance()
                .removeGameServer(_connectionIdentifier);
        }

        /* Statistiken des Spieleservers aktualisieren */
        else if (clientCommand instanceof UpdateGameServerStatsManagementServerCommand) {
            UpdateGameServerStatsManagementServerCommand command = (UpdateGameServerStatsManagementServerCommand) clientCommand;

            ManagementServer.getInstance()
                .updateGameServerStats(_connectionIdentifier, command);
        }

        /* Spieleserver abfragen */
        else if (clientCommand instanceof QueryGameServersManagementServerCommand) {
            /* Stelle die Serverliste zusammen */
            QueryGameServersManagementServerResponse response = new QueryGameServersManagementServerResponse();
            for (GameServerEntry entry : ManagementServer.getInstance()
                .getGameServers()) {
                response.addServerEntry(entry);
            }

            /* Sende die Serverliste zurück an den Client */
            try {
                _connection.sendMessage(response);
            } catch (SocketException e) {
            }
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit dem Client erfolgt ist.
     *
     * @return Immer false, da der Server keinen Timeout auslöst
     */
    @Override
    public boolean onConnectionTimeout() {
        /* Callback wird nicht aufgerufen, da kein Timeout spezifiert wurde */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgerteten ist.
     */
    @Override
    public void onConnectionError(Exception e) {
        /*
         * Bei Verbindungsfehler tue so, als ob die Verbindung normal
         * geschlossen wurde
         */
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zum Client beendet
     * wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param closedByUs
     *            true, wenn die Trennung von unserer Seite ausgeht, sonst
     *            false, wenn die Trennung von der Gegenstelle ausgeht
     */
    @Override
    public void onConnectionTerminated(boolean closedByUs) {
        /* Lösche Spieleservereintrag */
        ManagementServer.getInstance().removeGameServer(_connectionIdentifier);
    }
}
