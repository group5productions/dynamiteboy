/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;

/**
 * Dieses Interface wird von Managern innerhalb des Spieleservers implementiert,
 * die sämtliche Verbindungen zu Gegenstellen mit nur einer Instanz bearbeiten
 * möchten.
 *
 * @author Thomas Weber
 */
public interface IConnectionHandlingManager {
    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    boolean onConnectionEstablished(Messenger connection);

    /**
     * Diese Methode wird aufgerufen, wenn die Gegenstelle eine Nachricht an den
     * Manager schickt.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param clientCommand
     *            Nachricht, die die Gegenstelle gesendet hat
     */
    void onCommandReceived(Messenger connection, Message clientMessage);

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit der Gegenstelle erfolgt ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true wenn die Verbindung trotzdem beibehalten werden soll, oder
     *         false, wenn die Verbindung getrennt werden soll
     */
    boolean onConnectionTimeout(Messenger connection);

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    void onConnectionError(Messenger connection, Exception e);

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     */
    void onConnectionClosed(Messenger connection);
}