/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketException;
import java.util.logging.Logger;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.gameserver.Player.PlayerRole;
import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.HeartbeatCommand;
import org.g5p.dynamiteboy.networking.messages.RegisterPlayerGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.RegisterPlayerGameServerResponse;

/**
 * Diese Klasse behandelt Nachrichten, die im Spieleserver ankommen, während
 * seine Lobby offen ist. Spieler können sich an- bzw. abmelden, und der vom
 * Server ausgewählte Spielleiter startet durch Übermitteln der Konfiguration
 * das Spiel.
 *
 * @author Thomas Weber
 */
public final class LobbyManager implements IConnectionHandlingManager {
    private static final Logger _logger = Logger
        .getLogger(LobbyManager.class.getSimpleName());

    private final GameServer _server;

    /**
     * Behandelt die Registrierung eines neuen Spielers. Es wird geprüft, ob der
     * angegebene Spielername keine unerlaubten Zeichen enthält. Wenn ja, dann
     * wird die Registrierung abgelehnt. Ansonsten wird überprüft, ob ein
     * Spieler mit dem gleichen Namen existiert; in diesem Fall wird ein neuer
     * konfliktfreier Name erzeigt, ansonsten wird der Spieler unter seinem
     * Namen eingetragen. War der Spieler bereits angemeldet, wird stets eine
     * Erfolgsmeldung mit dem Namen, unter dem er sich ursprünglich angemeldet
     * hat, gesendet.
     *
     * @param connection
     *            Verbindung, auf der das Kommando ankam
     * @param command
     *            Kommando, welches der Spieler an den Server gesendet hat
     */
    private void _handlePlayerRegistration(Messenger connection,
        RegisterPlayerGameServerCommand command) {
        PlayerManager manager = _server.getPlayerManager();

        /*
         * Lasse den Spielernamen über die Parameterüberprüfung der
         * Nachrichtenklasse überprüfen
         */
        try {
            command.performArgumentCheck();
        } catch (IllegalArgumentException e) {
            /* Lehne die Registrierung ab */
            try {
                connection.sendMessage(
                    new RegisterPlayerGameServerResponse(true, ""));
            } catch (SocketException e2) {
            }

            _logger.severe(String.format("Spielername '%s' wurde abgelehnt",
                command.getName()));
            return;
        }

        synchronized (manager) {
            /*
             * Wurde diese Verbindung bereits mit einem Spieler assoziiert? Gebe
             * den existierenden Spielernamen zurück.
             */
            Player existingPlayer = manager.getPlayerByConnection(connection);
            if (existingPlayer != null) {
                try {
                    connection.sendMessage(new RegisterPlayerGameServerResponse(
                        false, existingPlayer.getName()));
                } catch (SocketException e) {
                }

                _logger.warning(String.format(
                    "Spieler bereits unter dem Namen '%s' angemeldet",
                    existingPlayer.getName()));
                return;
            }

            /*
             * Finde Spieler mit gleichem Namen. Bei Konflikt Zahl anhängen und
             * erneut probieren.
             */
            int i = 0;
            String currentName = command.getName();
            do {
                if (manager.getPlayerByName(currentName) == null)
                    break;

                i++;
                currentName = String.format("%s%d", command.getName(), i);
            } while (true);

            /* Sende Antwort an den Spieler zurück */
            try {
                connection.sendMessage(
                    new RegisterPlayerGameServerResponse(false, currentName));
            } catch (SocketException e) {
            }

            _logger.info(String.format(
                "Spieler wird unter dem Namen '%s' registriert", currentName));

            /*
             * Füge den Spieler mit dieser Verbindung zur Spielerverwaltung
             * hinzu. Da er seine Registierung bekommt, ist er für Updates vom
             * Server qualifiziert. Die Spielerverwaltung wird ihm die korrekte
             * Rolle zuweisen.
             */
            manager.addPlayer(
                new Player(currentName, connection, PlayerRole.WAITING));
        }
    }

    /**
     * Behandelt die Konfiguration der Arena, in der die nächste Runde gespielt
     * wird. Sie darf keine inhaltlichen Fehler haben und muss vom aktuellen
     * Spielleiter stammen, ansonsten wird diese abgelehnt. Bei Akzeptanz wird
     * mit dieser Konfiguration der Wechsel in den Spielmodus eingeleitet und
     * das Spiel in Kürze gestartet.
     *
     * @param connection
     *            Verbindung, auf der das Kommando ankam
     * @param command
     *            Kommando, welches der Spieler an den Server gesendet hat
     */
    private void _handleArenaConfiguration(Messenger connection,
        ConfigureArenaGameServerCommand command) {
        PlayerManager manager = _server.getPlayerManager();

        /* Überprüfe zuerst die Nachricht */
        try {
            command.performArgumentCheck();
        } catch (IllegalArgumentException e) {
            _logger.severe("Ungültige Arenakonfiguration übermittelt");
            return;
        }

        /* Gibt es einen Spieler auf der Verbindung? */
        Player player = manager.getPlayerByConnection(connection);
        if (player == null) {
            _logger.severe(
                "Unregistrierter Spieler versucht, Arena zu konfigurieren!");
            return;
        }

        /* Ist dieser Spieler Spielleiter? */
        if (player.getActiveRole() != PlayerRole.LEADER) {
            _logger.severe(String.format(
                "Spieler '%s', der kein Spielleiter ist, versucht, Arena zu konfigurieren!",
                player.getName()));
            return;
        }

        /*
         * Sind mindestens zwei Spieler angemeldet? (Der Client kann das auch
         * überprüfen, da alle Clients die Liste akzeptierter Spieler, maximal
         * 4, erhalten)
         */
        if (manager.getPlayersCount() < Globals.ROUND_MIN_PLAYERS) {
            _logger.warning(
                "Nicht genügend Spieler angemeldet, um das Spiel starten zu können");
            return;
        }

        /*
         * Setze die maximale Spielerzahl, um Änderungen in der Spielerliste
         * senden zu können
         */
        manager.setMaxActivePlayers(command.getMaxPlayers());

        /* Lege die Spiel(arena)konfiguration an */
        GameConfiguration gameConfig = new GameConfiguration(command);
        _server.setGameConfiguration(gameConfig);

        /* Schalte den Server um -- das Spiel kann beginnen!!! */
        _server.toggleOperationMode();
    }

    /**
     * Initialisiert einen neuen Verwalter für den Lobbymodus.
     *
     * @param server
     *            Spieleserver, zu der diese Instanz gehört
     */
    public LobbyManager(GameServer server) {
        this._server = server;
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Nehme die Verbindung einfach an */
        return true;
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Gegenstelle eine Nachricht an den
     * Manager schickt.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param clientCommand
     *            Nachricht, die die Gegenstelle gesendet hat
     */
    @Override
    public void onCommandReceived(Messenger connection, Message clientMessage) {
        /*
         * Ignoriere Herzschläge
         */
        if (clientMessage instanceof HeartbeatCommand) {
        }

        /*
         * Registriere Spieler
         */
        else if (clientMessage instanceof RegisterPlayerGameServerCommand) {
            RegisterPlayerGameServerCommand command = (RegisterPlayerGameServerCommand) clientMessage;
            _handlePlayerRegistration(connection, command);
        }

        /*
         * Akzeptiere Konfiguration der Spielrunde/-arena unter 2 Bedingungen: -
         * Die Nachricht stammt vom aktuellen Spielleiter - Es ist mindestens
         * noch 1 weiterer Spieler angemeldet
         */
        else if (clientMessage instanceof ConfigureArenaGameServerCommand) {
            ConfigureArenaGameServerCommand command = (ConfigureArenaGameServerCommand) clientMessage;
            _handleArenaConfiguration(connection, command);
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit der Gegenstelle erfolgt ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true wenn die Verbindung trotzdem beibehalten werden soll, oder
     *         false, wenn die Verbindung getrennt werden soll
     */
    @Override
    public boolean onConnectionTimeout(Messenger connection) {
        /* Gibt es einen Spieler zu diesem Timeout? Zeige ihn an */
        Player disconnectedPlayer = _server.getPlayerManager()
            .getPlayerByConnection(connection);
        if (disconnectedPlayer != null) {
            _logger.severe(String.format(
                "Zeitüberschreitung in Verbindung zu Spieler '%s' im "
                    + "Lobbymodus, leite Trennung ein",
                disconnectedPlayer.getName()));
        }

        /* Verbindung soll getrennt werden */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    @Override
    public void onConnectionError(Messenger connection, Exception e) {
        /* Gibt es einen Spieler zu diesem Fehler? Zeige ihn an */
        Player disconnectedPlayer = _server.getPlayerManager()
            .getPlayerByConnection(connection);
        if (disconnectedPlayer != null) {
            _logger.severe(String.format("Fehler in Verbindung zu Spieler "
                + "'%s' im Lobbymodus, leite Trennung ein. Exception-"
                + "Informationen folgen:", disconnectedPlayer.getName()));
        } else {
            _logger.severe("Fehler in Verbindung im Lobbymodus, leite "
                + "Trennung ein. Exception-Informationen folgen:");
        }

        /* Logge die Ausnahme */
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        _logger.severe(writer.toString());
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     */
    @Override
    public void onConnectionClosed(Messenger connection) {
        /*
         * Entferne den Spieler, sofern einer für diese Verbindung registriert
         * ist
         */
        String playerName = _server.getPlayerManager()
            .removePlayerByConnection(connection);
        if (playerName != null)
            _logger.info(String.format(
                "Spieler '%s' hat die Verbindung getrennt", playerName));
    }
}