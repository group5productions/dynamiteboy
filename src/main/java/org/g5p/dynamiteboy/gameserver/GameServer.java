/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.gameserver.Player.PlayerRole;
import org.g5p.dynamiteboy.networking.ManagementClientHandler;
import org.g5p.dynamiteboy.networking.MessageClient;
import org.g5p.dynamiteboy.networking.MessageMalformedException;
import org.g5p.dynamiteboy.networking.MessageServer;
import org.g5p.dynamiteboy.networking.messages.PlayersUpdatedClientCommand;
import org.g5p.dynamiteboy.tools.GameplayMode;
import org.g5p.dynamiteboy.tools.LobbyModePlayerEntry;

/**
 * Diese Klasse umfasst den Betrieb des Spieleservers. Er operiert in zwei Modi:
 * einem Lobbymodus, in welchem neue Spieler den Server betreten dürfen und wo
 * die Auswahl von Spielern für die nächste Runde stattfindet sowie die
 * Konfiguration der nächsten Runde durch den Spielleiter stattfindet, sowie der
 * eigentliche Spielmodus, in welchem der Spieleserver die Engine betreibt und
 * sämtliche Kommunikation mit den Spielern über das Netzwerk abwickelt.
 *
 * @author Thomas Weber
 */
public final class GameServer {
    private static final Logger _logger = Logger
        .getLogger(GameEngine.class.getSimpleName());

    /**
     * An den Verwaltungsthread des Spieleservers gesendete Ereignisse.
     */
    public enum GameServerManagementEvent {
        /**
         * Kein Ereignis wird ausgeführt
         */
        NONE,

        /**
         * Zwischen Lobbymodus und Spielmodus umschalten
         */
        TOGGLE_SERVER_MODE,

        /**
         * Aktuelle Statistiken an den Spieleserver übermitteln
         */
        SEND_MANAGEMENT_UPDATE,

        /**
         * Verbindung zum Verwaltungsserver wieder herstellen
         */
        RESTART_MANAGEMENT_CONNECTION,

        /**
         * Notabschaltung aufgrund einer internen Ausnahme in einem anderen
         * Thread als dem Verwaltungs-Thread (erfordert die Instanz der
         * geworfenen Ausnahme)
         */
        EMERGENCY_EXIT
    };

    private GameManager _gameManager;
    private LobbyManager _lobbyManager;
    private PlayerManager _playerManager;
    private GameConfiguration _gameConfig;
    private GameServerOperationMode _operationMode;

    private Runnable _serverStartedCallback = null;
    private Runnable _serverStoppedCallback = null;

    private Exception _engineException;
    private Exception _serverException;
    private GameServerConfiguration _serverConfiguration = null;
    private MessageServer _messageServer;

    private MessageClient _managementClientConnection;
    private ManagementClientHandler _managementClient;
    private Thread _serverManagementThread;

    private GameServerManagementEvent _serverManagementEvent;
    private boolean _serverManagementEventSent;
    private final Lock _serverManagementEventLock = new ReentrantLock();
    private final Condition _serverManagementEventCond = _serverManagementEventLock
        .newCondition();

    /**
     * Diese Methode setzt alle Member-Variablen der Klasse zurück, die keine
     * weiteren Objekte durch "new" erzeugen oder durch den Benutzer
     * konfigurierbar sind.
     */
    private void _resetMembers() {
        _gameManager = null;
        _lobbyManager = null;
        _playerManager = null;
        _gameConfig = null;
        _operationMode = GameServerOperationMode.NOT_OPERATING;

        _engineException = null;
        _serverException = null;
        _messageServer = null;

        _managementClientConnection = null;
        _managementClient = null;
        _serverManagementThread = null;

        _serverManagementEvent = GameServerManagementEvent.NONE;
        _serverManagementEventSent = false;
    }

    /**
     * Dieser Runnable sendet ein Statusupdate an den Verwaltungsserver sowie
     * eine neue Spielerliste an alle angemeldeten Spieler.
     */
    private final Runnable _sendStatusUpdateAndPlayerList = new Runnable() {
        @Override
        public void run() {
            /* Statusupdate für den Verwaltungsserver */
            _sendGameServerStatusUpdateAsync();

            /* Sende eine neue Spielerliste (Lobbymodus) */
            if (_operationMode == GameServerOperationMode.LOBBY) {
                /* Nur mit Spielern, die zugelassen werden */
                List<Player> players = _playerManager.getActivePlayers();
                if (players == null)
                    return;

                /* Stelle eine neue Spielerliste zusammen */
                PlayersUpdatedClientCommand playerUpdate = new PlayersUpdatedClientCommand();
                for (Player p : players) {
                    /* Trage Spielleiter als solche ein */
                    if (p.getActiveRole() == PlayerRole.LEADER) {
                        playerUpdate.addPlayerEntry(
                            new LobbyModePlayerEntry(p.getName(), true));
                    } else {
                        playerUpdate.addPlayerEntry(
                            new LobbyModePlayerEntry(p.getName(), false));
                    }
                }

                /*
                 * Sende eine neue Spielerliste an alle hier verwalteten Clients
                 */
                _playerManager.broadcastMessageToAllPlayers(playerUpdate);
            }
        }
    };

    /**
     * Diese Methode sendet ein Statusupdate an den Verwaltungsserver, indem ein
     * internes Ereignis an den Verwaltungs-Thread des Spieleservers gesendet
     * wird.
     */
    private void _sendGameServerStatusUpdateAsync() {
        _sendManagementThreadEvent(
            GameServerManagementEvent.SEND_MANAGEMENT_UPDATE);
    }

    /**
     * Diese Methode sendet ein Statusupdate an den Verwaltungsserver. Sie darf
     * nur direkt von _runServerManagementThread() aufgerufen werden! Alle
     * anderen Threads müssen über _sendGameServerStatusUpdateAsync() gehen.
     *
     * @throws MessageMalformedException
     *             wenn die zu sendende Nachricht zu lang wird
     * @throws SocketException
     *             bei Verbindungsproblemen
     */
    private void _sendGameServerStatusUpdate()
        throws MessageMalformedException, SocketException {
        /*
         * Bestimme Spielmodus, falle während offener Lobby auf Standard A
         * zurück
         */
        GameplayMode currentGameplayMode;
        if (_gameConfig != null)
            currentGameplayMode = _gameConfig.getGameplayMode();
        else
            currentGameplayMode = GameplayMode.STANDARD_A;

        /* Sende neue Statistik */
        _managementClient.updateGameServerStats(
            _playerManager.getPlayersCount(),
            _playerManager.getMaxActivePlayers(),
            _operationMode == GameServerOperationMode.LOBBY,
            currentGameplayMode);
    }

    /**
     * Diese Methode wechselt den Operationsmodus des Spieleservers, indem der
     * bisherige Operationsmodus aus- und der neue Operationsmodus eingeschalten
     * wird. Ist der bisherige Operationsmodus
     * {@literal GameServerOperationMode.NOT_OPERATING}, ist der Moduswechsel
     * äquivalent mit der erstmaligen Aktivierung eines Servers. Ist der neue
     * Operationsmodus {@literal GameServerOperationMode.NOT_OPERATING}, ist der
     * Moduswechsel äquivalent mit der Deaktivierung des Servers.
     *
     * @param newOperationMode
     *            Operationsmodus, zu dem der Spieleserver wechseln soll
     * @throws Exception
     *             wenn irgendein Fehler während des Zustandwechsels auftritt.
     *             Verbindungsfehler, DNS-Fehler, oder etwas anderes.
     */
    private void _toggleOperationMode(
        final GameServerOperationMode newOperationMode) throws Exception {
        GameServerOperationMode oldOperationMode = _operationMode;
        boolean forcePlayerListUpdate = false;

        /* Im Fall neuer Zustand == alter Zustand nichts tun */
        if (newOperationMode == oldOperationMode)
            return;

        /*
         * Schalte den bisherigen Operationsmodus aus und setze ihn temporär auf
         * NOT_CONNECTING, wenn im zweiten Teil eine Exception geworfen wird
         */
        switch (oldOperationMode) {
        case NOT_OPERATING:
            _logger.info("Spieleserver wird initialisiert");

            /* Server lief nicht -- initialisiere Spielerverwaltung */
            _gameManager = new GameManager(this);
            _lobbyManager = new LobbyManager(this);
            _playerManager = new PlayerManager(_sendStatusUpdateAndPlayerList,
                Globals.ROUND_MAX_PLAYERS);
            break;

        case LOBBY:
            /* Keine weiteres Herunterfahren notwendig */
            _logger.info("Spieleserver verlässt Lobbymodus");
            break;

        case GAME:
            /* Fahre das aktuelle Spiel herunter und lösche die Konfiguration */
            _gameManager.shutdownGame();
            _gameConfig = null;

            _logger.info("Spieleserver verlässt Spielmodus");
            break;
        }
        _operationMode = GameServerOperationMode.NOT_OPERATING;

        /* Schalte den neuen Operationsmodus ein */
        switch (newOperationMode) {
        case NOT_OPERATING:
            /*
             * Server wird nicht weiter betrieben -- lösche Spielerverwaltung,
             * das Schließen des Servers wird anschließend alle Spieler vom
             * Server kicken
             */
            _gameManager = null;
            _lobbyManager = null;
            _playerManager = null;

            _logger.info("Spieleserver wurde heruntergefahren");
            break;

        case LOBBY:
            /*
             * Update der Spielerliste wird nach Spiel gesendet, da sich während
             * des Spiels Spieler getrennt haben könnten
             */
            if (oldOperationMode == GameServerOperationMode.GAME)
                forcePlayerListUpdate = true;

            /* Keine weitere Initialisierung notwendig */
            _logger.info("Spieleserver betritt Lobbymodus");
            break;

        case GAME:
            /* Erzeuge ein neues Spiel mit den Spielern, die wir haben */
            List<Player> activePlayers = _playerManager.getActivePlayers();
            _gameManager.initializeGame(_gameConfig, activePlayers);

            _logger.info("Spieleserver betritt Spielmodus");
            break;
        }
        _operationMode = newOperationMode;

        /* Erzwinge ein Update der Spielerliste beim Wechsel Spiel -> Lobby */
        if (forcePlayerListUpdate)
            _sendStatusUpdateAndPlayerList.run();
    }

    /**
     * Stellt eine Verbindung zu einem Verwaltungsserver her. Im Detail wird ein
     * eventueller Hostname in eine IP-Adresse aufgelöst, der Client erstellt,
     * die Verbindung hergestellt, und der Spieleserver am Verwaltungsserver
     * angemeldet.
     *
     * @throws Exception
     *             wenn irgendein Problem während des Prozesses auftritt, der
     *             Spieleserver jedem Fall stets beendet, egal, um welche Sorte
     *             von Ausnahme es sich handelt.
     */
    private void _connectToManagementServer() throws Exception {
        /* Löse Adresse des Verwaltungsservers auf */
        InetAddress managementAddress = null;
        String managementAddressString = _serverConfiguration
            .getManagementServerHost();
        managementAddress = InetAddress.getByName(managementAddressString);

        /* Baue eine Verbindung zum Verwaltungsserver auf */
        _managementClientConnection = new MessageClient(managementAddress,
            Globals.MANAGEMENT_SERVER_PORT, 0);
        _managementClientConnection
            .setConnectionHandler(ManagementClientHandler.class);
        _managementClientConnection.startClient();

        /* Wir benötigen die Instanz von ManagementClientHandler */
        _managementClient = (ManagementClientHandler) _managementClientConnection
            .getConnectionHandlerInstance();

        /*
         * Warte, bis der Server erreichbar ist und verweigere den Start im
         * gegenteiligen Fall
         */
        _managementClient
            .waitUntilReadyOrTimeout(Globals.CONNECTION_TIMEOUT_MS);

        /*
         * Bei Trennung der Verbindung wird versucht, die Verbindung zum
         * Verwaltungsserver wieder herzustellen
         */
        _managementClient.setConnectionTerminationCallback(new Runnable() {
            @Override
            public void run() {
                if (!isMessageServerRunning())
                    return;
                _sendManagementThreadEvent(
                    GameServerManagementEvent.RESTART_MANAGEMENT_CONNECTION);
            }
        });

        /* Verbindung steht. Registriere uns am Verwaltungsserver */
        _managementClient.registerGameServer(
            _serverConfiguration.getServerName(),
            _serverConfiguration.getServerListenHost(),
            _serverConfiguration.getServerListenPort());
    }

    /**
     * Trennt, sofern existierend, eine Verbindung zum Verwaltungsserver.
     */
    private void _disconnectFromManagementServer() {
        /* Trenne eine existierende Verbindung zum Verwaltungsserver */
        if (_managementClientConnection != null) {
            _managementClientConnection.stopClient();
            _managementClientConnection = null;
            _managementClient = null;
        }
    }

    /**
     * Fährt den Server so weit hoch, dass dieser Verbindungen von außen
     * entgegen nehmen kann. Verbindungen mit den Clients werden durch die
     * Klasse {@link GameServerHandler} behandelt. Überführe den Server erst in
     * den Lobbymodus, bevor Verbindungen entgegen genommen werden.
     *
     * @throws Exception
     *             wenn irgendein Problem während des Prozesses auftritt, der
     *             Spieleserver jedem Fall stets beendet, egal, um welche Sorte
     *             von Ausnahme es sich handelt.
     */
    private void _startupServer() throws Exception {
        /*
         * Erzeuge den Server und konfiguriere ihn so, dass Client-Verbindungen
         * nach CONNECTION_TIMEOUT_MS Millisekunden getrennt werden können,
         * sofern keine Herzschläge gesendet werden
         */
        InetAddress anyAddress = new InetSocketAddress(0).getAddress();
        _messageServer = new MessageServer(anyAddress,
            _serverConfiguration.getServerListenPort(),
            Globals.CONNECTION_TIMEOUT_MS);

        /* Eingehende Nachrichten werden von GameServerHandler verwaltet */
        _messageServer.setConnectionHandler(GameServerHandler.class, this);

        /*
         * Führe einen Moduswechsel mit altem Zustand = NOT_OPERATING aus, aber
         * noch bevor der Server Verbindungen akzeptiert.
         *
         * Dadurch können wir gewährleisten, dass der Operationsmodus ab dem
         * Zeitpunkt der Verfügbarkeit niemals auf NOT_OPERATING steht, während
         * eine eingehende Verbindung verarbeitet wird.
         */
        _operationMode = GameServerOperationMode.NOT_OPERATING;
        _toggleOperationMode(GameServerOperationMode.LOBBY);

        /* Los geht's */
        _messageServer.startServer();
    }

    /**
     * Fährt den Spieleserver herunter. Beende den aktiven Verwalter des
     * Spielemodus erst, wenn der Server keine Verbindungen mehr bearbeitet.
     */
    private void _shutdownServer() {
        /*
         * Unterbreche den Thread des Spieleservers, dieser trennt allen
         * momentan verbundenen Spielern die Verbindung
         */
        if (_messageServer != null) {
            _messageServer.interrupt();
            try {
                _messageServer.join();
            } catch (InterruptedException e) {
            }
            _messageServer = null;
        }

        /*
         * Führe einen Moduswechsel zu NOT_OPERATING aus, um die Verwaltungs-
         * komponenten herunterzufahren.
         *
         * Dies wird erst nach der Abschaltung der Kommunikation durchgeführt,
         * um sicherzustellen, dass der Operationsmodus nie NOT_OPERATING ist,
         * während der Server für andere zur Verfügung steht.
         *
         * Der Codepfad zu NOT_OPERATING löst keine Ausnahme aus.
         */
        try {
            _toggleOperationMode(GameServerOperationMode.NOT_OPERATING);
        } catch (Exception e) {
        }
        _serverManagementThread = null;
    }

    /**
     * Betreibt den Verwaltungs-Thread des Spieleservers. An ihn können von
     * jedem Punkt des Servers aus Ereignisse gesendet werden, die vom Thread
     * bearbeitet und ausgeführt werden. Gleichzeitig kümmert sich der Thread um
     * die komplette Initialisierung und Deinitialisierung des Spieleservers,
     * inklusive das Bereitstellen des Dienstes sowie die Verbindung zum
     * Verwaltungsserver. Ausnahmen, die von intern kommen oder von extern
     * zugesendet werden, führen eine Notabschaltung des Servers durch.
     */
    private void _runServerManagementThread() {
        /* Setze Events zurück */
        _serverManagementEvent = GameServerManagementEvent.NONE;
        _serverManagementEventSent = false;

        try {
            /* Setze die Verbindung zum Verwaltungsserver auf */
            _connectToManagementServer();

            /* Setze den Spieleserver auf */
            _startupServer();

            /* Sende ein erstes Statusupdate an den Verwaltungsserver */
            _sendGameServerStatusUpdate();

            /*
             * Verbindung zum Verwaltungsserver steht, rufe Callback zur
             * Einsatzbereitschaft auf
             */
            if (_serverStartedCallback != null)
                _serverStartedCallback.run();

            /*
             * Betreibe die Hauptschleife des Threads, welcher auf
             * Unterbrechungen sowie Ereignisse wartet.
             *
             * Warum ein weiteres try? Wenn in der Ereignisbehandlung eine
             * Ausnahme auftritt, bleibt sonst der Event-Lock gesperrt, wenn der
             * Thread aufgrund der Exception aufräumt. Daher stellen wir mit dem
             * weiteren try sicher, dass der Lock immer entsperrt wird, wenn
             * eine Ausnahme auftritt, diese wird nach dem Block in das
             * übergeordnete try geworfen.
             */
            _serverManagementEventLock.lock();
            Exception inLockException = null;
            try {
                while (true) {
                    /*
                     * Warte auf irgendein Ereignis. Bei Unterbrechung des
                     * Threads wird der Stoppvorgang eingeleitet
                     */
                    try {
                        _serverManagementEventCond.await();
                    } catch (InterruptedException e) {
                        break;
                    }

                    /* Sporadisch aufgewacht? Wenn nicht, behandle Ereignisse */
                    if (_serverManagementEventSent) {
                        switch (_serverManagementEvent) {
                        case NONE:
                            break;
                        case TOGGLE_SERVER_MODE:
                            /*
                             * Schaltet zwischen Lobby und Spiel um, aber nie
                             * aus.
                             */
                            this._toggleOperationMode(
                                _operationMode == GameServerOperationMode.LOBBY
                                    ? GameServerOperationMode.GAME
                                    : GameServerOperationMode.LOBBY);
                            /* Sende neues Update durch "fall through" */
                        case SEND_MANAGEMENT_UPDATE:
                            _sendGameServerStatusUpdate();
                            break;
                        case RESTART_MANAGEMENT_CONNECTION:
                            _disconnectFromManagementServer();
                            _connectToManagementServer();
                            break;
                        case EMERGENCY_EXIT:
                            /* Führe eine Notabschaltung durch */
                            throw _engineException;
                        }
                        _serverManagementEventSent = false;
                    }
                }
            } catch (Exception e) {
                /* Erhalte die Ausnahme */
                inLockException = e;
            } finally {
                _serverManagementEventLock.unlock();
            }

            /* Wurde im inneren try eine Ausnahme geworfen? Weiter werfen */
            if (inLockException != null)
                throw inLockException;
        } catch (Exception e) {
            /* Erhalte die Ausnahme */
            _serverException = e;
        } finally {
            /* Fahre Spieleserver herunter */
            _shutdownServer();

            /* Trenne Verbindung zum Verwaltungsserver */
            _disconnectFromManagementServer();

            /* Thread wird beendet => Server wird beendet, rufe Callback auf */
            if (_serverStoppedCallback != null)
                _serverStoppedCallback.run();
        }
    }

    /**
     * Sendet ein Ereignis an den Verwaltungs-Thread des Spieleservers, für das
     * keine weiteren Daten übermittelt werden müssen.
     *
     * @param event
     *            Bezeichner des Ereignisses
     */
    private void _sendManagementThreadEvent(
        final GameServerManagementEvent event) {
        sendManagementThreadEvent(event, null);
    }

    /**
     * Sendet ein Ereignis an den Verwaltungs-Thread des Spieleservers, für das
     * eventuell weiteren Daten übermittelt werden müssen.
     *
     * @param event
     *            Bezeichner des Ereignisses
     * @param object
     *            Daten, für das Ereignis eventuell übermittelt werden müssen
     *            (kann null sein, wenn das Ereignis keine Daten erfordert)
     */
    public void sendManagementThreadEvent(final GameServerManagementEvent event,
        Object object) {
        /* Setze das neue Ereignis */
        _serverManagementEventLock.lock();
        if (event == GameServerManagementEvent.EMERGENCY_EXIT)
            _engineException = (Exception) object;
        _serverManagementEvent = event;
        _serverManagementEventSent = true;
        _serverManagementEventCond.signal();
        _serverManagementEventLock.unlock();
    }

    /**
     * Erzeugt eine neue Instanz der Spieleserver-Klasse, die die
     * spieletechnischen Datenstrukturen beherbergt.
     */
    public GameServer() {
        _resetMembers();
    }

    /**
     * Gibt die Instanz eines Verwalters zurück, der den momentan aktiven
     * Operationsmodus betreut. Darf nicht aufgerufen werden, wenn der Server
     * nicht läuft.
     *
     * Diese Methode ist ausschließlich für die Klasse GameServerHandler
     * bestimmt.
     *
     * @return Momentan aktiver Modusverwalter, oder null, wenn der Server
     *         ausgeschaltet ist
     */
    public synchronized IConnectionHandlingManager getActiveManager() {
        switch (_operationMode) {
        case LOBBY:
            return _lobbyManager;
        case GAME:
            return _gameManager;
        default:
            return null;
        }
    }

    /**
     * Gibt die Instanz der Spielerverwaltung zurück.
     *
     * @return Instanz der Spielerverwaltung
     */
    public PlayerManager getPlayerManager() {
        return _playerManager;
    }

    /**
     * Setzt eine neue Spiel(arena)konfiguration.
     *
     * @param gameConfig
     *            Konfiguration der Spielarena
     */
    public void setGameConfiguration(GameConfiguration gameConfig) {
        this._gameConfig = gameConfig;
    }

    /**
     * Schaltet, sofern der Server läuft, zwischen Lobbymodus und Spielmodus um.
     */
    public void toggleOperationMode() {
        if (_operationMode != GameServerOperationMode.NOT_OPERATING)
            _sendManagementThreadEvent(
                GameServerManagementEvent.TOGGLE_SERVER_MODE);
    }

    /**
     * Im Fall, dass der Spieleserver nicht durch den Benutzer beendet wurde,
     * gibt diese Funktion ein Ausnahmeobjekt zurück, mit der die Fehlerursache
     * angezeigt oder analysiert werden kann.
     *
     * @return Ausnahmeobjekt, welches den Server terminiert hat, oder null
     */
    public Exception getStoppingException() {
        return _serverException;
    }

    /**
     * Diese Methode setzt ein Callback, welches aufgeruden wird, wenn der
     * Spieleserver ohne Probleme initialisiert wurde und sich im
     * betriebsbereiten Zustand befindet.
     *
     * @param serverStartedCallback
     *            Funktion, die aufgerufen wird, wenn der Spieleserver
     *            betriebsbereit ist
     */
    public void setServerStartedCallback(Runnable serverStartedCallback) {
        this._serverStartedCallback = serverStartedCallback;
    }

    /**
     * Diese Methode setzt ein Callback, welches aufgeruden wird, wenn der
     * Spieleserver entweder manuell durch den Benutzer wird oder von sich aus
     * (Fehlerfall) beendet.
     *
     * @param serverStoppedCallback
     *            Funktion, die aufgerufen wird, wenn der Spieleserver seinen
     *            Betrieb beendet hat
     */
    public void setServerStoppedCallback(Runnable serverStoppedCallback) {
        this._serverStoppedCallback = serverStoppedCallback;
    }

    /**
     * Gibt die Konfiguration des Spieleservers zurück
     *
     * @return Konfiguration des Spieleservers
     */
    public GameServerConfiguration getServerConfiguration() {
        return _serverConfiguration;
    }

    /**
     * Legt die Konfiguration des Spieleservers fest. Ohne diese kann der
     * Spieleserver keine Verbindungen annehmen oder Spielerunden betreiben.
     *
     * @param configuration
     *            Konfiguration des Spieleservers
     */
    public void setServerConfiguration(GameServerConfiguration configuration) {
        _serverConfiguration = configuration;
    }

    /**
     * Startet den Spieleserver, sodass er über jedes Netzwerkinterface auf
     * einem vor dem Start eingestellten Port erreichbar ist. Der Spieleserver
     * muss vorher konfiguriert sein.
     *
     * @throws IllegalStateException
     *             wenn der Spieleserver bereits läuft oder wenn er nicht
     *             konfiguriert ist
     */
    public void startMessageServer() throws IllegalStateException {
        /* Spieleserver bereits gestartet? */
        if (isMessageServerRunning())
            throw new IllegalStateException("Spieleserver läuft bereits");

        /* Ist der Spieleserver konfiguriert? */
        if (_serverConfiguration == null)
            throw new IllegalStateException("Spieleserver nicht konfiguriert");

        /*
         * Setze stoppende Ausnahme zurück, es können bei Neustart andere Gründe
         * auftreten, oder der komplette Durchlauf verläuft ohne Probleme
         */
        _serverException = null;

        /*
         * Erstelle einen neuen Thread, der die Ausführung des eigentlichen
         * Servers sowie die Kommunikation mit dem Verwaltungsserver herstellt
         * und koordiniert
         */
        _serverManagementThread = new Thread(new Runnable() {
            @Override
            public void run() {
                _runServerManagementThread();
            }
        });

        /* Starte den neuen Thread */
        _serverManagementThread.start();
    }

    /**
     * Überprüft, ob der Spieleserver Verbindungen annimmt.
     *
     * @return true, wenn der Spieleserver Verbindungen annimmt, ansonsten false
     */
    public boolean isMessageServerRunning() {
        return _messageServer != null && _serverManagementThread != null;
    }

    /**
     * Stoppt den Spieleserver, sodass er auf einem vor dem Start eingestellten
     * Port keine Verbindungen mehr entgegen nimmt. Lief der Spieleserver nie,
     * passiert nichts.
     */
    public void stopMessageServer() {
        /* Läuft der Spieleserver? */
        if (!isMessageServerRunning())
            return;

        /*
         * Unterbreche den Verwaltungs-Thread des Spieleservers. Warte auf sein
         * Beenden, sonst können wir die Variablen nicht zurücksetzen.
         */
        _serverManagementThread.interrupt();
        try {
            _serverManagementThread.join();
        } catch (InterruptedException e) {
        }

        /*
         * Setze alle Variablen zurück, die nicht durch den Anwender
         * konfigurierbar sind, um einen sauberen Zustand wieder herzustellen
         */
        _resetMembers();
    }
}