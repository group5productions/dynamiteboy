/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketException;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Logger;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.gameserver.GameServer.GameServerManagementEvent;
import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.PlayerActionGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.RoundEndHighscoreClientCommand;

/**
 * Diese Klasse verwaltet Verbindungen während eines laufenden Spiels auf dem
 * Server.
 *
 * @author Thomas Weber
 */
public final class GameManager implements IConnectionHandlingManager {
    private static final Logger _logger = Logger
        .getLogger(GameManager.class.getSimpleName());

    private final GameServer _server;
    private List<Player> _participatingPlayers;
    private GameEngine _gameEngine;

    /**
     * Ein Consumer, der jedes Arenaupdate an alle Spieler sendet.
     */
    private final class ArenaUpdateConsumer
        implements Consumer<ArenaUpdatedClientCommand> {

        /**
         * Akzeptiert ein neues Arena-Update.
         *
         * @param command
         *            Kommando des Arena-Updates
         */
        @Override
        public void accept(ArenaUpdatedClientCommand command) {
            _sendMessageToParticipatingPlayers(command);
        }
    }

    /**
     * Ein Callback, das aufgerufen wird, wenn die Engine abstürzt.
     */
    private final class GameCrashCallback implements Consumer<Exception> {

        /**
         * Akzeptiert eine neue Ausnahme.
         *
         * @param e
         *            Ausnahme, die geworfen wurde
         */
        @Override
        public void accept(Exception e) {
            /* Notabschaltung des Servers */
            _server.sendManagementThreadEvent(
                GameServerManagementEvent.EMERGENCY_EXIT, e);
        }
    }

    /**
     * Ein Callback, das aufgerufen wird, wenn das Spiel zu Ende ist.
     */
    private final class GameEndCallback implements Runnable {
        @Override
        public void run() {
            /* Frage den Highscore netzwerkkompatibel ab */
            RoundEndHighscoreClientCommand command = _gameEngine
                .getPlayerScores();

            /* Sende ihn an alle Spieler */
            _sendMessageToParticipatingPlayers(command);

            /* Schalte zurück in die Lobby */
            _server.toggleOperationMode();
        }
    }

    /**
     * Sende eine Nachricht an alle Spieler, die (noch) an der Runde teilnehmen.
     *
     * @param command
     *            Nachricht, die versendet werden soll
     */
    private void _sendMessageToParticipatingPlayers(Message command) {
        if (_participatingPlayers == null)
            return;

        /* Sende Update an alle Spieler, auch wenn diese eliminiert sind */
        synchronized (_participatingPlayers) {
            for (Player player : _participatingPlayers) {
                try {
                    player.getConnection().sendMessage(command);
                } catch (SocketException e) {
                }
            }
        }
    }

    /**
     * Initialisiert einen neuen Verwalter für den Spielmodus.
     *
     * @param server
     *            Spieleserver, zu der diese Instanz gehört
     */
    public GameManager(GameServer server) {
        this._server = server;
        this._gameEngine = null;
        this._participatingPlayers = null;
    }

    /**
     * Initialisiert eine neue Spielerunde, in der die Spiele-Engine
     * initialisiert wird.
     *
     * @param gameConfig
     *            Konfiguration der Arena, in der die Runde gespielt wird
     * @param activePlayers
     *            Liste von serverseitigen Spielern, die an der Runde
     *            teilnehmen. Ihnen wird sequentiell die Spielernummern 1, 2,
     *            ... vergeben.
     */
    public void initializeGame(GameConfiguration gameConfig,
        List<Player> activePlayers) {
        /*
         * Extrahiere die Namen der Spieler in ein Array fester Größe (die
         * Engine identifiziert Spieler auf Basis der Spielernummer, hier durch
         * den Index repräsentiert, aber benötigt Namen für den Highscore).
         */
        this._participatingPlayers = activePlayers;
        String playerNames[] = new String[Globals.ROUND_MAX_PLAYERS];
        int i = 0;
        for (Player player : activePlayers)
            playerNames[i++] = player.getName();

        /*
         * Initialisiere eine neue Engine und starte diese. Beim Beenden dieser
         * wird zurück in den Lobbymodus geschaltet.
         */
        _gameEngine = new GameEngine(gameConfig, null, new GameCrashCallback(),
            new GameEndCallback(), new ArenaUpdateConsumer(), playerNames);
        _gameEngine.resetArenaLayout();
        _gameEngine.start();
    }

    /**
     * Fährt ein laufendes Spiel herunter, indem die Spiele-Engine sauber
     * beendet wird.
     */
    public void shutdownGame() {
        /* Unterbreche das Spiel und warte auf Beenden */
        if (_gameEngine != null && _gameEngine.hasGameStarted()) {
            _gameEngine.interrupt();
            try {
                _gameEngine.join();
            } catch (InterruptedException e) {
            }

            this._participatingPlayers = null;
            this._gameEngine = null;
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * hergestellt wurde.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Lehne neue Spieler ab */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Gegenstelle eine Nachricht an den
     * Manager schickt.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param clientCommand
     *            Nachricht, die die Gegenstelle gesendet hat
     */
    @Override
    public void onCommandReceived(Messenger connection, Message clientMessage) {
        /* Akzeptiere nur Spieleraktionen und ignoriere alles andere */
        if (!(clientMessage instanceof PlayerActionGameServerCommand))
            return;

        /* Ist Spieleraktion bekannt? */
        PlayerActionGameServerCommand actionCommand = (PlayerActionGameServerCommand) clientMessage;
        if (actionCommand.getAction() == null)
            return;

        /* Suche den Spieler, für den sie gilt */
        Player sentByPlayer = _server.getPlayerManager()
            .getPlayerByConnection(connection);
        if (sentByPlayer == null)
            return;
        String sentByPlayerName = sentByPlayer.getName();

        /*
         * Ist der Spieler noch aktiv auf dem Feld? Leite andernfalls nicht an
         * die Engine weiter
         */
        synchronized (_participatingPlayers) {
            for (Player player : _participatingPlayers) {
                String playerName = player.getName();
                if (playerName.equals(sentByPlayerName)) {
                    _gameEngine.performActorAction(playerName,
                        actionCommand.getAction());
                    return;
                }
            }
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit der Gegenstelle erfolgt ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @return true wenn die Verbindung trotzdem beibehalten werden soll, oder
     *         false, wenn die Verbindung getrennt werden soll
     */
    @Override
    public boolean onConnectionTimeout(Messenger connection) {
        /* Gibt es einen Spieler zu diesem Timeout? Zeige ihn an */
        Player disconnectedPlayer = _server.getPlayerManager()
            .getPlayerByConnection(connection);
        if (disconnectedPlayer != null) {
            _logger.severe(String.format(
                "Zeitüberschreitung in Verbindung zu Spieler '%s' im "
                    + "Spielmodus, leite Trennung ein",
                disconnectedPlayer.getName()));
        }

        /* Verbindung soll getrennt werden */
        return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgetreten ist.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     * @param e
     *            Objekt der Ausnahme, die intern aufgetreten ist, z.B. für
     *            nutzerlesbare Diagnosen
     */
    @Override
    public void onConnectionError(Messenger connection, Exception e) {
        /* Gibt es einen Spieler zu diesem Fehler? Zeige ihn an */
        Player disconnectedPlayer = _server.getPlayerManager()
            .getPlayerByConnection(connection);
        if (disconnectedPlayer != null) {
            _logger.severe(String.format("Fehler in Verbindung zu Spieler "
                + "'%s' im Spielmodus, leite Trennung ein. Exception-"
                + "Informationen folgen:", disconnectedPlayer.getName()));
        } else {
            _logger.severe("Fehler in Verbindung im Spielmodus, leite "
                + "Trennung ein. Exception-Informationen folgen:");
        }

        /* Logge die Ausnahme */
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        _logger.severe(writer.toString());
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zur Gegenstelle
     * beendet wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param connection
     *            Instanz der Verbindung zur Gegenstelle
     */
    @Override
    public void onConnectionClosed(Messenger connection) {
        /*
         * Entferne den Spieler, sofern einer für diese Verbindung registriert
         * ist
         */
        String playerName = _server.getPlayerManager()
            .removePlayerByConnection(connection);
        if (playerName != null) {
            _logger.info(String.format(
                "Spieler '%s' hat die Verbindung getrennt", playerName));

            /* Nur, wenn die Liste noch da ist */
            if (_participatingPlayers == null)
                return;

            /*
             * Suche den Spieler in unserer Liste. Steht er drin, löse die
             * Selbstzerstörung aus und entferne den Spieler aus zukünftigen
             * Updates.
             */
            synchronized (_participatingPlayers) {
                /* Suche zugehöriges Objekt */
                Player playerToRemove = null;
                for (Player player : _participatingPlayers) {
                    if (player.getName().equals(playerName)) {
                        playerToRemove = player;
                        break;
                    }
                }
                if (playerToRemove != null) {
                    /* Selbstzerstörung durch null-Aktion */
                    _gameEngine.performActorAction(playerName, null);

                    /* Entferne ihn aus unserer Liste */
                    _participatingPlayers.remove(playerToRemove);
                }
            }
        }
    }
}