/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import org.g5p.dynamiteboy.networking.Messenger;

/**
 * Ein Spieler wird durch die Spielerverwaltung durch einen Namen, seiner
 * Verbindung zum Spieleserver und einer Rolle (Spielleiter, teilnehmender
 * Spieler, Spieler auf Warteliste) identifiziert.
 *
 * Dies ist nicht unbedingt identisch mit dem Spieler in-game!
 *
 * @author Thomas Weber
 */
public final class Player {
    private final String _name;
    private final Messenger _connection;
    private PlayerRole _activeRole;

    /**
     * Rollen, die ein angemeldeter Spieler auf dem Spieleserver aufnehmen kann.
     */
    public enum PlayerRole {
        /**
         * Spielleiter (spielt auch in der nächsten Runde mit)
         */
        LEADER,

        /**
         * Spieler, der für die nächste Runde qualifiziert ist
         */
        MEMBER,

        /**
         * Spieler, der aufgrund von Überfüllung auf der Warteliste steht
         */
        WAITING
    }

    /**
     * Erzeugt einen neuen Spieler für die Spielerverwaltung.
     *
     * @param name
     *            Name des Spielers
     * @param connection
     *            Verbindung des Spielers zum Spieleserver
     * @param initialRole
     *            Rolle, die dem Spieler fürs Erste zugewiesen werden soll
     */
    public Player(String name, Messenger connection, PlayerRole initialRole) {
        this._name = name;
        this._connection = connection;
        this._activeRole = initialRole;
    }

    /**
     * Gibt den Namen des Spielers zurück.
     *
     * @return Name des Spielers
     */
    public String getName() {
        return _name;
    }

    /**
     * Gibt die Verbindung des Spielers zum Spieleserver zurück.
     *
     * @return Verbindung des Spielers zum Spieleserver
     */
    public Messenger getConnection() {
        return _connection;
    }

    /**
     * Gibt die Rolle des Spielers zurück.
     *
     * @return Rolle des Spielers
     */
    public PlayerRole getActiveRole() {
        return _activeRole;
    }

    /**
     * Weist dem Spieler eine neue Rolle zu.
     *
     * @param activeRole
     *            neue Rolle des Spielers
     */
    public void setActiveRole(PlayerRole activeRole) {
        _activeRole = activeRole;
    }
}
