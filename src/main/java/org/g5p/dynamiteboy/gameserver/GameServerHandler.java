/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.IMessageHandler;
import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;
import org.g5p.dynamiteboy.networking.messages.HeartbeatCommand;

/**
 * Diese Klasse empfängt Nachrichten, die für den Spieleserver gedacht sind und
 * lässt diese durch den aktiven Verwalter (je nach aktivem Modus, Lobby oder
 * Spiel) verarbeiten.
 *
 * @author Thomas Weber
 */
public final class GameServerHandler implements IMessageHandler {
    private final GameServer _server;
    private final Timer _heartbeatTimer;

    private Messenger _connection;

    /**
     * Legt eine neue Instanz einer Klasse an, die eine Verbindung zwischen
     * Spieleserver und Client verwaltet.
     *
     * @param object
     *            Instanz des Spieleservers
     */
    public GameServerHandler(Object object) {
        this._server = (GameServer) object;
        this._heartbeatTimer = new Timer();
        this._connection = null;
    }

    /**
     * Diese Methode wird aufgerufen, wenn sich der Client mit dem Server
     * verbunden hat.
     *
     * @param connection
     *            Instanz der Verbindung zum Client
     * @return true, wenn die Verbindung angenommen werden soll, ansonsten false
     */
    @Override
    public boolean onConnectionEstablished(Messenger connection) {
        /* Speichere die Verbindung zum Client für andere Callbacks */
        this._connection = connection;

        /* An den momentan aktiven Verwalter senden */
        boolean ret = true;
        IConnectionHandlingManager manager = _server.getActiveManager();
        if (manager != null)
            ret = manager.onConnectionEstablished(connection);

        /* Aktiviere regelmäßige Heartbeats an den Clients */
        if (ret) {
            _heartbeatTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    /* Sende bei jedem Aufruf einen Heartbeat an den Server */
                    try {
                        _connection.sendMessage(new HeartbeatCommand());
                    } catch (SocketException e) {
                    }
                }
            }, Globals.HEARTBEAT_INTERVAL_MS, Globals.HEARTBEAT_INTERVAL_MS);
        }
        return ret;
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Client eine Nachricht an den
     * Server schickt.
     *
     * @param clientCommand
     *            Nachricht, die der Client gesendet hat
     */
    @Override
    public void onCommandReceived(Message clientCommand) {
        /* An den momentan aktiven Verwalter senden */
        IConnectionHandlingManager manager = _server.getActiveManager();
        if (manager != null)
            manager.onCommandReceived(_connection, clientCommand);
    }

    /**
     * Diese Methode wird aufgerufen, wenn nach einem festgelegten Zeitpunkt
     * keine Kommunikation mit dem Client erfolgt ist.
     *
     * @return Immer false, da der Server keinen Timeout auslöst
     */
    @Override
    public boolean onConnectionTimeout() {
        /* Stoppe den Timer für Heartbeats so früh wie möglich */
        _heartbeatTimer.cancel();

        /* An den momentan aktiven Verwalter senden */
        IConnectionHandlingManager manager = _server.getActiveManager();
        if (manager != null)
            return manager.onConnectionTimeout(_connection);
        else
            return false;
    }

    /**
     * Diese Methode wird aufgerufen, wenn bei der Kommunikation ein
     * Netzwerkfehler o.Ä. aufgerteten ist.
     */
    @Override
    public void onConnectionError(Exception e) {
        /* Stoppe den Timer für Heartbeats so früh wie möglich */
        _heartbeatTimer.cancel();

        /* An den momentan aktiven Verwalter senden */
        IConnectionHandlingManager manager = _server.getActiveManager();
        if (manager != null)
            manager.onConnectionError(_connection, e);
    }

    /**
     * Diese Methode wird aufgerufen, wenn die Kommunikation zum Client beendet
     * wird, unabhängig davon, ob ein Fehler auftrat.
     *
     * @param closedByUs
     *            true, wenn die Trennung von unserer Seite ausgeht, sonst
     *            false, wenn die Trennung von der Gegenstelle ausgeht
     */
    @Override
    public void onConnectionTerminated(boolean closedByUs) {
        /* Stoppe den Timer für Heartbeats so früh wie möglich */
        _heartbeatTimer.cancel();

        /* An den momentan aktiven Verwalter senden */
        IConnectionHandlingManager manager = _server.getActiveManager();
        if (manager != null)
            manager.onConnectionClosed(_connection);
    }
}
