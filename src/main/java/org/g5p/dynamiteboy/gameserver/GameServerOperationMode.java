/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

/**
 * Diese Enumeration definiert die Ausführungsmodi, in denen der Spieleserver
 * operiert. Es existieren zwei Modi: Lobbymodus und Spielemodus.
 *
 * @author Thomas Weber
 */
public enum GameServerOperationMode {
    /**
     * Der Spieleserver wurde nicht betrieben oder er wird nicht mehr betrieben.
     * Dieser Zustand wird verwendet, um mittels einer Umschaltfunktion, die
     * zwischen den Spielemodi umschaltet, auch den Server aktivieren
     * beziehungsweise deaktivierten zu können.
     */
    NOT_OPERATING,

    /**
     * Die Lobby des Spieleservers steht offen, es läuft momentan keine
     * Spielerunde.
     *
     * Die Lobby akzeptiert Spieler, wobei einer als der Spielleiter ausgewählt
     * wird. Der Lobbymodus wird verlassen, wenn der Serveradministrator den
     * Server schließt oder der Spielleiter das Spiel beginnt.
     */
    LOBBY,

    /**
     * Die Lobby des Spieleservers ist geschlossen, es läuft momentan eine
     * Spielerunde.
     *
     * Verbindungsverlust der Spieler haben Konsequenzen, wohin gegen neue
     * Verbindungen die Spielerunde nicht beeinflussen. Der Spielemodus wird
     * verlassen, wenn die Spielerunde beendet ist.
     */
    GAME
}
