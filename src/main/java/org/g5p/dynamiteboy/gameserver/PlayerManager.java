/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.gameserver.Player.PlayerRole;
import org.g5p.dynamiteboy.networking.Message;
import org.g5p.dynamiteboy.networking.Messenger;

/**
 * Diese Klasse stellt die Beziehung zwischen einer Verbindung von Spieleserver
 * und Client sowie einem konkreten Spieler her, der durch seinen Namen
 * eindeutig identifiziert ist. Spieler können hinzugefügt, abgefragt, oder
 * entfernt werden. Jede Änderung der Spielerliste zieht eine Neubestimmung der
 * einzelnen Rollen (Spielleiter, Mitspieler, Wartelister) nach sich.
 *
 * @author Thomas Weber
 */
public final class PlayerManager {
    private final Lock _playerListLock = new ReentrantLock();
    private List<Player> _players;

    private int _maxActivePlayers;
    private Runnable _updateCallback;

    /**
     * Diese Methode wählt eine Spielerrolle anhand eines 0-basierten
     * Indexwertes i aus.
     *
     * @param i
     *            Indexwert eines Spielers
     * @return Eine Spielerrolle aus PlayerRole
     */
    private PlayerRole _getRoleByIndex(int i) {
        /* Markiere den ersten Spieler als Leiter */
        if (i == 0)
            return PlayerRole.LEADER;

        /* Alle weiteren erlaubten als normalen Spieler */
        else if (i < _maxActivePlayers)
            return PlayerRole.MEMBER;

        /* Der Rest landet auf der Warteliste */
        else
            return PlayerRole.WAITING;
    }

    /**
     * Diese Methode aktualisiert die Rollen aller Spieler und informiert den
     * Besitzer über ein Callback darüber, dass sich die Rollen bzw. die Liste
     * selbst geändert hat.
     */
    private void _updatePlayerRoles() {
        /* Aktualisiere Spielerrollen */
        int i = 0;
        for (Player p : _players) {
            p.setActiveRole(_getRoleByIndex(i));
            ++i;
        }

        /*
         * Benachrichtige Besitzer der Instanz über Änderungen in der
         * Spielerliste
         */
        if (_updateCallback != null)
            _updateCallback.run();
    }

    /**
     * Erstellt eine neue Instanz der Spielerverwaltung ohne Spieler.
     *
     * @param updateCallback
     *            Callback, welches ausgeführt wird, wenn sich die Spielerliste
     *            ändert (darf null sein)
     * @param maxActivePlayers
     *            Für den Moment festgelegte Zahl der Spieler, die an der
     *            nächsten Runde teilnehmen darf
     */
    public PlayerManager(Runnable updateCallback, int maxActivePlayers) {
        /* Singleplayer macht keinen Sinn, aber zu viele Spieler auch nicht */
        if (maxActivePlayers < Globals.ROUND_MIN_PLAYERS
            || maxActivePlayers > Globals.ROUND_MAX_PLAYERS) {
            throw new IndexOutOfBoundsException(
                "gewünschte maximale Spieleranzahl nicht zulässig");
        }

        this._players = new LinkedList<Player>();
        this._maxActivePlayers = maxActivePlayers;
        this._updateCallback = updateCallback;
    }

    /**
     * Gibt die Anzahl der maximal zulässigen Spieler für die nächste Runde
     * zurück.
     *
     * @return Anzahl der Spieler, die für die nächste Runde zugelassen sind
     */
    public int getMaxActivePlayers() {
        return _maxActivePlayers;
    }

    /**
     * Setzt die Anzahl der maximal zulässigen Spieler für die nächste Runde und
     * aktualisiert die Rollen der Spieler, sofern notwendig.
     */
    public void setMaxActivePlayers(int maxActivePlayers) {
        /* Singleplayer macht keinen Sinn, aber zu viele Spieler auch nicht */
        if (maxActivePlayers < Globals.ROUND_MIN_PLAYERS
            || maxActivePlayers > Globals.ROUND_MAX_PLAYERS) {
            throw new IndexOutOfBoundsException(
                "gewünschte maximale Spieleranzahl nicht zulässig");
        }

        /* Bei Gleichheit nichts verändern */
        if (maxActivePlayers == _maxActivePlayers)
            return;

        /*
         * Spielerrollen müssen aktualisiert werden, da neue Spieler in die
         * Runde eintreten können bzw. ausscheiden
         */
        _maxActivePlayers = maxActivePlayers;
        _updatePlayerRoles();
    }

    /**
     * Fügt einen neuen Spieler zur Spielerverwaltung hinzu. Es wird nicht
     * überprüft, ob sich der neue Spieler bereits in der Liste befindet.
     *
     * @param player
     *            Spieler, der hinzugefügt werden soll
     */
    public void addPlayer(Player player) {
        /* Wir verändern die Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();

        /* Spieler hinzufügen und Rollen aktualisieren */
        _players.add(player);
        _updatePlayerRoles();

        /* Kritischer Abschnitt wieder vorbei */
        _playerListLock.unlock();
    }

    /**
     * Gibt die Anzahl der von der Spielerverwaltung verwalteten Spieler zurück.
     *
     * @return Anzahl der Spieler in der Spielerverwaltung
     */
    public int getPlayersCount() {
        return _players.size();
    }

    /**
     * Erzeugt eine Liste von Spielern, die für die nächste Runde zugelassen
     * sind.
     *
     * @return Liste von Spielern, die an der nächsten Runde teilnehmen dürfen
     */
    public List<Player> getActivePlayers() {
        /* Bei 0 Spielern brauchen wir uns keine Mühe machen */
        int playerCount = _players.size();
        if (playerCount == 0)
            return null;

        /*
         * Wenn weniger Spieler als zulässig eingetragen sind, müssen wir den
         * Endpunkt der Teilliste anpassen, um einer Exception zu entgehen.
         */
        int selectionMax = playerCount < _maxActivePlayers ? playerCount
            : _maxActivePlayers;

        /*
         * Benutze nicht _players.subList() als "Optimierung" des Extrahierens
         * der ersten N Elemente aus einer Liste.
         *
         * Sublisten sind nur auf eine andere Sicht auf die gleiche Liste. Wenn
         * sich die Struktur der zugrunde liegenden Liste ändert, kann dies zu
         * undefiniertem Verhalten führen, z.B. ConcurrentModificationException.
         *
         * Für weitere Details, siehe https://stackoverflow.com/a/8817658
         */
        LinkedList<Player> activePlayers = new LinkedList<>();
        for (int i = 0; i < selectionMax; ++i)
            activePlayers.add(_players.get(i));
        return activePlayers;
    }

    /**
     * Findet einen Spieler anhand seines eindeutigen Namens.
     *
     * @param playerName
     *            Name des Spielers, nach dem gesucht werden soll
     * @return Instanz des Spieler mit dem Namen oder null, wenn der Spieler
     *         nicht in der Verwaltung steht
     */
    public Player getPlayerByName(String playerName) {
        /* Wir durchsuchen die Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();
        for (Player p : _players) {
            /*
             * Stimmt Name überein? Wenn ja, zurückgeben und kritischen
             * Abschnitt verlassen
             */
            if (p.getName().equals(playerName)) {
                _playerListLock.unlock();
                return p;
            }
        }

        /* Kritischer Abschnitt wieder vorbei, Spieler nicht gefunden */
        _playerListLock.unlock();
        return null;
    }

    /**
     * Findet einen Spieler anhand seiner Verbindung zum Spieleserver.
     *
     * @param connection
     *            Verbindung, zu der der Spieler gehört
     * @return Instanz des Spieler mit der Verbindung oder null, wenn der
     *         Spieler nicht in der Verwaltung steht
     */
    public Player getPlayerByConnection(Messenger connection) {
        /* Wir durchsuchen die Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();
        for (Player p : _players) {
            /*
             * Stimmt Verbindung überein? Wenn ja, zurückgeben und kritischen
             * Abschnitt verlassen
             */
            if (p.getConnection() == connection) {
                _playerListLock.unlock();
                return p;
            }
        }

        /* Kritischer Abschnitt wieder vorbei, Spieler nicht gefunden */
        _playerListLock.unlock();
        return null;
    }

    /**
     * Entfernt einen Spieler anhand des Spielerobjekts und aktualisiert die
     * Spielerrollen, wenn der zu löschende Spieler in der Verwaltung stand.
     *
     * @param player
     *            Spieler, der gelöscht wurde
     */
    public void removePlayer(Player player) {
        /* Wir bearbeiten die Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();

        /*
         * Entferne den Spieler. Wenn tatsächlich gelöscht, dann aktualisiere
         * die Spielerrollen
         */
        if (_players.remove(player))
            _updatePlayerRoles();

        /* Kritischer Abschnitt wieder vorbei */
        _playerListLock.unlock();
    }

    /**
     * Entfernt einen Spieler anhand seiner Verbindung zum Spieleserver.
     *
     * @param connection
     *            Verbindung, zu der der Spieler gehört
     * @return Name des Spielers, der getrennt wurde, oder null, wenn der
     *         Spieler nicht in der Verwaltung stand
     */
    public String removePlayerByConnection(Messenger connection) {
        Player playerToRemove = null;
        String removedPlayerName = null;

        /* Wir bearbeiten die Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();
        for (Player p : _players) {
            /* Stimmt Verbindung überein? Wenn ja, haben wir unseren Spieler */
            if (p.getConnection() == connection) {
                playerToRemove = p;
                removedPlayerName = p.getName();
                break;
            }
        }

        /*
         * Lösche den Spieler, sofern vorhanden, und aktualisiere die
         * Spielerrollen
         */
        if (playerToRemove != null && _players.remove(playerToRemove))
            _updatePlayerRoles();

        /* Kritischer Abschnitt wieder vorbei */
        _playerListLock.unlock();
        return removedPlayerName;
    }

    /**
     * Sendet eine Nachricht an alle angemeldeten Spieler.
     *
     * @param message
     *            Nachricht, die an alle angemeldeten Spieler übermittelt werden
     *            soll
     */
    public void broadcastMessageToAllPlayers(Message message) {
        if (message == null)
            throw new IllegalArgumentException("keine Nachricht zum Senden!");

        /* Wir benötigen eine feste Spielerliste, daher kritischer Abschnitt */
        _playerListLock.lock();
        for (Player p : _players) {
            /* SocketExceptions sind uns beim Broadcast egal */
            try {
                p.getConnection().sendMessage(message);
            } catch (SocketException e) {
            }
        }

        /* Kritischer Abschnitt wieder vorbei */
        _playerListLock.unlock();
    }
}