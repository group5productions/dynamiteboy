/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.gameserver;

import org.g5p.dynamiteboy.Globals;

/**
 * Diese Klasse speichert sämtliche Einstellungen, die ein Server-Administrator
 * am Spieleserver frei verändern kann, solange der Server keine Verbindungen
 * entgegen nimmt.
 *
 * @author Thomas Weber
 */
public final class GameServerConfiguration {
    private String _serverName;
    private String _serverListenHost;
    private int _serverListenPort;
    private String _managementServerHost;

    /**
     * Erzeugt eine neue Konfiguration des Spieleservers unter Einrichtung von
     * Standardeinstellungen.
     */
    public GameServerConfiguration() {
        this._serverName = Globals.GAME_SERVER_DEFAULT_NAME;
        this._serverListenHost = Globals.GAME_SERVER_DEFAULT_HOST;
        this._serverListenPort = Globals.GAME_SERVER_DEFAULT_PORT;
        this._managementServerHost = Globals.MANAGEMENT_SERVER_DEFAULT_HOST;
    }

    /**
     * Gibt den aktuellen Namen des Spieleservers zurück, unter dem dieser sich
     * beim Verwaltungsserver anmeldet.
     *
     * @return Name des Spieleservers
     */
    public String getServerName() {
        return _serverName;
    }

    /**
     * Setzt den Namen des Spieleservers, unter dem dieser sich beim
     * Verwaltungsserver anmelden soll.
     *
     * @param serverName
     *            Name des Spieleservers
     */
    public void setServerName(String serverName) {
        /* Name darf nicht leer sein */
        serverName = serverName.trim();
        if (serverName.isEmpty())
            throw new IllegalArgumentException(
                "Name des Spieleservers darf nicht leer sein");

        _serverName = serverName;
    }

    /**
     * Gibt den Hostnamen oder die IP-Adresse des Spieleservers zurück, so wie
     * dieser sich am Verwaltungsserver anmelden soll.
     *
     * @return Hostname oder IP-Adresse des Spieleservers, unter dem er
     *         erreichbar ist
     */
    public String getServerListenHost() {
        return _serverListenHost;
    }

    /**
     * Setzt den Hostnamen oder die IP-Adresse des Spieleservers, so wie dieser
     * sich am Verwaltungsserver anmelden soll.
     *
     * @return Hostname oder IP-Adresse des Spieleservers, unter dem er
     *         erreichbar ist
     */
    public void setServerListenHost(String serverListenHost) {
        /* Name darf nicht leer sein */
        serverListenHost = serverListenHost.trim();
        if (serverListenHost.isEmpty())
            throw new IllegalArgumentException(
                "Adresse des Spieleservers darf nicht leer sein");

        _serverListenHost = serverListenHost;
    }

    /**
     * Gibt den TCP-Port zurück, unter dem der Spieleserver erreicht werden
     * soll.
     *
     * @return TCP-Port des Spieleservers
     */
    public int getServerListenPort() {
        return _serverListenPort;
    }

    /**
     * Setzt den TCP-Port, unter dem der Spieleserver erreicht werden soll.
     *
     * @param serverListenPort
     *            TCP-Port des Spieleservers. Muss zwischen 1 und 65535 liegen
     *            und darf nicht {@link Globals#MANAGEMENT_SERVER_PORT} sein.
     * @throws IllegalArgumentException
     *             wenn der TCP-Port die Bedingungen verletzt
     */
    public void setServerListenPort(int serverListenPort)
        throws IllegalArgumentException {
        /* Lehne nicht erlaubte Ports ab */
        if (serverListenPort <= 0 || serverListenPort > 65535
            || serverListenPort == Globals.MANAGEMENT_SERVER_PORT) {
            throw new IllegalArgumentException(
                String.format("TCP-Port %d nicht für Spieleserver zulässig",
                    serverListenPort));
        }

        _serverListenPort = serverListenPort;
    }

    /**
     * Gibt den Hostnamen oder die IP-Adresse des Verwaltungsservers zurück, an
     * welchem sich der Spieleserver anmelden soll.
     *
     * @return Hostname oder IP-Adresse des Verwaltungsservers
     */
    public String getManagementServerHost() {
        return _managementServerHost;
    }

    /**
     * Setzt den Hostnamen oder die IP-Adresse des Verwaltungsservers, an
     * welchem sich der Spieleserver anmelden soll.
     *
     * @return Hostname oder IP-Adresse des Verwaltungsservers
     */
    public void setManagementServerHost(final String serverHost) {
        _managementServerHost = serverHost;
    }
}
