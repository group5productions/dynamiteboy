/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import org.g5p.dynamiteboy.engine.GameArena;

/**
 * Diese Klasse abstrahiert alle Objekte in der Spielarena.
 *
 * @author Christian Ulrich (in Grundzügen zumindest)
 * @author Thomas Weber
 */
public abstract class GameObject {
    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    public abstract boolean isDestroyable();

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    public abstract boolean isWalkable();

    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    public abstract GameObjectIdentifier getObjectIdentifier();

    /**
     * Aktualisiert den internen Tick des Objektes und führt Aktionen für diesen
     * Tick aus. Standarmäßig eine leere Implementation für Objekte, die keine
     * Ticks benötigen.
     *
     * @param arena
     *            Arena, in der die Aktionen durchgeführt werden sollen
     * @return true, wenn eine Animation im Gange ist, die nicht durch Spielende
     *         abgebrochen werden soll, ansonsten false
     */
    public boolean performTasks(GameArena arena) {
        return false;
    }
}