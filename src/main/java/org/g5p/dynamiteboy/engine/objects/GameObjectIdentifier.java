/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

/**
 * Diese Enumeration weist jedem Objekt der Arena eine feste ID zu.
 *
 * @author Thomas Weber
 * @author Andere am Softwareprojekt beteiligte Gruppen (Spezifikation)
 */
public enum GameObjectIdentifier {
    /**
     * Freies Feld
     */
    WALKABLE_TILE(0),

    /**
     * Feste Mauer
     */
    FIXED_WALL(1),

    /**
     * Zerstörbare Mauer
     */
    BREAKABLE_WALL(2),

    /**
     * Ausbreitung einer Bombenexplosion
     */
    EXPLOSION(3),

    /**
     * Powerup zur Erhöhung des Bombenradius
     */
    POWERUP_BOMB_RADIUS(21),

    /**
     * Powerup für mehr Bomben
     */
    POWERUP_MORE_BOMBS(22),

    /**
     * Powerup für temporäre Rüstung
     */
    POWERUP_ARMOR(23),

    /**
     * Powerup für schnelleres Laufen (A+B)
     */
    POWERUP_FASTER_RUN(24),

    /**
     * Powerup für Bomben treten (A+B)
     */
    POWERUP_KICK_BOMBS(25),

    /**
     * Powerup für Super Bombe (A+B)
     */
    POWERUP_SUPER_BOMB(26),

    /**
     * Powerup für maximaler Bombenradius (A+B)
     */
    POWERUP_BOMB_RADIUS_MAX(27),

    /**
     * Powerup für Bombenläufer (A+B)
     */
    POWERUP_BOMB_RUNNER(28),

    /**
     * Spieler 1
     */
    PLAYER_1(51),

    /**
     * Spieler 2
     */
    PLAYER_2(52),

    /**
     * Spieler 3
     */
    PLAYER_3(53),

    /**
     * Spieler 4
     */
    PLAYER_4(54),

    /**
     * Spieler 1, Rüstung tragend
     */
    PLAYER_1_ARMORED(55),

    /**
     * Spieler 2, Rüstung tragend
     */
    PLAYER_2_ARMORED(56),

    /**
     * Spieler 3, Rüstung tragend
     */
    PLAYER_3_ARMORED(57),

    /**
     * Spieler 4, Rüstung tragend
     */
    PLAYER_4_ARMORED(58),

    /**
     * Bombe (Zustand 1), gehört Spieler 1
     */
    BOMB_STATE_1_PLAYER_1(61),

    /**
     * Bombe (Zustand 2), gehört Spieler 1
     */
    BOMB_STATE_2_PLAYER_1(62),

    /**
     * Bombe (Zustand 3), gehört Spieler 1
     */
    BOMB_STATE_3_PLAYER_1(63),

    /**
     * Bombe (Zustand 1), gehört Spieler 2
     */
    BOMB_STATE_1_PLAYER_2(71),

    /**
     * Bombe (Zustand 2), gehört Spieler 2
     */
    BOMB_STATE_2_PLAYER_2(72),

    /**
     * Bombe (Zustand 3), gehört Spieler 2
     */
    BOMB_STATE_3_PLAYER_2(73),

    /**
     * Bombe (Zustand 1), gehört Spieler 3
     */
    BOMB_STATE_1_PLAYER_3(81),

    /**
     * Bombe (Zustand 2), gehört Spieler 3
     */
    BOMB_STATE_2_PLAYER_3(82),

    /**
     * Bombe (Zustand 3), gehört Spieler 3
     */
    BOMB_STATE_3_PLAYER_3(83),

    /**
     * Bombe (Zustand 1), gehört Spieler 4
     */
    BOMB_STATE_1_PLAYER_4(91),

    /**
     * Bombe (Zustand 2), gehört Spieler 4
     */
    BOMB_STATE_2_PLAYER_4(92),

    /**
     * Bombe (Zustand 3), gehört Spieler 4
     */
    BOMB_STATE_3_PLAYER_4(93),

    /**
     * Kein gültiges Objekt
     */
    NULL(255);

    private final int _value;

    /**
     * (Dieser Konstruktor dient der internen Nutzung.)
     *
     * @param value
     *            Wert, der in der Enumeration gespeichert werden soll
     */
    GameObjectIdentifier(int value) {
        this._value = value;
    }

    /**
     * Übersetzt einen Bezeichner zu einem Integer, der über das Netzwerk
     * transportiert werden kann.
     *
     * @param m
     *            Spielmodus
     */
    public int getValue() {
        return _value;
    }

    /**
     * Übersetzt einen über das Netzwerk übertragenen Bezeichner zurück.
     *
     * @param v
     *            Spielmodus, wie er über das Netzwerk übertragen wurde
     */
    public static GameObjectIdentifier fromValue(int v) {
        for (GameObjectIdentifier x : GameObjectIdentifier.values()) {
            if (x._value == v)
                return x;
        }
        return null;
    }

    /**
     * Entspricht der Wert einer Enumeration einer Bombe?
     *
     * @return true, wenn Enumerationswert für Bombe steht, ansonsten false
     */
    public static boolean isIdentifierABomb(GameObjectIdentifier ident) {
        switch (ident) {
        case BOMB_STATE_1_PLAYER_1:
        case BOMB_STATE_1_PLAYER_2:
        case BOMB_STATE_1_PLAYER_3:
        case BOMB_STATE_1_PLAYER_4:
        case BOMB_STATE_2_PLAYER_1:
        case BOMB_STATE_2_PLAYER_2:
        case BOMB_STATE_2_PLAYER_3:
        case BOMB_STATE_2_PLAYER_4:
        case BOMB_STATE_3_PLAYER_1:
        case BOMB_STATE_3_PLAYER_2:
        case BOMB_STATE_3_PLAYER_3:
        case BOMB_STATE_3_PLAYER_4:
            return true;
        default:
            return false;
        }
    }

    /**
     * Ist das Objekt mit diesem Bezeichner generell belaufbar?
     *
     * @return true, wenn das Objekt generell belaufbar ist, ansonsten false
     */
    public static boolean isIdentifierWalkable(GameObjectIdentifier ident) {
        switch (ident) {
        case EXPLOSION:
        case PLAYER_1:
        case PLAYER_1_ARMORED:
        case PLAYER_2:
        case PLAYER_2_ARMORED:
        case PLAYER_3:
        case PLAYER_3_ARMORED:
        case PLAYER_4:
        case PLAYER_4_ARMORED:
        case POWERUP_ARMOR:
        case POWERUP_BOMB_RADIUS:
        case POWERUP_MORE_BOMBS:
        case WALKABLE_TILE:
            return true;
        default:
            return false;
        }
    }
}
