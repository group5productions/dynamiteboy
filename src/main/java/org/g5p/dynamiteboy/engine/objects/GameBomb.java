/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import java.util.List;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameArena;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.tools.GameplayMode;

/**
 * Diese Klasse implementiert ein Objekt, welches eine Bombe auf dem Feld
 * repräsentiert.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameBomb extends GameObject {
    private final GameEngine _owningEngine;
    private final GameplayMode _gameplayMode;
    private final GameActor _plantedByPlayer;
    private int _bombTicksRemaining;
    private int _positionX;
    private int _positionY;

    private boolean _kickInProgress;
    private int _kickXDelta;
    private int _kickYDelta;

    /**
     * Führe eine Bombenexplosion an den angegebenen Koordinaten durch.
     *
     * @param arena
     *            Arena, in der die Explosion stattfindet
     * @param currentX
     *            X-Koordinate in der Arena, an der die Explosion stattfindet
     * @param currentY
     *            Y-Koordinate in der Arena, an der die Explosion stattfindet
     * @return true, wenn die Explosion weiter fortschreiten kann, und false,
     *         wenn ein Hindernis im Weg steht
     */
    private boolean _explodeSingleCoordinate(final GameArena arena,
        final int currentX, final int currentY) {
        final GameObject currentObject = arena.getArenaObject(currentX,
            currentY);

        /* Ist das Objekt unzerstörbar? Stopp. */
        if (!currentObject.isDestroyable())
            return false;

        boolean stopExplosion = false;

        /*
         * Haben wir einen Spieler erwischt? Wenn ja, erwische alle an diesen
         * Koordinaten.
         */
        final List<GameActor> playersBeingHit = arena.getActorObjects(currentX,
            currentY);
        if (playersBeingHit != null) {
            /*
             * Eliminiere alle Spieler auf dieser Koordinate, die nicht durch
             * eine Rüstung geschützt sind.
             */
            for (final GameActor currentPlayer : playersBeingHit) {
                if (!currentPlayer.isArmored())
                    currentPlayer.perish(_plantedByPlayer);
            }

            /*
             * Stoppe die Explosion, sofern das "Super Bombe"-Powerup nicht
             * aktiv ist.
             */
            if (!_plantedByPlayer.areSuperBombsEnabled())
                stopExplosion = true;
        }

        /*
         * Wähle das Objekt aus, welches gemalt werden soll, standardmäßig die
         * Explosion
         */
        GameObjectIdentifier paintedObjectIdent = GameObjectIdentifier.EXPLOSION;

        /*
         * Zerstörbare Wand. Die Explosion wird nach dieser Wand gestoppt,
         * sofern das "Super Bombe"-Powerup nicht aktiv ist. Es können zufällig
         * Powerups erscheinen, diese werden anstatt der Explosion gemalt.
         */
        if (currentObject instanceof GameBreakableWall) {
            /* Ist der Threshold für irgendein Powerup erreicht? */
            if (Globals.GAME_POWERUP_APPEARANCE_CHANCE_PERCENT > Math.random()
                * 100.0) {

                /* Wähle anhand der Verteilung einer weiteren Zufallszahl */
                final long powerupSelectionTenthPercent = Math
                    .round(Math.random() * 1000.0);

                /* Handhabe je nach Spielmodus unterschiedlich */
                if (_gameplayMode == GameplayMode.STANDARD_A_B)
                    paintedObjectIdent = _choosePowerupStandardAB(
                        powerupSelectionTenthPercent);
                else
                    paintedObjectIdent = _choosePowerupStandardA(
                        powerupSelectionTenthPercent);
            }

            /* Statistik: Wand zerstört */
            _plantedByPlayer.getStatistics().wallDestroyed();

            /*
             * Stoppe die Explosion, sofern das "Super Bombe"-Powerup nicht
             * aktiv ist.
             */
            if (!_plantedByPlayer.areSuperBombsEnabled())
                stopExplosion = true;
        }

        /* Male das ausgewählte Objekt */
        arena.setArenaObject(currentX, currentY, paintedObjectIdent);
        return stopExplosion ? false : true;
    }

    /**
     * Führe eine Bombenexplosion entlang einer angegebenen Richtung durch,
     * welche durch ein X-Delta und ein Y-Delta repräsentiert wird. Zulässige
     * Werte sind 0, -1 und +1, wobei genau eines der beiden Deltas 0 sein muss.
     *
     * @param arena
     *            Arena, in der die Explosion stattfindet
     * @param xDelta
     *            X-Delta der Richtung. +1 zeigt nach rechts und -1 zeigt nach
     *            links, bei 0 gilt das Y-Delta.
     * @param yDelta
     *            Y-Delta der Richtung. +1 zeigt nach unten und -1 zeigt nach
     *            oben, bei 0 gilt das X-Delta.
     */
    private void _explodeInDirection(final GameArena arena, final int xDelta,
        final int yDelta) {
        /* Iteriere jedes Objekt in die Richtung */
        for (int pos = 1; pos < _plantedByPlayer
            .getBombExplosionRadius(); ++pos) {

            /* Noch in den Limits der Arena? */
            final int currentX = _positionX + xDelta * pos;
            final int currentY = _positionY + yDelta * pos;
            if (!arena.areCoordinatesInBounds(currentX, currentY))
                break;

            /* Bearbeite jedes Objekt einzeln, breche ab bei Hindernis */
            if (!_explodeSingleCoordinate(arena, currentX, currentY))
                break;
        }
    }

    /**
     * Erzeugt eine neue Instanz einer Bombe.
     *
     * @param owningEngine
     *            Spiele-Engine, zu der die Bombe gehört
     * @param x
     *            X-Koordinate, auf der die Bombe liegen soll
     * @param y
     *            Y-Koordinate, auf der die Bombe liegen soll
     * @param plantedByPlayer
     *            Spieler, der die Bombe gelegt hat
     */
    public GameBomb(final GameEngine owningEngine, final int x, final int y,
        final GameActor plantedByPlayer) {
        this._owningEngine = owningEngine;

        /* Speichere Spielmodus zwischen */
        this._gameplayMode = owningEngine.getGameConfiguration()
            .getGameplayMode();

        this._plantedByPlayer = plantedByPlayer;
        this._positionX = x;
        this._positionY = y;

        this._kickInProgress = false;
        this._kickXDelta = 0;
        this._kickYDelta = 0;

        /* Berechne Ticks aus der Spielkonfiguration */
        this._bombTicksRemaining = owningEngine.getGameConfiguration()
            .getBombExplosionSeconds() * Globals.GAME_TICKS_PER_SECOND;
    }

    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    @Override
    public boolean isDestroyable() {
        /* Wir zerstören uns sogar selbst */
        return true;
    }

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    @Override
    public boolean isWalkable() {
        /* Bomben sind ein Hindernis */
        return false;
    }

    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    @Override
    public GameObjectIdentifier getObjectIdentifier() {
        /* Unterscheide nach dem Countdown bis zur Explosion */
        switch (_plantedByPlayer.getPlayerNumber()) {
        case 1:
            if (_bombTicksRemaining <= 5)
                return GameObjectIdentifier.BOMB_STATE_3_PLAYER_1;
            else if (_bombTicksRemaining <= 10)
                return GameObjectIdentifier.BOMB_STATE_2_PLAYER_1;
            else
                return GameObjectIdentifier.BOMB_STATE_1_PLAYER_1;
        case 2:
            if (_bombTicksRemaining <= 5)
                return GameObjectIdentifier.BOMB_STATE_3_PLAYER_2;
            else if (_bombTicksRemaining <= 10)
                return GameObjectIdentifier.BOMB_STATE_2_PLAYER_2;
            else
                return GameObjectIdentifier.BOMB_STATE_1_PLAYER_2;
        case 3:
            if (_bombTicksRemaining <= 5)
                return GameObjectIdentifier.BOMB_STATE_3_PLAYER_3;
            else if (_bombTicksRemaining <= 10)
                return GameObjectIdentifier.BOMB_STATE_2_PLAYER_3;
            else
                return GameObjectIdentifier.BOMB_STATE_1_PLAYER_3;
        case 4:
            if (_bombTicksRemaining <= 5)
                return GameObjectIdentifier.BOMB_STATE_3_PLAYER_4;
            else if (_bombTicksRemaining <= 10)
                return GameObjectIdentifier.BOMB_STATE_2_PLAYER_4;
            else
                return GameObjectIdentifier.BOMB_STATE_1_PLAYER_4;
        }
        return null;
    }

    /**
     * Führe mit jedem Tick Operationen auf dieser Bombe aus, unter anderem der
     * Countdown bis zur Explosion sowie das Durchführen eines Tretens der
     * Bombe.
     *
     * @param arena
     *            Arena, in der die Bombe steht
     * @return true, wenn eine Animationssequenz von dieser Bombe noch am Laufen
     *         ist, ansonsten false
     */
    @Override
    public boolean performTasks(final GameArena arena) {
        boolean preventGameStop = false;

        /*
         * Reduziere mit jedem Tick die Wartezeit bis zur Explosion, und lasse
         * sie hochgehen, wenn sie abgelaufen ist.
         */
        if (_bombTicksRemaining > 0) {
            --_bombTicksRemaining;
            if (_bombTicksRemaining == 0)
                explodeImmediately(arena);

            /* Lasse die Bombe noch explodieren, wenn das Spiel endet */
            preventGameStop = true;
        }

        /*
         * Wird die Bombe getreten, verschiebe sie in die entsprechende
         * Richtung, bis wir auf ein Hindernis stoßen.
         */
        if (_kickInProgress) {
            boolean stopKick = true;

            /* Ist die nächste Koordinate noch gültig? */
            final int newX = _positionX + _kickXDelta;
            final int newY = _positionY + _kickYDelta;
            if (arena.areCoordinatesInBounds(newX, newX)) {
                /*
                 * Spieler oder nicht belaufbares Objekt an neuer Stelle?
                 */
                if (arena.getActorObject(newX, newY) == null) {
                    GameObject newObject = arena.getArenaObject(newX, newY);
                    if (newObject != null && newObject.isWalkable()) {
                        /*
                         * Gut, setze uns an die neue Stelle und überschreibe
                         * die alte Stelle mit freiem Feld (nimmt also Powerups
                         * mit).
                         */
                        arena.setArenaObject(newX, newY, this);
                        arena.setArenaObject(_positionX, _positionY,
                            GameObjectIdentifier.WALKABLE_TILE);
                        _positionX = newX;
                        _positionY = newY;
                        stopKick = false;
                    }
                }
            }

            /* Beende das Kicken, wenn erforderlich */
            if (stopKick) {
                _kickInProgress = false;
                _kickXDelta = 0;
                _kickYDelta = 0;
            }
        }

        return preventGameStop;
    }

    /**
     * Gibt die X-Koordinate der Position der Bombe zurück.
     *
     * @return X-Koordinate der Position der Bombe
     */
    public int getPositionX() {
        return _positionX;
    }

    /**
     * Gibt die Y-Koordinate der Position der Bombe zurück.
     *
     * @return Y-Koordinate der Position der Bombe
     */
    public int getPositionY() {
        return _positionY;
    }

    /**
     * Gibt die Anzahl der Ticks zurück, die bis zur Explosion der Bombe
     * verbleiben.
     *
     * @return Anzahl der Ticks, die bis zur Explosion der Bombe verbleiben
     */
    public int getRemainingBombTicks() {
        return _bombTicksRemaining;
    }

    /**
     * Gibt den Spieler zurück, der diese Bombe gelegt hat.
     *
     * @return Spieler, der diese Bombe gelegt hat
     */
    public GameActor getBombPlanter() {
        return _plantedByPlayer;
    }

    /**
     * Legt die Position der Bombe fest.
     *
     * @param x
     *            X-Koordinate, an der die Bombe liegen soll
     * @param y
     *            Y-Koordinate, an der die Bombe liegen soll
     */
    public void setPosition(final int x, final int y) {
        /* Koordinaten innerhalb der Arena? */
        if (!_owningEngine.getGameArena().areCoordinatesInBounds(x, y)) {
            throw new IllegalArgumentException(
                String.format("Position {%d,%d} außerhalb der Arena", x, y));
        }

        this._positionX = x;
        this._positionY = y;
    }

    /**
     * Wählt auf Basis eines Wertes, der ein Zehntel Prozent repräsentiert, ein
     * Powerup aus den möglichen aus, die im Spielmodus "Standard A" erlaubt
     * sind.
     *
     * @param choice
     *            Integer-Wert zwischen 0 und 1000, der für ein Zehntel Prozent
     *            steht und in Wertebereiche aufgeteilt wird
     * @return Bezeichner des ausgewählten Powerups je nach Wertebereich der
     *         Auswahl
     */
    private GameObjectIdentifier _choosePowerupStandardA(long choice) {
        /* Wahrscheinlichkeit "Radius Erweiterung" */
        if (choice < Globals.GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A)
            return GameObjectIdentifier.POWERUP_BOMB_RADIUS;
        choice -= Globals.GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A;

        /* Wahrscheinlichkeit "Extra Bombe" */
        if (choice < Globals.GAME_POWERUP_MORE_BOMBS_CHANCE_10TH_PERCENT_A)
            return GameObjectIdentifier.POWERUP_MORE_BOMBS;

        /* Sonst "Rüstung" */
        return GameObjectIdentifier.POWERUP_ARMOR;
    }

    /**
     * Wählt auf Basis eines Wertes, der ein Zehntel Prozent repräsentiert, ein
     * Powerup aus den möglichen aus, die im Spielmodus "Standard A+B" erlaubt
     * sind.
     *
     * @param choice
     *            Integer-Wert zwischen 0 und 1000, der für ein Zehntel Prozent
     *            steht und in Wertebereiche aufgeteilt wird
     * @return Bezeichner des ausgewählten Powerups je nach Wertebereich der
     *         Auswahl
     */
    private GameObjectIdentifier _choosePowerupStandardAB(long choice) {
        /* Wahrscheinlichkeit "Radius Erweiterung" */
        if (choice < Globals.GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_BOMB_RADIUS;
        choice -= Globals.GAME_POWERUP_RADIUS_CHANCE_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Extra Bombe" */
        if (choice < Globals.GAME_POWERUP_MORE_BOMBS_CHANCE_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_MORE_BOMBS;
        choice -= Globals.GAME_POWERUP_MORE_BOMBS_CHANCE_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Rüstung" */
        if (choice < Globals.GAME_POWERUP_ARMOR_CHANCE_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_ARMOR;
        choice -= Globals.GAME_POWERUP_ARMOR_CHANCE_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Beschleunigung" */
        if (choice < Globals.GAME_POWERUP_SPEED_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_FASTER_RUN;
        choice -= Globals.GAME_POWERUP_SPEED_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Kick" */
        if (choice < Globals.GAME_POWERUP_BOMB_KICK_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_KICK_BOMBS;
        choice -= Globals.GAME_POWERUP_BOMB_KICK_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Super Bombe" */
        if (choice < Globals.GAME_POWERUP_SUPER_BOMB_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_SUPER_BOMB;
        choice -= Globals.GAME_POWERUP_SUPER_BOMB_10TH_PERCENT_A_B;

        /* Wahrscheinlichkeit "Maximum Radius" */
        if (choice < Globals.GAME_POWERUP_MAX_RADIUS_10TH_PERCENT_A_B)
            return GameObjectIdentifier.POWERUP_BOMB_RADIUS_MAX;

        /* Sonst "Bombenläufer" */
        return GameObjectIdentifier.POWERUP_BOMB_RUNNER;
    }

    /**
     * Führt eine unmittelbare Explosion der Bombe durch.
     *
     * @param arena
     *            Arena, in der die Explosion stattfindet
     */
    public void explodeImmediately(final GameArena arena) {
        /* Setze den Timer auf 0 zurück */
        _bombTicksRemaining = 0;

        /* Tracke diese Bombe weder bei Spieler noch in Arena */
        _plantedByPlayer.decreaseCurrentlyPlacedBombs();
        arena.untrackBomb(this);

        /* Bearbeite zuerst das Epizentrum */
        _explodeSingleCoordinate(arena, _positionX, _positionY);

        /* Male anstatt der Bombe eine Explosion */
        arena.setArenaObject(_positionX, _positionY,
            GameObjectIdentifier.EXPLOSION);

        /*
         * Breite die Explosion in alle vier Richtungen aus.
         */
        _explodeInDirection(arena, 1, 0);
        _explodeInDirection(arena, 0, 1);
        _explodeInDirection(arena, -1, 0);
        _explodeInDirection(arena, 0, -1);
    }

    /**
     * Die Bombe wird durch einen Spieler in eine Richtung getreten. Das X-Delta
     * und das Y-Delta geben die Richtung an. Zulässige Werte sind 0, -1 und +1,
     * wobei genau eines der beiden Deltas 0 sein muss.
     *
     * @param xDelta
     *            X-Delta der Richtung. +1 zeigt nach rechts und -1 zeigt nach
     *            links, bei 0 gilt das Y-Delta.
     * @param yDelta
     *            Y-Delta der Richtung. +1 zeigt nach unten und -1 zeigt nach
     *            oben, bei 0 gilt das X-Delta.
     */
    public void performKick(final int xDelta, final int yDelta) {
        /* Schalte Kickmodus ein */
        if (!_kickInProgress) {
            _kickInProgress = true;
            _kickXDelta = xDelta;
            _kickYDelta = yDelta;
        }
    }
}
