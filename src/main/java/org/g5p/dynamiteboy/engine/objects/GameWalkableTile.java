/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

/**
 * Diese Klasse implementiert ein Objekt, welches einen begehbaren Weg
 * repräsentiert.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameWalkableTile extends GameObject {
    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    @Override
    public boolean isDestroyable() {
        /* Die Explosion kann nur durch freie Felder verlaufen */
        return true;
    }

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    @Override
    public boolean isWalkable() {
        /* Spieler können aneinander vorbei laufen */
        return true;
    }

    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    @Override
    public GameObjectIdentifier getObjectIdentifier() {
        /* Es gibt nur eine ID */
        return GameObjectIdentifier.WALKABLE_TILE;
    }
}