/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

/**
 * Diese Klasse implementiert ein Objekt, welches eine unzerstörbare Wand
 * repräsentiert.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameFixedWall extends GameObject {
    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    @Override
    public boolean isDestroyable() {
        /* Kann nicht zerstört werden */
        return false;
    }

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    @Override
    public boolean isWalkable() {
        /* Kann nicht belaufen werden */
        return false;
    }

    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    @Override
    public GameObjectIdentifier getObjectIdentifier() {
        /* Es gibt nur eine ID */
        return GameObjectIdentifier.FIXED_WALL;
    }
}