/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

/**
 * Diese Klasse implementiert ein Objekt, welches das Powerup "Maximum Radius"
 * (nur im Spielmodus "Standard A+B" zulässig) repräsentiert.
 *
 * @author Thomas Weber
 */
public final class GamePowerupRadiusMax extends GamePowerup {
    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    @Override
    public GameObjectIdentifier getObjectIdentifier() {
        /* Dieses Powerup hat folgende ID */
        return GameObjectIdentifier.POWERUP_BOMB_RADIUS_MAX;
    }
}