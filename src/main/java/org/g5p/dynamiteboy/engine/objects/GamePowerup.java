/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

/**
 * Diese Klasse implementiert ein Objekt, welches ein nicht näher definiertes
 * Powerup repräsentiert. Alle Powerups können durch Bomben zerstört werden und
 * von den Spielern belaufen werden.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public abstract class GamePowerup extends GameObject {
    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    @Override
    public boolean isDestroyable() {
        /* Kann zerstört werden */
        return true;
    }

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    @Override
    public boolean isWalkable() {
        /* Kann belaufen werden */
        return true;
    }
}