/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import java.util.logging.Logger;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameActorStatistics;
import org.g5p.dynamiteboy.engine.GameArena;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * Diese Klasse implementiert ein Objekt, welches einen Spieler auf dem Feld
 * repräsentiert.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameActor extends GameObject {
    private static final Logger _logger = Logger
        .getLogger(GameActor.class.getSimpleName());

    private final GameEngine _owningEngine;
    private int _playerNumber;
    private int _positionX;
    private int _positionY;
    private PlayerAction _currentAction;
    private boolean _hasActionSet;

    private int _armorTicksRemaining;
    private boolean _suicideInitiated;
    private boolean _alive;

    private int _maxPlaceableBombs;
    private int _currentlyPlacedBombs;

    private int _bombExplosionRadius;
    private final int _bombExplosionRadiusMax;

    private boolean _bombsSuper;
    private boolean _walkThroughBombs;
    private boolean _kickBombs;

    private boolean _disableEvenOnlyMovements;
    private boolean _isCurrentTickEven;

    private final GameActorStatistics _statistics;

    /**
     * Fügt dem Spieler eine Rüstung hinzu. Diese wird die für die Runde
     * eingestellte Wirkungsdauer halten.
     */
    private void _applyArmor() {
        /* Berechne die Rüstungsdauer aus der Konfiguration der Engine */
        this._armorTicksRemaining = _owningEngine.getGameConfiguration()
            .getArmorEffectSeconds() * Globals.GAME_TICKS_PER_SECOND;

        /* Wird in Spielerliste übertragen, Update */
        _owningEngine.internalForcePlayerListUpdate();
    }

    /**
     * Erhöht die Anzahl der Bomben, die ein Spieler gleichzeitig legen kann, um
     * 1.
     */
    private void _increaseMaxPlaceableBombs() {
        ++_maxPlaceableBombs;
    }

    /**
     * Erhöht den Radius der Explosion von Bomben des Spielers um 1, aber nur
     * bis zu einem definierten Maximalwert.
     */
    private void _increaseBombExplosionRadius() {
        /* Limitiere Radius auf Maximalwert */
        if (_bombExplosionRadius < _bombExplosionRadiusMax)
            ++_bombExplosionRadius;
    }

    /**
     * Erhöht den Radius der Explosion von Bomben des Spielers auf den
     * Maximalwert (nur im Spielmodus "Standard A+B").
     */
    private void _setMaxBombExplosionRadius() {
        _bombExplosionRadius = _bombExplosionRadiusMax;
    }

    /**
     * Schaltet eine Beschränkung aus, nach der sich der Spieler nur jeden
     * zweiten (geraden) Tick bewegen kann, sodass dieser "schneller" laufen
     * kann (nur im Spielmodus "Standard A+B").
     */
    private void _disableEvenOnlyMovements() {
        _disableEvenOnlyMovements = true;
    }

    /**
     * Aktiviert das Powerup "Super Bombe", welches es Bomben ermöglicht, weiter
     * Schaden anzurichten als bis zur ersten Wand bzw. zum erstem Spieler (nur
     * im Spielmodus "Standard A+B").
     *
     * @author Donald J. Trump, 45th President of the United States
     */
    private void _makeBombsGreatAgain() {
        _bombsSuper = true;
    }

    /**
     * Ermöglicht dem Spieler das Laufen über Bomben (nur im Spielmodus
     * "Standard A+B"). Da dies inkompatibel mit der Möglichkeit, Bomben zu
     * kicken, ist, wird dies deaktiviert.
     */
    private void _enableWalkThroughBombs() {
        /* Ist mit "Tritt" inkompatibel */
        _kickBombs = false;
        _walkThroughBombs = true;
    }

    /**
     * Ermöglicht dem Spieler das Kicken von Bomben (nur im Spielmodus "Standard
     * A+B"). Da dies inkompatibel mit der Möglichkeit, über Bomben zu laufen,
     * ist, wird dies deaktiviert.
     */
    private void _enableBombKicks() {
        /* Ist mit "Bombenläufer" inkompatibel */
        _walkThroughBombs = false;
        _kickBombs = true;
    }

    /**
     * Führt eine Bewegung des Spielers in der Arena durch. Das X-Delta und das
     * Y-Delta geben die Richtung an. Zulässige Werte sind 0, -1 und +1, wobei
     * genau eines der beiden Deltas 0 sein muss.
     *
     * @param arena
     *            Arena, in der sich der Spieler bewegt
     * @param xDelta
     *            X-Delta der Richtung. +1 zeigt nach rechts und -1 zeigt nach
     *            links, bei 0 gilt das Y-Delta.
     * @param yDelta
     *            Y-Delta der Richtung. +1 zeigt nach unten und -1 zeigt nach
     *            oben, bei 0 gilt das X-Delta.
     * @return true wenn die Bewegung verarbeitet (nicht unbedingt
     *         durchgeführt!) wurde, ansonsten false
     */
    private boolean _performMovementAction(final GameArena arena,
        final int xDelta, final int yDelta) {

        /* Bewegungen erst im geraden Tick verarbeiten */
        if (!_disableEvenOnlyMovements && !_isCurrentTickEven)
            return false;

        /* Würden wir die Limits überschreiten? Dann Abbruch */
        if (!arena.areCoordinatesInBounds(_positionX + xDelta,
            _positionY + yDelta))
            return true;

        /* Hole das neue Feld */
        final GameObject objectAtNewPosition = arena
            .getArenaObject(_positionX + xDelta, _positionY + yDelta);
        assert objectAtNewPosition != null;

        /*
         * Kann dieses neue Feld belaufen werden? Wenn nicht, überprüfe, ob eine
         * Bombe liegt; wenn das Powerup "Bombenläufer" aktiviert ist, können
         * wir trotzdem auf die Bombe laufen.
         */
        if (!objectAtNewPosition.isWalkable()) {
            /* Keine Bombe? Geht nicht */
            if (!(objectAtNewPosition instanceof GameBomb))
                return true;
            else {
                /* Kann Bomben kicken? Versuche sie zu kicken */
                if (_kickBombs) {
                    ((GameBomb) objectAtNewPosition).performKick(xDelta,
                        yDelta);
                    return true;
                }

                /* Kann nicht über Bomben laufen? */
                if (!_walkThroughBombs)
                    return true;
            }
        }

        /*
         * Verschiebe uns an die neue Position und notiere den Schritt in der
         * Statistik. Dadurch, dass wir einen Schritt gelaufen sind, ist das
         * Senden eines Spielerupdates notwendig.
         */
        _positionX += xDelta;
        _positionY += yDelta;
        _statistics.stepWalked();
        _owningEngine.internalForcePlayerListUpdate();

        /*
         * War an dieser Stelle ein Powerup? Wenn ja, wende seinen Effekt an und
         * entferne es vom Spielfeld. Notiere das eingesammelte Powerup in der
         * Statistik.
         */
        if (objectAtNewPosition instanceof GamePowerup) {
            applyPowerup((GamePowerup) objectAtNewPosition);
            _statistics.powerupCollected();
            arena.setArenaObject(_positionX, _positionY,
                GameObjectIdentifier.WALKABLE_TILE);
        }
        return true;
    }

    /**
     * Führt eine Platzierung einer Bombe an den Koordinaten durch, auf denen
     * der Spieler momentan steht. Dies geht nur, wenn nicht bereits eine Bombe
     * auf der gleichen Position steht.
     *
     * @param arena
     *            Arena, in welcher die Bombe platziert werden soll
     */
    private void _performBombPlant(final GameArena arena) {
        /* Kann eine weitere Bombe platziert werden? (außer bei Suizid) */
        if (_currentlyPlacedBombs >= _maxPlaceableBombs && !_suicideInitiated)
            return;

        /* Steht auf dieser Position bereits eine Bombe? */
        if (arena.getBombObject(_positionX, _positionY) != null)
            return;

        /* Platziere eine Bombe */
        final GameBomb bomb = new GameBomb(_owningEngine, _positionX,
            _positionY, this);
        arena.setArenaObject(_positionX, _positionY, bomb);
        arena.trackBomb(bomb);
        ++_currentlyPlacedBombs;

        /* Statistik: Bombe platziert */
        _statistics.bombPlanted();
    }

    /**
     * Erzeugt eine neue Instanz eines Spielers.
     *
     * @param owningEngine
     *            Spiele-Engine, zu der der Spieler gehört
     * @param playerNumber
     *            Nummer des Spielers. Diese legt unter anderem die Platzierung
     *            des Spielers zum Spielstart fest, und interne Operationen sind
     *            von der Nummer abhängig.
     */
    public GameActor(final GameEngine owningEngine, final int playerNumber) {
        /* Spielernummer hat Grenzen */
        if (playerNumber < 1 || playerNumber > Globals.ROUND_MAX_PLAYERS) {
            throw new IllegalArgumentException(
                String.format("Spielernummer muss zwischen 1 und %d liegen",
                    Globals.ROUND_MAX_PLAYERS));
        }

        this._owningEngine = owningEngine;
        this._playerNumber = playerNumber;
        this._positionX = -1;
        this._positionY = -1;
        this._currentAction = null;
        this._hasActionSet = false;

        this._armorTicksRemaining = 0;
        this._alive = true;

        /* Es kann maximal eine Bombe platziert werden */
        this._maxPlaceableBombs = 1;
        this._currentlyPlacedBombs = 0;

        /* Der Radius 2 enthält die Bombe selbst als Radius 1 */
        this._bombExplosionRadius = 2;

        /*
         * Bestimme den maximalen Radius der Bombenexplosion aus dem Kleineren
         * von Breite und Höhe
         */
        GameConfiguration gameConfig = owningEngine.getGameConfiguration();
        this._bombExplosionRadiusMax = Math.min(gameConfig.getArenaWidth(),
            gameConfig.getArenaHeight());

        this._bombsSuper = false;
        this._walkThroughBombs = false;
        this._kickBombs = false;

        /* 1. Tick ist ungerade, fange damit an */
        this._disableEvenOnlyMovements = false;
        this._isCurrentTickEven = false;

        this._statistics = new GameActorStatistics();
    }

    /**
     * Gibt zurück, ob dieses Objekt durch eine Bombe zerstört werden kann.
     *
     * @return true, wenn das Objekt durch eine Bombe zerstört werden kann,
     *         ansonsten false
     */
    @Override
    public boolean isDestroyable() {
        /* Spieler können durch Explosion aus dem Spiel gekegelt werden */
        return true;
    }

    /**
     * Gibt zurück, ob auf diesem Objekt andere Objekte stehen dürfen.
     *
     * @return true, wenn auf diesem Objekt andere Objekte stehen dürfen,
     *         ansonsten false
     */
    @Override
    public boolean isWalkable() {
        /* Spieler können aneinander vorbei laufen */
        return true;
    }

    /**
     * Gibt die Bezeichnung des Objektes zurück, aus welcher sich die ID für den
     * Netzwerktransport ableitet.
     *
     * @return Bezeichnung des Objektes
     */
    @Override
    public GameObjectIdentifier getObjectIdentifier() {
        /*
         * Unterscheide nach Spielernummer und ob der Spieler eine Rüstung trägt
         */
        switch (_playerNumber) {
        case 1:
            return _armorTicksRemaining > 0
                ? GameObjectIdentifier.PLAYER_1_ARMORED
                : GameObjectIdentifier.PLAYER_1;
        case 2:
            return _armorTicksRemaining > 0
                ? GameObjectIdentifier.PLAYER_2_ARMORED
                : GameObjectIdentifier.PLAYER_2;
        case 3:
            return _armorTicksRemaining > 0
                ? GameObjectIdentifier.PLAYER_3_ARMORED
                : GameObjectIdentifier.PLAYER_3;
        case 4:
            return _armorTicksRemaining > 0
                ? GameObjectIdentifier.PLAYER_4_ARMORED
                : GameObjectIdentifier.PLAYER_4;
        }
        return null;
    }

    /**
     * Gibt die X-Koordinate der Position des Spielers zurück.
     *
     * @return X-Koordinate der Position des Spielers
     */
    public int getPositionX() {
        return _positionX;
    }

    /**
     * Gibt die Y-Koordinate der Position des Spielers zurück.
     *
     * @return Y-Koordinate der Position des Spielers
     */
    public int getPositionY() {
        return _positionY;
    }

    /**
     * Legt die Position des Spielers fest.
     *
     * @param x
     *            X-Koordinate, an der der Spieler stehen soll
     * @param y
     *            Y-Koordinate, an der der Spieler stehen soll
     */
    public void setPosition(final int x, final int y) {
        /* Koordinaten innerhalb der Arena? */
        if (!_owningEngine.getGameArena().areCoordinatesInBounds(x, y)) {
            throw new IllegalArgumentException(
                String.format("Position {%d,%d} außerhalb der Arena", x, y));
        }

        this._positionX = x;
        this._positionY = y;
    }

    /**
     * Führe mit jedem Tick Operationen auf diesem Spieler durch. Diese
     * Operationen umfassen die Überwachung des Rüstungseffektes sowie das
     * Ausführen von Aktionen der Spieler.
     *
     * @param arena
     *            Arena, in der der Spieler steht
     * @return Stets false, da keine Animationssequenz betrieben wird, die bis
     *         zu ihrem Ende bei Spielende dargestellt werden muss
     */
    @Override
    public synchronized boolean performTasks(final GameArena arena) {
        /* Schalte zwischen ungeradem und geradem Tick um */
        this._isCurrentTickEven ^= true;

        /* Reduziere mit jedem Tick die Haltbarkeit der Rüstung */
        if (_armorTicksRemaining > 0) {
            --_armorTicksRemaining;

            /* Rüstung verliert Wirkung -> Update Spielerliste */
            _owningEngine.internalForcePlayerListUpdate();
        }

        /*
         * Wurde eine Aktion gesetzt? (um nicht mit Selbstzerstörung zu
         * verwechseln)
         */
        if (!_hasActionSet)
            return false;

        /*
         * Aktion null entspricht Selbstzerstörung. Lasse diese wie Bombenlegung
         * aussehen, mit dem Unterschied, dass der Spieler nicht mehr
         * kontrolliert werden kann.
         *
         * Warum null? Dieser kann nur von der Engine initiiert werden und
         * verhindert das Senden von Selbstzerstörungen über das Netzwerk.
         */
        if (_currentAction == null) {
            _suicideInitiated = true;
            _logger.severe(String.format(
                "Spieler %d wird sich selbst zerstören!", _playerNumber));
            _performBombPlant(arena);
        }

        /*
         * Ansonsten behandle alle anderen Aktionen wie gewohnt.
         */
        else {
            switch (_currentAction) {
            case MOVE_DOWN:
                /* Wenn der Schritt nicht ausgeführt wurde, nicht löschen */
                if (!_performMovementAction(arena, 0, 1))
                    return false;
                break;
            case MOVE_LEFT:
                /* Wenn der Schritt nicht ausgeführt wurde, nicht löschen */
                if (!_performMovementAction(arena, -1, 0))
                    return false;
                break;
            case MOVE_RIGHT:
                /* Wenn der Schritt nicht ausgeführt wurde, nicht löschen */
                if (!_performMovementAction(arena, 1, 0))
                    return false;
                break;
            case MOVE_UP:
                /* Wenn der Schritt nicht ausgeführt wurde, nicht löschen */
                if (!_performMovementAction(arena, 0, -1))
                    return false;
                break;
            case PLANT_BOMB:
                _performBombPlant(arena);
                break;
            }
        }

        /* Schließe die Aktion ab */
        _hasActionSet = false;
        _currentAction = null;

        /* Rüstung ist kein Teil einer Animation */
        return false;
    }

    /**
     * Weist den Spieler an, beim nächsten Tick die angegebene Aktion
     * anzuwenden. Dies geht nur, wenn keine Selbstzerstörung eingeleitet wurde
     * und wenn der Spieler noch am Leben ist.
     *
     * @param action
     *            Aktion, die der Spieler ausführen soll, oder null, um eine
     *            Selbstzerstörung einzuleiten
     */
    public synchronized void performAction(final PlayerAction action) {
        /*
         * Nehme keine Aktion mehr an, wenn der Spieler nicht mehr am Leben ist
         * oder gerade ein Selbstmordattentat stattfindet
         */
        if (!_alive || _suicideInitiated)
            return;
        this._currentAction = action;
        this._hasActionSet = true;
    }

    /**
     * Gibt die Nummer des Spielers zurück.
     *
     * @return Nummer des Spielers
     */
    public int getPlayerNumber() {
        return _playerNumber;
    }

    /**
     * Gibt zurück, ob der Spieler eine Rüstung trägt.
     *
     * @return true, wenn der Spieler eine Rüstung trägt, ansonsten false
     */
    public boolean isArmored() {
        return _armorTicksRemaining > 0;
    }

    /**
     * Gibt die Anzahl der Ticks zurück, die die Rüstung des Spielers noch hält.
     *
     * @return 0 wenn keine Rüstung getragen wird bzw. eine vorher getragene
     *         Rüstung abgelaufen ist, ansonsten eine positive Integer-Zahl, die
     *         der Anzahl der verbleibenden Ticks entspricht
     */
    public int getRemainingArmorTicks() {
        return _armorTicksRemaining;
    }

    /**
     * Gibt zurück, ob der Spieler noch am Leben ist.
     *
     * @return true, wenn der Spieler noch am Leben ist, ansonsten false
     */
    public boolean isAlive() {
        return _alive;
    }

    /**
     * Der Spieler wird durch einen anderen Spieler oder durch sich selbst
     * getötet.
     *
     * @param inflictedBy
     *            Spieler, der den eigenen Spieler getötet hat. Ist dieser
     *            identisch mit dem eigenen Spieler, so gilt dies eine
     *            Selbsttötung, ansonsten wird eine Tötung für die Statistiken
     *            des Spielers gezählt.
     */
    public void perish(final GameActor inflictedBy) {
        /* Nur, wenn noch am Leben */
        if (!_alive)
            return;

        this._alive = false;
        _logger
            .severe(String.format("Spieler %d wurde getötet!", _playerNumber));

        /*
         * Lösche eine vorbereitete Aktion, damit diese nicht ausgeführt wird.
         */
        synchronized (this) {
            _hasActionSet = false;
            _currentAction = null;
        }

        /* Statistik: Selbstmord */
        if (inflictedBy == this)
            _statistics.performedSuicide();

        /* Statistik: Anderer Spieler gekillt, der kriegt den Kill */
        else
            inflictedBy.getStatistics().playerKilled();

        /* Spielerliste ändert sich dadurch, dass jemand getötet wurde */
        _owningEngine.internalForcePlayerListUpdate();
    }

    /**
     * Wendet den Effekt eines durch den Spieler aufgesammelten Powerups an.
     *
     * @param powerup
     *            Powerup, welches durch den Spieler aufgesammelt wurde
     */
    public void applyPowerup(final GamePowerup powerup) {
        /* Powerups in allen Spielmodi */
        if (powerup instanceof GamePowerupArmor) {
            _applyArmor();
        } else if (powerup instanceof GamePowerupBombs) {
            _increaseMaxPlaceableBombs();
        } else if (powerup instanceof GamePowerupRadius) {
            _increaseBombExplosionRadius();
        }

        /* Powerups aus Standard A+B */
        else if (powerup instanceof GamePowerupSpeed) {
            _disableEvenOnlyMovements();
        } else if (powerup instanceof GamePowerupRadiusMax) {
            _setMaxBombExplosionRadius();
        } else if (powerup instanceof GamePowerupSuperBomb) {
            _makeBombsGreatAgain();
        } else if (powerup instanceof GamePowerupBombRunner) {
            _enableWalkThroughBombs();
        } else if (powerup instanceof GamePowerupBombKick) {
            _enableBombKicks();
        }
    }

    /**
     * Gibt den Anzahl von Bomben zurück, die ein Spieler momentan gelegt hat.
     *
     * @return Anzahl von Bomben, die ein Spieler momentan gelegt hat
     */
    public int getCurrentlyPlacedBombs() {
        return _currentlyPlacedBombs;
    }

    /**
     * Verringere die Anzahl aktuell gelegter Bomben. Dies wird aufgerufen, wenn
     * eine Bombe des Spielers explodiert ist.
     */
    public void decreaseCurrentlyPlacedBombs() {
        if (_currentlyPlacedBombs > 0)
            --_currentlyPlacedBombs;
    }

    /**
     * Gibt den Anzahl von Bomben zurück, die ein Spieler gleichzeitig legen
     * kann.
     *
     * @return Anzahl von Bomben, die ein Spieler gleichzeitig legen kann
     */
    public int getMaxPlaceableBombs() {
        return _maxPlaceableBombs;
    }

    /**
     * Gibt den Radius von Explosionen durch Bomben des Spielers zurück.
     *
     * @return Radius von Explosionen durch Bomben des Spielers
     */
    public int getBombExplosionRadius() {
        return _bombExplosionRadius;
    }

    /**
     * Gibt zurück, ob der Spieler das Powerup "Super Bombe" eingesammelt hat,
     * somit aktiv ist.
     *
     * @return true, wenn das Powerup "Super Bombe" aktiv ist, ansonsten false
     */
    public boolean areSuperBombsEnabled() {
        return _bombsSuper;
    }

    /**
     * Gibt zurück, ob der Spieler das Powerup "Bombenläufer" eingesammelt hat,
     * somit aktiv ist.
     *
     * @return true, wenn das Powerup "Bombenläufer" aktiv ist, ansonsten false
     */
    public boolean canWalkOnBombs() {
        return _walkThroughBombs;
    }

    /**
     * Gibt die Statistiken des Spielers zurück.
     *
     * @return Statistiken des Spielers
     */
    public GameActorStatistics getStatistics() {
        return _statistics;
    }
}
