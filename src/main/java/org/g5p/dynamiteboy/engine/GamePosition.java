/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

/**
 * Diese Klasse beschreibt eine Position durch seine X- und Y-Koordinate. Java
 * enthält zwar eine Position-Klasse, die gehört aber zu AWT.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public class GamePosition {
    /**
     * X-Koordinate der Position
     */
    public int x;

    /**
     * Y-Koordinate der Position
     */
    public int y;

    /**
     * Setzt die Position auf die X- und Y-Koordinaten -1 und -1 zurück.
     */
    public void reset() {
        this.x = -1;
        this.y = -1;
    }

    /**
     * Initialisiert eine neue Position mit den X- und Y-Koordinaten -1 und -1.
     */
    public GamePosition() {
        reset();
    }

    /**
     * Initialisiert eine neue Position.
     *
     * @param x
     *            X-Koordinate der Position
     * @param y
     *            Y-Koordinate der Position
     */
    public GamePosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
}