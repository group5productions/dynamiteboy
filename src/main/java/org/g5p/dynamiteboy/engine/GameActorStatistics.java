/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

/**
 * Diese Klasse sammelt Statistiken und berechnet den Score für einen einzelnen
 * Spieler. Mehrere Instanzen für jeden Spieler ergeben einen Highscore.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameActorStatistics {
    private String _playerName = null;
    private int _playersKilled = 0;
    private int _wallsDestroyed = 0;
    private int _stepsWalked = 0;
    private int _bombsPlanted = 0;
    private int _collectedPowerups = 0;
    private boolean _suicided = false;
    private boolean _lastPlayer = false;
    private int _sum = -1;
    private boolean _sumCalculated = false;

    /**
     * Trägt einen Spielernamen in die Statistiken des Spielers ein.
     *
     * @param playerName
     *            Name des Spielers
     */
    public void enterPlayerName(String playerName) {
        _playerName = playerName;
    }

    /**
     * Gibt den in den Statistiken hinterlegten Spielernamen zurück.
     *
     * @return Name des Spielers
     */
    public String getPlayerName() {
        return _playerName;
    }

    /**
     * Zählt einen getöteten Spieler.
     */
    public void playerKilled() {
        ++_playersKilled;
    }

    /**
     * Gibt die Anzahl getöteter Spieler zurück.
     *
     * @return Anzahl getöteter Spieler
     */
    public int getKilledPlayers() {
        return _playersKilled;
    }

    /**
     * Zählt eine zerstörte Wand.
     */
    public void wallDestroyed() {
        ++_wallsDestroyed;
    }

    /**
     * Gibt die Anzahl zerstörter Wände zurück.
     *
     * @return Anzahl zerstörter Wände
     */
    public int getDestroyedWalls() {
        return _wallsDestroyed;
    }

    /**
     * Zählt einen gelaufenen Schritt.
     */
    public void stepWalked() {
        ++_stepsWalked;
    }

    /**
     * Gibt die Anzahl gelaufener Schritte zurück.
     *
     * @return Anzahl gelaufener Schritte
     */
    public int getWalkedSteps() {
        return _stepsWalked;
    }

    /**
     * Zählt eine gelegte Bombe.
     */
    public void bombPlanted() {
        ++_bombsPlanted;
    }

    /**
     * Gibt die Anzahl gelegter Bomben zurück.
     *
     * @return Anzahl gelegter Bomben
     */
    public int getPlantedBombs() {
        return _bombsPlanted;
    }

    /**
     * Zählt ein eingesammeltes Powerup.
     */
    public void powerupCollected() {
        ++_collectedPowerups;
    }

    /**
     * Gibt die Anzahl eingesammelter Powerups zurück.
     *
     * @return Anzahl eingesammelter Powerups
     */
    public int getCollectedPowerups() {
        return _collectedPowerups;
    }

    /**
     * Legt fest, dass der Spieler selbst umgekommen ist.
     */
    public void performedSuicide() {
        _suicided = true;
    }

    /**
     * Gibt zurück, ob der Spieler selbst umgekommen ist.
     *
     * @return true wenn der Spieler selbst umgekommen ist, ansonsten false
     */
    public boolean isSuicided() {
        return _suicided;
    }

    /**
     * Legt fest, dass der Spieler der letzte in der Arena ist.
     */
    public void markAsLastPlayer() {
        _lastPlayer = true;
    }

    /**
     * Gibt zurück, ob der Spieler der letzte in der Arena ist.
     *
     * @return true wenn der letzte in der Arena ist, ansonsten false
     */
    public boolean isLastPlayer() {
        return _lastPlayer;
    }

    /**
     * Berechnet den Score für diesen Spieler und macht ihn manipulationsfest.
     *
     * @return Score des Spielers
     */
    public int calculateScore() {
        /* Verhindere Manipulationen nach der Erstberechnung */
        if (_sumCalculated)
            return _sum;

        /* Berechne den Punktestand und speichere ihn */
        _sum = 20 * _playersKilled + _wallsDestroyed;
        if (_suicided)
            _sum -= 20;
        if (_lastPlayer)
            _sum += 20;

        _sumCalculated = true;
        return _sum;
    }
}