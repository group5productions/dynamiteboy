/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

import java.util.List;
import java.util.Arrays;
import java.util.LinkedList;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.objects.*;
import org.g5p.dynamiteboy.tools.GameModeFieldEntry;

/**
 * Diese Klasse speichert die durch die Spiele-Engine verwaltete Arena.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameArena {
    private static final GameBreakableWall STATIC_BREAKABLE_WALL = new GameBreakableWall();
    private static final GameFixedWall STATIC_FIXED_WALL = new GameFixedWall();
    private static final GameWalkableTile STATIC_WALKABLE_TILE = new GameWalkableTile();
    private static final GameExplosion STATIC_EXPLOSION = new GameExplosion();
    private static final GamePowerupRadius STATIC_POWERUP_RADIUS = new GamePowerupRadius();
    private static final GamePowerupBombs STATIC_POWERUP_BOMBS = new GamePowerupBombs();
    private static final GamePowerupArmor STATIC_POWERUP_ARMOR = new GamePowerupArmor();
    private static final GamePowerupSpeed STATIC_POWERUP_SPEED = new GamePowerupSpeed();
    private static final GamePowerupRadiusMax STATIC_POWERUP_RADIUS_MAX = new GamePowerupRadiusMax();
    private static final GamePowerupSuperBomb STATIC_POWERUP_SUPER_BOMB = new GamePowerupSuperBomb();
    private static final GamePowerupBombRunner STATIC_POWERUP_BOMB_RUNNER = new GamePowerupBombRunner();
    private static final GamePowerupBombKick STATIC_POWERUP_KICK_BOMBS = new GamePowerupBombKick();

    private final GameEngine _gameEngine;

    private final int _arenaWidth;
    private final int _arenaHeight;

    private boolean _scanBombExplosion;
    private int _arenaExplosionClearTick;
    private LinkedList<GameBomb> _arenaBombs;
    private GameObject _arena[];

    private GameObjectIdentifier _previousArenaIdentifiers[];
    private GamePosition _previousPlayerPositions[];
    private GameObjectIdentifier _previousPlayerIdentifiers[];
    private boolean _updateSentCoordinates[];

    private LinkedList<GameModeFieldEntry> _cachedDeltaUpdate;

    /**
     * Diese Methode übersetzt zweidimensionale Koordinaten in einen
     * eindimensionalen Index.
     *
     * @param x
     *            x-Koordinate in der Arena
     * @param y
     *            y-Koordinate in der Arena
     * @return eindimensionaler Index, der den Koordinaten entspricht
     */
    private int _posToIndex(final int x, final int y) {
        return _arenaWidth * y + x;
    }

    /**
     * Erstellt einen Snapshot des Zustandes der Arena sowie Daten der Spieler,
     * um auf der erzeugten Datenbasis später ein Delta-Update erstellen zu
     * können.
     */
    private void _applyArenaChanges() {
        /*
         * Speichere bloß den aktuellen Objektbezeichner für jedes Objekt in der
         * aktuellen Arena
         */
        for (int y = 0; y < _arenaHeight; ++y) {
            for (int x = 0; x < _arenaWidth; ++x) {
                final int position = _posToIndex(x, y);
                _previousArenaIdentifiers[position] = _arena[position]
                    .getObjectIdentifier();
            }
        }

        /*
         * Gehe die Spieler durch und notiere ihre Position und den Bezeichner,
         * den sie zuletzt hatten. Existiert ein Spieler nicht bzw. nicht mehr,
         * so wird seine Position auf {-1,1} und sein Bezeichner auf NULL
         * gesetzt.
         */
        int i = 0;
        for (final GameActor player : _gameEngine.getPlayerActors()) {
            if (player == null || !player.isAlive()) {
                _previousPlayerPositions[i].reset();
                _previousPlayerIdentifiers[i] = GameObjectIdentifier.NULL;
            } else {
                _previousPlayerPositions[i].x = player.getPositionX();
                _previousPlayerPositions[i].y = player.getPositionY();
                _previousPlayerIdentifiers[i] = player.getObjectIdentifier();
            }
            ++i;
        }
    }

    /**
     * Ersetze Explosionen auf dem Spielfeld durch begehbare Freifläche. (Damit
     * löscht die Explosionen liegen gelassene, im Explosionsbereich befindliche
     * Powerups aus.)
     */
    private void _clearBombExplosions() {
        /* Ersetze Explosionen durch Freifläche */
        for (int i = 0; i < _arena.length; ++i) {
            if (_arena[i]
                .getObjectIdentifier() == GameObjectIdentifier.EXPLOSION)
                _arena[i] = STATIC_WALKABLE_TILE;
        }
    }

    /**
     * Generiert einen einzelnen Eintrag für ein Delta-Update an den angegebenen
     * Koordinaten. Markiere Koordinaten, für die hier Updates generiert wurden,
     * damit im weiteren Verlauf keine weiteren Updates für die gleiche
     * Koordinate erzeugt werden, wenn aufgrund von Abhängigkeiten mehr als ein
     * Update (für eine weitere Koordinate) generiert werden muss.
     *
     * @param updates
     *            Liste, die bereits gesammelte Updates umfasst und zu der ein
     *            weiteres Update hier erzeugt werden soll
     * @param x
     *            X-Koordinate, für die ein Update erzeugt werden soll
     * @param y
     *            Y-Koordinate, für die ein Update erzeugt werden soll
     */
    private void _generateDeltaUpdateForCoordinates(
        final LinkedList<GameModeFieldEntry> updates, final int x,
        final int y) {

        /* Hole Objekt an aktueller Position */
        final int current1DPosition = _posToIndex(x, y);
        final GameObject objectNew = _arena[current1DPosition];
        final boolean isBombOnNewPosition = objectNew instanceof GameBomb;
        final boolean wasBombOnNewPosition = isBombOnNewPosition
            && GameObjectIdentifier.isIdentifierABomb(
                _previousArenaIdentifiers[current1DPosition]);

        /* Hole irgendeinen Spieler an aktueller Position */
        final GameActor playerNew = getActorObject(x, y);
        final boolean canPlayerNewWalkOnBomb = playerNew != null
            ? playerNew.canWalkOnBombs() : false;

        /* Generiere vorzeitig Updates zum Bedarf */
        final GameModeFieldEntry updateForObjectNew = new GameModeFieldEntry(x,
            y, objectNew.getObjectIdentifier().getValue());

        /*
         * An dieser Stelle befindet sich ein Spieler. Überprüfe dessen Position
         * und füge bei Änderung ein Update für seine neue *und* seine *alte*
         * Koordinate als Updates hinzu.
         */
        if (playerNew != null) {
            GamePosition oldPlayerPosition = _previousPlayerPositions[playerNew
                .getPlayerNumber() - 1];
            GameObjectIdentifier oldPlayerIdentifier = _previousPlayerIdentifiers[playerNew
                .getPlayerNumber() - 1];

            /*
             * Koordinaten oder Objekt-ID haben sich nicht geändert. Wenn wir
             * eine Bombe haben, wo vorher keine war, sende trotzdem ein Update,
             * wenn diese über dem Spieler liegt.
             */
            if (oldPlayerIdentifier == playerNew.getObjectIdentifier()
                && oldPlayerPosition.x == x && oldPlayerPosition.y == y) {
                if (isBombOnNewPosition && !wasBombOnNewPosition) {
                    if (isBombOnNewPosition && !canPlayerNewWalkOnBomb) {
                        updates.add(updateForObjectNew);
                        _updateSentCoordinates[current1DPosition] = true;
                    }
                }
                return;
            }

            /*
             * Update für neue Koordinate (= aktuelle Koordinate, immer gültig).
             * Wenn eine Bombe unter dem Spieler liegt, dann zeichne sie über
             * dem Spieler, außer der Spieler kann über sie laufen.
             */
            final GameModeFieldEntry updateForPlayerNew = new GameModeFieldEntry(
                x, y, playerNew.getObjectIdentifier().getValue());

            if (isBombOnNewPosition && !canPlayerNewWalkOnBomb)
                updates.add(updateForObjectNew);
            else
                updates.add(updateForPlayerNew);
            _updateSentCoordinates[current1DPosition] = true;

            /*
             * Update für alte Koordinate, sofern gültig. Frage wieder erst
             * Spieler, dann das Feld ab.
             */
            if (oldPlayerPosition.x > -1 && oldPlayerPosition.y > -1) {

                /* Hole Objekt an alter Position */
                final int previous1DPosition = _posToIndex(oldPlayerPosition.x,
                    oldPlayerPosition.y);
                final GameObject objectOld = _arena[previous1DPosition];
                final boolean isBombOnOldPosition = objectOld instanceof GameBomb;

                /* Hole Spieler an alter Position */
                final GameActor playerOld = getActorObject(oldPlayerPosition.x,
                    oldPlayerPosition.y);
                final boolean canPlayerOldWalkOnBomb = playerOld != null
                    ? playerOld.canWalkOnBombs() : false;

                /* Generiere vorzeitig Updates zum Bedarf */
                final GameModeFieldEntry updateForObjectOld = new GameModeFieldEntry(
                    oldPlayerPosition.x, oldPlayerPosition.y,
                    objectOld.getObjectIdentifier().getValue());

                /*
                 * Vorher stand ein Spieler. Wenn eine Bombe unter dem Spieler
                 * liegt, dann zeichne sie über dem Spieler, außer der Spieler
                 * kann über sie laufen.
                 */
                if (playerOld != null) {
                    final GameModeFieldEntry updateForPlayerOld = new GameModeFieldEntry(
                        oldPlayerPosition.x, oldPlayerPosition.y,
                        playerOld.getObjectIdentifier().getValue());

                    if (isBombOnOldPosition && !canPlayerOldWalkOnBomb)
                        updates.add(updateForObjectOld);
                    else
                        updates.add(updateForPlayerOld);
                }

                /*
                 * Vorher stand kein Spieler, setze Update für das Objekt.
                 */
                else
                    updates.add(updateForObjectOld);
                _updateSentCoordinates[previous1DPosition] = true;
            }
        }

        /*
         * An dieser Stelle befindet sich kein Spieler. Überprüfe, ob sich die
         * Objekte bzw. Objektbezeichner geändert haben, und füge in diesem Fall
         * ein neues Update hinzu.
         */
        else {
            final GameObjectIdentifier objectIdentPrev = _previousArenaIdentifiers[current1DPosition];

            /* Suche nach früherem Spieler auf diesen Koordinaten */
            boolean playerWasHere = false;
            for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i) {
                if (_previousPlayerPositions[i].x == x
                    && _previousPlayerPositions[i].y == y) {
                    playerWasHere = true;
                    break;
                }
            }

            /*
             * Füge Update hinzu, wenn vorher kein Objekt da war, wenn es sich
             * geändert hat, sich der Objektbezeichner geändert hat, oder wenn
             * vorher ein Spieler stand.
             */
            if (playerWasHere || objectIdentPrev == null
                || objectIdentPrev == GameObjectIdentifier.NULL
                || objectIdentPrev != objectNew.getObjectIdentifier()) {
                updates.add(updateForObjectNew);
                _updateSentCoordinates[current1DPosition] = true;
            }
        }
    }

    /**
     * Generiert ein Delta-Update und speichert es zwischen.
     *
     * @return Liste von aktualisierten Feldern der Arena
     */
    private LinkedList<GameModeFieldEntry> _generateDeltaUpdate() {
        final LinkedList<GameModeFieldEntry> updates = new LinkedList<>();

        /* Setze Status für alle Koordinaten zurück */
        Arrays.fill(this._updateSentCoordinates, 0, _arenaWidth * _arenaHeight,
            false);

        /*
         * Iteriere die komplette Arena, um Unterschiede zu finden. Ignoriere
         * Koordinaten, für die bereits ein Update durchgeführt wurde.
         */
        for (int y = 0; y < _arenaHeight; ++y) {
            for (int x = 0; x < _arenaWidth; ++x) {
                if (_updateSentCoordinates[_posToIndex(x, y)])
                    continue;
                _generateDeltaUpdateForCoordinates(updates, x, y);
            }
        }

        /*
         * Erzeuge einen neuen Snapshot mit dem aktuellen Zustand der Arena bzw.
         * Spieler.
         */
        _applyArenaChanges();
        return updates;
    }

    /**
     * Erzeugt eine neue Arena mit den angegebenen Größen. Die Arena wird
     * außerdem an eine Instanz der Spiele-Engine gebunden, um Informationen von
     * dieser abrufen zu können.
     *
     * @param arenaWidth
     *            Breite der Arena (7-21), nur ungerade Zahlen
     * @param arenaHeight
     *            Höhe der Arena (7-21), nur ungerade Zahlen
     * @param gameEngine
     *            Instanz der Spiele-Engine, an die die Arena gebunden wird
     */
    public GameArena(final int arenaWidth, final int arenaHeight,
        final GameEngine gameEngine) {
        /* Breite zwischen 7 und 21 sowie ungerade */
        if (arenaWidth < 7 || arenaWidth > 21 || (arenaWidth & 1) == 0) {
            throw new IllegalArgumentException(
                "Breite muss zwischen 7 und 21 liegen und ungerade sein");
        }

        /* Höhe zwischen 7 und 21 sowie ungerade */
        if (arenaHeight < 7 || arenaHeight > 21 || (arenaHeight & 1) == 0) {
            throw new IllegalArgumentException(
                "Höhe muss zwischen 7 und 21 liegen und ungerade sein");
        }

        this._gameEngine = gameEngine;

        this._arenaWidth = arenaWidth;
        this._arenaHeight = arenaHeight;

        this._arenaExplosionClearTick = 0;
        this._arenaBombs = new LinkedList<>();

        /*
         * Erzeuge die Arena grundsätzlich als eindimensionales Array. Warum? Um
         * die Fragmentierung des zugehörigen Speichers gering zu halten und
         * ständige Wechsel zwischen den Objekten für jede Zeile zu vermeiden.
         */
        this._arena = new GameObject[arenaWidth * arenaHeight];

        /*
         * Erzeuge analog zu oben ein Array, um vorige Bezeichner der Objekte in
         * der Arena zu speichern. Initialisiere diese mit NULL. Wird für das
         * Delta-Update benötigt.
         */
        this._previousArenaIdentifiers = new GameObjectIdentifier[arenaWidth
            * arenaHeight];
        Arrays.fill(_previousArenaIdentifiers, 0, arenaWidth * arenaHeight,
            GameObjectIdentifier.NULL);

        /*
         * Erzeuge ein Array, mit welchem wir Koordinaten von Spielern
         * speichern, um Änderungen für das Delta-Update feststellen zu können.
         */
        this._previousPlayerPositions = new GamePosition[Globals.ROUND_MAX_PLAYERS];
        for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i)
            this._previousPlayerPositions[i] = new GamePosition();

        /*
         * Erzeuge ein Array, um Updates für Spieler zu senden, wenn sich bloß
         * ihr Zustand ändert, aber die Position bleibt.
         */
        this._previousPlayerIdentifiers = new GameObjectIdentifier[Globals.ROUND_MAX_PLAYERS];
        Arrays.fill(_previousPlayerIdentifiers, 0, Globals.ROUND_MAX_PLAYERS,
            GameObjectIdentifier.NULL);

        /*
         * Erzeuge ein Array, mit welchem wir doppelte Updates für eine
         * Koordinate unterbinden können.
         */
        this._updateSentCoordinates = new boolean[arenaWidth * arenaHeight];
    }

    /**
     * Gibt die Breite der Arena zurück.
     *
     * @return Breite der Arena
     */
    public int getArenaWidth() {
        return _arenaWidth;
    }

    /**
     * Gibt die Höhe der Arena zurück.
     *
     * @return Höhe der Arena
     */
    public int getArenaHeight() {
        return _arenaHeight;
    }

    /**
     * Überprüft, ob sich die angegebenen Koordinaten innerhalb der Arena
     * befinden.
     *
     * @param x
     *            X-Koordinate
     * @param y
     *            Y-Koordinate
     * @return true, wenn sich die Koordinaten innerhalb der Arena befinden,
     *         ansonsten false
     */
    public boolean areCoordinatesInBounds(final int x, final int y) {
        return (x >= 0 && y >= 0 && x < _arenaWidth && y < _arenaHeight);
    }

    /**
     * Gibt einen (noch lebenden) Spieler zurück, der sich an den angegebenen
     * Koordinaten befindet.
     *
     * @param x
     *            X-Koordinate des Spielers
     * @param y
     *            Y-Koordinate des Spielers
     * @return Objekt des (noch lebenden) Spielers, wenn sich ein Spieler an den
     *         angegebenen Koordinaten befindet und diese innerhalb der Arena
     *         liegen, ansonsten null
     */
    public GameActor getActorObject(final int x, final int y) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return null;

        for (final GameActor player : _gameEngine.getPlayerActors()) {
            /*
             * Enthält aktueller Slot einen Spieler? Ist der aktuelle Spieler
             * einer, der ignoriert wird? Lebt er noch? Befindet er sich an den
             * Koordinaten {x,y}? Dann haben wir ihn.
             */
            if (player != null && player.isAlive()) {
                if (player.getPositionX() == x && player.getPositionY() == y)
                    return player;
            }
        }
        return null;
    }

    /**
     * Gibt alle (noch lebenden) Spieler zurück, die sich an den gleichen
     * angegebenen Koordinaten befinden.
     *
     * @param x
     *            X-Koordinate der Spieler
     * @param y
     *            Y-Koordinate der Spieler
     * @return Liste von Objekten (noch lebender) Spieler, die sich an den
     *         gleichen Koordinaten befinden, welche wiederum in der Arena
     *         liegt, ansonsten null
     */
    public List<GameActor> getActorObjects(final int x, final int y) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return null;

        final LinkedList<GameActor> playersOnCoord = new LinkedList<>();
        for (final GameActor player : _gameEngine.getPlayerActors()) {
            /*
             * Enthält aktueller Slot einen Spieler? Ist der aktuelle Spieler
             * einer, der ignoriert wird? Lebt er noch? Befindet er sich an den
             * Koordinaten {x,y}? Dann haben wir ihn.
             */
            if (player != null && player.isAlive()) {
                if (player.getPositionX() == x && player.getPositionY() == y)
                    playersOnCoord.add(player);
            }
        }
        return playersOnCoord.size() > 0 ? playersOnCoord : null;
    }

    /**
     * Gibt eine durch die Arena überwachte Bombe zurück, die sich an an den
     * angegebenen Koordinaten befindet.
     *
     * @param x
     *            X-Koordinate der Bombe
     * @param y
     *            Y-Koordinate der Bombe
     * @return Objekt der Bombe, wenn sich eine Bombe an den angegebenen
     *         Koordinaten befindet und diese innerhalb der Arena liegen,
     *         ansonsten null
     */
    public GameBomb getBombObject(final int x, final int y) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return null;

        /* Iteriere alle Bomben */
        for (final GameBomb bomb : _arenaBombs) {
            if (bomb.getPositionX() == x && bomb.getPositionY() == y)
                return bomb;
        }
        return null;
    }

    /**
     * Gibt ein Objekt in der Arena zurück, welches sich an den angegebenen
     * Koordinaten befindet.
     *
     * @param x
     *            X-Koordinate des Objekts
     * @param y
     *            Y-Koordinate des Objekts
     * @return Objekt, wenn sich ein Objekt an den angegebenen Koordinaten
     *         befindet und diese innerhalb der Arena liegen, ansonsten null
     */
    public GameObject getArenaObject(final int x, final int y) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return null;

        /* Lese aus dem Array */
        return _arena[_posToIndex(x, y)];
    }

    /**
     * Platziert eine Instanz eines Objektes an den angegebenen Koordinaten in
     * der Arena.
     *
     * @param x
     *            X-Koordinate, an der das Objekt platziert werden soll
     * @param y
     *            Y-Koordinate, an der das Objekt platziert werden soll
     * @param objekt
     *            Objektinstanz, welche in die Arena platziert werden soll
     */
    public void setArenaObject(final int x, final int y,
        final GameObject object) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return;

        /* Schreibe in das Array */
        _arena[_posToIndex(x, y)] = object;
    }

    /**
     * Platziert eine Objekt anhand seines Bezeichners an den angegebenen
     * Koordinaten in der Arena. Dies funktioniert nur mit ausgewählten
     * trivialen Objekten mit maximal einem möglichen Bezeichner: freier Weg,
     * Wände, Explosion, Powerups.
     *
     * @param x
     *            X-Koordinate, an der das Objekt platziert werden soll
     * @param y
     *            Y-Koordinate, an der das Objekt platziert werden soll
     * @param objectIdentifier
     *            Bezeichner des Objekts, welche in die Arena platziert werden
     *            soll
     * @throws IllegalArgumentException
     *             wenn ein nicht unterstütztes Objekt platziert werden soll
     *             (weil es dann kein triviales Objekt ist)
     */
    public void setArenaObject(final int x, final int y,
        final GameObjectIdentifier objectIdentifier) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return;

        /*
         * Wähle anhand des übergebenen Bezeichners eine statische Instanz des
         * zugehörigen Objektes aus.
         */
        GameObject objectToDraw;
        switch (objectIdentifier) {
        case WALKABLE_TILE:
            objectToDraw = STATIC_WALKABLE_TILE;
            break;
        case EXPLOSION:
            objectToDraw = STATIC_EXPLOSION;
            break;
        case POWERUP_BOMB_RADIUS:
            objectToDraw = STATIC_POWERUP_RADIUS;
            break;
        case POWERUP_MORE_BOMBS:
            objectToDraw = STATIC_POWERUP_BOMBS;
            break;
        case POWERUP_ARMOR:
            objectToDraw = STATIC_POWERUP_ARMOR;
            break;
        case POWERUP_FASTER_RUN:
            objectToDraw = STATIC_POWERUP_SPEED;
            break;
        case POWERUP_BOMB_RADIUS_MAX:
            objectToDraw = STATIC_POWERUP_RADIUS_MAX;
            break;
        case POWERUP_SUPER_BOMB:
            objectToDraw = STATIC_POWERUP_SUPER_BOMB;
            break;
        case POWERUP_BOMB_RUNNER:
            objectToDraw = STATIC_POWERUP_BOMB_RUNNER;
            break;
        case POWERUP_KICK_BOMBS:
            objectToDraw = STATIC_POWERUP_KICK_BOMBS;
            break;
        default:
            throw new IllegalArgumentException(
                "angegebenes Objekt kann nicht gezeichnet werden");
        }

        /* Schreibe in das Array */
        _arena[_posToIndex(x, y)] = objectToDraw;
    }

    /**
     * Erzeugt das Spielfeld. Es umfasst einen festen Rahmen um die äußersten X-
     * und Y-Koordinaten. Weiterhin befinden sich auf ungeraden Zeilen
     * zerstörbare Wände, auf geraden Zeilen wechseln sich feste und zerstörbare
     * Wände gegenseitig ab. An den vier Ecken des Spielfeldes (stets auf einer
     * ungeraden Zeile und auf einer zerstörbaren) werden Spieler platziert. Ist
     * für eine Ecke ein Spieler definiert, wird das Feld, auf dem der Spieler
     * steht, sowie ein Feld in belaufbare Richtungen aufgemacht, um eine
     * unvermeidbare Selbsttötung zu verhindern.
     *
     * @param gameActors
     *            Liste von Objekten, die die Spieler repräsentieren. Diese
     *            Spieler werden auf das Feld platziert.
     */
    public void generateField(final GameActor[] gameActors) {
        /* Iteriere jede Zeile */
        for (int y = 0; y < _arenaHeight; ++y) {

            /* Erste und letzte Zeile: nur feste Wände */
            if (y == 0 || y == _arenaHeight - 1) {
                Arrays.fill(_arena, _posToIndex(0, y),
                    _posToIndex(_arenaWidth, y), STATIC_FIXED_WALL);
            }

            /*
             * Ungerade Zeile: links und rechts feste Wand, ansonsten sprengbare
             * Mauer
             */
            else if (y % 2 == 1) {
                _arena[_posToIndex(0, y)] = STATIC_FIXED_WALL;
                Arrays.fill(_arena, _posToIndex(1, y),
                    _posToIndex(_arenaWidth - 1, y), STATIC_BREAKABLE_WALL);
                _arena[_posToIndex(_arenaWidth - 1, y)] = STATIC_FIXED_WALL;
            }

            /* Gerade Zeile: Abwechselnd sprengbare Mauer und feste Wand */
            else {
                for (int x = 0; x < _arenaWidth; ++x) {
                    _arena[_posToIndex(x, y)] = (x % 2 == 0) ? STATIC_FIXED_WALL
                        : STATIC_BREAKABLE_WALL;
                }
            }
        }

        /* Platziere die Spieler und mache Platz frei */
        for (final GameActor player : gameActors) {
            if (player == null)
                continue;

            int x = 0, y = 0, xDelta = 0, yDelta = 0;
            switch (player.getPlayerNumber()) {
            /* Spieler 1 ist oben links */
            case 1:
                x = 1;
                xDelta = 1;
                y = 1;
                yDelta = 1;
                break;

            /* Spieler 2 ist oben rechts */
            case 2:
                x = _arenaWidth - 2;
                xDelta = -1;
                y = 1;
                yDelta = 1;
                break;

            /* Spieler 3 ist unten links */
            case 3:
                x = 1;
                xDelta = 1;
                y = _arenaHeight - 2;
                yDelta = -1;
                break;

            /* Spieler 4 ist unten rechts */
            case 4:
                x = _arenaWidth - 2;
                xDelta = -1;
                y = _arenaHeight - 2;
                yDelta = -1;
                break;
            }

            /* Platziere den Spieler */
            player.setPosition(x, y);

            /* Mache die Arena für den Spieler frei */
            _arena[_posToIndex(x, y)] = STATIC_WALKABLE_TILE;
            _arena[_posToIndex(x + xDelta, y)] = STATIC_WALKABLE_TILE;
            _arena[_posToIndex(x, y + yDelta)] = STATIC_WALKABLE_TILE;
        }
    }

    /**
     * Führt Aufgaben durch, die nur über alle Bomben hinweg durchgeführt werden
     * können. Explosionen werden nach kurzer Zeit aufgelöst, Einzeloperationen
     * auf jeder Bombe durchgeführt und Kettenexplosionen umgesetzt.
     *
     * @return true, wenn eine Animationssequenz von mindestens einer Bombe noch
     *         am Laufen ist, ansonsten false
     */
    public boolean performBombTasks() {
        boolean animationPending = false;

        /* Explosionen auflösen? */
        if (_arenaExplosionClearTick > 0) {
            animationPending = true;
            --_arenaExplosionClearTick;
            if (_arenaExplosionClearTick == 0)
                _clearBombExplosions();
        }

        /*
         * Speichere die aktuelle Bombenliste und iteriere auf dieser. Während
         * des Durchlaufs kann sich die Liste getrackter Bomben ändern.
         */
        LinkedList<GameBomb> arenaBombsCopy = new LinkedList<GameBomb>(
            _arenaBombs);
        for (final GameBomb bomb : arenaBombsCopy) {
            if (bomb.performTasks(this))
                animationPending = true;
        }

        /*
         * Auch nach dem Durchlauf sind wir durch in-iteration-Modifikationen
         * durch Kettenexplosionen nicht geschützt. Aber wollen trotzdem bereits
         * explodierten Bomben tracken. Also neue Kopie der Bombenliste ziehen.
         */
        arenaBombsCopy = new LinkedList<GameBomb>(_arenaBombs);

        /*
         * Überprüfe, ob sich weitere, noch nicht explodierte, Bomben im
         * Explosionsbereich befinden.
         */
        if (_scanBombExplosion) {
            _scanBombExplosion = false;

            for (int y = 0; y < _arenaHeight; ++y) {
                for (int x = 0; x < _arenaWidth; ++x) {
                    /* Ist an dieser Stelle eine Explosion? */
                    if (_arena[_posToIndex(x, y)]
                        .getObjectIdentifier() != GameObjectIdentifier.EXPLOSION) {
                        continue;
                    }

                    /* Detoniere alle Bomben an dieser Stelle */
                    for (final GameBomb bomb : arenaBombsCopy) {
                        if (bomb.getPositionX() == x
                            && bomb.getPositionY() == y)
                            bomb.explodeImmediately(this);
                    }
                }
            }
        }
        return animationPending;
    }

    /**
     * Überwacht eine frisch platzierte Bombe.
     *
     * @param bomb
     *            Neue Bombe, die überwacht werden soll
     */
    public void trackBomb(final GameBomb bomb) {
        _arenaBombs.add(bomb);
    }

    /**
     * Entferne eine frisch explodierte Bombe, und fordere die Überprüfung von
     * Kettenexplosionen an.
     *
     * @param bomb
     *            Explodierte Bombe, die nicht mehr überwacht werden soll
     */
    public void untrackBomb(final GameBomb bomb) {
        _arenaBombs.remove(bomb);

        /*
         * Löse die Explosion nach 3 Ticks auf und suche nach Kettenreaktionen
         */
        _arenaExplosionClearTick = 3;
        _scanBombExplosion = true;
    }

    /**
     * Fragt ab, ob ein Delta-Update vorliegt, indem es dieses generiert und
     * zwischenspeichert. Wenn eines vorliegt, muss es sofort abgerufen werden.
     *
     * @return true, wenn ein Delta-Update vorliegt, ansonsten false
     */
    public boolean isDeltaUpdateAvailable() {
        /* Kein Delta-Update? Generiere es */
        if (_cachedDeltaUpdate == null)
            _cachedDeltaUpdate = _generateDeltaUpdate();

        /* Prüfe anhand Anzahl der Einträge */
        if (!_cachedDeltaUpdate.isEmpty())
            return true;
        else {
            _cachedDeltaUpdate = null;
            return false;
        }
    }

    /**
     * Fragt nach einem zwischengespeicherten Delta-Update ab und gibt es
     * zurück; ansonsten generiere es.
     *
     * @return Liste von aktualisierten Feldern der Arena
     */
    public List<GameModeFieldEntry> queryDeltaUpdate() {
        if (_cachedDeltaUpdate != null) {
            /* Gebe zwischengespeichertes Update zurück, wenn vorhanden */
            List<GameModeFieldEntry> update = _cachedDeltaUpdate;
            _cachedDeltaUpdate = null;
            return update;
        } else {
            /* Direkt generieren */
            return _generateDeltaUpdate();
        }
    }
}
