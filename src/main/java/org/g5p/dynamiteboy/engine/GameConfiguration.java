/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.tools.GameplayMode;

/**
 * Diese Klasse speichert die Konfiguration einer Spielrunde für die Engine des
 * Spiels. Sie kann vom Spielleiter konfiguriert werden.
 *
 * @author Thomas Weber
 */
public final class GameConfiguration {
    private final int arenaWidth;
    private final int arenaHeight;
    private final int playTimeMinutes;
    private final int armorEffectSeconds;
    private final int bombExplosionSeconds;
    private final GameplayMode gameplayMode;

    /**
     * Erzeugt eine neue Spiel(arena)konfiguration durch Anwendung von vielfach
     * erprobten Standardeinstellungen.
     */
    public GameConfiguration() {
        this.arenaWidth = Globals.GAME_ARENA_WIDTH_DEFAULT;
        this.arenaHeight = Globals.GAME_ARENA_HEIGHT_DEFAULT;
        this.playTimeMinutes = Globals.GAME_ROUND_DURATION_DEFAULT_MINUTES;
        this.armorEffectSeconds = Globals.GAME_ARMOR_EFFECT_DURATION_SECONDS;
        this.bombExplosionSeconds = Globals.GAME_BOMB_COUNTDOWN_SECONDS;
        this.gameplayMode = GameplayMode.STANDARD_A_B;
    }

    /**
     * Erzeugt eine neue Spiel(arena)konfiguration durch Kopieren der
     * Informationen aus einem Kommando, welches die Spielarena konfiguriert.
     *
     * @param command
     *            Kommando, welches Einstellungen der Arena enthält
     */
    public GameConfiguration(ConfigureArenaGameServerCommand command) {
        command.performArgumentCheck();
        this.arenaWidth = command.getArenaWidth();
        this.arenaHeight = command.getArenaHeight();
        this.playTimeMinutes = command.getPlayTimeMinutes();
        this.armorEffectSeconds = command.getArmorEffectSeconds();
        this.bombExplosionSeconds = command.getBombExplosionSeconds();
        this.gameplayMode = command.getGameplayMode();
    }

    /**
     * Gibt die Breite des Spielfelds zurück.
     *
     * @return Breite des Spielfelds
     */
    public int getArenaWidth() {
        return this.arenaWidth;
    }

    /**
     * Gibt die Höhe des Spielfelds zurück.
     *
     * @return Höhe des Spielfelds
     */
    public int getArenaHeight() {
        return this.arenaHeight;
    }

    /**
     * Gibt die Dauer der Runde in Minuten zurück.
     *
     * @return Dauer der Runde in Minuten
     */
    public int getPlayTimeMinutes() {
        return this.playTimeMinutes;
    }

    /**
     * Gibt die Dauer des Rüstungseffekts in Sekunden zurück.
     *
     * @return Dauer des Rüstungseffekts in Sekunden
     */
    public int getArmorEffectSeconds() {
        return this.armorEffectSeconds;
    }

    /**
     * Gibt den Countdown zur Bombenexplosion in Sekunden zurück.
     *
     * @return Countdown zur Bombenexplosion in Sekunden
     */
    public int getBombExplosionSeconds() {
        return this.bombExplosionSeconds;
    }

    /**
     * Gibt den eingestellten Spielmodus zurück.
     *
     * @return STANDARD_A für "Standard A", STANDARD_A_B für "Standard A+B"
     */
    public GameplayMode getGameplayMode() {
        return this.gameplayMode;
    }
}
