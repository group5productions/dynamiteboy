/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.objects.*;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.tools.GameModeFieldEntry;

/**
 * Diese Klasse speichert die Arena, wie sie durch den Client verwaltet wird.
 * Diese ist grundsätzlich read-only, außer, dass Updates der Arena vom Server
 * integriert werden dürfen. Diesse Klasse ist als Hilfe für die künstliche
 * Intelligenz gedacht, die auf Clientseite läuft.
 *
 * @author Thomas Weber
 */
public final class GameArenaClient {
    private final int _arenaWidth;
    private final int _arenaHeight;

    private final GameObjectIdentifier _arena[];
    private final GamePosition _arenaPlayerPositions[];
    private final LinkedList<GamePosition> _arenaBombPositions;

    /**
     * Diese Methode übersetzt zweidimensionale Koordinaten in einen
     * eindimensionalen Index.
     *
     * @param x
     *            x-Koordinate in der Arena
     * @param y
     *            y-Koordinate in der Arena
     * @return eindimensionaler Index, der den Koordinaten entspricht
     */
    private int _posToIndex(final int x, final int y) {
        return _arenaWidth * y + x;
    }

    /**
     * Wendet ein einzelnes Delta-Update, welches über das Netzwerk übertragen
     * wurde oder von der Spiele-Engine direkt stammt, auf die lokal
     * gespeicherte Arena an. Halte dabei die Positionen von Spielern und Bomben
     * im Blick zum schnellen Zugriff.
     *
     * @param entry
     *            Eintrag eines Delta-Updates, welches übernommen werden soll
     */
    private void _applyDeltaUpdate(GameModeFieldEntry entry) {
        /* Extrahiere Informationen aus dem Eintrag */
        final int x = entry.getX();
        final int y = entry.getY();
        GameObjectIdentifier ident = GameObjectIdentifier
            .fromValue(entry.getId());

        /* Für den Fall, dass ein nicht unterstütztes Objekt übertragen wurde */
        if (ident == null)
            ident = GameObjectIdentifier.WALKABLE_TILE;

        /*
         * Übernehme den Objektbezeichner an den angegebenen Koordinaten in die
         * Arena
         */
        _arena[_posToIndex(x, y)] = ident;

        /*
         * Überwache mit jedem neuen Update die Position der Spieler und der
         * Bomben auf dem Spielfeld.
         *
         * Bedingt durch die Natur der Updates und ihrem Format kann während
         * eines Updatevorgangs zwischenzeitlich ein Spieler verloren gehen,
         * später jedoch wieder auftauchen. Mehr erlaubt das Format momentan
         * nicht.
         */
        switch (ident) {
        /* Aktualisiere die Positionen von Spielern */
        case PLAYER_1:
        case PLAYER_1_ARMORED:
            _arenaPlayerPositions[0].x = x;
            _arenaPlayerPositions[0].y = y;
            break;
        case PLAYER_2:
        case PLAYER_2_ARMORED:
            _arenaPlayerPositions[1].x = x;
            _arenaPlayerPositions[1].y = y;
            break;
        case PLAYER_3:
        case PLAYER_3_ARMORED:
            _arenaPlayerPositions[2].x = x;
            _arenaPlayerPositions[2].y = y;
            break;
        case PLAYER_4:
        case PLAYER_4_ARMORED:
            _arenaPlayerPositions[3].x = x;
            _arenaPlayerPositions[3].y = y;
            break;

        /*
         * Füge neue Bomben (im Zustand 1) hinzu und behalte existierende
         * Bomben, wenn diese von Zustand 1 zu 2 bzw. von 2 zu 3 wechseln.
         */
        case BOMB_STATE_1_PLAYER_1:
        case BOMB_STATE_1_PLAYER_2:
        case BOMB_STATE_1_PLAYER_3:
        case BOMB_STATE_1_PLAYER_4:
            _arenaBombPositions.add(new GamePosition(x, y));
            break;
        case BOMB_STATE_2_PLAYER_1:
        case BOMB_STATE_2_PLAYER_2:
        case BOMB_STATE_2_PLAYER_3:
        case BOMB_STATE_2_PLAYER_4:
        case BOMB_STATE_3_PLAYER_1:
        case BOMB_STATE_3_PLAYER_2:
        case BOMB_STATE_3_PLAYER_3:
        case BOMB_STATE_3_PLAYER_4:
            break;

        /* Kein Spieler und keine Bombe */
        default:
            /* Haben wir Spieler verloren? */
            for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i) {
                if (x == _arenaPlayerPositions[i].x
                    && y == _arenaPlayerPositions[i].y) {
                    _arenaPlayerPositions[i].reset();
                }
            }

            /*
             * Haben wir Bomben verloren? Lösche ALLE Bomben einer Koordinate,
             * da diese eventuell mehrmals getrackt wurden.
             */
            for (final Iterator<GamePosition> iterator = _arenaBombPositions
                .iterator(); iterator.hasNext();) {
                GamePosition position = iterator.next();
                if (position.x == x && position.y == y)
                    iterator.remove();
            }
            break;
        }
    }

    /**
     * Erzeugt eine neue Spielarena, die ausschließlich durch Updates der
     * Spiele-Engine erzeugt wird.
     *
     * @param arenaWidth
     *            Breite der Arena
     * @param arenaHeight
     *            Höhe der Arena
     */
    public GameArenaClient(final int arenaWidth, final int arenaHeight) {
        /* Breite zwischen 7 und 21 sowie ungerade */
        if (arenaWidth < 7 || arenaWidth > 21 || (arenaWidth & 1) == 0)
            throw new IllegalArgumentException(
                "Breite muss zwischen 7 und 21 liegen und ungerade sein");

        /* Höhe zwischen 7 und 21 sowie ungerade */
        if (arenaHeight < 7 || arenaHeight > 21 || (arenaHeight & 1) == 0)
            throw new IllegalArgumentException(
                "Höhe muss zwischen 7 und 21 liegen und ungerade sein");

        this._arenaWidth = arenaWidth;
        this._arenaHeight = arenaHeight;

        /*
         * Erzeuge die Arena als eindimensionales Array. Warum? Weil GameArena
         * diese genauso verwaltet. Jedoch benutzen wir bloß die Bezeichner der
         * Objekte und nicht die kompletten Objekte. (Wir wollen ja auch Details
         * vom Client verstecken. Für die KI reicht es, zu wissen, dass
         * bestimmte Objekte da sind.)
         */
        this._arena = new GameObjectIdentifier[arenaWidth * arenaHeight];

        /*
         * In der Arena können sich maximal so Spieler aufhalten, wie es die
         * globalen Einstellungen vorsehen. Initialisiere die Koordinaten aller
         * Spieler mit {-1,-1}, welche in der Arena nicht zulässig ist.
         */
        _arenaPlayerPositions = new GamePosition[Globals.ROUND_MAX_PLAYERS];
        for (int i = 0; i < Globals.ROUND_MAX_PLAYERS; ++i)
            _arenaPlayerPositions[i] = new GamePosition();

        /*
         * Wir werden nicht wissen, wie viele Bomben es werden, also halten wir
         * uns flexibel.
         */
        _arenaBombPositions = new LinkedList<>();
    }

    /**
     * Gibt die Breite der Arena zurück.
     *
     * @return Breite der Arena
     */
    public int getArenaWidth() {
        return _arenaWidth;
    }

    /**
     * Gibt die Höhe der Arena zurück.
     *
     * @return Höhe der Arena
     */
    public int getArenaHeight() {
        return _arenaHeight;
    }

    /**
     * Überprüft, ob sich die angegebenen Koordinaten innerhalb der Arena
     * befinden.
     *
     * @param x
     *            X-Koordinate
     * @param y
     *            Y-Koordinate
     * @return true, wenn sich die Koordinaten innerhalb der Arena befinden,
     *         ansonsten false
     */
    public boolean areCoordinatesInBounds(final int x, final int y) {
        return (x >= 0 && y >= 0 && x < _arenaWidth && y < _arenaHeight);
    }

    /**
     * Gibt den Bezeichner des Objektes zurück, welches sich an den angegebenen
     * Koordinaten befindet.
     *
     * @param x
     *            X-Koordinate des Objektes
     * @param y
     *            Y-Koordinate des Objektes
     * @return Bezeichner des Objektes an den Koordinaten, oder null, wenn die
     *         Koordinaten außerhalb der Arena liegen
     */
    public synchronized GameObjectIdentifier getArenaObject(final int x,
        final int y) {
        /* Innerhalb der Arena? */
        if (!areCoordinatesInBounds(x, y))
            return null;

        /* Lese aus dem Array */
        return _arena[_posToIndex(x, y)];
    }

    /**
     * Überprüft, ob das Objekt, welches sich an den angegebenen Koordinaten
     * befindet, belaufen werden kann.
     *
     * @param x
     *            X-Koordinate des Objektes
     * @param y
     *            Y-Koordinate des Objektes
     * @return true, wenn das Objekt an den Koordinaten belaufen werden kann,
     *         oder null, wenn die Koordinaten außerhalb der Arena liegen bzw.
     *         das Objekt nicht belaufen werden kann
     */
    public synchronized boolean isArenaObjectWalkable(final int x,
        final int y) {
        /* Hole das Arena-Objekt an den angegebenen Koordinaten */
        GameObjectIdentifier ident = getArenaObject(x, y);
        if (ident == null)
            return false;

        /*
         * Hinweis: Da auf Clientseite nicht alle Informationen zur Verfügung
         * stehen, kann es sein, dass ein Objekt doch belaufbar ist, welches es
         * nach seinen Standardeigenschaften aber nicht ist. Das A+B-Powerup
         * "Bombenläufer" ermöglicht es beispielsweise, durch Bomben
         * durchzulaufen. Solche Fälle müssten durch Code als Sonderfall
         * behandelt werden.
         */
        return GameObjectIdentifier.isIdentifierWalkable(ident);
    }

    /**
     * Gibt die Position eines Spielers anhand der Nummer zurück.
     *
     * @param player
     *            Nummer des Spielers
     * @return Koordinaten des Spielers, oder {-1, -1}, wenn der Spieler nicht
     *         auf dem Feld sichtbar ist
     */
    public synchronized GamePosition getPlayerPosition(final int player) {
        /* Überprüfe Spielernummer */
        if (player < 1 || player > Globals.ROUND_MAX_PLAYERS)
            throw new IllegalArgumentException("Unzulässiger Spieler");

        return _arenaPlayerPositions[player - 1];
    }

    /**
     * Gibt eine Liste von Positionen von Bomben, die momentan auf dem Feld
     * liegen, zurück.
     *
     * @return Liste von Positionen von Bomben, die momentan auf dem Feld liegen
     */
    public synchronized List<GamePosition> getBombPositions() {
        return _arenaBombPositions;
    }

    /**
     * Wende eine Menge von Delta-Updates, die in eine Menge gebündelt sind,
     * hintereinander auf die lokale Kopie der Arena an. Halte dabei die
     * Positionen von Spielern und Bomben im Blick zum schnellen Zugriff.
     *
     * @param updateCommand
     *            Kommando, welches das komplette Update mit allen Einträgen
     *            enthält. Dieses stammt aus dem Netzwerk oder aus der
     *            Spiele-Engine direkt.
     */
    public synchronized void applyDeltaUpdates(
        final ArenaUpdatedClientCommand updateCommand) {
        /* Iteriere die Updates */
        for (final GameModeFieldEntry entry : updateCommand.getUpdatedFields())
            _applyDeltaUpdate(entry);
    }
}
