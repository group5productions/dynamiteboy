/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Logger;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.objects.GameActor;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.networking.messages.RoundEndHighscoreClientCommand;
import org.g5p.dynamiteboy.tools.GameModeFieldEntry;
import org.g5p.dynamiteboy.tools.GameModePlayerEntry;
import org.g5p.dynamiteboy.tools.HighscorePlayerEntry;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * Diese Klasse implementiert einen Thread, der den Kern des Spieles operiert.
 * Vor dem Start wird er mit Konfigurationen befüllt und kann während des
 * Betriebs manipuliert werden. Nach Ablauf der Zeit oder bei 1 verbleibendem
 * Spieler terminiert sich der Thread selbst und berechnet die Punktestatistik,
 * die danach aufgerufen werden kann. Jede Spielerunde benötigt eine neue
 * Instanz des Threads.
 *
 * @author Christian Ulrich
 * @author Thomas Weber
 */
public final class GameEngine extends Thread {
    private static final Logger _logger = Logger
        .getLogger(GameEngine.class.getSimpleName());

    private final LinkedHashMap<String, GameActor> _gameActorsByName;
    private final GameActor _gameActorsByNumber[];
    private GameArena _gameArena;

    private final Runnable _gameStartedCallback;
    private final Consumer<Exception> _gameCrashedCallback;
    private final Runnable _gameStoppedCallback;
    private final Consumer<ArenaUpdatedClientCommand> _arenaUpdatedCallback;
    private final GameConfiguration _gameConfig;

    private boolean _gameStarted;
    private boolean _gameStopped;
    private Timer _gameDurationTimer;
    private boolean _sendPlayerUpdate = false;

    /**
     * Dieser Timer-Task implementiert die Dauer der Spielerunde. Er wird zwei
     * Mal gefeuert: zum Startschuss und zum Ende des Spiels. Dabei wird zum
     * Ende des Spiels der Thread der Engine unterbrochen, wodurch die
     * Spielerunde beendet wird.
     */
    class GameEngineDurationTask extends TimerTask {
        private GameEngine _engineThread;
        private boolean _gameStarted;

        /**
         * Initialisiert den Task mit dem Thread der Engine.
         *
         * @param engineThread
         *            Thread der Spiele-Engine
         */
        public GameEngineDurationTask(GameEngine engineThread) {
            this._engineThread = engineThread;
            this._gameStarted = false;
        }

        /**
         * Diese Methode wird jedes Mal aufgerufen, wenn der Countdown innerhalb
         * des Timers abgelaufen ist.
         */
        @Override
        public void run() {
            /* Spiel noch nicht gestartet? Sende ein Signal an den Thread */
            if (!_gameStarted) {
                _gameStarted = true;
                synchronized (_engineThread) {
                    _engineThread.notify();
                }
            } else {
                /* Unterbreche die Engine */
                _engineThread.interrupt();
            }
        }
    }

    /**
     * Diese Methode überprüft, ob von einer Menge beliebiger boolescher Werte
     * genau ein Wert true ist.
     *
     * @param args
     *            Beliebig viele boolesche Werte
     * @return Index des Wertes, der true gewesen war, ansonsten -1
     */
    private static int _isOnlyOneBooleanSet(boolean... args) {
        int lastTrueIndex = -1;
        for (int i = 0; i < args.length; ++i) {
            if (args[i]) {
                /* Zwei Mal true entdeckt */
                if (lastTrueIndex > -1)
                    return -1;

                /* Erstes Mal true entdeckt */
                else
                    lastTrueIndex = i;
            }
        }
        return lastTrueIndex;
    }

    /**
     * Hilfsmethode, die das Iterieren der Spielerliste (nach Name) vereinfacht.
     *
     * @param iterationCallback
     *            Callback, der für jeden Eintrag der Spielerliste aufgerufen
     *            wird und Spielername sowie das dazugehörige Objekt umfasst
     */
    private void _iterateOnPlayers(
        BiConsumer<String, GameActor> iterationCallback) {
        /* Lege einen Iterator über die Einträge an */
        Iterator<Map.Entry<String, GameActor>> playerIterator = _gameActorsByName
            .entrySet().iterator();

        /* Itertiere durch */
        while (playerIterator.hasNext()) {
            Map.Entry<String, GameActor> playerEntry = playerIterator.next();

            /*
             * Sofern Callback und Spieler anliegt, gebe Spielername (key) und
             * Objekt (value) weiter
             */
            GameActor player = playerEntry.getValue();
            if (player != null && iterationCallback != null)
                iterationCallback.accept(playerEntry.getKey(), player);
        }
    }

    /**
     * Sendet ein Arenaupdate an ein Callback. Es kann ausgewählt werden, ob
     * Spieler und/oder die Arena aktualisiert werden.
     *
     * @param includePlayers
     *            true, wenn die Spielerliste übertragen werden soll, ansonsten
     *            false
     * @param includeArena
     *            true, wenn Änderungen des Spielfeldes übertragen werden
     *            sollen, ansonsten false
     */
    private void _sendArenaUpdate(boolean includePlayers,
        boolean includeArena) {
        /* Ohne Callback bringt das Update nichts */
        if (_arenaUpdatedCallback == null)
            return;

        /* Ohne Daten bringt das Update nichts */
        if (!includePlayers && !includeArena)
            return;
        ArenaUpdatedClientCommand update = new ArenaUpdatedClientCommand();

        /*
         * Stelle Spieler zusammen durch Iteration der Spielerliste
         */
        if (includePlayers) {
            _iterateOnPlayers(new BiConsumer<String, GameActor>() {
                @Override
                public void accept(String playerName, GameActor player) {
                    /* Nur lebende Spieler */
                    if (!player.isAlive())
                        return;

                    update.addPlayerEntry(
                        new GameModePlayerEntry(player.getPlayerNumber(),
                            playerName, player.getPositionX(),
                            player.getPositionY(), player.isArmored()));
                }
            });
        }

        /*
         * Stelle Felder zusammen durch Abfragen des Arenaupdates
         */
        if (includeArena) {
            List<GameModeFieldEntry> updatedFields = _gameArena
                .queryDeltaUpdate();

            for (GameModeFieldEntry field : updatedFields)
                update.addFieldEntry(field);
        }

        /* Versende es */
        _arenaUpdatedCallback.accept(update);
    }

    /**
     * Bereitet die Hauptschleife der Spiele-Engine für seine Ausführung vor.
     */
    private void _runPrepare() {
        /* Markiere als gestartet */
        _gameStarted = true;

        /* Sende komplette Arena als Update an die Spieler */
        _sendArenaUpdate(true, true);

        /*
         * Lege einen Timer an, der nach Ablauf der Spielzeit den Server in die
         * Lobby zurück schaltet
         */
        long gameDurationMs = 1000 * 60 * _gameConfig.getPlayTimeMinutes();
        _gameDurationTimer = new Timer();
        _gameDurationTimer.schedule(new GameEngineDurationTask(this),
            Globals.GAME_ROUND_COUNTDOWN_MS, gameDurationMs);

        /* Warte, bis der Countdown freigegeben wurde */
        _logger.info("Warte auf Countdown...");
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                /* Markiere uns als unterbrochen */
                this.interrupt();
            }
        }

        /* Rufe ein Callback auf, welches über den Spielstart informiert */
        if (_gameStartedCallback != null)
            _gameStartedCallback.run();
        _logger.info("Spiel gestartet");
    }

    /**
     * Operiert einen Durchlauf der Hauptschleife der Spiele-Engine, indem
     * Aktionen von Spielern und Bomben verarbeitet werden. (Verzögerung wird
     * hier nicht behandelt.)
     *
     * @return true, wenn die Schleife fortgeführt werden soll, ansonsten false
     */
    private boolean _runSingleLoop() {
        /*
         * Führe Aktionen aus, die bei jedem interaktiven Objekt (Spieler und
         * Bomben) für diesen Tick momentan anstehen
         */
        final boolean animationPending = _gameArena.performBombTasks();
        for (final GameActor player : _gameActorsByNumber) {
            if (player != null)
                player.performTasks(_gameArena);
        }

        /*
         * Überprüfe, ob nur noch 1 Spieler am Leben ist. Dann ist das Spiel
         * automatisch zu Ende. Nur, wenn alle Bomben bereits explodiert sind.
         * Auch bei 0 Spielern ist es vorbei.
         */
        if (!animationPending) {
            boolean anyPlayerAlive = false;
            final boolean[] aliveStatus = new boolean[Globals.ROUND_MAX_PLAYERS];
            int i = 0;
            for (GameActor player : _gameActorsByNumber) {
                if (player != null) {
                    aliveStatus[i] = player.isAlive();
                    anyPlayerAlive |= aliveStatus[i];
                }
                ++i;
            }

            /* Beende das Spiel, wenn keiner mehr am Leben ist */
            if (!anyPlayerAlive)
                return false;

            int lastPlayerStanding = _isOnlyOneBooleanSet(aliveStatus);
            if (lastPlayerStanding > -1) {
                /*
                 * Markiere den Spieler als den letzten Spieler für die
                 * Statistik und beende das Spiel
                 */
                _gameActorsByNumber[lastPlayerStanding].getStatistics()
                    .markAsLastPlayer();
                return false;
            }
        }

        /*
         * Sende das letzte Update an die Spieler, aber nur, wenn sich was
         * geändert hat
         */
        _sendArenaUpdate(_sendPlayerUpdate,
            _gameArena.isDeltaUpdateAvailable());
        _sendPlayerUpdate = false;

        /* Setze die Ausführung weiter fort */
        return true;
    }

    /**
     * Räumt Ressourcen auf, die nach Beendigung der Spiele-Engine nicht mehr
     * notwendig sind.
     */
    private void _runCleanup() {
        _logger.info("Spiel beendet");

        /* Stoppe den Timer */
        _gameDurationTimer.cancel();

        /* Rufe ein Callback auf, welches über das Spielende informiert */
        _gameStarted = false;
        _gameStopped = true;
        if (_gameStoppedCallback != null)
            _gameStoppedCallback.run();
    }

    /**
     * Initialisiere eine Instanz der Spiele-Engine.
     *
     * @param config
     *            Konfiguration der Spielarena
     * @param gameStartedCallback
     *            Callback, welches aufgerufen wird, wenn das Spiel gestartet
     *            wurde
     * @param gameCrashedCallback
     *            Callback, welches aufgerufen wird, wenn das Spiel abgestürzt
     *            ist
     * @param gameStoppedCallback
     *            Callback, welches aufgerufen wird, wenn das Spiel beendet
     *            wurde
     * @param arenaUpdatedCallback
     *            Callback, welches aufgerufen wird, wenn die Spielarena
     *            und/oder die Spieler geändert wurden
     * @param playerList
     *            Ein Array von Spielernamen. Dieses Array muss genau
     *            {@link Globals#ROUND_MAX_PLAYERS} Elemente besitzen. Spieler,
     *            die nicht auf dem Spielfeld platziert werden sollen, müssen im
     *            Array als null angegeben werden.
     */
    public GameEngine(GameConfiguration config, Runnable gameStartedCallback,
        Consumer<Exception> gameCrashedCallback, Runnable gameStoppedCallback,
        Consumer<ArenaUpdatedClientCommand> arenaUpdatedCallback,
        String playerList[]) {

        /*
         * Liste muss exakt so viele Spieler haben wie maximal zulässig. Es
         * können Strings null bleiben, wenn weniger Spieler auf dem Feld sein
         * sollen.
         */
        if (playerList.length != Globals.ROUND_MAX_PLAYERS) {
            throw new IllegalArgumentException(
                String.format("Spielerliste muss genau %d Spieler umfassen",
                    Globals.ROUND_MAX_PLAYERS));
        }

        this._gameArena = null;

        this._gameStartedCallback = gameStartedCallback;
        this._gameCrashedCallback = gameCrashedCallback;
        this._gameStoppedCallback = gameStoppedCallback;
        this._arenaUpdatedCallback = arenaUpdatedCallback;
        this._gameConfig = config;

        this._gameStarted = false;
        this._gameStopped = false;

        /*
         * Erzeuge Akteure für jeden Spieler. Ermögliche eine Adressierung auf
         * Basis des Spielernamens als auch auf Basis der Nummer, der ihm
         * zugewiesen wurde.
         */
        _gameActorsByNumber = new GameActor[Globals.ROUND_MAX_PLAYERS];
        _gameActorsByName = new LinkedHashMap<>();
        for (int i = 0; i < playerList.length; ++i) {
            /*
             * Halte den Eintrag frei, müssen aber konfliktfreien Namen wählen.
             * Am besten einer mit Zeichen, die sonst ungültig sind.
             */
            if (playerList[i] == null) {
                _gameActorsByName.put(String.format("-%d", i + 1), null);
                _gameActorsByNumber[i] = null;
            }

            /*
             * Erzeuge einen neuen Spieler und referenziere das Objekt in beiden
             * Kollektionen
             */
            else {
                GameActor player = new GameActor(this, i + 1);
                _gameActorsByName.put(playerList[i], player);
                _gameActorsByNumber[i] = player;
            }
        }

        /* Lege die Arena bereits an */
        _gameArena = new GameArena(_gameConfig.getArenaWidth(),
            _gameConfig.getArenaHeight(), this);
    }

    /**
     * Diese Methode erzwingt ein Update der Spielerliste beim nächsten Update.
     * (Nur für interne Nutzung der Engine bestimmt.)
     */
    public void internalForcePlayerListUpdate() {
        _sendPlayerUpdate = true;
    }

    /**
     * Gibt die Konfiguration des aktuellen Spiels zurück.
     *
     * @return Konfiguration des aktuellen Spiels
     */
    public GameConfiguration getGameConfiguration() {
        return _gameConfig;
    }

    /**
     * Gibt die Arena des aktuellen Spiels zurück.
     *
     * @return Arena des aktuellen Spiels
     */
    public GameArena getGameArena() {
        return _gameArena;
    }

    /**
     * Überprüft, ob das Spiel bereits gestartet wurde.
     *
     * @return true, wenn das Spiel bereits gestartet wurde, ansonsten false
     */
    public boolean hasGameStarted() {
        return _gameStarted;
    }

    /**
     * Überschreibt das Spielfeld mit einem, welches dem Standardraster für das
     * Spiel entspricht.
     */
    public void resetArenaLayout() {
        /*
         * Platziere die Spieler und alle Objekte in der Arena und speichere den
         * aktuellen Zustand, um das erste große Delta-Update erzeugen zu
         * können.
         */
        _gameArena.generateField(getPlayerActors());
    }

    /**
     * Diese Methode betreibt den Thread der Spiele-Engine. Dabei werden
     * Ausnahmen in der Engine abgefangen und (sofern definiert) an einen
     * Callback übergeben, der diese verarbeitet. Ansonsten wird die Ausnahme
     * geloggt. Der Betrieb der Engine selbst wird durch eine Hauptschleife
     * unterstützt, die die Ausführung jeder Iteration auf eine Maximalzahl von
     * Ticks pro Sekunde beschränkt.
     */
    @Override
    public void run() {
        /*
         * Führe das gesamte Spiel in einem try-Block aus.
         */
        try {
            /* Bereite Ausführung vor */
            _runPrepare();

            /* Bestimme Höchstdauer eines Ticks */
            final long tickDurationMs = Math
                .round(1000.0 / Globals.GAME_TICKS_PER_SECOND);

            /*
             * Hauptschleife des Spiels läuft so lange, bis der Thread
             * unterbrochen wird. Notiere die aktuelle Startzeit, diese wird
             * erst zum Ende der Schleife fortgeführt.
             */
            long tickStartNs = System.nanoTime();
            while (!this.isInterrupted()) {
                /* Führe Iteration der Schleife durch */
                if (!_runSingleLoop())
                    break;

                /*
                 * Schlafe so viele Millisekunden wie möglich, um die Abstände
                 * zwischen den Ticks konstant zu halten. Wenn wir während des
                 * Schlafens unterbrochen werden, dann wurde die Engine beendet,
                 * also brechen wir aus der Schleife.
                 */
                long tickProcessLengthMs = (System.nanoTime() - tickStartNs)
                    / 1000000;
                if (tickProcessLengthMs < tickDurationMs) {
                    try {
                        Thread.sleep(tickDurationMs - tickProcessLengthMs);
                    } catch (InterruptedException e) {
                        this.interrupt();
                        break;
                    }
                }

                /* Merke neue Startzeit nach dem Warten */
                tickStartNs = System.nanoTime();
            }

            /* Räume auf */
            _runCleanup();
        } catch (Exception e) {
            /*
             * Ups, Ausnahme. Übergebe diese an das Callback. Ansonsten logge
             * es, weil diese eigentlich nicht passieren sollte.
             */
            if (_gameCrashedCallback != null) {
                _gameCrashedCallback.accept(e);
            } else {
                _logger
                    .severe("Engine abgestürzt. Details der Ausnahme folgen:");

                /* Logge die Ausnahme */
                StringWriter writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                _logger.severe(writer.toString());
            }
        }
    }

    /**
     * Gibt ein Array von Objekten zurück, welches die Spieler repräsentiert. Es
     * sind stets {@link Globals#ROUND_MAX_PLAYERS} Elemente in diesem Array;
     * ist für eine Spielernummer kein Spieler zugewiesen, ist das Element in
     * der entsprechenden Indexnummer null.
     *
     * @return Array von Objekten, welches die Spieler repräsentiert
     */
    public GameActor[] getPlayerActors() {
        return _gameActorsByNumber;
    }

    /**
     * Führt eine Aktion für einen Spieler aus. Dabei wird der Spieler durch
     * seinen eindeutigen Namen identifiziert.
     *
     * @param playerName
     *            Name, durch den der Spieler identifiziert wird
     * @param action
     *            Aus dem Netzwerk stammende Aktion. Ist diese null, wird die
     *            Selbstzerstörung der Spielfigur eingeleitet.
     */
    public void performActorAction(String playerName, PlayerAction action) {
        /* Spiel muss laufen */
        if (!_gameStarted)
            return;

        /* Finde das Objekt des Spielers */
        GameActor actor = _gameActorsByName.get(playerName);
        if (actor == null)
            return;

        /*
         * Von einem anderen Thread, aber performAction() ist als synchronized
         * markiert
         */
        actor.performAction(action);
    }

    /**
     * Führt eine Aktion für einen Spieler aus. Dabei wird der Spieler durch
     * seine Nummer in der Arena (von 1 bis {@link Globals#ROUND_MAX_PLAYERS})
     * identifiziert.
     *
     * @param playerNumber
     *            Nummer (von 1 bis {@link Globals#ROUND_MAX_PLAYERS}), durch
     *            den der Spieler identifiziert wird
     * @param action
     *            Aus dem Netzwerk stammende Aktion. Ist diese null, wird die
     *            Selbstzerstörung der Spielfigur eingeleitet.
     */
    public void performActorAction(int playerNumber, PlayerAction action) {
        /* Spiel muss laufen */
        if (!_gameStarted)
            return;

        /* Finde das Objekt des Spielers */
        if (playerNumber < 1 || playerNumber > Globals.ROUND_MAX_PLAYERS)
            return;
        GameActor actor = _gameActorsByNumber[playerNumber - 1];
        if (actor == null)
            return;

        /*
         * Von einem anderen Thread, aber performAction() ist als synchronized
         * markiert
         */
        actor.performAction(action);
    }

    /**
     * Rufe die Statistiken des Spiels in Form des Kommandos zurück, welches
     * über das Netzwerk versendet werden kann. Das Abrufen ist nur möglich,
     * wenn die Spiele-Engine ihre Ausführung beendet hat (z.B. während des
     * Ende-Callbacks).
     *
     * @return Kommando, welches Statistiken über jeden Spieler erfasst und je
     *         dessen Highscore enthält
     */
    public RoundEndHighscoreClientCommand getPlayerScores() {
        /* Nur, wenn das Spiel zu Ende ist */
        if (_gameStarted || !_gameStopped)
            throw new IllegalStateException(
                "Highscores können erst nach Spielende abgerufen werden");

        /* Lege ein neues Kommando an */
        RoundEndHighscoreClientCommand command = new RoundEndHighscoreClientCommand();

        /*
         * Iteriere die Statistiken aller Spieler, die präsent sind. Berechne
         * ihren Highscore, sodass diese nicht manipuliert werden können. Dann
         * konvertiere die Statistiken ins Netzwerkformat.
         */
        _iterateOnPlayers(new BiConsumer<String, GameActor>() {
            @Override
            public void accept(String playerName, GameActor player) {
                if (playerName == null || player == null)
                    return;
                GameActorStatistics stat = player.getStatistics();

                /* Berechne nicht nur, sondern trage auch einen Namen ein */
                stat.enterPlayerName(playerName);
                stat.calculateScore();

                /* Lege Eintrag im Netzwerkformat an */
                HighscorePlayerEntry entry = new HighscorePlayerEntry(
                    stat.getPlayerName(), stat.getWalkedSteps(),
                    stat.getPlantedBombs(), stat.getDestroyedWalls(),
                    stat.getKilledPlayers(), stat.getCollectedPowerups(),
                    stat.isSuicided(), stat.isLastPlayer(),
                    stat.calculateScore());

                /* Füge ihn hinzu */
                command.addPlayerEntry(entry);
            }
        });
        return command;
    }
}
