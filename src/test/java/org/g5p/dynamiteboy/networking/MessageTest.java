/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.networking;

import org.g5p.dynamiteboy.networking.messages.ConfigureArenaGameServerCommand;
import org.g5p.dynamiteboy.networking.messages.HeartbeatCommand;
import org.g5p.dynamiteboy.tools.GameplayMode;
import org.junit.Test;

import com.google.gson.annotations.Expose;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;

/**
 * Diese Klasse testet das Decodieren und Codieren von Nachrichten zwischen
 * Java-Objektinstanzen und JSON-Strings (mit kleinen Ergänzungen wegen des
 * Protokolls).
 *
 * @author Thomas Weber
 */
public class MessageTest {
    /**
     * Testklasse für Nachrichten ohne weiteren Payload.
     */
    private final class _TestEncodeMessage1 extends Message {
        /** Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
        public static final String JSON_COMMAND_NAME = "testEncodeMessage1";

        /**
         * Erzeugt eine neue Nachricht durch Kopie einer Nachricht der gleichen
         * Klasse.
         *
         * @param message
         *            Nachricht, die kopiert werden soll
         */
        public _TestEncodeMessage1(_TestEncodeMessage1 message) {
            super(message);
        }

        /**
         * Erzeugt eine neue Nachricht.
         */
        public _TestEncodeMessage1() {
            super(JSON_COMMAND_NAME);
        }
    }

    /**
     * Testklasse für Nachrichten mit einfachem Integer-Payload.
     */
    private final class _TestEncodeMessage2 extends Message {
        /** Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
        public static final String JSON_COMMAND_NAME = "testEncodeMessage2";

        @Expose
        private int myInteger;

        /**
         * Erzeugt eine neue Nachricht durch Kopie einer Nachricht der gleichen
         * Klasse.
         *
         * @param message
         *            Nachricht, die kopiert werden soll
         */
        public _TestEncodeMessage2(_TestEncodeMessage2 message) {
            super(message);
        }

        /**
         * Erzeugt eine neue Nachricht.
         *
         * @param myInteger
         *            Wert, der übermittelt werden soll
         */
        public _TestEncodeMessage2(final int myInteger) {
            super(JSON_COMMAND_NAME);
            this.myInteger = myInteger;
        }
    }

    /**
     * Testklasse für Nachrichten mit einfachem Boolean-Payload.
     */
    private final class _TestEncodeMessage3 extends Message {
        /** Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
        public static final String JSON_COMMAND_NAME = "testEncodeMessage3";

        @Expose
        private boolean myBoolean;

        /**
         * Erzeugt eine neue Nachricht durch Kopie einer Nachricht der gleichen
         * Klasse.
         *
         * @param message
         *            Nachricht, die kopiert werden soll
         */
        public _TestEncodeMessage3(_TestEncodeMessage3 message) {
            super(message);
        }

        /**
         * Erzeugt eine neue Nachricht.
         *
         * @param myBoolean
         *            Wert, der übermittelt werden soll
         */
        public _TestEncodeMessage3(final boolean myBoolean) {
            super(JSON_COMMAND_NAME);
            this.myBoolean = myBoolean;
        }
    }

    /**
     * Testklasse für Nachrichten mit einfachem String-Payload.
     */
    private final class _TestEncodeMessage4 extends Message {
        /** Bezeichner der Nachricht, wie sie in JSON übermittelt wird */
        public static final String JSON_COMMAND_NAME = "testEncodeMessage4";

        @Expose
        private String myString;

        /**
         * Erzeugt eine neue Nachricht durch Kopie einer Nachricht der gleichen
         * Klasse.
         *
         * @param message
         *            Nachricht, die kopiert werden soll
         */
        public _TestEncodeMessage4(_TestEncodeMessage4 message) {
            super(message);
        }

        /**
         * Erzeugt eine neue Nachricht.
         *
         * @param myString
         *            Wert, der übermittelt werden soll
         */
        public _TestEncodeMessage4(final String myString) {
            super(JSON_COMMAND_NAME);
            this.myString = myString;
        }
    }

    /**
     * Testet das Codieren eines leeren Nachrichtenobjektes nach JSON.
     */
    @Test
    public void testEncodeNoPayload() {
        /* Einfache Nachricht ohne zusätzlichen Inhalt */
        final byte[] message1Bytes = new _TestEncodeMessage1().packMessage();
        final String message1String = new String(message1Bytes,
            StandardCharsets.UTF_8);
        final String message1Expected = "33{\"command\":\"testEncodeMessage1\"}\n";
        assertTrue(message1String.equals(message1Expected));
    }

    /**
     * Testet das Codieren von Nachrichten mit einfachem Integer-Wert nach JSON.
     */
    @Test
    public void testEncodeInteger() {
        /* Integer-Wert 7 */
        final byte[] message1Bytes = new _TestEncodeMessage2(7).packMessage();
        final String message1String = new String(message1Bytes,
            StandardCharsets.UTF_8);
        final String message1Expected = "59{\"command\":\"testEncodeMessage2\",\"content\":{\"myInteger\":7}}\n";
        assertTrue(message1String.equals(message1Expected));

        /* Integer-Wert INT_MAX */
        final byte[] message2Bytes = new _TestEncodeMessage2(2147483647)
            .packMessage();
        final String message2String = new String(message2Bytes,
            StandardCharsets.UTF_8);
        final String message2Expected = "68{\"command\":\"testEncodeMessage2\",\"content\":{\"myInteger\":2147483647}}\n";
        assertTrue(message2String.equals(message2Expected));

        /* Integer-Wert INT_MIN */
        final byte[] message3Bytes = new _TestEncodeMessage2(-2147483648)
            .packMessage();
        final String message3String = new String(message3Bytes,
            StandardCharsets.UTF_8);
        final String message3Expected = "69{\"command\":\"testEncodeMessage2\",\"content\":{\"myInteger\":-2147483648}}\n";
        assertTrue(message3String.equals(message3Expected));
    }

    /**
     * Testet das Codieren von Nachrichten mit einfachem booleschen Wert nach
     * JSON.
     */
    @Test
    public void testEncodeBoolean() {
        /* true */
        final byte[] message1Bytes = new _TestEncodeMessage3(true)
            .packMessage();
        final String message1String = new String(message1Bytes,
            StandardCharsets.UTF_8);
        final String message1Expected = "62{\"command\":\"testEncodeMessage3\",\"content\":{\"myBoolean\":true}}\n";
        assertTrue(message1String.equals(message1Expected));

        /* false */
        final byte[] message2Bytes = new _TestEncodeMessage3(false)
            .packMessage();
        final String message2String = new String(message2Bytes,
            StandardCharsets.UTF_8);
        final String message2Expected = "63{\"command\":\"testEncodeMessage3\",\"content\":{\"myBoolean\":false}}\n";
        assertTrue(message2String.equals(message2Expected));
    }

    /**
     * Testet das Codieren von Nachrichten mit einfachem String nach JSON.
     */
    @Test
    public void testEncodeString() {
        // FIXME: konvertiere non-ASCII nach bytes
        /* Nur ASCII */
        final byte[] message1Bytes = new _TestEncodeMessage4(
            "Group5Productions").packMessage();
        final String message1String = new String(message1Bytes,
            StandardCharsets.UTF_8);
        final String message1Expected = "76{\"command\":\"testEncodeMessage4\",\"content\":{\"myString\":\"Group5Productions\"}}\n";
        assertTrue(message1String.equals(message1Expected));

        /* Umlaute werden als 2 Byte gezählt */
        final byte[] message2Bytes = new _TestEncodeMessage4(
            "Käse Brötchen Überleben Weißbrot").packMessage();
        final String message2String = new String(message2Bytes,
            StandardCharsets.UTF_8);
        final String message2Expected = "95{\"command\":\"testEncodeMessage4\",\"content\":{\"myString\":\"Käse Brötchen Überleben Weißbrot\"}}\n";
        assertTrue(message2String.equals(message2Expected));

        /* CJK werden als 3 Byte gezählt */
        final byte[] message3Bytes = new _TestEncodeMessage4("ドイツ語")
            .packMessage();
        final String message3String = new String(message3Bytes,
            StandardCharsets.UTF_8);
        final String message3Expected = "71{\"command\":\"testEncodeMessage4\",\"content\":{\"myString\":\"ドイツ語\"}}\n";
        assertTrue(message3String.equals(message3Expected));
    }

    /**
     * Testet das Decodieren erwarteter und unerwarteter Nachrichten.
     */
    @Test
    public void testDecodeMessages() {
        /* Heartbeat -- ohne Daten */
        final String message1String = "{\"command\":\"xxHeartbeat\"}\n";
        try {
            Message message1 = Message.unpackMessage(message1String);
            if (!(message1 instanceof HeartbeatCommand))
                fail("Nachricht 1 wurde nicht nach HeartbeatCommand decodiert");
        } catch (MessageMalformedException e) {
            fail("Nachricht 1 hat Fehler produziert");
        }

        /* Arenakonfiguration -- viele Integer-Daten */
        final String message2String = "{\"command\":\"gsConfigureArena\",\"content\":{"
            + "\"arenaWidth\":11,\"arenaHeight\":9,\"playTimeMinutes\":7,"
            + "\"armorEffectSeconds\":6,\"bombExplosionSeconds\":4,\"maxPlayers\":3,"
            + "\"gameplayMode\":2}}\n";
        try {
            Message message2 = Message.unpackMessage(message2String);
            if (!(message2 instanceof ConfigureArenaGameServerCommand))
                fail(
                    "Nachricht 2 wurde nicht nach ConfigureArenaGameServerCommand decodiert");

            ConfigureArenaGameServerCommand message2Command = (ConfigureArenaGameServerCommand) message2;
            assertEquals(message2Command.getArenaWidth(), 11);
            assertEquals(message2Command.getArenaHeight(), 9);
            assertEquals(message2Command.getPlayTimeMinutes(), 7);
            assertEquals(message2Command.getArmorEffectSeconds(), 6);
            assertEquals(message2Command.getBombExplosionSeconds(), 4);
            assertEquals(message2Command.getMaxPlayers(), 3);
            assertEquals(message2Command.getGameplayMode(),
                GameplayMode.STANDARD_A_B);
        } catch (MessageMalformedException e) {
            fail("Nachricht 2 hat Fehler produziert");
        }

        /* Nicht im Protokoll vorhergesehene Nachricht */
        final String message3String = "{\"command\":\"Hallo\"}\n";
        try {
            Message.unpackMessage(message3String);
            fail("Nachricht 3 hat keinen Fehler produziert");
        } catch (Exception e) {
            /* Erfolgsfall */
        }

        /* Irgendein JSON */
        final String message4String = "{\"xx\":\"yy\"}\n";
        try {
            Message.unpackMessage(message4String);
            fail("Nachricht 4 hat keinen Fehler produziert");
        } catch (Exception e) {
            /* Erfolgsfall */
        }

        /* Irgendein Müll */
        final String message5String = "A\n";
        try {
            Message.unpackMessage(message5String);
            fail("Nachricht 5 hat keinen Fehler produziert");
        } catch (Exception e) {
            /* Erfolgsfall */
        }
    }
}
