/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.client;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameArena;
import org.g5p.dynamiteboy.engine.GameArenaClient;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.engine.objects.GameActor;
import org.g5p.dynamiteboy.engine.objects.GameObjectIdentifier;
import org.g5p.dynamiteboy.engine.objects.GamePowerupBombs;
import org.g5p.dynamiteboy.engine.objects.GamePowerupRadius;
import org.g5p.dynamiteboy.networking.messages.ArenaUpdatedClientCommand;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * "Dieser Test soll bestimmen ob die KI in der Lage ist sich vor drohenden
 * Detonationen in Sicherheit zu bringen. Dazu wird ein Spielfeld erstellt und
 * eine Bombe bei Spieler Blau gelegt. Dabei wird geprüft ob die Klasse KI() in
 * der Lage ist darauf zu reagieren. Der Test ermittelt dazu den Status von
 * Spieler Rot, nach der Detonation noch am Leben ist."
 *
 * @author Maik Hunger (Originalcode)
 * @author Thomas Weber (eigene Portierung auf aktuelle Spiele-Engine)
 */
public final class PlayerAITest {
    private GameEngine _testEngine;
    private GameArenaClient _testArenaClient;
    private PlayerAI _testAI;
    private boolean _allowStart;

    /**
     * Präpariert die Umgebung vor der Ausführung jedes Tests.
     */
    @Before
    public void setUp() {
        System.out.println("Beginne KI-Test");
        System.out.println("Erstelle Client und KI");

        /* Erzeuge eine Spiele-Engine mit Standardeinstellungen für die Arena */
        final String[] players = new String[Globals.ROUND_MAX_PLAYERS];
        players[1] = "PlayerAITestBlue";
        players[3] = "PlayerAITestRed";

        /* Müssen Delta-Updates an _testArenaClient durchreichen */
        final GameConfiguration config = new GameConfiguration();
        _allowStart = false;
        _testEngine = new GameEngine(config, () -> {
            synchronized (this) {
                _allowStart = true;
                notify();
            }
        }, null, null, (ArenaUpdatedClientCommand command) -> {
            _testArenaClient.applyDeltaUpdates(command);
        }, players);

        /* Setze die Arena auf das Standardspielfeld */
        _testEngine.resetArenaLayout();

        final GameActor player2 = _testEngine.getPlayerActors()[1];
        for (int i = 0; i < 3; ++i)
            player2.applyPowerup(new GamePowerupRadius());
        player2.applyPowerup(new GamePowerupBombs());

        _testArenaClient = new GameArenaClient(config.getArenaWidth(),
            config.getArenaHeight());
        _testAI = new PlayerAI(_testArenaClient, 4);
    }

    /**
     * Bereinigt die Umgebung nach der Ausführung jedes Tests.
     */
    @After
    public void tearDown() {
        _testAI = null;
        _testArenaClient = null;
        _testEngine = null;
        System.out.println("Client und KI gelöscht");
    }

    /**
     * Überprüft, ob die KI in der Lage ist, vor einer von einem anderen Spieler
     * gelegten Bombe auszuweichen.
     */
    @Test
    public void testAIBombEscape() {
        System.out.println("Initialisiere Test-Spielfeld");

        /*
         * im Folgenden wird das Spielfeld vorbereitet. Zu erst werden
         * zerstörbare Mauern entfernt
         */
        final GameArena arena = _testEngine.getGameArena();
        final int arenaHeight = arena.getArenaHeight();
        arena.setArenaObject(2, 1, GameObjectIdentifier.WALKABLE_TILE);
        for (int j = 1; j < arenaHeight - 1; j++)
            arena.setArenaObject(3, j, GameObjectIdentifier.WALKABLE_TILE);
        for (int j = 1; j < 6; j++)
            arena.setArenaObject(j, 5, GameObjectIdentifier.WALKABLE_TILE);

        /*
         * Nun werden die Spieler 1 und 3 auf die Test-Positionen umgesetzt
         * wobei Spieler 3 durch die KI gesteuert wird
         */
        GameActor[] players = _testEngine.getPlayerActors();
        players[1].setPosition(3, 3);
        players[3].setPosition(3, 5);

        System.out.println("Starte Ausweichtest");
        _testEngine.start();

        /* (Warte, bis das Spiel losgeht.) */
        while (!_allowStart) {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        /*
         * (Spieler 2: Platziere eine Bombe. Renne von ihr weg. Spieler 4: Führe
         * alle 2 Ticks einen KI-Schritt aus.)
         */
        final PlayerAction[] player2Actions = new PlayerAction[] {
            PlayerAction.PLANT_BOMB, PlayerAction.MOVE_UP, PlayerAction.MOVE_UP,
            PlayerAction.MOVE_UP, PlayerAction.MOVE_LEFT, null };

        /* ticksToRun wird noch runter gesetzt */
        int player2ActionIndex = 0;
        int ticksToRun = 1 << 30;
        boolean endIsNear = false;
        while (ticksToRun-- >= 0) {
            /* Bewegung geht nur jeden zweiten Schritt */
            if ((ticksToRun & 1) == 0) {
                /* Solange noch Bewegungen möglich sind für Spieler 2 */
                if (player2Actions[player2ActionIndex] != null) {
                    players[1]
                        .performAction(player2Actions[player2ActionIndex]);
                    ++player2ActionIndex;
                } else {
                    /*
                     * Wenn alle Bewegungen ausgeführt sind und die Bombe
                     * hochgegangen ist, warte eine Sekunde.
                     */
                    if (!endIsNear) {
                        if (players[1].getCurrentlyPlacedBombs() == 0) {
                            System.out.println("xxx");
                            ticksToRun = Globals.GAME_TICKS_PER_SECOND;
                            endIsNear = true;
                        }
                    }
                }

                /* Führe einen KI-Schritt durch */
                final PlayerAction action = _testAI.performAIStep();
                if (action != null)
                    players[3].performAction(_testAI.performAIStep());
            }

            /* Schlafe kurz */
            try {
                Thread.sleep(1000 / Globals.GAME_TICKS_PER_SECOND);
            } catch (InterruptedException e) {
            }
        }

        /* Beide Spieler müssen noch am Leben sein */
        assertTrue(players[1].isAlive());
        assertTrue(players[3].isAlive());
        System.out.println("Test erfolgreich");
    }
}
