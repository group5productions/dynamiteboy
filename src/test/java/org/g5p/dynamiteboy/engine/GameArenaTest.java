/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;

/**
 * "Bei diesem Test wird die Korrektheit der Methode GetWidth und GetHeight
 * getestet. Diese Methoden dienen der Ausgabe der Spielfeld-Höhe/ -Breite."
 *
 * Die Tests wurden vom Originalcode von Januar 2017 nur dort angepasst, wo
 * aufgrund von Änderungen in der Logik der ursprüngliche Code nicht mehr
 * funktionsfähig war.
 *
 * @author Zaur Rustamkhanov
 * @author Thomas Weber (eigene Portierung auf aktuelle Spiele-Engine)
 */
public final class GameArenaTest {
    private GameEngine _testEngine;

    /**
     * Präpariert die Umgebung vor der Ausführung jedes Tests.
     */
    @Before
    public void setUp() {
        /* Erzeuge eine Spiele-Engine mit Standardeinstellungen für die Arena */
        final String[] players = new String[Globals.ROUND_MAX_PLAYERS];

        players[0] = "GameArenaTest";
        _testEngine = new GameEngine(new GameConfiguration(), null, null, null,
            null, players);

        /* Setze die Arena auf das Standardspielfeld */
        _testEngine.resetArenaLayout();
    }

    /**
     * Bereinigt die Umgebung nach der Ausführung jedes Tests.
     */
    @After
    public void tearDown() {
        _testEngine = null;
    }

    @Test
    public void testGetWidth() {
        System.out.println("getWidth");
        final int result = _testEngine.getGameArena().getArenaWidth();

        /* (muss Standardwert aus Konstanten entsprechen) */
        assertEquals(result, Globals.GAME_ARENA_WIDTH_DEFAULT);
    }

    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        final int result = _testEngine.getGameArena().getArenaHeight();

        /* (muss Standardwert aus Konstanten entsprechen) */
        assertEquals(result, Globals.GAME_ARENA_HEIGHT_DEFAULT);
    }
}
