/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameArena;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * "Ausgangspunkt: Komponententest der Klasse Spieler. Klasse wird nur für sich
 * betrachtet. Prüfung ob Methoden „richtig“ arbeiten. Max. Anzahl an
 * plazierbaren Bomben. Max. Radius angenommen."
 *
 * Die Tests wurden vom Originalcode von Januar 2017 nur dort angepasst, wo
 * aufgrund von Änderungen in der Logik der ursprüngliche Code nicht mehr
 * funktionsfähig war.
 *
 * @author Michael Richter
 * @author Thomas Weber (Portierung auf aktuelle Spiele-Engine)
 */
public final class GameActorTest {
    private GameEngine _testEngine;
    private GameActor _testSp;

    /**
     * Präpariert die Umgebung vor der Ausführung jedes Tests.
     */
    @Before
    public void setUp() {
        System.out.println("Starte Tests");
        System.out.println("Erstelle Spieler und Spielfeld");

        /* Erzeuge eine Spiele-Engine mit Standardeinstellungen für die Arena */
        final String[] players = new String[Globals.ROUND_MAX_PLAYERS];
        players[0] = "GameActorTest";
        players[1] = "GameActorTestPlaceholder";
        _testEngine = new GameEngine(new GameConfiguration(), null, null, null,
            null, players);

        /*
         * Keine Initialisierung des Spielfelds selbst notwendig, nur für
         * bestimmte Felder, wenn diese benötigt werden.
         */

        /* Extrahiere ersten Spieler aus der Runde */
        _testSp = _testEngine.getPlayerActors()[0];

        /* Verpasse dem Spieler 11 Mal Powerup "Extra Bombe" */
        for (int i = 0; i < 11; ++i)
            _testSp.applyPowerup(new GamePowerupBombs());

        /* Spielfeld muss quadratisch sein für Test 6 */
        assertEquals(_testEngine.getGameArena().getArenaWidth(),
            _testEngine.getGameArena().getArenaHeight());
    }

    /**
     * Bereinigt die Umgebung nach der Ausführung jedes Tests.
     */
    @After
    public void tearDown() {
        System.out.println("Lösche Spieler und Spielfeld");
        _testSp = null;
        _testEngine = null;
    }

    /**
     * Überprüft die Tötung des Spielers und ob dieser noch am Leben ist.
     */
    @Test
    public void testDeath() {
        System.out.println("Test 1 - Tod");

        _testSp.perish(_testSp);
        assertFalse(_testSp.isAlive());
    }

    /**
     * Überprüft die Funktionsfähigkeit der Rüstung.
     */
    @Test
    public void testArmor() {
        /* Abgeschlossen */
        System.out.println("Test 2 - Rüstung setzten");

        _testSp.applyPowerup(new GamePowerupArmor());
        assertTrue(
            _testSp.getRemainingArmorTicks() == Globals.GAME_TICKS_PER_SECOND
                * _testEngine.getGameConfiguration().getArmorEffectSeconds());
    }

    /**
     * Überprüft die Funktionfähigkeit des Powerups "Extra Bombe" auf den
     * Spieler.
     */
    @Test
    public void testMaxPlaceableBombs() {
        /* wenn es ein max gibt wird dieser Wert noch überschritten */
        System.out.println("Test 3 - BombenPlus");

        /*
         * (Neuaufbau der Spiele-Engine hat keine Obergrenze für parallel
         * platzierte Bomben, daher wurde dieser Check entfernt.)
         */
        for (int i = 0; i <= 15; ++i) {
            final int actBomb = _testSp.getMaxPlaceableBombs();
            _testSp.applyPowerup(new GamePowerupBombs());
            assertEquals(_testSp.getMaxPlaceableBombs(), actBomb + 1);
            System.out.println(_testSp.getMaxPlaceableBombs());
        }
    }

    /**
     * Überprüft, ob die Anzahl gleichzeitig gelegter Bomben nicht ihr Maximum
     * überschreitet.
     */
    @Test
    public void testCurrentlyPlacedBombs() {
        /* Es können noch mehr Bomben gelegt werden als möglich */
        System.out.println("Test 4 - BombenPlus");

        /*
         * (Der Originalcode nahm an, dass unendlich viele Bomben an der
         * aktuellen Stelle gelegt werden können. Das geht nicht mehr. Mache die
         * erste Reihe auf dem Spielfeld frei und platziere in der Reihe
         * Bomben.)
         */
        final GameArena arena = _testEngine.getGameArena();
        for (int x = 1; x < arena.getArenaWidth() - 1; ++x)
            arena.setArenaObject(x, 1, GameObjectIdentifier.WALKABLE_TILE);

        /* so viele legen wie maxbomb */
        for (int i = 0; i <= 15; ++i) {
            _testSp.performAction(PlayerAction.PLANT_BOMB);
            _testSp.performTasks(arena);
            _testSp.performAction(PlayerAction.MOVE_RIGHT);
            _testSp.performTasks(arena);

            assertTrue(_testSp.getCurrentlyPlacedBombs() <= _testSp
                .getMaxPlaceableBombs());
        }
    }

    /**
     * Überprüft die Reduktion der Anzahl platzierter Bomben.
     */
    @Test
    public void testDecreaseCurrentlyPlacedBombs() {
        /* Bombcount muss >= 1 kann noch daunter laufen */
        System.out.println("Test 5 - Bomben reduziert");

        for (int i = 0; i < 15; i++) {
            _testSp.decreaseCurrentlyPlacedBombs();
            /* (kann nicht unter 0 fallen, vorher stand eine 1) */
            assertTrue(_testSp.getCurrentlyPlacedBombs() >= 0);
        }
    }

    /**
     * Überprüft die Funktionsfähigkeit des Powerups "Raduiserweiterung".
     */
    @Test
    public void test_RadiusPlus() {
        System.out.println("Test 6 - RadiusPlus");

        /*
         * Paameter benötigt -- so viel RadiusPlus bis maxradius (Anfangsradius
         * ist 2)
         */
        for (int i = 2; i < 15; i++) {
            final int bevRa = _testSp.getBombExplosionRadius();
            _testSp.applyPowerup(new GamePowerupRadius());

            /* (Powerup besitzt Maximalwert aus min(width, height) */
            final int arenaWidth = _testEngine.getGameArena().getArenaWidth();
            if (i >= arenaWidth)
                assertEquals(_testSp.getBombExplosionRadius(), arenaWidth);
            else
                assertEquals(_testSp.getBombExplosionRadius(), bevRa + 1);
        }
    }

    /**
     * Überprüft die Funktionsfähigkeit des Countdowns zum Ablauf der
     * Rüstungseffekts.
     */
    @Test
    public void testArmorTicks() {
        System.out.println("Test 7 - decreaseRüstung");

        _testSp.applyPowerup(new GamePowerupArmor());

        /* (50 -> 70) */
        for (int i = 0; i < 70; i++) {
            final int bevRü = _testSp.getRemainingArmorTicks();
            _testSp.performTasks(_testEngine.getGameArena());

            /* (Fällt irgendwann auf 0, aber nicht drunter) */
            if (bevRü > 0)
                assertEquals(_testSp.getRemainingArmorTicks(), bevRü - 1);
            else
                assertEquals(_testSp.getRemainingArmorTicks(), 0);
        }
    }

    /**
     * Überprüft das Setzen des Spielers auf eine absolute Position.
     */
    @Test
    public void testSetPosition() {
        System.out.println("Test 8 - Position");

        /* (verschoben aus setUp) */
        final int testPosX = 3;
        final int testPosY = 3;

        _testSp.setPosition(testPosX, testPosY);
        /* (API hat sich geändert) */
        final int testSpPosX = _testSp.getPositionX();
        final int testSpPosY = _testSp.getPositionY();

        /*
         * (X = Breite bzw. Y = Höhe liegen außerhalb der zulässigen
         * Koordinaten)
         */
        assertTrue(testSpPosX == testPosX && testSpPosX >= 0
            && testSpPosX < _testEngine.getGameArena().getArenaWidth());
        assertTrue(testSpPosY == testPosY && testSpPosY >= 0
            && testSpPosY < _testEngine.getGameArena().getArenaHeight());
    }
}