/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;

/**
 * "Bei diesem Test wird die korrekte Umsetzung der Bombensetter und getter
 * getestet. Der Test überprüft, die Zustände der Instanz nachdem eine konkrete
 * Funktion ausgeführt wurde. Alle Tests erfüllten die Erwartungen
 * (fehlerfrei)."
 *
 * Die Tests wurden vom Originalcode von Januar 2017 nur dort angepasst, wo
 * aufgrund von Änderungen in der Logik der ursprüngliche Code nicht mehr
 * funktionsfähig war.
 *
 * @author Steven Jung
 * @author Thomas Weber (eigene Portierung auf aktuelle Spiele-Engine)
 */
public final class GameBombTest {
    private GameEngine _testEngine;

    /**
     * Präpariert die Umgebung vor der Ausführung jedes Tests.
     */
    @Before
    public void setUp() {
        /* Erzeuge eine Spiele-Engine mit Standardeinstellungen für die Arena */
        final String[] players = new String[Globals.ROUND_MAX_PLAYERS];

        /*
         * Absichtlich Spieler 2, um zu vermeiden, dass Tests sich auf Spieler 1
         * fixieren.
         */
        players[1] = "GameBombTest";
        _testEngine = new GameEngine(new GameConfiguration(), null, null, null,
            null, players);

        /* Setze die Arena auf das Standardspielfeld */
        _testEngine.resetArenaLayout();
    }

    /**
     * Bereinigt die Umgebung nach der Ausführung jedes Tests.
     */
    @After
    public void tearDown() {
        _testEngine = null;
    }

    /**
     * Testet die Position der Bombe.
     */
    @Test
    public void testGetPosition() {
        GameBomb myBombe = new GameBomb(_testEngine, 2, 1,
            _testEngine.getPlayerActors()[1]);

        /* (API geändert) */
        int[] testPosition = new int[] { myBombe.getPositionX(),
            myBombe.getPositionY() };
        assertEquals(2, testPosition.length);
        assertArrayEquals(new int[] { 2, 1 }, testPosition);
    }

    /**
     * Testet den Countdown-Timer zum Zeitpunkt der Bombenlegung.
     */
    @Test
    public void testGetTimer() {
        GameBomb myBombe = new GameBomb(_testEngine, 2, 1,
            _testEngine.getPlayerActors()[1]);

        final int testtimer = myBombe.getRemainingBombTicks();
        assertEquals(testtimer, Globals.GAME_TICKS_PER_SECOND
            * _testEngine.getGameConfiguration().getBombExplosionSeconds());
    }

    /**
     * Testet den Countdown-Timer zum Zeitpunkt der Explosion.
     */
    @Test
    public void testSetTimerZero() {
        GameBomb myBombe = new GameBomb(_testEngine, 2, 1,
            _testEngine.getPlayerActors()[1]);

        myBombe.explodeImmediately(_testEngine.getGameArena());
        final int testtimer = myBombe.getRemainingBombTicks();
        assertEquals(testtimer, 0);
    }

    /**
     * Überprüft die Zuordnung von Spieler zu Bombe.
     */
    @Test
    public void testKonstruktor() {
        GameBomb myBombe = new GameBomb(_testEngine, 2, 1,
            _testEngine.getPlayerActors()[1]);

        final int testplayer = myBombe.getBombPlanter().getPlayerNumber();
        /* Array wurde in getPosition schon getestet */
        /* Timer wurde in getTimer schon getestet */
        assertEquals(2, testplayer);
    }
}