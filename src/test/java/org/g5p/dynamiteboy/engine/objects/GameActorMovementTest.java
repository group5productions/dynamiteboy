/**
 * Diese Datei ist Teil von Dynamite Boy.
 * (c) 2016-2017 Group5Productions. GPLv3 oder höher: https://www.gnu.org/licenses/
 */

package org.g5p.dynamiteboy.engine.objects;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.g5p.dynamiteboy.Globals;
import org.g5p.dynamiteboy.engine.GameConfiguration;
import org.g5p.dynamiteboy.engine.GameEngine;
import org.g5p.dynamiteboy.tools.PlayerAction;

/**
 * "Bei diesem Test wird die korrekte Umsetzung der Bewegungsbefehle getestet.
 * Der Test testet die PlayerAction()-Methode der Spielfeld-Klasse, die für die
 * Umsetzung der Spielerbefehle zuständig ist. Der Test vergleicht die
 * Spielerposition vor und nach dem Ausführen der Methode."
 *
 * @author Christian Ulrich (Originalcode)
 * @author Thomas Weber (eigene Portierung auf aktuelle Spiele-Engine)
 */
public final class GameActorMovementTest {
    private GameEngine _testEngine;

    /**
     * Testet die Bewegung eines Spielers in eine Richtung, auch unter
     * Betrachtung eines möglichen Hindernisses.
     *
     * @param player
     *            Spieler, der bewegt werden soll
     * @param action
     *            Aktion, die auf den Spieler angewendet werden soll; nur die
     *            MOVE_* Konstanten sind zulässig.
     * @param expectBlocked
     *            true, wenn erwartet wird, dass der Spieler nicht in die
     *            angegebene Richtung bewegt werden kann, ansonsten false
     */
    private void _testMovementDirection(final GameActor player,
        final PlayerAction action, final boolean expectBlocked) {

        /* Speichere Originalposition */
        final int originalX = player.getPositionX();
        final int originalY = player.getPositionY();

        /* Führe die Bewegung mitsamt Tick-Processing durch */
        player.performAction(action);
        player.performTasks(_testEngine.getGameArena());

        /*
         * Wenn eine Änderung erwartet wurde, muss sich die Position ändern,
         * ansonsten muss sie stabil bleiben.
         */
        if (expectBlocked) {
            assertEquals(player.getPositionX(), originalX);
            assertEquals(player.getPositionY(), originalY);
        } else {
            /* Bestimme Delta für die Richtung */
            int xDelta, yDelta;
            switch (action) {
            case MOVE_LEFT:
                xDelta = -1;
                yDelta = 0;
                break;
            case MOVE_RIGHT:
                xDelta = 1;
                yDelta = 0;
                break;
            case MOVE_UP:
                xDelta = 0;
                yDelta = -1;
                break;
            case MOVE_DOWN:
                xDelta = 0;
                yDelta = 1;
                break;
            default:
                fail("Unerlaubter Wert als Richtung angegeben");
                return;
            }
            assertEquals(player.getPositionX(), originalX + xDelta);
            assertEquals(player.getPositionY(), originalY + yDelta);
        }
    }

    /**
     * Präpariert die Umgebung vor der Ausführung jedes Tests.
     */
    @Before
    public void setUp() {
        /* Erzeuge eine Spiele-Engine mit Standardeinstellungen für die Arena */
        final String[] players = new String[Globals.ROUND_MAX_PLAYERS];
        players[0] = "GameActorMovementTestP1";
        players[1] = "GameActorMovementTestP2";
        players[2] = "GameActorMovementTestP3";
        players[3] = "GameActorMovementTestP4";
        _testEngine = new GameEngine(new GameConfiguration(), null, null, null,
            null, players);

        /* Setze die Arena auf das Standardspielfeld */
        _testEngine.resetArenaLayout();
    }

    /**
     * Bereinigt die Umgebung nach der Ausführung jedes Tests.
     */
    @After
    public void tearDown() {
        _testEngine = null;
    }

    /**
     * Überprüft die Funktionsfähigkeit der Bewegung in alle vier Richtungen,
     * auch unter Blockade der Laufrichtung durch ein Hindernis. Spieler 1, oben
     * links.
     */
    @Test
    public void testMovementPlayer1() {
        final GameActor player = _testEngine.getPlayerActors()[0];

        /*
         * Spieler 1 kann vom Ausgangspunkt nach unten und nach rechts laufen.
         */
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_UP, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_UP, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, true); // block
    }

    /**
     * Überprüft die Funktionsfähigkeit der Bewegung in alle vier Richtungen,
     * auch unter Blockade der Laufrichtung durch ein Hindernis. Spieler 2, oben
     * rechts.
     */
    @Test
    public void testMovementPlayer2() {
        final GameActor player = _testEngine.getPlayerActors()[1];

        /*
         * Spieler 1 kann vom Ausgangspunkt nach unten und nach rechts laufen.
         */
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_UP, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_UP, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, true); // block
    }

    /**
     * Überprüft die Funktionsfähigkeit der Bewegung in alle vier Richtungen,
     * auch unter Blockade der Laufrichtung durch ein Hindernis. Spieler 3,
     * unten links.
     */
    @Test
    public void testMovementPlayer3() {
        final GameActor player = _testEngine.getPlayerActors()[2];

        /*
         * Spieler 1 kann vom Ausgangspunkt nach unten und nach rechts laufen.
         */
        _testMovementDirection(player, PlayerAction.MOVE_UP, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_UP, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, true); // block
    }

    /**
     * Überprüft die Funktionsfähigkeit der Bewegung in alle vier Richtungen,
     * auch unter Blockade der Laufrichtung durch ein Hindernis. Spieler 4,
     * unten rechts.
     */
    @Test
    public void testMovementPlayer4() {
        final GameActor player = _testEngine.getPlayerActors()[3];

        /*
         * Spieler 1 kann vom Ausgangspunkt nach unten und nach rechts laufen.
         */
        _testMovementDirection(player, PlayerAction.MOVE_UP, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_UP, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_DOWN, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_LEFT, true); // block
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, false); // ok
        _testMovementDirection(player, PlayerAction.MOVE_RIGHT, true); // block
    }
}